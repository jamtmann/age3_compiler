import flask
from flask import request
import os
import subprocess

app = flask.Flask(__name__)

# HTTP codes
BAD_REQUEST = 400
OK = 200


@app.route('/', methods=['GET'])
def home():
    return '''<h1>Durokan's age3 code analyzer landing page</h1>
<p>A prototype API for error checking of .xs scripts</p>'''


@app.route('/xs', methods=['POST'])
def postman():
    """ the main xs request endpoint """

    f = open("./inputs/_temp.xs", "wb")
    f.write(request.data)
    f.close()
    output = subprocess.check_output("./aoe < ./inputs/_temp.xs", shell=True)
    os.remove("./inputs/_temp.xs")
    response = app.response_class(
        response=output,
        status=OK,
        mimetype='text/plain'
    )
    return response


if __name__ == '__main__':
    # app.config["DEBUG"] = True
    app.run(host='0.0.0.0')
