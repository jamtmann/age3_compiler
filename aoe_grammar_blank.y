%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list_lab.c"

extern int linect;
extern int yylex();
extern char* yytext;
int yyerror(char*);

static node_ptr class_scope = NULL;
static node_ptr my_list = NULL;
int debug;

// %type <strnum> exp

%}

%union{
 char* str;
 int num;
 float decimal;
 struct s1 {char* str; int kind; int j_type;} strnum;
}

%token INCLUDE_T VOID_T RETURN_T
%token INT_T LONG_T FLOAT_T BOOL_T VECTOR_T STRING_T 
%token IF_T ELIF_T ELSE_T TRUE_T FALSE_T FOR_T WHILE_T BREAK_T CONTINUE_T
%token INCR DECR LT EQ LE GE GT NE ASSIGN PLUS MINUS MULT DIVIDE BANG
%token COMMA LP RP LB RB LC RC SEMI QUOTE
%token PLUS_EQ MINUS_EQ DIV_EQ MULT_EQ
%token <strnum> STRING_LIT
%token <strnum> ID
%token <num> INT_LIT
%token <decimal> FLOAT_LIT

%nonassoc ASSIGN
%left LT LE GT GE EQ NE
%right BANG
%left PLUS MINUS OR AND
%left MULT DIVIDE MOD
%left INCR DECR

%%
program : decl_list
 ;
decl_list:
 | decl_list statement
 | decl_list function
 | decl_list INCLUDE_T STRING_LIT SEMI
 ;
var_type : VOID_T
 | VECTOR_TR
 | INT_T
 | FLOAT_T
 | BOOL_T
 | STRING_T
 | LONG_T
 ;
func_param : parameters
 | VOID_T
;
function : var_type ID LP func_param RP LC statements RC 
 ;
statements : statement statements
 |
 ;
param_starter : var_type ID 
;
parameters : param_starter COMMA parameters
 | param_starter ASSIGN exp COMMA parameters
 | param_starter ASSIGN exp
 | param_starter
 | COMMA parameters 
 |
 ;
statement : define_stmt SEMI
 | condition_stmt
 | while_stmt
 | for_stmt
 | return_stmt SEMI
 | call_stmt SEMI
 | assignment_stmt SEMI
 | BREAK_T SEMI
 | CONTINUE_T SEMI
 | ctr SEMI 
 | ID PLUS_EQ exp SEMI
 | ID MINUS_EQ exp SEMI
 | ID DIV_EQ exp SEMI
 | ID MULT_EQ exp SEMI
 ;
ctr : exp INCR
 | exp DECR 
;
while_stmt : WHILE_T LP exp RP LC RC 
 ;
for_bool : EQ
 | NE
 | LT
 | LE
 | GT
 | GE
 ;
for_stmt : FOR_T LP assignment_stmt SEMI for_bool exp RP statement_num
 ;
condition_stmt : IF_T LP exp RP statement_num optional_elif optional_else
 ;
statement_num : LC statements RC
 | statement
 ;
optional_elif : ELIF_T LP exp RP statement_num optional_elif
 |
 ;
optional_else : ELSE_T statement_num
 |
 ;
define_stmt : var_type ID ASSIGN exp
 ;
assignment_stmt : ID ASSIGN exp
 ;
return_stmt : RETURN_T exp 
 ;
call_stmt : ID LP RP
 | ID LP param_list RP
 ;
exp : exp EQ exp
 | exp NE exp
 | exp LT exp
 | exp LE exp
 | exp GT exp
 | exp GE exp
 | exp AND exp
 | exp OR exp
 | BANG exp
 | exp PLUS exp
 | exp MINUS exp
 | exp MULT exp
 | exp DIVIDE exp
 | exp MOD exp 
 | MINUS exp
 | LP exp RP 
 | ID LP RP
 | ID LP param_list RP
 | ctr 
 | INT_LIT 
 | STRING_LIT 
 | FLOAT_LIT
 | ID 
 | TRUE_T 
 | FALSE_T
 ;
param_list : param_list COMMA exp 
 | exp
 | param_list COMMA 
 | 
 ;
%%

int main() {
 //toggle to 0 to turn off prints, toggle for 1 to turn on prints
 debug = 0;

  if(debug){printf("Entering global scope\n");}
  //my_list = push(my_list,0);
  //class_scope = push(class_scope,0); 
  yyparse();
  
  /*
  //print_class_list(class_scope);
  if(debug){
  	print_list(my_list);
  //printf("--spooky-- this shouldn't exist -->");
  	print_list(class_scope);
  }
  if(!verify_main(class_scope)){
   printf("Program doesn't end in Main and have a main method\n");
  }
  delete_list(class_scope);
  delete_list(my_list);
  */
  printf("You did it!\n");
  return 0;
}

int yyerror(char *s) {
// This is the funciton that gets called if yacc detects a syntax error
printf("Syntax Error line %d\n",linect);
printf("msg: %s\n",yytext);
exit(1);
}
