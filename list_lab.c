#include <stdio.h>
#include <stdlib.h>
#define LCLASS 1
#define LFUNC 2
#define LVAR 3
#define LARR 4
extern int linect;
extern int debug;

typedef struct r{
 char* text;
 struct r *next;
 int num;
 int type;//this is VAR/FUNC/CLASS
 int length;
 int j_type; //this is its actual type like INT/BOOL/STR/STACK/etc
 int has_init;
 int param;
 int lvar_offset;
 int* param_list;
} content, *content_ptr;

typedef struct s{
	struct s *next, *previous;
        struct r *c_list;
	int value;
} node, *node_ptr;

typedef struct s2{
	struct s2 *next, *previous;
	int* arr;
} node2, *node2_ptr;

node_ptr pop(node_ptr list);
node_ptr push(node_ptr list, int value);

node2_ptr pop2(node2_ptr list);
node2_ptr push2(node2_ptr list);
void add_item2(node2_ptr list, int item);

int* peek2(node2_ptr list);

content_ptr cpop(content_ptr list);
content_ptr cpush(content_ptr list, char *a,int b);
int slength(char *c);
int cmp(char *a, char *b);
void share_scope_num(node_ptr a);
node_ptr get_end(node_ptr list);
void print_content(content_ptr list){
 while(list){
  //printf("%s 's length is: %d. type is %d\n",list->text,slength(list->text),list->type);
  printf("%s 's type is %d\n",list->text,list->type);
  list = list->next;
 }
}

void print_class_content(content_ptr list){
 while(list){
  //printf("%s 's length is: %d. type is %d\n",list->text,slength(list->text),list->type);
  printf("%s 's kind is %d, type is %d, and param number is %d\n",list->text,list->type,list->j_type,list->length);
  list = list->next;
 }
}

void print_list(node_ptr list) {
   while (list) {
	printf("Scope number: %d\n",list->value);
        print_content(list->c_list);
	list = list->next;
   }
   printf("\n");
}

void print_list2(node2_ptr list) {
   while (list) {
	int i = 0;
	for(i = 0; i < 20; i++){
		if(list->arr[i] == -1){
			break;
		}
		printf("arr: %d\n", list->arr[i]);
	}
	list = list->next;
   }
   printf("\n");
}

void add_item2(node2_ptr list, int item){
	if(list){
		list->arr[list->arr[0]] = item;
		list->arr[0] = list->arr[0] + 1;
	}
}

void print_class_list(node_ptr list) {
   while (list) {
	printf("Scope number: %d\n",list->value);
        print_class_content(list->c_list);
	list = list->next;
   }
   printf("\n");
}

int find_scope(node_ptr list, char *a, int b){
 while(list){
  content_ptr t = list->c_list;
  while(t){
   if(((cmp(a,t->text)==1)&&(b==t->type))){
   //if((cmp(a,t->text)==1)){
    //printf("%s | found on scope: %d\n",a,list->value);
    return list->value;
   }
   t=t->next;
  }
  list = list->next;
 } 
 //printf("%s | not found in any scope\n",a);
 return -1;
}

node_ptr get_class_scope(node_ptr list, char *a, int b){
 while(list){
  content_ptr t = list->c_list;
  while(t){
   if(((cmp(a,t->text)==1)&&(b==t->type))){
    return list;
   }
   t=t->next;
  }
  list = list->next;
 } 
 return NULL;
}

content_ptr class_given_num(node_ptr my_list,int num){
 node_ptr joe = my_list;
 //print_list(joe);
 content_ptr t;
 while(joe){
  t = joe->c_list;
  //print_content(t);
  while(t){
   if(((t->text)[0]>='A')&&((t->text)[0]<='Z')&&(t->j_type==num)){
    if(debug){printf("YAYY we found %s\n",t->text);}
    return t;
   }
   t=t->next;
  }
  joe=joe->next;
 }
 if(debug){
	printf("no match found\n");
 }
// printf("YUCK\n");
 return NULL;
}

content_ptr get_curr_class(node_ptr my_list){
 node_ptr joe = my_list;
 //print_list(joe);
 content_ptr t;
 if(joe){
  t = joe->c_list;
  //print_content(t);
  while(t){
   if(((t->text)[0]>='A')&&((t->text)[0]<='Z')){
    if(debug){printf("YAYY we found %s\n",t->text);}
    return t;
   }
   t=t->next;
  }
 }
 if(debug){printf("no match found\n");}
// printf("YUCK\n");
 return NULL;
}
char* get_curr_ctext(node_ptr my_list){
 content_ptr joe = get_curr_class(my_list);
 if(joe){
  return joe->text;
 }else{
  printf("big off in get_curr_ctext we gotta return NULL and we dont check this so our code will seg fault lol\n");
  return NULL;
 }
}

content_ptr get_class_ptr(node_ptr my_list, char *text, int num){
 node_ptr joe = my_list;
 content_ptr t;
 if(joe){
  t = joe->c_list;
  while(t){
   if((cmp(t->text,text)==1)&&(num==t->type)){
    if(debug){printf("Found %s with kind %d\n",text,num);}
    return t;
   }
   t=t->next;
  }
 }
 if(debug){
 	printf("no match found\n");
 }
 return NULL;
}

content_ptr get_combined_ptr(node_ptr my_list, char *class_text, char *method_text, int class_num, int var){
 node_ptr temp_node = get_class_scope(my_list,class_text,class_num);
 if(temp_node){
 	content_ptr temp_content = get_class_ptr(temp_node,method_text,var);
 	if(temp_content){
  		return temp_content;
 	}else if(var==LFUNC){
 		 printf("Line: %d method %s not found in scope of %s in get_class_ptr\n",linect,method_text,class_text);
	}
 }else{
  //printf("Line: %d class %s not found in get_combined_ptr\n",linect,class_text);
 }
  return NULL;
}

void delete_content(content_ptr list){
 while(list){
  list = cpop(list);
 }
}

void delete_list(node_ptr list) {
   node_ptr t;
   while (list) {
    delete_content(list->c_list);
    list = pop(list);
   }
}

node_ptr new_node(int value) {
   node_ptr t = (node_ptr)malloc(sizeof(node));
  // t->value = value;
   t->value = 0;
   t->next = t->previous = NULL;
   t->c_list = NULL;
   return t;
}
node_ptr new_node2(int value) {
   node_ptr t = (node_ptr)malloc(sizeof(node));
   t->value = value;
   t->next = t->previous = NULL;
   t->c_list = NULL;
   return t;
}

node2_ptr new_node2_(){
   node2_ptr t = (node2_ptr)malloc(sizeof(node2));
   t->arr = (int*)malloc(sizeof(int)*20);
   for(int i = 1; i < 20; i++){
   	t->arr[i] = -1;
   }
   t->arr[0] = 1;
   t->next = t->previous = NULL;
   return t;
}

content_ptr new_content(char *text, int a){
 content_ptr t = (content_ptr)malloc(sizeof(content));
 t->text = text;
 t->next = NULL;
 t->type = a;
 t->length = 0;
 t->j_type = 0;
 t->has_init = 0;
 t->lvar_offset = -1;
 t->param=0;
 // hardcode at maximum 19 parameters for now plus one counting where to insert
 // ie: index 0 is used to track the "tail" of the array
 t->param_list = (int*)malloc(sizeof(int)*20);
 if(t->param_list==NULL){
 	printf("malloc failed oh no!\n");
 }
 t->param_list[0] = 1;
 for(int i = 1; i < 20; i++){
  t->param_list[i] = -1;
 }
 return t;
}

node_ptr push(node_ptr list, int value){
 node_ptr t = list;
 node_ptr s = new_node(value);
 if(t == NULL){
  /*printf("popping scope %d\n",s->value);
  if(s->c_list){
  print_content(s->c_list);
  }*/
  return s;
 }
 s->next = t;
 s->value = t->value + 1;
 /*printf("adding scope %d\n",t->value);
  if(t->c_list){
  print_content(t->c_list);
 }*/
 return s;
}


node2_ptr push2(node2_ptr list){
 node2_ptr t = list;
 node2_ptr s = new_node2_();
 if(t == NULL){
  return s;
 }
 s->next = t;
 return s;
}

int* peek2(node2_ptr list){
	if(list != NULL){
		return list->arr;
	}
}

content_ptr cpush(content_ptr list, char *text,int a){
 content_ptr s = new_content(text,a);
 if(list == NULL){
  return s;
 }
 s->next = list;
 return s;
}

//ret 0 if not duplicate, 1 if duplicate
int check_dup(node_ptr my_list, char *text, int a){
   content_ptr t;
   node_ptr joe = my_list;
  while(joe){
   t = joe->c_list;
   //if (t == NULL){printf("boof ret 1\n"); return 1;}
   while (t){
     //compare strings and break
     if(debug){
	 printf("%s %d -- %s %d (check_dup)\n",text,a,t->text,t->type);
     }
     //printf("current: %s -- adding: %s\n",t->text,text);
     if((cmp(t->text,text)==1)){
    // if((cmp(t->text,text)==1)&&(t->type==a)){
      //printf("variable/func: %s : in scope -check_dup\n",text);
      return 1;
     }
     t = t->next;
    }
   joe = joe->next;
  }
      //printf("variable/func: %s : out of scope -check_dup\n",text);
   return 0;
}

content_ptr get_ptr(node_ptr my_list, char *text, int num){
 node_ptr joe = my_list;
 content_ptr t;
 while(joe){
  t = joe->c_list;
  while(t){
   //if((cmp(t->text,text)==1)&&(num==t->type)){
   if((cmp(t->text,text)==1)){
    /*MAYBE UNDO THIS COMMENT*/
    //if(debug){printf("Found %s with type %d\n",text,num);}
    return t;
   }
   t=t->next;
  }
  joe=joe->next;
 }
 if(debug){printf("no match found\n");}
 return NULL;
}

content_ptr cappend(content_ptr list, char *text,int a) {
   content_ptr t = list; 
   content_ptr s = new_content(text,a);
  if(text[0]=='\0'){
    //printf("BOOF\n");
  }else{
   if (t == NULL) return s;
   
    //printf("current: %s -- adding: %s\n",t->text,text);
    //if((cmp(t->text,text)==1)&&(a==t->type)){
    if(cmp(t->text,text)==1){
     if(a==t->type){
      printf("Error -- Line: %d ID: \"%s\" already exists in scope\n",linect,text);
     }else{
      printf("Error -- Line: %d ID: \"%s\" variable/function mismatch\n",linect,text);
     }
     cpop(s);
     return list;
    }
 
   while (t->next != NULL){
    //compare strings and break
     
    //printf("current: %s -- adding: %s\n",t->text,text);
    //if((cmp(t->text,text)==1)&&(t->type==a)){
    if(cmp(t->text,text)==1){
     if(a==t->type){
      printf("Error -- Line: %d ID: \"%s\" already exists in scope\n",linect,text);
     }else{
      printf("Errorr -- Line: %d ID: \"%s\" variable/function mismatch\n",linect,text);
     }
      cpop(s);
      return list;
    }
    t = t->next;
    if(t->next==NULL){ 
     if(cmp(t->text,text)==1){
      if(a==t->type){
       printf("Error -- Line: %d ID: \"%s\" already exists in scope\n",linect,text);
      }else{
       printf("Error -- Line: %d ID: \"%s\" variable/function mismatch\n",linect,text);
      }
       cpop(s);
       return list;
     }
    }
   }
   t->next = s;
  }
//   s->previous = t; 
   return list;
}

void add(node_ptr my_list, char *text, int a){
 if(my_list){
  //if(check_dup(my_list->c_list,text,a)){
   my_list->c_list = cappend(my_list->c_list,text,a);
   my_list->c_list->num = my_list->value;
  //}
 }
}

node_ptr add_to_back(node_ptr list, int value) {
   node_ptr t = list; 
   node_ptr s = new_node(value);
   if (t == NULL) return s;
   while (t->next != NULL)  
     t = t->next;
   t->next = s;
   s->previous = t; 
   return list;
}

node_ptr pop(node_ptr list){
 if(list){
  node_ptr t;
  t = list;

  //debug
  /*
  printf("popping scope %d\n",list->value);
  if(list->c_list){
  print_content(list->c_list);
  }*/
  list = list->next;
  /*if(list){
  if(list->c_list){
  print_content(list->c_list);
  }
  }*/
  t->next = NULL;
  free(t);
 }
 return list;
}

node2_ptr pop2(node2_ptr list){
 if(list){
  node2_ptr t;
  t = list;

  list = list->next;
  t->next = NULL;
  free(t);
 }
 return list;
}

content_ptr cpop(content_ptr list){
 if(list){
  content_ptr t;
  t = list;
  list = list->next;
  t->next=NULL;
  free(t->text);
  if(t->param_list){
   free(t->param_list);
  }
  free(t);
 }
 return list;
}

void link(node_ptr scope, content_ptr list){
 if(scope == NULL){
  printf("null scope in link\n");
 }
 if(list ==NULL){
  printf("null list in link\n");
 }
 printf("%d\n",scope->value);
 scope->c_list = list;
 list->num = scope->value;

}
/*
int main() {
   int in_val;
   node_ptr my_list = NULL;
 
   char *joe1 = malloc(4);
   char *joe2 = malloc(4);
   char *joe3= malloc(4);
   char *b1 = malloc(4);
   char *b2 = malloc(4);
   char *b3 = malloc(4);
    
   b1[0] = 'b';
   b1[1] = '4';
   b1[2] = '\0';
   b2[0] = 'b';
   b2[1] = '4';
   b2[2] = '\0';
   b3[0] = 'j';
   b3[1] = '1';
   b3[2] = '\0';
   
   joe1[0] = 'j';
   joe1[1] = '1';
   joe1[2] = '\0';
   joe2[0] = 'j';
   joe2[1] = '2';
   joe2[2] = '\0';
   joe3[0] = 'j';
   joe3[1] = '3';
   joe3[2] = 'j';
   joe3[3] = '\0';
   
    
   my_list = push(my_list,0);
   my_list = push(my_list,1);
   my_list = push(my_list,2);
	add(my_list,joe1);
        add(my_list,joe2);
	add(my_list,joe3);
   my_list = push(my_list,3);
   my_list = push(my_list,4);
  	add(my_list,b1);
	add(my_list,b2);
	add(my_list,b3);
 
   printf("List:\n");
   print_list(my_list);
   find_scope(my_list,"j3j");
   find_scope(my_list,"j1");
   find_scope(my_list,"heck!");
   //printf("results of j1 == j1: %d and j12 == j1: %d\n",cmp("j1","j1"),cmp("j1","j13"));
   delete_list(my_list);
}*/

//ret string length
int slength(char *a){
 int i = 0;
 while(a!=NULL){
  if(a[i]=='\0'){
   return i;
  }
  i++;
 }
 return i;
}


//ret 1 if strings equal else -1
int cmp(char *a, char *b){
 int i = 0;
 int isEqual = 0;
 if((a == NULL) || (b==NULL)){
  printf("Null pointers during cmp\n");
  return 0;
 }
 while(isEqual == 0){
  if(a[i]=='\0'){
   if(b[i]=='\0'){
    isEqual = 1;
    return isEqual;
   }
   isEqual = -1;
   return isEqual;
  }
 
  if(b[i]=='\0'){
   if(a[i]=='\0'){
    isEqual = 1;
    return isEqual;
   }
   isEqual = -1;
   return isEqual;
  }
  if(a[i] != b[i]){
   isEqual = -1;
   return isEqual;
  }
  i++;
 }
}

char* methodify(char *a){
 int alen = slength(a)+2;
 char* temp = malloc(alen);
 strcpy(temp,a);
 temp[alen]='\0';
 temp[alen-1]=')';
 temp[alen-2]='(';

 free(a);
 return temp;
}

int verify_main(node_ptr my_list){
 node_ptr t = my_list;
 content_ptr f = NULL;
 int spotted_M = 0;
 int spotted_m = 0;
 if(my_list){
  f=my_list->c_list;
  if(f){
   if(cmp(f->text,"Main")==1){
    spotted_M = 1;
   }
  }
  f=f->next;
  while(f){
   if(cmp(f->text,"main")==1){
    spotted_m = 1;
    return (spotted_M & spotted_m);
   }
   f=f->next;
  }
 }
 return 0;
}

//not currently used
int get_scope_num_class(node_ptr my_list, char *class, char *func){ 
 node_ptr t = my_list;
 content_ptr f = NULL;
 while(t){
  f = t->c_list;
  if(f){
   if(cmp(f->text,class)==1){
    //we have detected the Class in scope
    printf("Class: %s detected\n",class);
    while(f){
     if(cmp(f->text,func)==1){
      printf("Line: %d Function: %s detected in %s scope\n",linect,func,class);
      return t->value;
     }
     f=f->next;
    }
    printf("Line: %d Class: %s found but function: %s not found\n",linect,class,func);
    return -1;
   }
  }
  t = t->next;
 }
 printf("Line: %d Class: %s not found\n",linect,class);
 return -1;
}

node_ptr pushv(node_ptr list, int value){
 node_ptr t = list;
 node_ptr s = new_node(value);
 s->value = value;
 if(t == NULL){
  return s;
 }
 s->next = t;
 s->value = value;
 return s;
}

node_ptr pushv2(node_ptr list, int value){
 node_ptr t = list;
 node_ptr s = new_node2(value);
 if(t == NULL){
  return s;
 }
 s->next = t;
 s->value = value;
 t->previous = s;
 return s;
}

node_ptr get_end(node_ptr list){
	while(list){
		if(list->next ==NULL){
			return list;
		}else{
			list = list->next;
		}
	}
}
