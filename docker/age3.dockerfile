FROM python:3

WORKDIR /src/

RUN set -xe \
   && apt-get update \
    && apt-get install python3-pip -y
RUN pip3 install --upgrade pip

RUN pip install flask

RUN apt-get install flex -y && apt-get install bison -y

COPY ./ ./

RUN export PYTHONPATH="${PYTHONPATH}:/src/"

EXPOSE 5000

ENV FLASK_ENV=development
ENV FLASK_APP=/src/service.py

RUN make

# if --host=0.0.0.0 is ommitted and 127.0.0.1 is used instead for example
# docker will claim the connection is reset when you send it a post request
ENTRYPOINT [ "flask", "run", "--host=0.0.0.0"]

## ---- instructions ----
## go to the age3_compiler directory and run 
## docker image build -t bin/age3 -f ./docker/age3.dockerfile . 
## docker run -p 5000:5000 bin/age3

## from a place where a .xs file exists
## curl -H "Content-Type:text/plain" --data-binary @inputs/tiny.xs 0.0.0.0:5000/xs
# try this if the following fails
## curl -H "Content-Type:text/plain" --data-binary @inputs/tiny.xs 127.0.0.1:5000/xs
# or the more general
## curl -H "Content-Type:text/plain" --data-binary @filename.xs 127.0.0.1:5000/xs

## sometimes windows tries to alias curl to Invoke-WebRequest, so if you get an error about "Headers", run curl.exe instead of curl

