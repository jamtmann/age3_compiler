//Hardcoded variables used to turn an original map into observer maps
int NumberTeams=2;
int NumberPlayers=3;
int NumberNonGaiaPlayers=2;

// GREAT PLAINS
// Nov 06 - YP update
include "mercenaries.xs";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";

/*[User defined functions start]*/

string DefaultLightingSet="great plains";
string ArtStyleP1="Euro";
string ArtStyleP2="Euro";

void cCo(string xs="") {
        rmAddTriggerCondition("Timer");
        rmSetTriggerConditionParam("Param1","0) && ("+xs+");//");
}

void cEf(string xs="") {
	rmAddTriggerEffect("SetIdleProcessing");
	 rmSetTriggerEffectParam("IdleProc", "true); "+xs+" trSetUnitIdleProcessing(true");
}

void Timer(float time=0) {
	rmAddTriggerCondition("Timer");
	rmSetTriggerConditionParam("Param1", ""+time);
}

void AgeUpPCT(int i=0, int plyr=1) {
	cEf("*/ else if(kbGetTechPercentComplete("+i+")>0 && kbGetTechPercentComplete("+i+")<1){ /*");
	cEf("*/ AGE_PROG_"+plyr+"=100*kbGetTechPercentComplete("+i+");} /*");
}

void ElIf_XP_PC(int a=0, int b=0, int c=0){
	cEf("*/ 	else if(XP<"+a+"){perc=100*(XP-"+b+")/"+c+";} /*");
}

void elifreturn(int i=0, string tbr=""){
	cEf("*/ else if(i=="+i+"){return(\""+tbr+"\");} /*");
}

void uniquemults(int i=0, int tkid=0){
	cEf("*/ 	if(intarraymults("+i+","+tkid+")==1){if(tempcration==0){tempcration="+tkid+";}else{tempcration=-1;break;}} /*");
}

void constanthero(int yadda=0, string bladda=""){
	cEf("*/ 	"+bladda+"=0; /*");
	cEf("*/ 	for(i=0; <=7){ /*");
	cEf("*/ 	if(trIsGadgetVisible(\"ObzCustomConfig-"+yadda+"-pv\"+i)){"+bladda+"="+bladda+"+aexpb(2,i);} /*");
	cEf("*/ 	} /*");
}

//what techs are pre-researched and hence should not be included?

bool isCounted(int i=-1) {
if(i<168){
return(false);
}
else if(i<303){
if(i==169){return(false);}
else if(i>=171 && i<=192){return(false);}
else if(i>=194 && i<=258){return(false);}
else if(i>=301 && i<=302){return(false);}
}
else if(i<494){
if(i==419){return(false);}
else if(i>=421 && i<=425){return(false);}
else if(i>=430 && i<=434){return(false);}
else if(i>=437 && i<=443){return(false);}
else if(i==484){return(false);}
else if(i>=486 && i<=493){return(false);}
}
else if(i<1107){
if(i>=529 && i<=532){return(false);}
else if(i==536){return(false);}
else if(i>=565 && i<=567){return(false);}
else if(i==569){return(false);}
else if(i>=581 && i<=596){return(false);}
else if(i>=607 && i<=608){return(false);}
else if(i==610){return(false);}
else if(i==661){return(false);}
else if(i==1056){return(false);}
else if(i==1074){return(false);}
else if(i==1075){return(false);}
else if(i==1080){return(false);}
else if(i>=1095 && i<=1097){return(false);}
else if(i>=1103 && i<=1106){return(false);}
}
else if(i<1573){
if(i>=1131 && i<=1138){return(false);}
else if(i>=1231 && i<=1234){return(false);}
else if(i==1474){return(false);}
else if(i==1502){return(false);}
else if(i==1505){return(false);}
else if(i==1506){return(false);}
else if(i==1524){return(false);}
else if(i==1525){return(false);}
else if(i==1528){return(false);}
else if(i==1530){return(false);}
else if(i>=1543 && i<=1556){return(false);}
else if(i>=1559 && i<=1562){return(false);}
else if(i==1572){return(false);}
}
else if(i<2078){
if(i>=1582 && i<=1588){return(false);}
else if(i==1743){return(false);}
else if(i==1753){return(false);}
else if(i==1760){return(false);}
else if(i==1765){return(false);}
else if(i>=1842 && i<=1853){return(false);}
else if(i==1971){return(false);}
else if(i>=1986 && i<=1989){return(false);}
else if(i==2016){return(false);}
else if(i>=2072 && i<=2077){return(false);}
}
else if(i<2289){
if(i>=2086 && i<=2090){return(false);}
else if(i>=2092 && i<=2094){return(false);}
else if(i==2100){return(false);}
else if(i==2107){return(false);}
else if(i>=2125 && i<=2127){return(false);}
else if(i==2138){return(false);}
else if(i==2139){return(false);}
else if(i==2141){return(false);}
else if(i>=2204 && i<=2211){return(false);}
else if(i==2213){return(false);}
else if(i>=2266 && i<=2281){return(false);}
else if(i==2286){return(false);}
else if(i==2288){return(false);}
}
else if(i<2549){
if(i>=2294 && i<=2295){return(false);}
else if(i>=2334 && i<=2337){return(false);}
else if(i==2342){return(false);}
else if(i>=2360 && i<=2362){return(false);}
else if(i==2364){return(false);}
else if(i==2388){return(false);}
else if(i>=2398 && i<=2407){return(false);}
else if(i==2415){return(false);}
else if(i>=2422 && i<=2425){return(false);}
else if(i>=2428 && i<=2429){return(false);}
else if(i==2451){return(false);}
else if(i==2461){return(false);}
else if(i==2472){return(false);}
else if(i==2483){return(false);}
else if(i>=2486 && i<=2490){return(false);}
else if(i==2495){return(false);}
else if(i==2509){return(false);}
else if(i>=2526 && i<=2527){return(false);}
else if(i==2529){return(false);}
else if(i==2530){return(false);}
else if(i>=2537 && i<=2543){return(false);}
else if(i==2548){return(false);}
}
return(true);
}

bool isHCTech(int i=-1) {
if(i<550){
if(i>=374 && i<=380){return(true);}
else if(i==387){return(true);}
else if(i==396){return(true);}
else if(i==402){return(true);}
else if(i==405){return(true);}
else if(i==420){return(true);}
else if(i==451){return(true);}
else if(i>=510 && i<=512){return(true);}
else if(i==521){return(true);}
else if(i==549){return(true);}
}
else if(i<613){
if(i>=562 && i<=564){return(true);}
else if(i==568){return(true);}
else if(i==570){return(true);}
else if(i>=574 && i<=578){return(true);}
else if(i==598){return(true);}
else if(i==599){return(true);}
else if(i==606){return(true);}
else if(i==609){return(true);}
else if(i==611){return(true);}
else if(i==612){return(true);}
}
else if(i<1103){
if(i>=614 && i<=629){return(true);}
else if(i>=633 && i<=656){return(true);}
else if(i==658){return(true);}
else if(i==659){return(true);}
else if(i>=677 && i<=683){return(true);}
else if(i>=688 && i<696){return(true);}
else if(i>=705 && i!=864 && i!=965 && i!=966 && i!=981 && i<=1000){return(true);}
else if(i>=1006 && i!=1056 && i!=1069 && i<=1073){return(true);}
else if(i>=1081 && i<=1094){return(true);}
else if(i>=1100 && i<=1102){return(true);}
}
else if(i<1474){
if(i>=1164 && i<=1202){return(true);}
else if(i==1209){return(true);}
else if(i==1210){return(true);}
else if(i>=1215 && i<=1230){return(true);}
else if(i>=1235 && i<=1256){return(true);}
else if(i>=1269 && i<=1303){return(true);}
else if(i>=1306 && i<=1310){return(true);}
else if(i>=1315 && i<=1336){return(true);}
else if(i>=1345 && i<=1379){return(true);}
else if(i>=1389 && i<=1423){return(true);}
else if(i>=1470 && i<=1473){return(true);}
}
else if(i<1741){
if(i==1497){return(true);}
else if(i==1503){return(true);}
else if(i==1504){return(true);}
else if(i==1529){return(true);}
else if(i==1541){return(true);}
else if(i==1542){return(true);}
else if(i>=1573 && i<=1577){return(true);}
else if(i==1581){return(true);}
else if(i>=1589 && i<=1661){return(true);}
else if(i>=1665 && i!=1675 && i<=1740){return(true);}
}
else if(i<2072){
if(i==1742){return(true);}
else if(i>=1746 && i<=1752){return(true);}
else if(i==1757){return(true);}
else if(i==1771){return(true);}
else if(i>=1799 && i<=1811){return(true);}
else if(i>=1854 && i!=1923 && i!=1939 && i!=1950 && i<=1965){return(true);}
else if(i>=1973 && i<=1979){return(true);}
else if(i>=1982 && i<=1985){return(true);}
else if(i>=1992 && i!=1998 && i<=2008){return(true);}
else if(i>=2017 && i<=2071){return(true);}
}
else if(i<2236){
if(i==2091){return(true);}
else if(i==2095){return(true);}
else if(i==2096){return(true);}
else if(i==2097){return(true);}
else if(i>=2128 && i<=2132){return(true);}
else if(i==2137){return(true);}
else if(i==2140){return(true);}
else if(i==2214){return(true);}
else if(i>=2220 && i<=2224){return(true);}
else if(i==2235){return(true);}
}
else if(i<2318){
if(i==2236){return(true);}
else if(i==2237){return(true);}
else if(i>=2239 && i<=2265){return(true);}
else if(i==2282){return(true);}
else if(i==2284){return(true);}
else if(i==2285){return(true);}
else if(i==2296){return(true);}
else if(i==2297){return(true);}
else if(i==2298){return(true);}
else if(i==2317){return(true);}
}
else if(i<2412){
if(i==2318){return(true);}
else if(i==2319){return(true);}
else if(i>=2322 && i!=2329 && i<=2332){return(true);}
else if(i>=2338 && i<=2341){return(true);}
else if(i==2343){return(true);}
else if(i>=2346 && i!=2353 && i<=2358){return(true);}
else if(i==2363){return(true);}
else if(i==2364){return(true);}
else if(i==2387){return(true);}
else if(i>=2395 && i<=2397){return(true);}
else if(i>=2408 && i<=2411){return(true);}
}
else if(i<2548){
if(i==2426){return(true);}
else if(i==2427){return(true);}
else if(i>=2430 && i!=2451 && i!=2461 && i!=2468 && i!=2472 && i<=2482){return(true);}
else if(i==2484){return(true);}
else if(i==2485){return(true);}
else if(i>=2491 && i<=2494){return(true);}
else if(i>=2502 && i<=2508){return(true);}
else if(i>=2512 && i<=2524){return(true);}
else if(i>=2532 && i!=2534 && i<=2536){return(true);}
else if(i>=2545 && i<=2547){return(true);}
}
return(false);
}

/*[User defined functions end]*/

// Main entry point for random map script
void main(void)
{
   // Text
   // These status text lines are used to manually animate the map generation progress bar
   rmSetStatusText("",0.01);

   //Chooses which natives appear on the map
   int subCiv0=-1;
   int subCiv1=-1;
   int subCiv2=-1;
   int subCiv3=-1;
   int subCiv4=-1;
   int subCiv5=-1;

	// Choose which variation to use.  1=southeast trade route, 2=northwest trade route
	int whichMap=rmRandInt(1,2);
	// int whichMap=2;

	// Are there extra meeting poles?
	int extraPoles=rmRandInt(1,2);
	extraPoles=1;
	// int extraPoles=2;

   if (rmAllocateSubCivs(6) == true)
   {
		subCiv0=rmGetCivID("Comanche");
		rmEchoInfo("subCiv0 is Comanche "+subCiv0);
		if (subCiv0 >= 0)
			rmSetSubCiv(0, "Comanche");

		subCiv1=rmGetCivID("Cheyenne");
		rmEchoInfo("subCiv1 is Cheyenne "+subCiv1);
		if (subCiv1 >= 0)
			rmSetSubCiv(1, "Cheyenne");

		subCiv2=rmGetCivID("Cheyenne");
		rmEchoInfo("subCiv2 is Cheyenne "+subCiv2);
		if (subCiv2 >= 0)
			rmSetSubCiv(2, "Cheyenne");
		
		subCiv3=rmGetCivID("Comanche");
		rmEchoInfo("subCiv3 is Comanche "+subCiv3);
		if (subCiv3 >= 0)
			rmSetSubCiv(3, "Comanche");
		
		subCiv4=rmGetCivID("Comanche");
		rmEchoInfo("subCiv4 is Comanche "+subCiv4);
		if (subCiv4 >= 0)
			rmSetSubCiv(4, "Comanche");
		
		subCiv5=rmGetCivID("Cheyenne");
		rmEchoInfo("subCiv5 is Cheyenne "+subCiv5);
		if (subCiv5 >= 0)
			rmSetSubCiv(5, "Cheyenne");
   }

   // Picks the map size
	int playerTiles=11000;
   if (NumberNonGaiaPlayers >4)
		playerTiles = 10000;
   if (NumberNonGaiaPlayers >6)
      playerTiles = 8500;

	int size=2.0*sqrt(NumberNonGaiaPlayers*playerTiles);
	rmEchoInfo("Map size="+size+"m x "+size+"m");
	rmSetMapSize(size, size);

	// Picks a default water height
	rmSetSeaLevel(0.0);

    // Picks default terrain and water
	rmSetMapElevationParameters(cElevTurbulence, 0.02, 7, 0.5, 8.0);
	rmSetBaseTerrainMix("great plains drygrass");
	rmTerrainInitialize("yukon\ground1_yuk", 5);
	rmSetLightingSet("great plains");
	rmSetMapType("greatPlains");
	rmSetMapType("land");
	rmSetWorldCircleConstraint(true);
	rmSetMapType("grass");

	chooseMercs();

	// Define some classes. These are used later for constraints.
	int classPlayer=rmDefineClass("player");
	rmDefineClass("classPatch");
	rmDefineClass("starting settlement");
	rmDefineClass("startingUnit");
	rmDefineClass("classForest");
	rmDefineClass("importantItem");
	rmDefineClass("secrets");
	rmDefineClass("natives");	
	rmDefineClass("classHillArea");
	rmDefineClass("socketClass");
	rmDefineClass("nuggets");

   // -------------Define constraints
   // These are used to have objects and areas avoid each other
   
   // Map edge constraints
   int playerEdgeConstraint=rmCreateBoxConstraint("player edge of map", rmXTilesToFraction(6), rmZTilesToFraction(6), 1.0-rmXTilesToFraction(6), 1.0-rmZTilesToFraction(6), 0.01);

   // Player constraints
   int playerConstraint=rmCreateClassDistanceConstraint("player vs. player", classPlayer, 10.0);
   int smallMapPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players a lot", classPlayer, 70.0);
   int nuggetPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players a lot", classPlayer, 40.0);
   int minePlayerConstraint=rmCreateClassDistanceConstraint("far mines stay away from players a lot", classPlayer, 58.0);


   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 25.0);
   int coinForestConstraint=rmCreateClassDistanceConstraint("coin vs. forest", rmClassID("classForest"), 15.0);
   int avoidBison=rmCreateTypeDistanceConstraint("bison avoids food", "bison", 40.0);
   int avoidPronghorn=rmCreateTypeDistanceConstraint("pronghorn avoids food", "pronghorn", 35.0);
	int avoidCoin=rmCreateTypeDistanceConstraint("coin avoids coin", "gold", 30.0);
	int avoidStartingCoin=rmCreateTypeDistanceConstraint("starting coin avoids coin", "gold", 22.0);
   int avoidNugget=rmCreateTypeDistanceConstraint("nugget avoid nugget", "AbstractNugget", 40.0);
   int avoidNuggetSmall=rmCreateTypeDistanceConstraint("avoid nuggets by a little", "AbstractNugget", 10.0);

	int avoidFastCoin=-1;
	if (NumberNonGaiaPlayers >6)
	{
		avoidFastCoin=rmCreateTypeDistanceConstraint("fast coin avoids coin", "gold", rmXFractionToMeters(0.14));
	}
	else if (NumberNonGaiaPlayers >4)
	{
		avoidFastCoin=rmCreateTypeDistanceConstraint("fast coin avoids coin", "gold", rmXFractionToMeters(0.16));
	}
	else
	{
		avoidFastCoin=rmCreateTypeDistanceConstraint("fast coin avoids coin", "gold", rmXFractionToMeters(0.18));
	}

   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);
   int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("short avoid impassable land", "Land", false, 2.0);
   int patchConstraint=rmCreateClassDistanceConstraint("patch vs. patch", rmClassID("classPatch"), 5.0);

   // Unit avoidance - for things that aren't in the starting resources.
   int avoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units", rmClassID("startingUnit"), 30.0);
   int avoidStartingUnitsSmall=rmCreateClassDistanceConstraint("objects avoid starting units small", rmClassID("startingUnit"), 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

   // VP avoidance
   int avoidTradeRoute = rmCreateTradeRouteDistanceConstraint("trade route", 6.0);
   int avoidTradeRouteSmall = rmCreateTradeRouteDistanceConstraint("trade route small", 4.0);
   // int avoidTradeRoutePlayer = rmCreateTradeRouteDistanceConstraint("trade route player", 30.0);
   int avoidImportantItem=rmCreateClassDistanceConstraint("important stuff avoids each other", rmClassID("importantItem"), 15.0);

	int avoidSocket=rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 8.0);
	int avoidSocketMore=rmCreateClassDistanceConstraint("bigger socket avoidance", rmClassID("socketClass"), 15.0);

   // Constraint to avoid water.
   int avoidWater4 = rmCreateTerrainDistanceConstraint("avoid water", "Land", false, 4.0);
   int avoidWater20 = rmCreateTerrainDistanceConstraint("avoid water medium", "Land", false, 20.0);
   int avoidWater40 = rmCreateTerrainDistanceConstraint("avoid water long", "Land", false, 40.0);

	// Avoid the hilly areas.
	int avoidHills = rmCreateClassDistanceConstraint("avoid Hills", rmClassID("classHillArea"), 5.0);
	
	// natives avoid natives
	int avoidNatives = rmCreateClassDistanceConstraint("avoid Natives", rmClassID("natives"), 40.0);
	int avoidNativesNuggets = rmCreateClassDistanceConstraint("nuggets avoid Natives", rmClassID("natives"), 20.0);

	int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // directional constraints
	int Eastward=rmCreatePieConstraint("eastMapConstraint", 0.5, 0.5, 0, rmZFractionToMeters(0.5), rmDegreesToRadians(0), rmDegreesToRadians(180));
	int Westward=rmCreatePieConstraint("westMapConstraint", 0.5, 0.5, 0, rmZFractionToMeters(0.5), rmDegreesToRadians(180), rmDegreesToRadians(360));

   // Text
   rmSetStatusText("",0.10);
	
   // TRADE ROUTE PLACEMENT
   int tradeRouteID = rmCreateTradeRoute();

   int socketID=rmCreateObjectDef("sockets to dock Trade Posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
	rmSetObjectDefAllowOverlap(socketID, true);
   rmAddObjectDefToClass(socketID, rmClassID("socketClass"));
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 8.0);

	if ( whichMap == 1 )
	{
		rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.6);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.2, 0.25, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.2, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.8, 0.25, 10, 4);
	}
	else
	{
		rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.2, 0.75, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.8, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.8, 0.75, 10, 4);
	}

	rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.5, 20, 4);

   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
	// add the sockets along the trade route.
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.15);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.85);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	if ( extraPoles > 1 )
	{
	   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.35);
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

		socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.65);
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
	}
      
   // DEFINE AREAS
   // Set up player starting locations.
   
   	for(i=2; <=7) 
	{
		rmSetPlacementTeam(i); //Place players 3-8, since rmSetPlacementTeam, rmSetPlacementSection, rmPlacePlayersCircular uses the number of total players, nothing we can manipulate
		rmPlacePlayersCircular(0.5, 0.5, 0);
	}
	   rmSetPlacementTeam(-1);
   
   if ( whichMap == 1 )
   {
	   	rmSetPlacementSection(0.7, 0.3); // 0.5
   }
   else
   {
	    rmSetPlacementSection(0.2, 0.8); // 0.5	   	
   }
   rmSetTeamSpacingModifier(0.7);
   if ( NumberNonGaiaPlayers < 3 )
   {
		rmPlacePlayersCircular(0.3, 0.3, 0);
   }
   else
   {
		rmPlacePlayersCircular(0.38, 0.38, 0);
   }
   

	/*
	// Place the first team.
   rmSetPlacementTeam(0);
	if ( whichMap == 1 ) // se trade route
	{
		rmSetPlayerPlacementArea(0.7, 0.5, 0.9, 0.8);
	}
	else
	{
		rmSetPlayerPlacementArea(0.7, 0.2, 0.9, 0.5);
	}
   rmPlacePlayersCircular(0.3, 0.4, rmDegreesToRadians(5.0));

	// Now place Second Team
   rmSetPlacementTeam(1);
   	if ( whichMap == 1 ) // nw trade route
	{
		rmSetPlayerPlacementArea(0.1, 0.5, 0.3, 0.8);
	}
	else
	{
		rmSetPlayerPlacementArea(0.1, 0.2, 0.3, 0.5);
	}
   rmPlacePlayersCircular(0.3, 0.4, rmDegreesToRadians(5.0));
   */

	//	rmPlacePlayersCircular(0.45, 0.45, rmDegreesToRadians(5.0));

   // Set up player areas.
   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <NumberPlayers)
   {
      // Create the area.
      int id=rmCreateArea("Player"+i);
      // Assign to the player.
      rmSetPlayerArea(i, id);
      // Set the size.
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, playerEdgeConstraint); 
	  rmAddAreaConstraint(id, avoidTradeRoute); 
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

	int numTries = -1;
	int failCount = -1;

	// Text
   rmSetStatusText("",0.20);

	// Two hilly areas
   int hillNorthwestID=rmCreateArea("northwest hills");
   int avoidNW = rmCreateAreaDistanceConstraint("avoid nw", hillNorthwestID, 3.0);

   rmSetAreaSize(hillNorthwestID, 0.2, 0.2);
   rmSetAreaWarnFailure(hillNorthwestID, false);
   rmSetAreaSmoothDistance(hillNorthwestID, 10);
	rmSetAreaMix(hillNorthwestID, "great plains grass");
	rmAddAreaTerrainLayer(hillNorthwestID, "great_plains\ground8_gp", 0, 4);
   rmAddAreaTerrainLayer(hillNorthwestID, "great_plains\ground1_gp", 4, 10);
   rmSetAreaElevationType(hillNorthwestID, cElevTurbulence);
   rmSetAreaElevationVariation(hillNorthwestID, 6.0);
   rmSetAreaBaseHeight(hillNorthwestID, 5);
   rmSetAreaElevationMinFrequency(hillNorthwestID, 0.05);
   rmSetAreaElevationOctaves(hillNorthwestID, 3);
   rmSetAreaElevationPersistence(hillNorthwestID, 0.3);      
   rmSetAreaElevationNoiseBias(hillNorthwestID, 0.5);
   rmSetAreaElevationEdgeFalloffDist(hillNorthwestID, 20.0);
	rmSetAreaCoherence(hillNorthwestID, 0.9);
	rmSetAreaLocation(hillNorthwestID, 0.5, 0.9);
   rmSetAreaEdgeFilling(hillNorthwestID, 5);
	rmAddAreaInfluenceSegment(hillNorthwestID, 0.1, 0.95, 0.9, 0.95);
	rmSetAreaHeightBlend(hillNorthwestID, 1);
	rmSetAreaObeyWorldCircleConstraint(hillNorthwestID, false);
	rmAddAreaToClass(hillNorthwestID, rmClassID("classHillArea"));
	rmBuildArea(hillNorthwestID);

   int hillSoutheastID=rmCreateArea("southeast hills");
   int avoidSE = rmCreateAreaDistanceConstraint("avoid se", hillSoutheastID, 3.0);

   rmSetAreaSize(hillSoutheastID, 0.2, 0.2);
   rmSetAreaWarnFailure(hillSoutheastID, false);
   rmSetAreaSmoothDistance(hillSoutheastID, 10);
	rmSetAreaMix(hillSoutheastID, "great plains grass");
	rmAddAreaTerrainLayer(hillSoutheastID, "great_plains\ground8_gp", 0, 4);
   rmAddAreaTerrainLayer(hillSoutheastID, "great_plains\ground1_gp", 4, 10);
   rmSetAreaElevationType(hillSoutheastID, cElevTurbulence);
   rmSetAreaElevationVariation(hillSoutheastID, 6.0);
   rmSetAreaBaseHeight(hillSoutheastID, 5);
   rmSetAreaElevationMinFrequency(hillSoutheastID, 0.05);
   rmSetAreaElevationOctaves(hillSoutheastID, 3);
   rmSetAreaElevationPersistence(hillSoutheastID, 0.3);
   rmSetAreaElevationNoiseBias(hillSoutheastID, 0.5);
   rmSetAreaElevationEdgeFalloffDist(hillSoutheastID, 20.0);
	rmSetAreaCoherence(hillSoutheastID, 0.9);
	rmSetAreaLocation(hillSoutheastID, 0.5, 0.1);
   rmSetAreaEdgeFilling(hillSoutheastID, 5);
	rmAddAreaInfluenceSegment(hillSoutheastID, 0.1, 0.05, 0.9, 0.05);
	rmSetAreaHeightBlend(hillSoutheastID, 1);
	rmSetAreaObeyWorldCircleConstraint(hillSoutheastID, false);
	rmAddAreaToClass(hillSoutheastID, rmClassID("classHillArea"));
	rmBuildArea(hillSoutheastID);

   // Build the areas.
   // rmBuildAllAreas();

   // Text
   rmSetStatusText("",0.30);

   // Placement order of non-trade route stuff
   // Natives -> Secrets -> Nuggets -> Small Ponds (whee)

/*[Special unit placement start]*/
	for(i=3; <= cNumberNonGaiaPlayers) {
				int unit = rmCreateObjectDef("Target"+i);
				rmAddObjectDefItem(unit,"Target",1,1);
				rmPlaceObjectDefAtLoc(unit,i,0.45+0.01*i,0.985,1);
	}
/*[Special unit placement end]*/

    //STARTING UNITS and RESOURCES DEFS
	int startingUnits = rmCreateStartingUnitsObjectDef(5.0);
	rmSetObjectDefMinDistance(startingUnits, 7.0);
	rmSetObjectDefMaxDistance(startingUnits, 12.0);

	int startingTCID = rmCreateObjectDef("startingTC");
	if ( rmGetNomadStart())
	{
		rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 0.0);
	}
	else
	{
		rmAddObjectDefItem(startingTCID, "TownCenter", 1, 0.0);
	}
	rmAddObjectDefToClass(startingTCID, rmClassID("startingUnit"));
	rmSetObjectDefMinDistance(startingTCID, 0.0);
	rmSetObjectDefMaxDistance(startingTCID, 9.0);
	rmAddObjectDefConstraint(startingTCID, avoidTradeRouteSmall);

	// rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);

	int StartAreaTreeID=rmCreateObjectDef("starting trees");
	rmAddObjectDefItem(StartAreaTreeID, "TreeGreatPlains", 1, 0.0);
	rmSetObjectDefMinDistance(StartAreaTreeID, 12.0);
	rmSetObjectDefMaxDistance(StartAreaTreeID, 18.0);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidStartingUnitsSmall);

	int StartBisonID=rmCreateObjectDef("starting bison");
	rmAddObjectDefItem(StartBisonID, "Bison", 4, 4.0);
      rmSetObjectDefCreateHerd(StartBisonID, true);
	rmSetObjectDefMinDistance(StartBisonID, 10.0);
	rmSetObjectDefMaxDistance(StartBisonID, 12.0);
	rmSetObjectDefCreateHerd(StartBisonID, true);
	rmAddObjectDefConstraint(StartBisonID, avoidStartingUnitsSmall);

	int playerNuggetID=rmCreateObjectDef("player nugget");
	rmAddObjectDefItem(playerNuggetID, "nugget", 1, 0.0);
	rmAddObjectDefToClass(playerNuggetID, rmClassID("nuggets"));
	rmAddObjectDefToClass(playerNuggetID, rmClassID("startingUnit"));
      rmSetObjectDefMinDistance(playerNuggetID, 28.0);
      rmSetObjectDefMaxDistance(playerNuggetID, 35.0);
	rmAddObjectDefConstraint(playerNuggetID, avoidNugget);
	rmAddObjectDefConstraint(playerNuggetID, avoidNativesNuggets);
	rmAddObjectDefConstraint(playerNuggetID, avoidTradeRouteSmall);
	rmAddObjectDefConstraint(playerNuggetID, circleConstraint);
	// rmAddObjectDefConstraint(playerNuggetID, avoidImportantItem);

	int startSilver3ID = rmCreateObjectDef("player farther silver");
	rmAddObjectDefItem(startSilver3ID, "mine", 1, 0.0);
	rmAddObjectDefConstraint(startSilver3ID, avoidTradeRoute);
	rmSetObjectDefMinDistance(startSilver3ID, 60.0);
	rmSetObjectDefMaxDistance(startSilver3ID, 65.0);
	rmAddObjectDefConstraint(startSilver3ID, avoidAll);
	rmAddObjectDefConstraint(startSilver3ID, minePlayerConstraint);
	rmAddObjectDefConstraint(startSilver3ID, avoidCoin);

      // Extra tree clumps near players - to ensure fair access to wood
      int extraTreesID=rmCreateObjectDef("extra trees");
      rmAddObjectDefItem(extraTreesID, "TreeGreatPlains", 6, 6.0);
      rmSetObjectDefMinDistance(extraTreesID, 20);
      rmSetObjectDefMaxDistance(extraTreesID, 23);
      rmAddObjectDefConstraint(extraTreesID, avoidAll);
      rmAddObjectDefConstraint(extraTreesID, avoidSocket);
      rmAddObjectDefConstraint(extraTreesID, avoidTradeRoute);
      rmAddObjectDefConstraint(extraTreesID, avoidStartingUnitsSmall);

	rmSetStatusText("",0.40);
	
	int silverType = -1;
	int playerGoldID = -1;

 	for(i=1; <NumberPlayers)
	{
		rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
    
    if(ypIsAsian(i) && rmGetNomadStart() == false)
      rmPlaceObjectDefAtLoc(ypMonasteryBuilder(i, 1), i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
    
		rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		// vector closestPoint=rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingUnits, i));
		// rmSetHomeCityGatherPoint(i, closestPoint);

		// Everyone gets two ore ObjectDefs, one pretty close, the other a little further away.
		silverType = rmRandInt(1,10);
		playerGoldID = rmCreateObjectDef("player silver closer "+i);
		rmAddObjectDefItem(playerGoldID, "mine", 1, 0.0);
		// rmAddObjectDefToClass(playerGoldID, rmClassID("importantItem"));
		rmAddObjectDefConstraint(playerGoldID, avoidTradeRoute);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingCoin);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingUnitsSmall);
		rmSetObjectDefMinDistance(playerGoldID, 15.0);
		rmSetObjectDefMaxDistance(playerGoldID, 20.0);
		
		// Place two gold mines at start and 3rd father off
		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startSilver3ID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		// Placing starting trees...
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmPlaceObjectDefAtLoc(StartBisonID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmPlaceObjectDefAtLoc(extraTreesID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmSetNuggetDifficulty(1, 1);
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	}


	// NATIVE AMERICANS
   float NativeVillageLoc = rmRandFloat(0,1); //

   if (subCiv0 == rmGetCivID("Comanche"))
   {  
      int comancheVillageAID = -1;
      int comancheVillageType = rmRandInt(1,5);
      comancheVillageAID = rmCreateGrouping("comanche village A", "native comanche village "+comancheVillageType);
      rmSetGroupingMinDistance(comancheVillageAID, 0.0);
      rmSetGroupingMaxDistance(comancheVillageAID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(comancheVillageAID, avoidImpassableLand);
      rmAddGroupingToClass(comancheVillageAID, rmClassID("importantItem"));
      rmAddGroupingToClass(comancheVillageAID, rmClassID("natives"));
      rmAddGroupingConstraint(comancheVillageAID, avoidNatives);
      rmAddGroupingConstraint(comancheVillageAID, avoidTradeRoute);
      rmAddGroupingConstraint(comancheVillageAID, avoidAll);
	  rmAddGroupingConstraint(comancheVillageAID, avoidStartingUnits);
		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(comancheVillageAID, 0, 0.5, 0.5);
		}
		else
		{
			rmPlaceGroupingAtLoc(comancheVillageAID, 0, 0.35, 0.15);
		}
	}

   if (subCiv1 == rmGetCivID("Cheyenne"))
   {   
      int cheyenneVillageAID = -1;
      int cheyenneVillageType = rmRandInt(1,5);
      cheyenneVillageAID = rmCreateGrouping("cheyenne village A", "native cheyenne village "+cheyenneVillageType);
      rmSetGroupingMinDistance(cheyenneVillageAID, 0.0);
      rmSetGroupingMaxDistance(cheyenneVillageAID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(cheyenneVillageAID, avoidImpassableLand);
      rmAddGroupingToClass(cheyenneVillageAID, rmClassID("importantItem"));
      rmAddGroupingToClass(cheyenneVillageAID, rmClassID("natives"));
      rmAddGroupingConstraint(cheyenneVillageAID, avoidNatives);
      rmAddGroupingConstraint(cheyenneVillageAID, avoidTradeRoute);
      rmAddGroupingConstraint(cheyenneVillageAID, avoidAll);
	  rmAddGroupingConstraint(cheyenneVillageAID, avoidStartingUnits);
		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(cheyenneVillageAID, 0, 0.7, 0.6); 
		}
		else
		{
			rmPlaceGroupingAtLoc(cheyenneVillageAID, 0, 0.65, 0.15); 
		}
	}

   // Text
   rmSetStatusText("",0.50);
	if(subCiv2 == rmGetCivID("Cheyenne"))
   {   
      int cheyenneVillageID = -1;
      cheyenneVillageType = rmRandInt(1,5);
      cheyenneVillageID = rmCreateGrouping("cheyenne village", "native cheyenne village "+cheyenneVillageType);
      rmSetGroupingMinDistance(cheyenneVillageID, 0.0);
      rmSetGroupingMaxDistance(cheyenneVillageID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(cheyenneVillageID, avoidImpassableLand);
      rmAddGroupingToClass(cheyenneVillageID, rmClassID("importantItem"));
      rmAddGroupingToClass(cheyenneVillageID, rmClassID("natives"));
      rmAddGroupingConstraint(cheyenneVillageID, avoidNatives);
		rmAddGroupingConstraint(cheyenneVillageID, avoidTradeRoute);
		rmAddGroupingConstraint(cheyenneVillageID, avoidStartingUnits);
      rmAddGroupingConstraint(cheyenneVillageID, avoidAll);

		if ( extraPoles == 1 )
		{
			if ( whichMap == 1 )
			{
				rmPlaceGroupingAtLoc(cheyenneVillageID, 0, 0.2, 0.8);
			}
			else
			{
				rmPlaceGroupingAtLoc(cheyenneVillageID, 0, 0.15, 0.85);
			}
		}
   }

	if(subCiv3 == rmGetCivID("Comanche"))
   {   
      int comancheVillageBID = -1;
      comancheVillageType = rmRandInt(1,5);
      comancheVillageBID = rmCreateGrouping("comanche village B", "native comanche village "+comancheVillageType);
      rmSetGroupingMinDistance(comancheVillageBID, 0.0);
      rmSetGroupingMaxDistance(comancheVillageBID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(comancheVillageBID, avoidImpassableLand);
      rmAddGroupingToClass(comancheVillageBID, rmClassID("importantItem"));
      rmAddGroupingToClass(comancheVillageBID, rmClassID("natives"));
      rmAddGroupingConstraint(comancheVillageBID, avoidNatives);
	  rmAddGroupingConstraint(comancheVillageBID, avoidTradeRoute);
	  rmAddGroupingConstraint(comancheVillageBID, avoidStartingUnits);
      rmAddGroupingConstraint(comancheVillageBID, avoidAll);
		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(comancheVillageBID, 0, 0.3, 0.6);
		}
		else
		{
			rmPlaceGroupingAtLoc(comancheVillageBID, 0, 0.35, 0.45);
		}
   }

	if(subCiv4 == rmGetCivID("Comanche"))
   {
      int comancheVillageID = -1;
      comancheVillageType = rmRandInt(1,5);
      comancheVillageID = rmCreateGrouping("comanche village", "native comanche village "+comancheVillageType);
      rmSetGroupingMinDistance(comancheVillageID, 0.0);
	  rmSetGroupingMaxDistance(comancheVillageID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(comancheVillageID, avoidImpassableLand);
      rmAddGroupingToClass(comancheVillageID, rmClassID("importantItem"));
      rmAddGroupingToClass(comancheVillageID, rmClassID("natives"));
      rmAddGroupingConstraint(comancheVillageID, avoidNatives);
      rmAddGroupingConstraint(comancheVillageID, avoidTradeRoute);
	  rmAddGroupingConstraint(comancheVillageID, avoidStartingUnits);
      rmAddGroupingConstraint(comancheVillageID, avoidAll);
		if ( extraPoles == 1 )
		{
			if ( whichMap == 1 )
			{
				rmPlaceGroupingAtLoc(comancheVillageID, 0, 0.8, 0.8);
			}
			else
			{
				rmPlaceGroupingAtLoc(comancheVillageID, 0, 0.85, 0.85);
			}
		}
	
	}

   if (subCiv5 == rmGetCivID("Cheyenne"))
   {   
      int cheyenneVillageBID = -1;
      cheyenneVillageType = rmRandInt(1,5);
      cheyenneVillageBID = rmCreateGrouping("cheyenne village B", "native cheyenne village "+cheyenneVillageType);
      rmSetGroupingMinDistance(cheyenneVillageBID, 0.0);
      rmSetGroupingMaxDistance(cheyenneVillageBID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(cheyenneVillageBID, avoidImpassableLand);
      rmAddGroupingToClass(cheyenneVillageBID, rmClassID("importantItem"));
      rmAddGroupingToClass(cheyenneVillageBID, rmClassID("natives"));
      rmAddGroupingConstraint(cheyenneVillageBID, avoidNatives);
      rmAddGroupingConstraint(cheyenneVillageBID, avoidTradeRoute);
	  rmAddGroupingConstraint(cheyenneVillageBID, avoidStartingUnits);
      rmAddGroupingConstraint(cheyenneVillageBID, avoidAll);

		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(cheyenneVillageBID, 0, 0.5, 0.9);
		}
		else
		{
			rmPlaceGroupingAtLoc(cheyenneVillageBID, 0, 0.65, 0.45);
		}
   }

   // Text
   rmSetStatusText("",0.60);

   // Define and place Nuggets
  
	int nuggetID= rmCreateObjectDef("nugget"); 
	rmAddObjectDefItem(nuggetID, "Nugget", 1, 0.0);
	rmSetObjectDefMinDistance(nuggetID, 0.0);
	rmSetObjectDefMaxDistance(nuggetID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(nuggetID, shortAvoidImpassableLand);
  	rmAddObjectDefConstraint(nuggetID, avoidNugget);
	rmAddObjectDefConstraint(nuggetID, nuggetPlayerConstraint);
  	rmAddObjectDefConstraint(nuggetID, playerConstraint);
  	rmAddObjectDefConstraint(nuggetID, avoidTradeRoute);
	rmAddObjectDefConstraint(nuggetID, avoidSocketMore);
	rmAddObjectDefConstraint(nuggetID, avoidNativesNuggets);
	rmAddObjectDefConstraint(nuggetID, circleConstraint);
  	rmAddObjectDefConstraint(nuggetID, avoidAll);
	rmSetNuggetDifficulty(1, 3);
	rmPlaceObjectDefAtLoc(nuggetID, 0, 0.5, 0.5, NumberNonGaiaPlayers*4);
   
   // Text
   rmSetStatusText("",0.70); 

	// Ponds o' Fun
	int pondClass=rmDefineClass("pond");
   int pondConstraint=rmCreateClassDistanceConstraint("ponds avoid ponds", rmClassID("pond"), 60.0);

   // int numPonds=rmRandInt(1,4);
	int numPonds=4;
   for(i=0; <numPonds)
   {
      int smallPondID=rmCreateArea("small pond"+i);
      rmSetAreaSize(smallPondID, rmAreaTilesToFraction(170), rmAreaTilesToFraction(200));
      rmSetAreaWaterType(smallPondID, "great plains pond");
      rmSetAreaBaseHeight(smallPondID, 4);
      rmSetAreaMinBlobs(smallPondID, 1);
      rmSetAreaMaxBlobs(smallPondID, 5);
      rmSetAreaMinBlobDistance(smallPondID, 5.0);
      rmSetAreaMaxBlobDistance(smallPondID, 70.0);
      rmAddAreaToClass(smallPondID, pondClass);
      rmSetAreaCoherence(smallPondID, 0.5);
      rmSetAreaSmoothDistance(smallPondID, 5);
      rmAddAreaConstraint(smallPondID, pondConstraint);
      rmAddAreaConstraint(smallPondID, playerConstraint);
      rmAddAreaConstraint(smallPondID, avoidTradeRoute);
      rmAddAreaConstraint(smallPondID, avoidAll);
		rmAddAreaConstraint(smallPondID, avoidSocket);
		rmAddAreaConstraint(smallPondID, avoidImportantItem);
		rmAddAreaConstraint(smallPondID, avoidNW);
		rmAddAreaConstraint(smallPondID, avoidSE);
		rmAddAreaConstraint(smallPondID, avoidStartingUnits);
		rmAddAreaConstraint(smallPondID, avoidNuggetSmall);
      rmSetAreaWarnFailure(smallPondID, false);
      rmBuildArea(smallPondID);
   }

	// Build grassy areas everywhere.  Whee!
	numTries=6*NumberNonGaiaPlayers;
	failCount=0;
	for (i=0; <numTries)
	{   
		int grassyArea=rmCreateArea("grassyArea"+i);
		rmSetAreaWarnFailure(grassyArea, false);
		rmSetAreaSize(grassyArea, rmAreaTilesToFraction(1000), rmAreaTilesToFraction(1000));
		rmSetAreaForestType(grassyArea, "Great Plains grass");
		rmSetAreaForestDensity(grassyArea, 0.3);
		rmSetAreaForestClumpiness(grassyArea, 0.1);
		rmAddAreaConstraint(grassyArea, avoidHills);
		rmAddAreaConstraint(grassyArea, avoidTradeRoute);
		rmAddAreaConstraint(grassyArea, avoidSocket);
		rmAddAreaConstraint(grassyArea, avoidAll);
		rmAddAreaConstraint(grassyArea, avoidNatives);
		rmAddAreaConstraint(grassyArea, avoidStartingUnits);
		rmAddAreaConstraint(grassyArea, avoidNuggetSmall);
		if(rmBuildArea(grassyArea)==false)
		{
			// Stop trying once we fail 5 times in a row.
			failCount++;
			if(failCount==5)
				break;
		}
		else
			failCount=0; 
	}
	
	// Place resources
	// FAST COIN - 2 extra per player beyond starting resources and far mine.
	int silverID = -1;
	int silverWestID = -1;
	int silverCount = (NumberNonGaiaPlayers);
	rmEchoInfo("silver count = "+silverCount);

	for(i=0; < silverCount)
	{
		silverType = rmRandInt(1,10);
		silverID = rmCreateObjectDef("silver "+i);
		rmAddObjectDefItem(silverID, "mine", 1, 0.0);
		rmSetObjectDefMinDistance(silverID, 0.0);
		rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.35));
		rmAddObjectDefConstraint(silverID, avoidFastCoin);
		rmAddObjectDefConstraint(silverID, avoidAll);
		rmAddObjectDefConstraint(silverID, avoidImpassableLand);
		rmAddObjectDefConstraint(silverID, avoidTradeRoute);
		rmAddObjectDefConstraint(silverID, avoidSocket);
		rmAddObjectDefConstraint(silverID, coinForestConstraint);
		rmAddObjectDefConstraint(silverID, smallMapPlayerConstraint);
		rmAddObjectDefConstraint(silverID, avoidNuggetSmall);
		rmAddObjectDefConstraint(silverID, Eastward);
		int result = rmPlaceObjectDefAtLoc(silverID, 0, 0.75, 0.5);
		if(result == 0)
			break;
   }

	for(i=0; < silverCount)
	{
		silverType = rmRandInt(1,10);
		silverWestID = rmCreateObjectDef("silver west "+i);
		rmAddObjectDefItem(silverWestID, "mine", 1, 0.0);
		rmSetObjectDefMinDistance(silverWestID, 0.0);
		rmSetObjectDefMaxDistance(silverWestID, rmXFractionToMeters(0.35));
		rmAddObjectDefConstraint(silverWestID, avoidFastCoin);
		rmAddObjectDefConstraint(silverWestID, avoidAll);
		rmAddObjectDefConstraint(silverWestID, avoidImpassableLand);
		rmAddObjectDefConstraint(silverWestID, avoidTradeRoute);
		rmAddObjectDefConstraint(silverWestID, avoidSocket);
		rmAddObjectDefConstraint(silverWestID, coinForestConstraint);
		rmAddObjectDefConstraint(silverWestID, smallMapPlayerConstraint);
		rmAddObjectDefConstraint(silverWestID, avoidNuggetSmall);
		rmAddObjectDefConstraint(silverWestID, Westward);
		int result2 = rmPlaceObjectDefAtLoc(silverWestID, 0, 0.25, 0.5);
		if(result2 == 0)
			break;
   }

   // Text
   rmSetStatusText("",0.80);

	// bison	
   int bisonID=rmCreateObjectDef("bison herd per player");
   rmAddObjectDefItem(bisonID, "bison", rmRandInt(12,16), 12.0);
   rmSetObjectDefCreateHerd(bisonID, true);
   rmSetObjectDefMinDistance(bisonID, 40.0);
   rmSetObjectDefMaxDistance(bisonID, 45.0);
   rmAddObjectDefConstraint(bisonID, avoidBison);
   rmAddObjectDefConstraint(bisonID, avoidAll);
   rmAddObjectDefConstraint(bisonID, avoidImpassableLand);
   rmAddObjectDefConstraint(bisonID, avoidTradeRoute);
   rmAddObjectDefConstraint(bisonID, avoidSocket);
   rmAddObjectDefConstraint(bisonID, avoidStartingUnits);
   rmAddObjectDefConstraint(bisonID, avoidNuggetSmall);
   rmAddObjectDefConstraint(bisonID, nuggetPlayerConstraint);
   rmPlaceObjectDefPerPlayer(bisonID, false, 1);

   int bisonNum = rmRandInt(12,16);
   bisonNum+=5;
   int bisonEID=rmCreateObjectDef("bison herds east");
   rmAddObjectDefItem(bisonEID, "bison", bisonNum, 12.0);
   rmSetObjectDefCreateHerd(bisonEID, true);
   rmSetObjectDefMinDistance(bisonEID, 0.0);
   rmSetObjectDefMaxDistance(bisonEID, rmXFractionToMeters(0.35));
   rmAddObjectDefConstraint(bisonEID, avoidBison);
   rmAddObjectDefConstraint(bisonEID, avoidAll);
   rmAddObjectDefConstraint(bisonEID, avoidImpassableLand);
   rmAddObjectDefConstraint(bisonEID, avoidTradeRoute);
   rmAddObjectDefConstraint(bisonEID, avoidSocket);
   rmAddObjectDefConstraint(bisonEID, avoidStartingUnits);
   rmAddObjectDefConstraint(bisonEID, avoidNuggetSmall);
   rmAddObjectDefConstraint(bisonEID, smallMapPlayerConstraint);
   rmAddObjectDefConstraint(bisonEID, Eastward);
   rmPlaceObjectDefAtLoc(bisonEID, 0, 0.75, 0.5, NumberNonGaiaPlayers);

   int bisonWID=rmCreateObjectDef("bison herds west");
   rmAddObjectDefItem(bisonWID, "bison", bisonNum, 12.0);
   rmSetObjectDefCreateHerd(bisonWID, true);
   rmSetObjectDefMinDistance(bisonWID, 0.0);
   rmSetObjectDefMaxDistance(bisonWID, rmXFractionToMeters(0.35));
   rmAddObjectDefConstraint(bisonWID, avoidBison);
   rmAddObjectDefConstraint(bisonWID, avoidAll);
   rmAddObjectDefConstraint(bisonWID, avoidImpassableLand);
   rmAddObjectDefConstraint(bisonWID, avoidTradeRoute);
   rmAddObjectDefConstraint(bisonWID, avoidSocket);
   rmAddObjectDefConstraint(bisonWID, avoidStartingUnits);
   rmAddObjectDefConstraint(bisonWID, avoidNuggetSmall);
   rmAddObjectDefConstraint(bisonWID, smallMapPlayerConstraint);
   rmAddObjectDefConstraint(bisonWID, Westward);
   rmPlaceObjectDefAtLoc(bisonWID, 0, 0.25, 0.5, NumberNonGaiaPlayers);

	// pronghorn	
   int pronghornID=rmCreateObjectDef("pronghorn herd");
   rmAddObjectDefItem(pronghornID, "pronghorn", rmRandInt(6,9), 10.0);
   rmSetObjectDefCreateHerd(pronghornID, true);
   rmSetObjectDefMinDistance(pronghornID, 0.0);
   rmSetObjectDefMaxDistance(pronghornID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(pronghornID, avoidBison);
   rmAddObjectDefConstraint(pronghornID, avoidPronghorn);
   rmAddObjectDefConstraint(pronghornID, avoidAll);
   rmAddObjectDefConstraint(pronghornID, avoidImpassableLand);
   rmAddObjectDefConstraint(pronghornID, avoidTradeRoute);
   rmAddObjectDefConstraint(pronghornID, avoidSocket);
   rmAddObjectDefConstraint(pronghornID, avoidStartingUnits);
   rmAddObjectDefConstraint(pronghornID, avoidNuggetSmall);
   rmAddObjectDefConstraint(pronghornID, smallMapPlayerConstraint);
   rmPlaceObjectDefAtLoc(pronghornID, 0, 0.5, 0.5, NumberNonGaiaPlayers*2);

	// Text
   rmSetStatusText("",0.90);

   int seConstraint = rmCreateAreaConstraint("se", hillSoutheastID);
   int heightConstraint = rmCreateMaxHeightConstraint("tree height", 6.0);
   int lowForestClass = rmDefineClass("low forest");
   int lowForestConstraint = rmCreateClassDistanceConstraint("low forest", lowForestClass, 4.0);

   // RANDOM TREES
   int count = 0;
   int maxCount = 20 * NumberNonGaiaPlayers;
   int maxFailCount = 10 * NumberNonGaiaPlayers;
   failCount = 0;
   for(i=1; <10)
   {
		int treeArea = rmCreateArea("se tree"+i);
		rmSetAreaSize(treeArea, 0.001, 0.003);
		rmSetAreaForestType(treeArea, "great plains forest");
		rmSetAreaForestDensity(treeArea, 0.8);
		rmSetAreaForestClumpiness(treeArea, 0.1);
		rmAddAreaConstraint(treeArea, seConstraint);
		rmAddAreaConstraint(treeArea, lowForestConstraint);
		rmAddAreaConstraint(treeArea, heightConstraint);
		rmAddAreaConstraint(treeArea, avoidImpassableLand);
		rmAddAreaConstraint(treeArea, avoidTradeRoute);
		rmAddAreaConstraint(treeArea, avoidSocket);
		rmAddAreaConstraint(treeArea, avoidStartingUnits);
		rmAddAreaConstraint(treeArea, avoidAll);
		rmAddAreaToClass(treeArea, lowForestClass);
		rmAddAreaConstraint(treeArea, avoidNuggetSmall);
		rmSetAreaWarnFailure(treeArea, false);
		bool ok = rmBuildArea(treeArea);
		if(ok)
		{
			count++;
			if(count > maxCount)
			break;
		}
		else
		{
			failCount++;
			if(failCount > maxFailCount)
			break;
		}
   }

   int nwConstraint = rmCreateAreaConstraint("nw", hillNorthwestID);
   int maxAreas = 5 * NumberNonGaiaPlayers;
   int maxFails = 5 * NumberNonGaiaPlayers;
   count = 0;
   failCount = 0;
   for(i=1; <10)
   {
      treeArea = rmCreateArea("nw tree"+i);
      rmSetAreaSize(treeArea, 0.001, 0.003);
	   rmSetAreaForestType(treeArea, "great plains forest");
	   rmSetAreaForestDensity(treeArea, 0.8);
	   rmSetAreaForestClumpiness(treeArea, 0.1);
	   rmAddAreaConstraint(treeArea, nwConstraint);
	   rmAddAreaConstraint(treeArea, lowForestConstraint);
	   rmAddAreaConstraint(treeArea, heightConstraint);
		rmAddAreaConstraint(treeArea, avoidImpassableLand);
		rmAddAreaConstraint(treeArea, avoidTradeRoute);
		rmAddAreaConstraint(treeArea, avoidSocket);
		rmAddAreaConstraint(treeArea, avoidStartingUnits);
		rmAddAreaConstraint(treeArea, avoidAll);
		rmAddAreaConstraint(treeArea, avoidNuggetSmall);
	   rmAddAreaToClass(treeArea, lowForestClass);
	   rmSetAreaWarnFailure(treeArea, false);
      ok = rmBuildArea(treeArea);
      if(ok)
      {
         count++;
         if(count > maxCount)
            break;
      }
      else
      {
         failCount++;
         if(failCount > maxFailCount)
            break;
      }
   }

   // Mini-forests out on the plains (mostly)
   int forestTreeID = 0;
   numTries=5*NumberNonGaiaPlayers;
   failCount=0;
   for (i=0; <numTries)
   {   
		int forest=rmCreateArea("center forest "+i);
		rmSetAreaWarnFailure(forest, false);
		rmSetAreaSize(forest, rmAreaTilesToFraction(100), rmAreaTilesToFraction(100));
		rmSetAreaForestType(forest, "great plains forest");
		rmSetAreaForestDensity(forest, 0.9);
		rmSetAreaForestClumpiness(forest, 0.8);
		rmSetAreaForestUnderbrush(forest, 0.0);
		rmSetAreaCoherence(forest, 0.5);
		// rmSetAreaSmoothDistance(forest, 10);
		rmAddAreaToClass(forest, rmClassID("classForest")); 
		// rmAddAreaConstraint(forest, lowForestConstraint);
		rmAddAreaConstraint(forest, forestConstraint);
		rmAddAreaConstraint(forest, playerConstraint);
		rmAddAreaConstraint(forest, avoidImportantItem);
		rmAddAreaConstraint(forest, avoidImpassableLand); 
		rmAddAreaConstraint(forest, avoidTradeRoute);
		rmAddAreaConstraint(forest, avoidSocket);
		rmAddAreaConstraint(forest, avoidAll);
		rmAddAreaConstraint(forest, avoidHills);
		rmAddAreaConstraint(forest, avoidNuggetSmall);
		// rmAddAreaConstraint(forest, avoidFastCoin);
		rmAddAreaConstraint(forest, avoidStartingUnits);
		if(rmBuildArea(forest)==false)
		{
			// Stop trying once we fail 10 times in a row.
			failCount++;
			if(failCount==10)
			break;
		}
		else
			failCount=0; 
   }
   
  // check for KOTH game mode
  if(rmGetIsKOTH()) {
    
    int randLoc = rmRandInt(1,3);
    float xLoc = 0.5;
    float yLoc = 0.5;
    float walk = 0.075;
    
    ypKingsHillPlacer(xLoc, yLoc, walk, 0);
    rmEchoInfo("XLOC = "+xLoc);
    rmEchoInfo("XLOC = "+yLoc);
  }

	// 50% chance of buffalo carcasses - either one or two if they're there.
	int areThereBuffalo=rmRandInt(1, 2);
	int howManyBuffalo=rmRandInt(1, 2);
	if ( areThereBuffalo == 1 )
	{
		int bisonCarcass=rmCreateGrouping("Bison Carcass", "gp_carcass_bison");
		rmSetGroupingMinDistance(bisonCarcass, 0.0);
		rmSetGroupingMaxDistance(bisonCarcass, rmXFractionToMeters(0.5));
		rmAddGroupingConstraint(bisonCarcass, avoidNW);
		rmAddGroupingConstraint(bisonCarcass, avoidSE);
		rmAddGroupingConstraint(bisonCarcass, avoidImpassableLand);
		rmAddGroupingConstraint(bisonCarcass, playerConstraint);
		rmAddGroupingConstraint(bisonCarcass, avoidTradeRoute);
		rmAddGroupingConstraint(bisonCarcass, avoidSocket);
		rmAddGroupingConstraint(bisonCarcass, avoidStartingUnits);
		rmAddGroupingConstraint(bisonCarcass, avoidAll);
		rmAddGroupingConstraint(bisonCarcass, avoidNuggetSmall);
		rmPlaceGroupingAtLoc(bisonCarcass, 0, 0.5, 0.5, howManyBuffalo);
	}

	// Perching Vultures - add a couple somewhere.
	int vultureID=rmCreateObjectDef("perching vultures");
	rmAddObjectDefItem(vultureID, "PropVulturePerching", 1, 0.0);
	rmSetObjectDefMinDistance(vultureID, 0.0);
	rmSetObjectDefMaxDistance(vultureID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(vultureID, avoidNW);
	rmAddObjectDefConstraint(vultureID, avoidSE);
	rmAddObjectDefConstraint(vultureID, avoidBison);
	rmAddObjectDefConstraint(vultureID, avoidAll);
	rmAddObjectDefConstraint(vultureID, avoidNuggetSmall);
	rmAddObjectDefConstraint(vultureID, avoidImpassableLand);
	rmAddObjectDefConstraint(vultureID, avoidTradeRoute);
	rmAddObjectDefConstraint(vultureID, avoidSocket);
	rmAddObjectDefConstraint(vultureID, avoidStartingUnits);
	rmAddObjectDefConstraint(vultureID, circleConstraint);
	rmAddObjectDefConstraint(vultureID, playerConstraint);
	rmPlaceObjectDefAtLoc(vultureID, 0, 0.5, 0.5, 2);


	int grassPatchGroupType=-1;
	int grassPatchGroup=-1;
	// Two grass patches per player.

	for(i=1; <2*NumberNonGaiaPlayers)
   {
		grassPatchGroupType=rmRandInt(1, 7);
		grassPatchGroup=rmCreateGrouping("Grass Patch Group"+i, "gp_grasspatch0"+grassPatchGroupType);
		rmSetGroupingMinDistance(grassPatchGroup, 0.0);
		rmSetGroupingMaxDistance(grassPatchGroup, rmXFractionToMeters(0.5));
		rmAddGroupingConstraint(grassPatchGroup, avoidNW);
		rmAddGroupingConstraint(grassPatchGroup, avoidSE);
		rmAddGroupingConstraint(grassPatchGroup, avoidImpassableLand);
		rmAddGroupingConstraint(grassPatchGroup, playerConstraint);
		rmAddGroupingConstraint(grassPatchGroup, avoidTradeRoute);
		rmAddGroupingConstraint(grassPatchGroup, avoidSocket);
		rmAddGroupingConstraint(grassPatchGroup, avoidNuggetSmall);
		rmAddGroupingConstraint(grassPatchGroup, circleConstraint);
		rmAddGroupingConstraint(grassPatchGroup, avoidAll);
		rmPlaceGroupingAtLoc(grassPatchGroup, 0, 0.5, 0.5, 1);
	}

	int flowerPatchGroupType=-1;
	int flowerPatchGroup=-1;

	// Also two "flowers" per player.
	for(i=1; <2*NumberNonGaiaPlayers)
   {
		flowerPatchGroupType=rmRandInt(1, 8);
		flowerPatchGroup=rmCreateGrouping("Flower Patch Group"+i, "gp_flower0"+flowerPatchGroupType);
		rmSetGroupingMinDistance(flowerPatchGroup, 0.0);
		rmSetGroupingMaxDistance(flowerPatchGroup, rmXFractionToMeters(0.5));
		rmAddGroupingConstraint(flowerPatchGroup, avoidNW);
		rmAddGroupingConstraint(flowerPatchGroup, avoidSE);
		rmAddGroupingConstraint(flowerPatchGroup, avoidImpassableLand);
		rmAddGroupingConstraint(flowerPatchGroup, playerConstraint);
		rmAddGroupingConstraint(flowerPatchGroup, avoidTradeRoute);
		rmAddGroupingConstraint(flowerPatchGroup, avoidSocket);
		rmAddGroupingConstraint(flowerPatchGroup, avoidNuggetSmall);
		rmAddGroupingConstraint(flowerPatchGroup, avoidAll);
		rmAddGroupingConstraint(flowerPatchGroup, circleConstraint);
		rmPlaceGroupingAtLoc(flowerPatchGroup, 0, 0.5, 0.5, 1);
	}

   	// And a couple of geysers.
	int geyserID=rmCreateGrouping("Geysers", "prop_geyser");
	rmSetGroupingMinDistance(geyserID, 0.0);
	rmSetGroupingMaxDistance(geyserID, rmXFractionToMeters(0.5));
	rmAddGroupingConstraint(geyserID, avoidNW);
	rmAddGroupingConstraint(geyserID, avoidSE);
	rmAddGroupingConstraint(geyserID, avoidBison);
	rmAddGroupingConstraint(geyserID, avoidAll);
	rmAddGroupingConstraint(geyserID, avoidImpassableLand);
	rmAddGroupingConstraint(geyserID, avoidTradeRoute);
	rmAddGroupingConstraint(geyserID, avoidSocket);
	rmAddGroupingConstraint(geyserID, avoidStartingUnits);
	rmAddGroupingConstraint(geyserID, avoidNuggetSmall);
	rmAddGroupingConstraint(geyserID, playerConstraint);
	rmAddGroupingConstraint(geyserID, circleConstraint);
	rmPlaceGroupingAtLoc(geyserID, 0, 0.5, 0.5, 2);

/*[Special features start]*/

if(rmGetPlayerCiv(1)>=15 && rmGetPlayerCiv(1)<=17){ArtStyleP1="Nat";}
else if(rmGetPlayerCiv(1)==19){ArtStyleP1="Japan";}
else if(rmGetPlayerCiv(1)==20){ArtStyleP1="China";}
else if(rmGetPlayerCiv(1)==21){ArtStyleP1="India";}
if(rmGetPlayerCiv(2)>=15 && rmGetPlayerCiv(2)<=17){ArtStyleP2="Nat";}
else if(rmGetPlayerCiv(2)==19){ArtStyleP2="Japan";}
else if(rmGetPlayerCiv(2)==20){ArtStyleP2="China";}
else if(rmGetPlayerCiv(2)==21){ArtStyleP2="India";}

	rmCreateTrigger("Load");
	rmSwitchToTrigger(rmTriggerID("Load"));
	cEf("}} int FP=-1; int cjaso=1; /*");
	cEf("*/ int P1StartXP=0; int P2StartXP=0; int autoupdates=0; /*");
	cEf("*/ int timeS1=0; int timeS2=0; int periodS1=1; int periodS2=4; /*");
	cEf("*/ int P1StartAge=0; int P2StartAge=0; int shouldinitiate=1; /*");
	cEf("*/ int cherryorickshaw1=0; int cherryorickshaw2=0; /*");
	cEf("*/ int autoscroll=1; int arghpdwhy=0; /*");
//Autoscroll will occur if left scroll has not been pressed in 5 seconds, and there is shipment or tech addition on right.
//(switch from 1 to -4 when scrolling to left and then add 1 if not equal to 1 in 1 second timer)
	cEf("*/ int cooldown1=0; int cooldown2=0; /*");
//Note: Cooldowns indicate time since last shipment discrepancy.
	cEf("*/ int configuremode=0; /*");
//As it implies. 0=False, 1=True

//By default don't show advanced stats
	cEf("*/ int showadvstats=-1; int tempshowadvstats=-1; /*");
//But show the toolbar
	cEf("*/ int showbartools=1; int tempshowbartools=1; int showspecialcase=-1; /*");

//Aizamk's (my) presets :D

	cEf("*/ int INC_POP_PROJ_1=29; /*");
	cEf("*/ int INC_POP_SPCE_1=2; /*");
	cEf("*/ int INC_AGE_PROG_1=54; /*");
	cEf("*/ int INC_AGE_PROG_2=160; /*");
	cEf("*/ int INC_POP_PROJ_2=49; /*");
	cEf("*/ int INC_POP_SPCE_2=2; /*");
	cEf("*/ int INC_SHP_PROG_1=3; /*");
	cEf("*/ int INC_SHP_NUMR_1=14; /*");
	cEf("*/ int INC_RES_FOOD_1=30; /*");
	cEf("*/ int INC_RES_WOOD_1=22; /*");
	cEf("*/ int INC_RES_GOLD_1=22; /*");
	cEf("*/ int INC_RES_TRDE_1=22; /*");
	cEf("*/ int INC_RES_FOOD_2=94; /*");
	cEf("*/ int INC_RES_WOOD_2=22; /*");
	cEf("*/ int INC_RES_GOLD_2=22; /*");
	cEf("*/ int INC_RES_TRDE_2=22; /*");
	cEf("*/ int INC_SHP_PROG_2=32; /*");
	cEf("*/ int INC_SHP_NUMR_2=15; /*");

	cEf("*/ int INC_STT_CONF_1=150; /*"); //Note this is now a constant.
	cEf("*/ int INC_STT_CONF_2=7; /*");
	cEf("*/ int INC_STT_CONF_3=13; /*");
	cEf("*/ int INC_STT_CONF_4=20; /*");
	cEf("*/ int INC_STT_CONF_5=14; /*");
	cEf("*/ int INC_STT_CONF_6=15; /*");
	cEf("*/ int INC_STT_CONF_7=14; /*");
	cEf("*/ int INC_STT_CONF_8=14; /*");

	cEf("*/ int ROW_STT_1=2; /*"); //Row number for first line of stats
	cEf("*/ int ROW_STT_2=6; /*"); //Row number for second line of stats

//	cEf("*/ int INC_RO1_SLOT_0=141; /*");
//	cEf("*/ int INC_RO1_SLOT_1=155; /*");
//	cEf("*/ int INC_RO1_SLOT_2=169; /*");
//	cEf("*/ int INC_RO1_SLOT_3=183; /*");
//	cEf("*/ int INC_RO1_SLOT_4=197; /*");

//	cEf("*/ int INC_RO2_SLOT_0=541; /*");
//	cEf("*/ int INC_RO2_SLOT_1=555; /*");
//	cEf("*/ int INC_RO2_SLOT_2=569; /*");
//	cEf("*/ int INC_RO2_SLOT_3=583; /*");
//	cEf("*/ int INC_RO2_SLOT_4=597; /*");

//For tracking progress of animated ageupbar.
	cEf("*/ int AgeUpBarProgress1=0; int AgeUpBarProgress2=0; /*");

//For resource display type.
	cEf("*/ int ResDispType=1; /*");

//Cost of Units Lost - Constants.
	cEf("*/ int DeadCostFOOD_1=0; int DeadCostWOOD_1=0; int DeadCostGOLD_1=0; int DeadCostTRDE_1=0; /*");
	cEf("*/ int DeadCostFOOD_2=0; int DeadCostWOOD_2=0; int DeadCostGOLD_2=0; int DeadCostTRDE_2=0; /*");

//Special stuff for lighting.
	cEf("*/ string DefaultLightingSet=\""+DefaultLightingSet+"\"; /*");

//To enable scrolling in bottom-right UI
	cEf("*/ int startindex1=0; /*");
	cEf("*/ int startindex2=0; /*");

//Economic, Military Population
	cEf("*/ int POP_ECON_1=0; /*");
	cEf("*/ int POP_MILT_1=0; /*");

	cEf("*/ int POP_ECON_2=0; /*");
	cEf("*/ int POP_MILT_2=0; /*");

	cEf("*/ int showdead=-1; /*");
	cEf("*/ int renderfog=-1; /*");
	cEf("*/ int needtohidemenu=1; /*");

	cEf("*/ int aexpb(int number=0, int exponent=0) { /*");
	cEf("*/ int result=1; /*");
	cEf("*/ for(i=1; <=exponent){result=result*number;} /*");
	cEf("*/ return(result); /*");
	cEf("*/ } /*");

	cEf("*/ void updatepopulation(int wtv=-1) { /*");
	cEf("*/ POP_ECON_1=0; POP_MILT_1=0; /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"Settler\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+2*trPlayerUnitCountSpecific(1,\"SettlerWagon\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"Coureur\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"CoureurCree\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"SettlerNative\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"ypSettlerAsian\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"ypSettlerJapanese\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"ypSettlerIndian\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"FishingBoat\"); /*");
	cEf("*/ POP_ECON_1=POP_ECON_1+trPlayerUnitCountSpecific(1,\"ypFishingBoatAsian\"); /*");
	cEf("*/ POP_MILT_1=trPlayerGetPopulation(1)-POP_ECON_1; /*");
	cEf("*/ POP_ECON_2=0; POP_MILT_2=0; /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"Settler\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+2*trPlayerUnitCountSpecific(2,\"SettlerWagon\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"Coureur\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"CoureurCree\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"SettlerNative\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"ypSettlerAsian\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"ypSettlerJapanese\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"ypSettlerIndian\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"FishingBoat\"); /*");
	cEf("*/ POP_ECON_2=POP_ECON_2+trPlayerUnitCountSpecific(2,\"ypFishingBoatAsian\"); /*");
	cEf("*/ POP_MILT_2=trPlayerGetPopulation(2)-POP_ECON_2; /*");
	cEf("*/ } /*");

//CONFIGURATOR 9001 STUFF!
	cEf("*/ int CFG_CURRENT=0; /*");

	cEf("*/ void CFG_CHECKS(int number=1) { /*");
	cEf("*/ 		if(CFG_CURRENT!=number && number>0){ /*");
for(k=1; <=26){
	cEf("*/ 		if(CFG_CURRENT=="+k+"){gadgetUnreal(\"ObzConfig-"+k+"-c\");} /*");
	cEf("*/ 		if(number=="+k+"){gadgetReal(\"ObzConfig-"+k+"-c\");} /*");
}
	cEf("*/ 		CFG_CURRENT=number; /*");
	cEf("*/ 		} /*");
	cEf("*/ } /*");

for(k=1; <=26){
	cEf("*/ void setconfig"+k+"(int wtv=-1) { /*");
	cEf("*/ CFG_CHECKS("+k+"); /*");
	cEf("*/ } /*");
}

	cEf("*/ void keymap(int wtv=-1) { /*");
for(k=1; <=26){
	cEf("*/ trEventSetHandler("+k+"1337,\"setconfig"+k+"\"); /*");
}
	cEf("*/ trEventSetHandler(20000,\"toggleadv\"); /*");
	cEf("*/ trEventSetHandler(49999,\"modconfig9\"); /*");
	cEf("*/ trEventSetHandler(50001,\"modconfig11\"); /*");
	cEf("*/ trEventSetHandler(49995,\"modconfig5\"); /*");
	cEf("*/ trEventSetHandler(50005,\"modconfig15\"); /*");
	cEf("*/ trEventSetHandler(49990,\"modconfig0\"); /*");
	cEf("*/ trEventSetHandler(50010,\"modconfig20\"); /*");
	cEf("*/ trEventSetHandler(50000,\"spewconfig\"); /*");
	cEf("*/ trEventSetHandler(12341,\"p1shiftright\"); /*");
	cEf("*/ trEventSetHandler(43211,\"p1shiftleft\"); /*");
	cEf("*/ trEventSetHandler(12342,\"p2shiftright\"); /*");
	cEf("*/ trEventSetHandler(43212,\"p2shiftleft\"); /*");
	cEf("*/ trEventSetHandler(50505,\"debugtecharrays\"); /*");
	cEf("*/ trEventSetHandler(53312,\"showToolbars\"); /*");
	cEf("*/ trEventSetHandler(53321,\"hideToolbars\"); /*");
for(j=0; <10){
	cEf("*/ trEventSetHandler(7000"+j+",\"setdisplaytype"+j+"\"); /*");
}
for(p=1;<=2){
for(s=0;<5){
	cEf("*/ trEventSetHandler(60"+s+"0"+p+",\"FINC_"+p+"_"+s+"\"); /*");
}
}


	cEf("*/ trEventSetHandler(44444,\"toggledeath\"); /*"); //THE NUMBER 4!
	cEf("*/ trEventSetHandler(99500,\"togglefog\"); /*"); //From dark to light!

	cEf("*/ trEventSetHandler(10001,\"res_1280x800\"); /*");
	cEf("*/ trEventSetHandler(10002,\"res_1920x1080\"); /*");
	cEf("*/ trEventSetHandler(10003,\"res_1366x768\"); /*");
	cEf("*/ trEventSetHandler(10004,\"res_1024x768\"); /*");
	cEf("*/ trEventSetHandler(10005,\"res_1440x1080\"); /*");
	cEf("*/ trEventSetHandler(10006,\"res_1280x600\"); /*");

	cEf("*/ trEventSetHandler(30003,\"AUTupd0\"); /*");
	cEf("*/ trEventSetHandler(30103,\"AUTupd1\"); /*");
	cEf("*/ trEventSetHandler(30203,\"AUTupd2\"); /*");
	cEf("*/ trEventSetHandler(30303,\"AUTupd3\"); /*");

	cEf("*/ trEventSetHandler(70107,\"LTDefault\"); /*");
	cEf("*/ trEventSetHandler(70207,\"LTSunset\"); /*");
	cEf("*/ trEventSetHandler(70307,\"LTNightlight\"); /*");
	cEf("*/ trEventSetHandler(70407,\"LTNight\"); /*");
	cEf("*/ trEventSetHandler(70507,\"LTSnow\"); /*");

	cEf("*/ trEventSetHandler(80108,\"letitclear\"); /*");
	cEf("*/ trEventSetHandler(80208,\"letitrain\"); /*");
	cEf("*/ trEventSetHandler(80308,\"letitsnow\"); /*");

	cEf("*/ trEventSetHandler(91678,\"softupdate\"); /*");
	cEf("*/ trEventSetHandler(91379,\"hardupdate\"); /*");

	cEf("*/ trEventSetHandler(90109,\"setrescur\"); /*");
	cEf("*/ trEventSetHandler(90209,\"setrestot\"); /*");
	cEf("*/ trEventSetHandler(90309,\"setrestech\"); /*");
	cEf("*/ trEventSetHandler(90409,\"setresdead\"); /*");

	cEf("*/ trEventSetHandler(88888,\"toggleconfiguremode\"); /*");

	cEf("*/ map(\"F1\",\"game\", \"gadgetToggle("+"\\"+"ObzConfig"+"\\"+")\"); /*");

	cEf("*/ } /*");

//Lighting and Weather
	cEf("*/ void LTDefault(int wtv=-1) { /*");
	cEf("*/ trSetLighting(\""+DefaultLightingSet+"\",5); /*");
	cEf("*/ } /*");
	cEf("*/ void LTSunset(int wtv=-1) { /*");
	cEf("*/ trSetLighting(\"london sunset\",5); /*");
	cEf("*/ } /*");
	cEf("*/ void LTNightlight(int wtv=-1) { /*");
	cEf("*/ trSetLighting(\"st_petersburg_night\",5); /*");
	cEf("*/ } /*");
	cEf("*/ void LTNight(int wtv=-1) { /*");
	cEf("*/ trSetLighting(\"spc14anight\",5); /*");
	cEf("*/ } /*");
	cEf("*/ void LTSnow(int wtv=-1) { /*");
	cEf("*/ trSetLighting(\"319a_snow\",5); /*");
	cEf("*/ } /*");
	cEf("*/ void letitclear(int wtv=-1) { /*");
	cEf("*/ trRenderSnow(0); trRenderRain(0); /*");
	cEf("*/ } /*");
	cEf("*/ void letitrain(int wtv=-1) { /*");
	cEf("*/ trRenderRain(100); /*");
	cEf("*/ } /*");
	cEf("*/ void letitsnow(int wtv=-1) { /*");
	cEf("*/ trRenderSnow(100); /*");
	cEf("*/ } /*");

	cEf("*/ void spewconfig(int wtv=-1) { /*");
	cEf("*/ string spew=\"obsUI-\"; /*");
	cEf("*/ spew=spew+INC_POP_PROJ_1+\",\"; /*");
	cEf("*/ spew=spew+INC_POP_SPCE_1+\",\"; /*");
	cEf("*/ spew=spew+INC_AGE_PROG_1+\",\"; /*");
	cEf("*/ spew=spew+INC_AGE_PROG_2+\",\"; /*");
	cEf("*/ spew=spew+INC_POP_PROJ_2+\",\"; /*");
	cEf("*/ spew=spew+INC_POP_SPCE_2+\",\"; /*");
	cEf("*/ spew=spew+INC_SHP_PROG_1+\",\"; /*");
	cEf("*/ spew=spew+INC_SHP_NUMR_1+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_FOOD_1+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_WOOD_1+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_GOLD_1+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_TRDE_1+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_FOOD_2+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_WOOD_2+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_GOLD_2+\",\"; /*");
	cEf("*/ spew=spew+INC_RES_TRDE_2+\",\"; /*");
	cEf("*/ spew=spew+INC_SHP_PROG_2+\",\"; /*");
	cEf("*/ spew=spew+INC_SHP_NUMR_2+\",\"; /*");
	cEf("*/ spew=spew+ROW_STT_2+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_2+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_3+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_4+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_5+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_6+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_7+\",\"; /*");
	cEf("*/ spew=spew+INC_STT_CONF_8; /*");
	cEf("*/ trChatSendToPlayer(0, FP, spew); saveCamera(\"\"+spew);} /*");

//FOR ADVANCED INFO UI

//0=Economic, 1=Military, 2=Naval, 3=Queue (every single unit) 4=Technologies 5=Shipments
	cEf("*/ int infodisptype=0; /*");


	cEf("*/ string extrainfo=\"\"; /*");

	cEf("*/ string statsid_P1_0=\"\"; /*");
	cEf("*/ string statsid_P1_1=\"\"; /*");
	cEf("*/ string statsid_P1_2=\"\"; /*");
	cEf("*/ string statsid_P1_3=\"\"; /*");
	cEf("*/ string statsid_P1_4=\"\"; /*");
	cEf("*/ string statsid_P2_0=\"\"; /*");
	cEf("*/ string statsid_P2_1=\"\"; /*");
	cEf("*/ string statsid_P2_2=\"\"; /*");
	cEf("*/ string statsid_P2_3=\"\"; /*");
	cEf("*/ string statsid_P2_4=\"\"; /*");

	cEf("*/ void INFO_CHECKS(int disp=1, int slot=0, string old=\"\", string new=\"\") { /*");
	cEf("*/ if(old!=new){ /*");
	cEf("*/ gadgetUnreal(\"ObzAdvStats-P\"+disp+\"-Slot\"+slot+\"-\"+old+\"\"); /*");
	cEf("*/ gadgetReal(\"ObzAdvStats-P\"+disp+\"-Slot\"+slot+\"-\"+new+\"\"); /*");
	cEf("*/ } /*");
	cEf("*/ } /*");

	cEf("*/ void addstrtoarray(int i=-1, string add=\"cowhax\") { /*");
	cEf("*/ int arrayindex=0; /*");
	cEf("*/ bool included=false; /*");
	cEf("*/ 		while(xsArrayGetString(i,arrayindex)!=\"\"){ /*");
	cEf("*/ 			if(xsArrayGetString(i,arrayindex)==add){included=true; break;} /*");
	cEf("*/ 		arrayindex=arrayindex+1; /*");
	cEf("*/ 		if(arrayindex==xsArrayGetSize(i)){break;} /*");
	cEf("*/ 		} /*");
	cEf("*/ if(included==false){xsArraySetString(i,arrayindex,add);} /*");
	cEf("*/ } /*");

	cEf("*/ void arrayaddint(int i=-1, int add=-1) { /*");
	cEf("*/ for(arrayindex=0; <xsArrayGetSize(i)){ /*");
	cEf("*/ if(xsArrayGetInt(i,arrayindex)==-1){xsArraySetInt(i,arrayindex,add); break;} /*");
	cEf("*/ } /*");
	cEf("*/ } /*");

//ADDED 1.6 NEW ARRAY FUNCTIONS
	cEf("*/ bool strarrayincludes(int i=-1, string check=\"cowhax\") { /*");
	cEf("*/ int arrayindex=0; /*");
	cEf("*/ bool included=false; /*");
	cEf("*/ 		while(xsArrayGetString(i,arrayindex)!=\"\"){ /*");
	cEf("*/ 			if(xsArrayGetString(i,arrayindex)==check){included=true; break;} /*");
	cEf("*/ 		arrayindex=arrayindex+1; /*");
	cEf("*/ 		if(arrayindex==xsArrayGetSize(i)){break;} /*");
	cEf("*/ 		} /*");
	cEf("*/ return(included); /*");
	cEf("*/ } /*");

	cEf("*/ void forceaddtostrarray(int i=-1, string add=\"cowhax\") { /*");
	cEf("*/ int arrayindex=0; /*");
	cEf("*/ 		while(xsArrayGetString(i,arrayindex)!=\"\"){ /*");
	cEf("*/ 		arrayindex=arrayindex+1; /*");
	cEf("*/ 		if(arrayindex==xsArrayGetSize(i)){break;} /*");
	cEf("*/ 		} /*");
	cEf("*/ xsArraySetString(i,arrayindex,add); /*");
	cEf("*/ } /*");

	cEf("*/ int intarraymults(int i=-1, int check=1337) { /*");
	cEf("*/ int arrayindex=0; int counts=0; /*");
	cEf("*/ bool included=false; /*");
	cEf("*/ 		while(xsArrayGetInt(i,arrayindex)!=-1){ /*");
	cEf("*/ 			if(xsArrayGetInt(i,arrayindex)==check){counts++;} /*");
	cEf("*/ 		arrayindex=arrayindex+1; /*");
	cEf("*/ 		if(arrayindex==xsArrayGetSize(i)){break;} /*");
	cEf("*/ 		} /*");
	cEf("*/ return(counts); /*");
	cEf("*/ } /*");

	cEf("*/ void forceaddtointarray(int i=-1, int add=1337) { /*");
	cEf("*/ int arrayindex=0; /*");
	cEf("*/ 		while(xsArrayGetInt(i,arrayindex)!=-1){ /*");
	cEf("*/ 		arrayindex=arrayindex+1; /*");
	cEf("*/ 		if(arrayindex==xsArrayGetSize(i)){break;} /*");
	cEf("*/ 		} /*");
	cEf("*/ xsArraySetInt(i,arrayindex,add); /*");
	cEf("*/ } /*");
//END OF NEW 1.6 FUNCTIONS

	cEf("*/ void resetstrarray(int i=-1) { /*");
	cEf("*/ int size=xsArrayGetSize(i); /*");
	cEf("*/ for(k=0; <size){xsArraySetString(i,k,\"\");} /*");
	cEf("*/ } /*");

	cEf("*/ void resetintarray(int i=-1) { /*");
	cEf("*/ int size=xsArrayGetSize(i); /*");
	cEf("*/ for(k=0; <size){xsArraySetInt(i,k,-1);} /*");
	cEf("*/ } /*");

	cEf("*/ void resetzeroarray(int i=-1) { /*");
	cEf("*/ int size=xsArrayGetSize(i); /*");
	cEf("*/ for(k=0; <size){xsArraySetInt(i,k,0);} /*");
	cEf("*/ } /*");

	cEf("*/ int arraylengthint(int i=-1) { int length=0; /*");
	cEf("*/ for(arrayindex=0; <xsArrayGetSize(i)){ /*");
	cEf("*/ if(xsArrayGetInt(i,arrayindex)==-1){length=arrayindex; break;} /*");
	cEf("*/ } /*");
	cEf("*/ return(length); } /*");

	cEf("*/ int arraylengthstr(int i=-1) { int length=0; /*");
	cEf("*/ for(arrayindex=0; <xsArrayGetSize(i)){ /*");
	cEf("*/ if(xsArrayGetString(i,arrayindex)==\"\"){length=arrayindex; break;} /*");
	cEf("*/ } /*");
	cEf("*/ return(length); } /*");

for(p=1;<=2){
for(s=0;<5){
	cEf("*/ void FINC_"+p+"_"+s+"(int wtv=-1) { /*");
	cEf("*/ trPlayerSetActive("+p+"); uiFindType(\"\"+statsid_P"+p+"_"+s+"); trPlayerSetActive(FP); /*");
	cEf("*/ } /*");
}
}

	cEf("*/ void testlag(int wtv=-1) { /*");
	cEf("*/ xsSetContextPlayer(1); /*");
	cEf("*/ for(i=0;<3000){if(kbGetTechPercentComplete(i)>0){ /*");
	cEf("*/ trChatSendToPlayer(0, FP, \"\"+i);}} /*");
	cEf("*/ } /*");

	cEf("*/ void updateinfo1(int arrid=-1, int startindex=0){ /*");
	cEf("*/ if (startindex>=0){ /*");


for(n=0;<5){
	cEf("*/ INFO_CHECKS(1,"+n+",statsid_P1_"+n+",xsArrayGetString(arrid,"+n+"+startindex)); /*");
	cEf("*/ statsid_P1_"+n+"=xsArrayGetString(arrid,"+n+"+startindex); /*");
}
	cEf("*/ } /*");
	cEf("*/ } /*");

	cEf("*/ void updateinfo2(int arrid=-1, int startindex=0){ /*");
	cEf("*/ if (startindex>=0){ /*");
for(n=0;<5){
	cEf("*/ INFO_CHECKS(2,"+n+",statsid_P2_"+n+",xsArrayGetString(arrid,"+n+"+startindex)); /*");
	cEf("*/ statsid_P2_"+n+"=xsArrayGetString(arrid,"+n+"+startindex); /*");
}
	cEf("*/ } /*");
	cEf("*/ } /*");

	cEf("*/ string index2protonameECO(int i=-1) { /*"); //Economic units, Bank+Factory, Healer units, Scout Units, Crates, Wagons
	cEf("*/ if(i<22){ /*");
	cEf("*/ if(i==0){return(\"Settler\");} /*");
	cEf("*/ else if(i==1){return(\"SettlerWagon\");} /*");
	cEf("*/ else if(i==2){return(\"Coureur\");} /*");
	cEf("*/ else if(i==3){return(\"CoureurCree\");} /*");
	cEf("*/ else if(i==4){return(\"SettlerNative\");} /*");
	cEf("*/ else if(i==5){return(\"ypSettlerAsian\");} /*");
	cEf("*/ else if(i==6){return(\"ypSettlerJapanese\");} /*");
	cEf("*/ else if(i==7){return(\"ypSettlerIndian\");} /*");
	cEf("*/ else if(i==8){return(\"FishingBoat\");} /*");
	cEf("*/ else if(i==9){return(\"ypFishingBoatAsian\");} /*");
	cEf("*/ else if(i==10){return(\"Bank\");} /*");
	cEf("*/ else if(i==11){return(\"ypBankAsian\");} /*");
	cEf("*/ else if(i==12){return(\"ypDojo\");} /*");
	cEf("*/ else if(i==13){return(\"Factory\");} /*");
	cEf("*/ else if(i==14){return(\"Imam\");} /*");
	cEf("*/ else if(i==15){return(\"Missionary\");} /*");
	cEf("*/ else if(i==16){return(\"Priest\");} /*");
	cEf("*/ else if(i==17){return(\"Surgeon\");} /*");
	cEf("*/ else if(i==18){return(\"NatMedicineMan\");} /*");
	cEf("*/ else if(i==19){return(\"xpMedicineMan\");} /*");
	cEf("*/ else if(i==20){return(\"xpMedicineManAztec\");} /*");
	cEf("*/ else if(i==21){return(\"ypMongolScout\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<44){ /*");
	cEf("*/ if(i==22){return(\"NativeScout\");} /*");
	cEf("*/ else if(i==23){return(\"ypNativeScout\");} /*");
	cEf("*/ else if(i==24){return(\"Envoy\");} /*");
	cEf("*/ else if(i==25){return(\"CrateofFood\");} /*");
	cEf("*/ else if(i==26){return(\"CrateofFoodLarge\");} /*");
	cEf("*/ else if(i==27){return(\"CrateofWood\");} /*");
	cEf("*/ else if(i==28){return(\"CrateofWoodLarge\");} /*");
	cEf("*/ else if(i==29){return(\"CrateofCoin\");} /*");
	cEf("*/ else if(i==30){return(\"CrateofCoinLarge\");} /*");
	cEf("*/ else if(i==31){return(\"CoveredWagon\");} /*");
	cEf("*/ else if(i==32){return(\"OutpostWagon\");} /*");
	cEf("*/ else if(i==33){return(\"FortWagon\");} /*");
	cEf("*/ else if(i==34){return(\"BankWagon\");} /*");
	cEf("*/ else if(i==35){return(\"FactoryWagon\");} /*");
	cEf("*/ else if(i==36){return(\"WarHutTravois\");} /*");
	cEf("*/ else if(i==37){return(\"FarmTravois\");} /*");
	cEf("*/ else if(i==38){return(\"NoblesHutTravois\");} /*");
	cEf("*/ else if(i==39){return(\"xpBuilder\");} /*");
	cEf("*/ else if(i==40){return(\"xpBuilderWar\");} /*");
	cEf("*/ else if(i==41){return(\"TradingPostTravois\");} /*");
	cEf("*/ else if(i==42){return(\"xpBuilderStart\");} /*");
	cEf("*/ else if(i==43){return(\"YPCastleWagon\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<67){ /*");
	cEf("*/ if(i==44){return(\"YPMonasteryWagon\");} /*");
	cEf("*/ else if(i==45){return(\"ypTradingPostWagon\");} /*");
	cEf("*/ else if(i==46){return(\"ypMarketWagon\");} /*");
	cEf("*/ else if(i==47){return(\"YPMilitaryRickshaw\");} /*");
	cEf("*/ else if(i==48){return(\"YPRicePaddyWagon\");} /*");
	cEf("*/ else if(i==49){return(\"YPBerryWagon1\");} /*");
	cEf("*/ else if(i==50){return(\"YPGroveWagon\");} /*");
	cEf("*/ else if(i==51){return(\"YPDockWagon\");} /*");
	cEf("*/ else if(i==52){return(\"YPStableWagon\");} /*");
	cEf("*/ else if(i==53){return(\"ypBankWagon\");} /*");
	cEf("*/ else if(i==54){return(\"ypBlockhouseWagon\");} /*");
	cEf("*/ else if(i==55){return(\"ypArsenalWagon\");} /*");
	cEf("*/ else if(i==56){return(\"ypChurchWagon\");} /*");
	cEf("*/ else if(i==57){return(\"YPSacredFieldWagon\");} /*");
	cEf("*/ else if(i==58){return(\"Cow\");} /*");
	cEf("*/ else if(i==59){return(\"ypSacredCow\");} /*");
	cEf("*/ else if(i==60){return(\"Llama\");} /*");
	cEf("*/ else if(i==61){return(\"Sheep\");} /*");
	cEf("*/ else if(i==62){return(\"ypWaterBuffalo\");} /*");
	cEf("*/ else if(i==63){return(\"ypGoat\");} /*");
	cEf("*/ else if(i==64){return(\"ypYak\");} /*");
	cEf("*/ else if(i==65){return(\"ypShrineJapanese\");} /*");
	cEf("*/ else if(i==66){return(\"ypSacredField\");} /*");
	cEf("*/ } /*");
	cEf("*/ return(\"Learicorn\"); /*");
	cEf("*/ } /*");

	cEf("*/ string index2protonameMIL(int i=-1) { /*"); //Land Military units
	cEf("*/ if(i<15){ /*");
	cEf("*/ if(i==0){return(\"Pikeman\");} /*");
	cEf("*/ else if(i==1){return(\"Musketeer\");} /*");
	cEf("*/ else if(i==2){return(\"Crossbowman\");} /*");
	cEf("*/ else if(i==3){return(\"Rodelero\");} /*");
	cEf("*/ else if(i==4){return(\"Skirmisher\");} /*");
	cEf("*/ else if(i==5){return(\"Halberdier\");} /*");
	cEf("*/ else if(i==6){return(\"Cacadore\");} /*");
	cEf("*/ else if(i==7){return(\"Strelet\");} /*");
	cEf("*/ else if(i==8){return(\"Dopplesoldner\");} /*");
	cEf("*/ else if(i==9){return(\"Janissary\");} /*");
	cEf("*/ else if(i==10){return(\"Longbowman\");} /*");
	cEf("*/ else if(i==11){return(\"xpWarBow\");} /*");
	cEf("*/ else if(i==12){return(\"xpWarClub\");} /*");
	cEf("*/ else if(i==13){return(\"xpWarRifle\");} /*");
	cEf("*/ else if(i==14){return(\"xpAenna\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<30){ /*");
	cEf("*/ if(i==15){return(\"xpMusketWarrior\");} /*");
	cEf("*/ else if(i==16){return(\"xpTomahawk\");} /*");
	cEf("*/ else if(i==17){return(\"xpCoyoteMan\");} /*");
	cEf("*/ else if(i==18){return(\"xpMacehualtin\");} /*");
	cEf("*/ else if(i==19){return(\"xpPumaMan\");} /*");
	cEf("*/ else if(i==20){return(\"xpArrowKnight\");} /*");
	cEf("*/ else if(i==21){return(\"xpEagleKnight\");} /*");
	cEf("*/ else if(i==22){return(\"xpJaguarKnight\");} /*");
	cEf("*/ else if(i==23){return(\"xpSkullKnight\");} /*");
	cEf("*/ else if(i==24){return(\"ypYumi\");} /*");
	cEf("*/ else if(i==25){return(\"ypKensei\");} /*");
	cEf("*/ else if(i==26){return(\"ypAshigaru\");} /*");
	cEf("*/ else if(i==27){return(\"ypQiangPikeman\");} /*");
	cEf("*/ else if(i==28){return(\"ypChangdao\");} /*");
	cEf("*/ else if(i==29){return(\"ypChuKoNu\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<45){ /*");
	cEf("*/ if(i==30){return(\"ypArquebusier\");} /*");
	cEf("*/ else if(i==31){return(\"ypRajput\");} /*");
	cEf("*/ else if(i==32){return(\"ypNatMercGurkha\");} /*");
	cEf("*/ else if(i==33){return(\"Hussar\");} /*");
	cEf("*/ else if(i==34){return(\"Lancer\");} /*");
	cEf("*/ else if(i==35){return(\"Dragoon\");} /*");
	cEf("*/ else if(i==36){return(\"Ruyter\");} /*");
	cEf("*/ else if(i==37){return(\"Cossack\");} /*");
	cEf("*/ else if(i==38){return(\"Oprichnik\");} /*");
	cEf("*/ else if(i==39){return(\"Uhlan\");} /*");
	cEf("*/ else if(i==40){return(\"WarWagon\");} /*");
	cEf("*/ else if(i==41){return(\"CavalryArcher\");} /*");
	cEf("*/ else if(i==42){return(\"Cuirassier\");} /*");
	cEf("*/ else if(i==43){return(\"xpHorseman\");} /*");
	cEf("*/ else if(i==44){return(\"xpMusketRider\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<60){ /*");
	cEf("*/ if(i==45){return(\"xpAxeRider\");} /*");
	cEf("*/ else if(i==46){return(\"xpBowRider\");} /*");
	cEf("*/ else if(i==47){return(\"xpRifleRider\");} /*");
	cEf("*/ else if(i==48){return(\"xpCoupRider\");} /*");
	cEf("*/ else if(i==49){return(\"xpDogSoldier\");} /*");
	cEf("*/ else if(i==50){return(\"ypNaginataRider\");} /*");
	cEf("*/ else if(i==51){return(\"ypYabusame\");} /*");
	cEf("*/ else if(i==52){return(\"ypSteppeRider\");} /*");
	cEf("*/ else if(i==53){return(\"ypKeshik\");} /*");
	cEf("*/ else if(i==54){return(\"ypIronFlail\");} /*");
	cEf("*/ else if(i==55){return(\"ypMeteorHammer\");} /*");
	cEf("*/ else if(i==56){return(\"ypMahout\");} /*");
	cEf("*/ else if(i==57){return(\"ypHowdah\");} /*");
	cEf("*/ else if(i==58){return(\"ypSowar\");} /*");
	cEf("*/ else if(i==59){return(\"ypZamburak\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<75){ /*");
	cEf("*/ if(i==60){return(\"Falconet\");} /*");
	cEf("*/ else if(i==61){return(\"OrganGun\");} /*");
	cEf("*/ else if(i==62){return(\"AbusGun\");} /*");
	cEf("*/ else if(i==63){return(\"Grenadier\");} /*");
	cEf("*/ else if(i==64){return(\"Culverin\");} /*");
	cEf("*/ else if(i==65){return(\"Mortar\");} /*");
	cEf("*/ else if(i==66){return(\"xpRam\");} /*");
	cEf("*/ else if(i==67){return(\"xpMantlet\");} /*");
	cEf("*/ else if(i==68){return(\"xpLightCannon\");} /*");
	cEf("*/ else if(i==69){return(\"xpHorseArtillery\");} /*");
	cEf("*/ else if(i==70){return(\"xpPetard\");} /*");
	cEf("*/ else if(i==71){return(\"xpPetardNitro\");} /*");
	cEf("*/ else if(i==72){return(\"ypFlamingArrow\");} /*");
	cEf("*/ else if(i==73){return(\"ypMorutaru\");} /*");
	cEf("*/ else if(i==74){return(\"ypFlameThrower\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<90){ /*");
	cEf("*/ if(i==75){return(\"ypHandMortar\");} /*");
	cEf("*/ else if(i==76){return(\"ypMercFlailiphant\");} /*");
	cEf("*/ else if(i==77){return(\"ypSiegeElephant\");} /*");
	cEf("*/ else if(i==78){return(\"xpColonialMilitia\");} /*");
	cEf("*/ else if(i==79){return(\"xpGatlingGun\");} /*");
	cEf("*/ else if(i==80){return(\"ypDelinquentThuggee\");} /*");
	cEf("*/ else if(i==81){return(\"ypConsulateTercio\");} /*");
	cEf("*/ else if(i==82){return(\"ypConsulateRedcoat\");} /*");
	cEf("*/ else if(i==83){return(\"ypConsulateLifeGuard\");} /*");
	cEf("*/ else if(i==84){return(\"ypConsulateJinete\");} /*");
	cEf("*/ else if(i==85){return(\"ypConsulateGuerreiros\");} /*");
	cEf("*/ else if(i==86){return(\"ypConsulateGarrochista\");} /*");
	cEf("*/ else if(i==87){return(\"ypConsulateCarabineer\");} /*");
	cEf("*/ else if(i==88){return(\"ypConsulateFalconet\");} /*");
	cEf("*/ else if(i==89){return(\"ypConsulateCulverin\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<105){ /*");
	cEf("*/ if(i==90){return(\"ypConsulateMortar\");} /*");
	cEf("*/ else if(i==91){return(\"ypConsulateGreatBombard\");} /*");
	cEf("*/ else if(i==92){return(\"ypConsulateHorseArtillery\");} /*");
	cEf("*/ else if(i==93){return(\"ypConsulateBestieros\");} /*");
	cEf("*/ else if(i==94){return(\"ypConsulateEspadachins\");} /*");
	cEf("*/ else if(i==95){return(\"ypConsulateStadhouders\");} /*");
	cEf("*/ else if(i==96){return(\"ypConsulateTufanciCorps\");} /*");
	cEf("*/ else if(i==97){return(\"ypConsulateGendarmes\");} /*");
	cEf("*/ else if(i==98){return(\"ypConsulateCzapakaUhlan\");} /*");
	cEf("*/ else if(i==99){return(\"ypConsulateZweihander\");} /*");
	cEf("*/ else if(i==100){return(\"ypConsulateRogersRanger\");} /*");
	cEf("*/ else if(i==101){return(\"ypConsulateCannon\");} /*");
	cEf("*/ else if(i==102){return(\"ypConsulateGardener\");} /*");
	cEf("*/ else if(i==103){return(\"ypConsulateKalmuck\");} /*");
	cEf("*/ else if(i==104){return(\"ypConsulateBashkirPony\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<120){ /*");
	cEf("*/ if(i==105){return(\"ypConsulatePrussianNeedleGun\");} /*");
	cEf("*/ else if(i==106){return(\"ypConsulateYoungGarde\");} /*");
	cEf("*/ else if(i==107){return(\"ypConsulateSiberianCossack\");} /*");
	cEf("*/ else if(i==108){return(\"SaloonOutlawRider\");} /*");
	cEf("*/ else if(i==109){return(\"SaloonOutlawRifleman\");} /*");
	cEf("*/ else if(i==110){return(\"SaloonPirate\");} /*");
	cEf("*/ else if(i==111){return(\"MercLandsknecht\");} /*");
	cEf("*/ else if(i==112){return(\"MercSwissPikeman\");} /*");
	cEf("*/ else if(i==113){return(\"MercHighlander\");} /*");
	cEf("*/ else if(i==114){return(\"MercMameluke\");} /*");
	cEf("*/ else if(i==115){return(\"MercStradiot\");} /*");
	cEf("*/ else if(i==116){return(\"MercBlackRider\");} /*");
	cEf("*/ else if(i==117){return(\"MercManchu\");} /*");
	cEf("*/ else if(i==118){return(\"MercRonin\");} /*");
	cEf("*/ else if(i==119){return(\"MercJaeger\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<135){ /*");
	cEf("*/ if(i==120){return(\"MercHackapell\");} /*");
	cEf("*/ else if(i==121){return(\"MercBarbaryCorsair\");} /*");
	cEf("*/ else if(i==122){return(\"MercElmeti\");} /*");
	cEf("*/ else if(i==123){return(\"MercFusilier\");} /*");
	cEf("*/ else if(i==124){return(\"MercGreatCannon\");} /*");
	cEf("*/ else if(i==125){return(\"MercNinja\");} /*");
	cEf("*/ else if(i==126){return(\"ypDacoit\");} /*");
	cEf("*/ else if(i==127){return(\"ypMercIronTroop\");} /*");
	cEf("*/ else if(i==128){return(\"ypMercArsonist\");} /*");
	cEf("*/ else if(i==129){return(\"ypMercJatLancer\");} /*");
	cEf("*/ else if(i==130){return(\"ypMercYojimbo\");} /*");
	cEf("*/ else if(i==131){return(\"ypThuggee\");} /*");
	cEf("*/ else if(i==132){return(\"ypWokouBlindMonk\");} /*");
	cEf("*/ else if(i==133){return(\"ypWokouPirate\");} /*");
	cEf("*/ else if(i==134){return(\"ypWokouWanderingHorseman\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<150){ /*");
	cEf("*/ if(i==135){return(\"ypWokouWaywardRonin\");} /*");
	cEf("*/ else if(i==136){return(\"ypRepentantArsonist\");} /*");
	cEf("*/ else if(i==137){return(\"ypRepentantBarbaryCorsair\");} /*");
	cEf("*/ else if(i==138){return(\"ypRepentantBlackRider\");} /*");
	cEf("*/ else if(i==139){return(\"ypRepentantDacoit\");} /*");
	cEf("*/ else if(i==140){return(\"ypRepentantElmeti\");} /*");
	cEf("*/ else if(i==141){return(\"ypRepentantBlindMonk\");} /*");
	cEf("*/ else if(i==142){return(\"ypRepentantFusilier\");} /*");
	cEf("*/ else if(i==143){return(\"ypRepentantHackapell\");} /*");
	cEf("*/ else if(i==144){return(\"ypRepentantHighlander\");} /*");
	cEf("*/ else if(i==145){return(\"ypRepentantIronTroop\");} /*");
	cEf("*/ else if(i==146){return(\"ypRepentantJaeger\");} /*");
	cEf("*/ else if(i==147){return(\"ypRepentantJatLancer\");} /*");
	cEf("*/ else if(i==148){return(\"ypRepentantLandsknecht\");} /*");
	cEf("*/ else if(i==149){return(\"ypRepentantMameluke\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<165){ /*");
	cEf("*/ if(i==150){return(\"ypRepentantManchu\");} /*");
	cEf("*/ else if(i==151){return(\"ypRepentantGreatCannon\");} /*");
	cEf("*/ else if(i==152){return(\"ypRepentantNinja\");} /*");
	cEf("*/ else if(i==153){return(\"ypRepentantOutlawPistol\");} /*");
	cEf("*/ else if(i==154){return(\"ypRepentantOutlawRider\");} /*");
	cEf("*/ else if(i==155){return(\"ypRepentantOutlawRifleman\");} /*");
	cEf("*/ else if(i==156){return(\"ypRepentantPirate\");} /*");
	cEf("*/ else if(i==157){return(\"ypRepentantRonin\");} /*");
	cEf("*/ else if(i==158){return(\"ypRepentantSmuggler\");} /*");
	cEf("*/ else if(i==159){return(\"ypRepentantStradiot\");} /*");
	cEf("*/ else if(i==160){return(\"ypRepentantSwissPikeman\");} /*");
	cEf("*/ else if(i==161){return(\"ypRepentantThuggee\");} /*");
	cEf("*/ else if(i==162){return(\"ypRepentantWanderingHorseman\");} /*");
	cEf("*/ else if(i==163){return(\"ypRepentantWaywardRonin\");} /*");
	cEf("*/ else if(i==164){return(\"ypRepentantYojimbo\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<180){ /*");
	cEf("*/ if(i==165){return(\"ypNatChakram\");} /*");
	cEf("*/ else if(i==166){return(\"ypNatConquistador\");} /*");
	cEf("*/ else if(i==167){return(\"ypNatRattanShield\");} /*");
	cEf("*/ else if(i==168){return(\"ypNatTigerClaw\");} /*");
	cEf("*/ else if(i==169){return(\"ypNatWarElephant\");} /*");
	cEf("*/ else if(i==170){return(\"NatBolasWarrior\");} /*");
	cEf("*/ else if(i==171){return(\"NatBlowgunWarrior\");} /*");
	cEf("*/ else if(i==172){return(\"NatApacheCavalry\");} /*");
	cEf("*/ else if(i==173){return(\"NatBlackwoodArcher\");} /*");
	cEf("*/ else if(i==174){return(\"NatCheyenneRider\");} /*");
	cEf("*/ else if(i==175){return(\"NatClubman\");} /*");
	cEf("*/ else if(i==176){return(\"NatClubmanLoyal\");} /*");
	cEf("*/ else if(i==177){return(\"NatHolcanSpearman\");} /*");
	cEf("*/ else if(i==178){return(\"NatHorseArcher\");} /*");
	cEf("*/ else if(i==179){return(\"NatHuaminca\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<195){ /*");
	cEf("*/ if(i==180){return(\"NatHuronMantlet\");} /*");
	cEf("*/ else if(i==181){return(\"NatKlamathRifleman\");} /*");
	cEf("*/ else if(i==182){return(\"NatLightningWarrior\");} /*");
	cEf("*/ else if(i==183){return(\"NatMapucheClubman\");} /*");
	cEf("*/ else if(i==184){return(\"NatNavajoRifleman\");} /*");
	cEf("*/ else if(i==185){return(\"NatRifleman\");} /*");
	cEf("*/ else if(i==186){return(\"NatSharktoothBowman\");} /*");
	cEf("*/ else if(i==187){return(\"NatTracker\");} /*");
	cEf("*/ else if(i==188){return(\"ypNatSohei\");} /*");
	cEf("*/ else if(i==189){return(\"PetBear\");} /*");
	cEf("*/ else if(i==190){return(\"WarDog\");} /*");
	cEf("*/ else if(i==191){return(\"PetJaguar\");} /*");
	cEf("*/ else if(i==192){return(\"PetCougar\");} /*");
	cEf("*/ else if(i==193){return(\"PetGrizzly\");} /*");
	cEf("*/ else if(i==194){return(\"PetPolarBear\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<210){ /*");
	cEf("*/ if(i==195){return(\"PetWolf\");} /*");
	cEf("*/ else if(i==196){return(\"ExplorerDog\");} /*");
	cEf("*/ else if(i==197){return(\"PetCoyote\");} /*");
	cEf("*/ else if(i==198){return(\"ypMonkDisciple\");} /*");
	cEf("*/ else if(i==199){return(\"YPPetTiger\");} /*");
	cEf("*/ else if(i==200){return(\"YPPetRhino\");} /*");
	cEf("*/ else if(i==201){return(\"ypPetKomodoDragon\");} /*");
	cEf("*/ else if(i==202){return(\"ypPetOrangutan\");} /*");
	cEf("*/ else if(i==203){return(\"YPPetPanda\");} /*");
	cEf("*/ else if(i==204){return(\"ypPetLion\");} /*");
	cEf("*/ else if(i==205){return(\"YPPetWhiteTiger\");} /*");
	cEf("*/ else if(i==206){return(\"ypPetTibetanMacaque\");} /*");
	cEf("*/ else if(i==207){return(\"ypPetSnowMonkey\");} /*");
	cEf("*/ else if(i==208){return(\"Rocket\");} /*");
	cEf("*/ else if(i==209){return(\"GreatBombard\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<225){ /*");
	cEf("*/ if(i==210){return(\"Cannon\");} /*");
	cEf("*/ else if(i==211){return(\"xpSpy\");} /*");
	cEf("*/ else if(i==212){return(\"ypFlyingCrow\");} /*");
	cEf("*/ else if(i==213){return(\"ypDaimyoMototada\");} /*");
	cEf("*/ else if(i==214){return(\"ypShogunTokugawa\");} /*");
	cEf("*/ else if(i==215){return(\"ypDaimyoKiyomasa\");} /*");
	cEf("*/ else if(i==216){return(\"ypDaimyoMasamune\");} /*");
	cEf("*/ else if(i==217){return(\"Spahi\");} /*");
	cEf("*/ else if(i==218){return(\"ypUrumi\");} /*");
	cEf("*/ else if(i==219){return(\"Minuteman\");} /*");
	cEf("*/ else if(i==220){return(\"xpWarrior\");} /*");
	cEf("*/ else if(i==221){return(\"ypIrregular\");} /*");
	cEf("*/ else if(i==222){return(\"ypPeasant\");} /*");
	cEf("*/ else if(i==223){return(\"ypRajputMansabdar\");} /*");
	cEf("*/ else if(i==224){return(\"ypUrumiMansabdar\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<240){ /*");
	cEf("*/ if(i==225){return(\"ypSepoyMansabdar\");} /*");
	cEf("*/ else if(i==226){return(\"ypNatMercGurkhaJemadar\");} /*");
	cEf("*/ else if(i==227){return(\"ypSowarMansabdar\");} /*");
	cEf("*/ else if(i==228){return(\"ypZamburakMansabdar\");} /*");
	cEf("*/ else if(i==229){return(\"ypMahoutMansabdar\");} /*");
	cEf("*/ else if(i==230){return(\"ypMercFlailiphantMansabdar\");} /*");
	cEf("*/ else if(i==231){return(\"ypHowdahMansabdar\");} /*");
	cEf("*/ else if(i==232){return(\"ypSiegeElephantMansabdar\");} /*");
	cEf("*/ else if(i==233){return(\"ypConsulateRonin\");} /*");
	cEf("*/ else if(i==234){return(\"ypConsulateNinja\");} /*");
	cEf("*/ else if(i==235){return(\"ypConsulateYamabushi\");} /*");
	cEf("*/ else if(i==236){return(\"ypConsulateShinobi\");} /*");
	cEf("*/ else if(i==237){return(\"GrizzlyBear\");} /*");
	cEf("*/ else if(i==238){return(\"PolarBear\");} /*");
	cEf("*/ else if(i==239){return(\"Jaguar\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<255){ /*");
	cEf("*/ if(i==240){return(\"BlackBear\");} /*");
	cEf("*/ else if(i==241){return(\"Cougar\");} /*");
	cEf("*/ else if(i==242){return(\"Wolf\");} /*");
	cEf("*/ else if(i==243){return(\"Coyote\");} /*");
	cEf("*/ else if(i==244){return(\"OutlawRider\");} /*");
	cEf("*/ else if(i==245){return(\"OutlawBlowgunner\");} /*");
	cEf("*/ else if(i==246){return(\"OutlawRifleman\");} /*");
	cEf("*/ else if(i==247){return(\"Alligator\");} /*");
	cEf("*/ else if(i==248){return(\"Pirate\");} /*");
	cEf("*/ else if(i==249){return(\"OutlawPistol\");} /*");
	cEf("*/ else if(i==250){return(\"ypWokou\");} /*");
	cEf("*/ else if(i==251){return(\"ypWanderingHorseman\");} /*");
	cEf("*/ else if(i==252){return(\"ypBlindMonk\");} /*");
	cEf("*/ else if(i==253){return(\"ypFugitiveDacoit\");} /*");
	cEf("*/ else if(i==254){return(\"ypLionTailedMacaque\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<269){ /*");
	cEf("*/ if(i==255){return(\"ypRhino\");} /*");
	cEf("*/ else if(i==256){return(\"ypTiger\");} /*");
	cEf("*/ else if(i==257){return(\"ypLion\");} /*");
	cEf("*/ else if(i==258){return(\"ypMonitorLizard\");} /*");
	cEf("*/ else if(i==259){return(\"ypTibetanMacaque\");} /*");
	cEf("*/ else if(i==260){return(\"ypBlackPanther\");} /*");
	cEf("*/ else if(i==261){return(\"ypWaywardRonin\");} /*");
	cEf("*/ else if(i==262){return(\"ypWokouArmy\");} /*");
	cEf("*/ else if(i==263){return(\"ypSnowLeopard\");} /*");
	cEf("*/ else if(i==264){return(\"ypWhiteTiger\");} /*");
	cEf("*/ else if(i==265){return(\"ypSnowMonkey\");} /*");
	cEf("*/ else if(i==266){return(\"ypPanda\");} /*");
	cEf("*/ else if(i==267){return(\"ypOrangutan\");} /*");
	cEf("*/ else if(i==268){return(\"SaloonOutlawPistol\");} /*");
	cEf("*/ else if(i==269){return(\"ypSepoy\");} /*");
	cEf("*/ } /*");
	cEf("*/ return(\"Learicorn\"); /*");
	cEf("*/ } /*");

	cEf("*/ string index2protonameWAT(int i=-1) { /*"); //Water Military Units
	cEf("*/ if(i<10){ /*");
	cEf("*/ if(i==0){return(\"Canoe\");} /*");
	cEf("*/ else if(i==1){return(\"Caravel\");} /*");
	cEf("*/ else if(i==2){return(\"Galleon\");} /*");
	cEf("*/ else if(i==3){return(\"Galley\");} /*");
	cEf("*/ else if(i==4){return(\"Fluyt\");} /*");
	cEf("*/ else if(i==5){return(\"Frigate\");} /*");
	cEf("*/ else if(i==6){return(\"Monitor\");} /*");
	cEf("*/ else if(i==7){return(\"xpWarCanoe\");} /*");
	cEf("*/ else if(i==8){return(\"xpTlalocCanoe\");} /*");
	cEf("*/ else if(i==9){return(\"ypFireship\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<18){ /*");
	cEf("*/ if(i==10){return(\"ypFuchuan\");} /*");
	cEf("*/ else if(i==11){return(\"ypFune\");} /*");
	cEf("*/ else if(i==12){return(\"ypAtakabune\");} /*");
	cEf("*/ else if(i==13){return(\"ypTekkousen\");} /*");
	cEf("*/ else if(i==14){return(\"Privateer\");} /*");
	cEf("*/ else if(i==15){return(\"ypMarathanCatamaran\");} /*");
	cEf("*/ else if(i==16){return(\"ypWokouJunk\");} /*");
	cEf("*/ else if(i==17){return(\"xpIronclad\");} /*");
	cEf("*/ } /*");
	cEf("*/ return(\"Learicorn\"); /*");
	cEf("*/ } /*");

	cEf("*/ string index2protonameRMD(int i=-1) { /*"); //All Remaining Units (These likely won't be shown in unit counter but will be in trained if applicable)
	cEf("*/ if(i<10){ /*");
	cEf("*/ if(i==0){return(\"Explorer\");} /*");
	cEf("*/ else if(i==1){return(\"xpIroquoisWarChief\");} /*");
	cEf("*/ else if(i==2){return(\"xpLakotaWarchief\");} /*");
	cEf("*/ else if(i==3){return(\"xpAztecWarchief\");} /*");
	cEf("*/ else if(i==4){return(\"ypMonkChinese\");} /*");
	cEf("*/ else if(i==5){return(\"ypMonkChinese2\");} /*");
	cEf("*/ else if(i==6){return(\"ypMonkIndian\");} /*");
	cEf("*/ else if(i==7){return(\"ypMonkIndian2\");} /*");
	cEf("*/ else if(i==8){return(\"ypMonkJapanese\");} /*");
	cEf("*/ else if(i==9){return(\"ypMonkJapanese2\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<20){ /*");
	cEf("*/ if(i==10){return(\"ypOldHanArmy\");} /*");
	cEf("*/ else if(i==11){return(\"ypStandardArmy\");} /*");
	cEf("*/ else if(i==12){return(\"ypMingArmy\");} /*");
	cEf("*/ else if(i==13){return(\"ypTerritorialArmy\");} /*");
	cEf("*/ else if(i==14){return(\"ypForbiddenArmy\");} /*");
	cEf("*/ else if(i==15){return(\"ypImperialArmy\");} /*");
	cEf("*/ else if(i==16){return(\"ypMongolianArmy\");} /*");
	cEf("*/ else if(i==17){return(\"ypBlackFlagArmy\");} /*");
	cEf("*/ else if(i==18){return(\"ypDojoNaginataRiderArmy\");} /*");
	cEf("*/ else if(i==19){return(\"ypDojoKenseiArmy\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<30){ /*");
	cEf("*/ if(i==20){return(\"ypDojoYabusameArmy\");} /*");
	cEf("*/ else if(i==21){return(\"ypDojoYumiArmy\");} /*");
	cEf("*/ else if(i==22){return(\"ypDojoAshigaruArmy\");} /*");
	cEf("*/ else if(i==23){return(\"ypConsulateArmySpanish3\");} /*");
	cEf("*/ else if(i==24){return(\"ypConsulateArmySpanish2\");} /*");
	cEf("*/ else if(i==25){return(\"ypConsulateArmySpanish1\");} /*");
	cEf("*/ else if(i==26){return(\"ypConsulateArmyRussian3\");} /*");
	cEf("*/ else if(i==27){return(\"ypConsulateArmyRussian2\");} /*");
	cEf("*/ else if(i==28){return(\"ypConsulateArmyRussian1\");} /*");
	cEf("*/ else if(i==29){return(\"ypConsulateArmyOttoman3\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<40){ /*");
	cEf("*/ if(i==30){return(\"ypConsulateArmyOttoman2\");} /*");
	cEf("*/ else if(i==31){return(\"ypConsulateArmyOttoman1\");} /*");
	cEf("*/ else if(i==32){return(\"ypConsulateArmyGerman3\");} /*");
	cEf("*/ else if(i==33){return(\"ypConsulateArmyGerman2\");} /*");
	cEf("*/ else if(i==34){return(\"ypConsulateArmyGerman1\");} /*");
	cEf("*/ else if(i==35){return(\"ypConsulateArmyFrench3\");} /*");
	cEf("*/ else if(i==36){return(\"ypConsulateArmyFrench2\");} /*");
	cEf("*/ else if(i==37){return(\"ypConsulateArmyFrench1\");} /*");
	cEf("*/ else if(i==38){return(\"ypConsulateArmyDutch3\");} /*");
	cEf("*/ else if(i==39){return(\"ypConsulateArmyDutch2\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<50){ /*");
	cEf("*/ if(i==40){return(\"ypConsulateArmyDutch1\");} /*");
	cEf("*/ else if(i==41){return(\"ypConsulateArmyBritish3\");} /*");
	cEf("*/ else if(i==42){return(\"ypConsulateArmyBritish2\");} /*");
	cEf("*/ else if(i==43){return(\"ypConsulateArmyBritish1\");} /*");
	cEf("*/ else if(i==44){return(\"TownCenter\");} /*");
	cEf("*/ else if(i==45){return(\"LivestockPen\");} /*");
	cEf("*/ else if(i==46){return(\"Market\");} /*");
	cEf("*/ else if(i==47){return(\"Mill\");} /*");
	cEf("*/ else if(i==48){return(\"Dock\");} /*");
	cEf("*/ else if(i==49){return(\"Outpost\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<60){ /*");
	cEf("*/ if(i==50){return(\"Blockhouse\");} /*");
	cEf("*/ else if(i==51){return(\"TradingPost\");} /*");
	cEf("*/ else if(i==52){return(\"Church\");} /*");
	cEf("*/ else if(i==53){return(\"Barracks\");} /*");
	cEf("*/ else if(i==54){return(\"Stable\");} /*");
	cEf("*/ else if(i==55){return(\"ArtilleryDepot\");} /*");
	cEf("*/ else if(i==56){return(\"Arsenal\");} /*");
	cEf("*/ else if(i==57){return(\"Plantation\");} /*");
	cEf("*/ else if(i==58){return(\"Capitol\");} /*");
	cEf("*/ else if(i==59){return(\"HouseEast\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<70){ /*");
	cEf("*/ if(i==60){return(\"HouseMed\");} /*");
	cEf("*/ else if(i==61){return(\"Manor\");} /*");
	cEf("*/ else if(i==62){return(\"House\");} /*");
	cEf("*/ else if(i==63){return(\"FortFrontier\");} /*");
	cEf("*/ else if(i==64){return(\"FieldHospital\");} /*");
	cEf("*/ else if(i==65){return(\"NativeEmbassy\");} /*");
	cEf("*/ else if(i==66){return(\"Saloon\");} /*");
	cEf("*/ else if(i==67){return(\"FirePit\");} /*");
	cEf("*/ else if(i==68){return(\"Teepee\");} /*");
	cEf("*/ else if(i==69){return(\"Longhouse\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<80){ /*");
	cEf("*/ if(i==70){return(\"HouseAztec\");} /*");
	cEf("*/ else if(i==71){return(\"Farm\");} /*");
	cEf("*/ else if(i==72){return(\"WarHut\");} /*");
	cEf("*/ else if(i==73){return(\"Corral\");} /*");
	cEf("*/ else if(i==74){return(\"NoblesHut\");} /*");
	cEf("*/ else if(i==75){return(\"ypTradeMarketAsian\");} /*");
	cEf("*/ else if(i==76){return(\"ypStableJapanese\");} /*");
	cEf("*/ else if(i==77){return(\"ypRicePaddy\");} /*");
	cEf("*/ else if(i==78){return(\"ypMonastery\");} /*");
	cEf("*/ else if(i==79){return(\"YPDockAsian\");} /*");
	cEf("*/ } /*");
	cEf("*/ else if(i<93){ /*");
	cEf("*/ if(i==80){return(\"ypConsulate\");} /*");
	cEf("*/ else if(i==81){return(\"ypCastle\");} /*");
	cEf("*/ else if(i==82){return(\"ypBarracksJapanese\");} /*");
	cEf("*/ else if(i==83){return(\"ypBerryBuilding\");} /*");
	cEf("*/ else if(i==84){return(\"ypChurch\");} /*");
	cEf("*/ else if(i==85){return(\"ypArsenalAsian\");} /*");
	cEf("*/ else if(i==86){return(\"ypWarAcademy\");} /*");
	cEf("*/ else if(i==87){return(\"ypVillage\");} /*");
	cEf("*/ else if(i==88){return(\"ypSacredField\");} /*");
	cEf("*/ else if(i==89){return(\"ypHouseIndian\");} /*");
	cEf("*/ else if(i==90){return(\"ypCaravanserai\");} /*");
	cEf("*/ else if(i==91){return(\"ypGroveBuilding\");} /*");
	cEf("*/ else if(i==92){return(\"YPBarracksIndian\");} /*");
	cEf("*/ } /*");
	cEf("*/ return(\"Learicorn\"); /*");
	cEf("*/ } /*");

	cEf("*/ void updateinfoarrays(int wtv=-1){ /*");
	cEf("*/ if(showdead==-1){resetstrarray(0); resetstrarray(1);} /*");
	cEf("*/ string temp=\"\"; int tempindex=0; /*");

	cEf("*/ if(infodisptype==0){ /*");
	cEf("*/ 		for(i=0;<67){ /*");
	cEf("*/ 			temp=index2protonameECO(i); /*");
	cEf("*/ 			if(trPlayerUnitCountSpecific(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerUnitCountSpecific(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ } /*");
	cEf("*/ else if(infodisptype==1){ /*");
	cEf("*/ 		for(i=0;<270){ /*");
	cEf("*/ 			temp=index2protonameMIL(i); /*");
	cEf("*/ 			if(trPlayerUnitCountSpecific(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerUnitCountSpecific(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ } /*");
	cEf("*/ else if(infodisptype==2){ /*");
	cEf("*/ 		for(i=0;<18){ /*");
	cEf("*/ 			temp=index2protonameWAT(i); /*");
	cEf("*/ 			if(trPlayerUnitCountSpecific(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerUnitCountSpecific(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ } /*");
	cEf("*/ else if(infodisptype==3){ /*");
	cEf("*/ 		for(i=0;<67){ /*");
	cEf("*/ 			temp=index2protonameECO(i); /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ 		for(i=0;<93){ /*");
	cEf("*/ 			temp=index2protonameRMD(i); /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ 		for(i=0;<270){ /*");
	cEf("*/ 			temp=index2protonameMIL(i); /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ 		for(i=0;<18){ /*");
	cEf("*/ 			temp=index2protonameWAT(i); /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(1,temp)>0){addstrtoarray(0,temp);} /*");
	cEf("*/ 			if(trPlayerCountBuildingInProgress(2,temp)>0){addstrtoarray(1,temp);} /*");
	cEf("*/ 		} /*");
	cEf("*/ } /*");

	cEf("*/ } /*");

	cEf("*/ void updatedeadarrays(int wtv=-1){ /*");
	cEf("*/ string tempunit=\"\"; int todaysquery=0; /*");
	cEf("*/ resetzeroarray(14); resetzeroarray(15); /*");

	cEf("*/ xsSetContextPlayer(1); kbLookAtAllUnitsOnMap(); /*");
	cEf("*/ todaysquery=kbUnitQueryCreate(\"Blargh1\"); /*");

	cEf("*/ for(i=0;<300){ /*");
	cEf("*/ 	tempunit=xsArrayGetString(0,i); /*");
	cEf("*/ 	if(tempunit!=\"\"){ /*");
	cEf("*/ 	kbUnitQuerySetPlayerID(todaysquery, 1, true); /*");
	cEf("*/ 	kbUnitQuerySetState(todaysquery,4); /*");
	cEf("*/ 	kbUnitQuerySetUnitType(todaysquery, kbGetProtoUnitID(tempunit)); /*");
	cEf("*/ 	xsArraySetInt(14,i,kbUnitQueryExecute(todaysquery)); /*");
	cEf("*/ 	} /*");
	cEf("*/ else{break;} /*");
	cEf("*/ } /*");

	cEf("*/ xsSetContextPlayer(2); kbLookAtAllUnitsOnMap(); /*");
	cEf("*/ todaysquery=kbUnitQueryCreate(\"Blargh2\"); /*");

	cEf("*/ for(i=0;<300){ /*");
	cEf("*/ 	tempunit=xsArrayGetString(1,i); /*");
	cEf("*/ 	if(tempunit!=\"\"){ /*");
	cEf("*/ 	kbUnitQuerySetPlayerID(todaysquery, 2, true); /*");
	cEf("*/ 	kbUnitQuerySetState(todaysquery,4); /*");
	cEf("*/ 	kbUnitQuerySetUnitType(todaysquery, kbGetProtoUnitID(tempunit)); /*");
	cEf("*/ 	xsArraySetInt(15,i,kbUnitQueryExecute(todaysquery)); /*");
	cEf("*/ 	} /*");
	cEf("*/ else{break;} /*");
	cEf("*/ } /*");

	cEf("*/ } /*");

	cEf("*/ void handleimprovements(int wtv=-1) { /*");
	cEf("*/ int indicator=0; resetintarray(5); resetintarray(11); /*");

	cEf("*/ xsSetContextPlayer(1); /*");
	cEf("*/ for(i=0;<3000){ /*");
	cEf("*/ 	if(kbGetTechPercentComplete(i)>0){ /*");
	cEf("*/ 		if(kbGetTechPercentComplete(i)==1){ /*");
	cEf("*/ 			indicator=xsArrayGetInt(2,i); /*");
	cEf("*/ 			if(indicator==-2){ /*");
	cEf("*/ 			arrayaddint(3,i); arrayaddint(4,trTime()); /*");
	cEf("*/ 			xsArraySetInt(2,i,1); /*");
	cEf("*/ 			} /*");
	cEf("*/ 			else if(indicator==-3){ /*");
	cEf("*/ 			arrayaddint(6,i); arrayaddint(7,trTime()); cooldown1=0; /*");
	cEf("*/ 			xsArraySetInt(2,i,1); /*");
	cEf("*/ 			} /*");
	cEf("*/ 		} /*");
	cEf("*/ 		else{ /*");
	cEf("*/ 		arrayaddint(5,i); /*");
	cEf("*/ 		} /*");
	cEf("*/ 	} /*");
	cEf("*/ } /*");

	cEf("*/ xsSetContextPlayer(2); /*");
	cEf("*/ for(i=0;<3000){ /*");
	cEf("*/ 	if(kbGetTechPercentComplete(i)>0){ /*");
	cEf("*/ 		if(kbGetTechPercentComplete(i)==1){ /*");
	cEf("*/ 			indicator=xsArrayGetInt(8,i); /*");
	cEf("*/ 			if(indicator==-2){ /*");
	cEf("*/ 			arrayaddint(9,i); arrayaddint(10,trTime()); /*");
	cEf("*/ 			xsArraySetInt(8,i,1); /*");
	cEf("*/ 			} /*");
	cEf("*/ 			else if(indicator==-3){ /*");
	cEf("*/ 			arrayaddint(12,i); arrayaddint(13,trTime()); cooldown2=0; /*");
	cEf("*/ 			xsArraySetInt(8,i,1); /*");
	cEf("*/ 			} /*");
	cEf("*/ 		} /*");
	cEf("*/ 		else{ /*");
	cEf("*/ 		arrayaddint(11,i); /*");
	cEf("*/ 		} /*");
	cEf("*/ 	} /*");
	cEf("*/ } /*");
	cEf("*/ } /*");

//To assist with crate logic
	cEf("*/ int numcreated(string unitcreate=\"\", int playercreate=-1){ /*");
	cEf("*/ xsSetContextPlayer(playercreate); kbLookAtAllUnitsOnMap(); /*");
	cEf("*/ int output=0; /*");
	cEf("*/ int creationquery=0; /*");
	cEf("*/ creationquery=kbUnitQueryCreate(\"CreationRecords\"); /*");
	cEf("*/ kbUnitQuerySetPlayerID(creationquery, playercreate, true); /*");
	cEf("*/ kbUnitQuerySetState(creationquery,4); /*");
	cEf("*/ kbUnitQuerySetUnitType(creationquery, kbGetProtoUnitID(unitcreate)); /*");
	cEf("*/ output=kbUnitQueryExecute(creationquery)+trPlayerUnitCountSpecific(1,unitcreate); /*");
	cEf("*/ return(output); /*");
	cEf("*/ } /*");

//Other stuff
	cEf("*/ rule _ConstantLoader active runImmediately { if (true){FP=trCurrentPlayer(); /*");
	cEf("*/ P1StartXP=trPlayerResourceCount(1,\"Xp\"); P2StartXP=trPlayerResourceCount(2,\"Xp\");   /*");
	cEf("*/ P1StartAge=kbGetAgeForPlayer(1); P2StartAge=kbGetAgeForPlayer(2);   /*");
	cEf("*/ cherryorickshaw1=numcreated(\"YPBerryWagon1\",1); cherryorickshaw2=numcreated(\"YPBerryWagon1\",2);   /*");

	cEf("*/ if(FP>2){uiFindType(\"Target\"); /*");
	cEf("*/ uiDeleteAllSelectedUnits();trSetFogAndBlackmap(false, false);} /*");

	cEf("*/ xsArrayCreateString(300,\"\",\"P1Info\");   /*");			//0
	cEf("*/ xsArrayCreateString(300,\"\",\"P2Info\");   /*");			//1

//A -2 indicates it is a technology, -3 a shipment, 1 a completed improvement. -1 indicates do not count.

	cEf("*/ xsArrayCreateInt(3000,-1,\"P1Improvements_Indicator\");   /*");		//2

	cEf("*/ xsArrayCreateInt(300,-1,\"P1Technologies_Completed\");   /*");		//3
	cEf("*/ xsArrayCreateInt(300,-1,\"P1Technologies_Timestamps\");   /*");		//4
	cEf("*/ xsArrayCreateInt(300,-1,\"P1Technologies_Researching\");   /*");	//5

	cEf("*/ xsArrayCreateInt(300,-1,\"P1Shipments\");   /*");			//6
	cEf("*/ xsArrayCreateInt(300,-1,\"P1Shipments_Timestamps\");   /*");		//7


	cEf("*/ xsArrayCreateInt(3000,-1,\"P2Improvements_Indicator\");   /*");		//8

	cEf("*/ xsArrayCreateInt(300,-1,\"P2Technologies_Completed\");   /*");		//9
	cEf("*/ xsArrayCreateInt(300,-1,\"P2Technologies_Timestamps\");   /*");		//10
	cEf("*/ xsArrayCreateInt(300,-1,\"P2Technologies_Researching\");   /*");	//11

	cEf("*/ xsArrayCreateInt(300,-1,\"P2Shipments\");   /*");			//12
	cEf("*/ xsArrayCreateInt(300,-1,\"P2Shipments_Timestamps\");   /*");		//13

//Newly added arrays to track unit lost counts. These correspond to the 'temporary' arrays 0 and 1.

	cEf("*/ xsArrayCreateInt(300,0,\"P1Vanquished\");   /*");			//14
	cEf("*/ xsArrayCreateInt(300,0,\"P2Vanquished\");   /*");		        //15


for(p=3;<=cNumberNonGaiaPlayers){
	//Ally all observers if we have more than 3 players.
	for(q=p;<=cNumberNonGaiaPlayers){
		if(cNumberNonGaiaPlayers>3){cEf("*/ trPlayerSetDiplomacy("+p+", "+q+", \"ally\");   /*");}
	}

//Disable XP trickle
	cEf("*/ trTechSetStatus("+p+", 529, 2);   /*");
}

for(h=0;<3000){
	if(isCounted(h)){
		if(isHCTech(h)){cEf("*/ xsArraySetInt(2,"+h+",-3); /*");
		cEf("*/ xsArraySetInt(8,"+h+",-3); /*");}
		else{cEf("*/ xsArraySetInt(2,"+h+",-2); /*");
		cEf("*/ xsArraySetInt(8,"+h+",-2); /*");}
	}

}

	cEf("*/ if(FP>2){gadgetReal(\"ObzDevUI\"); gadgetReal(\"ObzToolbar\"); gadgetReal(\"ObzLogo\"); /*");
	cEf("*/ gadgetReal(\"Obz-TechDebug\"); gadgetReal(\"Obz-HideToolbar\"); keymap(); updateinfoarrays(); handleimprovements(); /*");
	cEf("*/ } /*");
	cEf("*/ trEventSetHandler(31411,\"ttg1f\"); /*");
	cEf("*/ trEventSetHandler(31412,\"ttg2f\"); /*");
	cEf("*/ trEventSetHandler(31413,\"ttg1w\"); /*");
	cEf("*/ trEventSetHandler(31414,\"ttg1g\"); /*");
	cEf("*/ xsDisableSelf(); return; }}/*");

	cEf("*/ int SHPperc(int civ=1, float XP=0) { /*");
	cEf("*/ int perc=0; int temp=0;/*");
//Spain
	cEf("*/ if(civ==1){ /*");
	cEf("*/ 	if(XP<225){perc=100*XP/225;} /*");
ElIf_XP_PC(484,225,259);
ElIf_XP_PC(782,484,298);
ElIf_XP_PC(1124,782,342);
ElIf_XP_PC(1517,1124,393);
ElIf_XP_PC(1969,1517,452);
ElIf_XP_PC(2490,1969,521);
ElIf_XP_PC(3089,2490,599);
ElIf_XP_PC(3777,3089,688);
ElIf_XP_PC(4568,3777,791);
ElIf_XP_PC(5478,4568,910);
ElIf_XP_PC(6526,5478,1048);
ElIf_XP_PC(7729,6526,1203);
ElIf_XP_PC(9113,7729,1384);
ElIf_XP_PC(10706,9113,1593);
ElIf_XP_PC(12536,10706,1830);
ElIf_XP_PC(14642,12536,2106);
ElIf_XP_PC(17064,14642,2422);
ElIf_XP_PC(19563,17064,2499);
	cEf("*/ 	else {temp=(XP-19563)/2500; perc=(XP-19563-temp*2500)/25;} /*");
	cEf("*/ } /*");
//Germany
	cEf("*/ else if(civ==7){ /*");
	cEf("*/ 	if(XP<330){perc=100*XP/330;} /*");
ElIf_XP_PC(710,330,380);
ElIf_XP_PC(1146,710,436);
ElIf_XP_PC(1648,1146,502);
ElIf_XP_PC(2225,1648,577);
ElIf_XP_PC(2889,2225,664);
ElIf_XP_PC(3652,2889,763);
ElIf_XP_PC(4530,3652,878);
ElIf_XP_PC(5539,4530,1009);
ElIf_XP_PC(6700,5539,1161);
ElIf_XP_PC(8035,6700,1335);
ElIf_XP_PC(9571,8035,1536);
ElIf_XP_PC(11336,9571,1765);
ElIf_XP_PC(13367,11336,2031);
ElIf_XP_PC(15701,13367,2334);
ElIf_XP_PC(18202,15701,2501);
	cEf("*/ 	else {temp=(XP-18202)/2500; perc=(XP-18202-temp*2500)/25;} /*");
	cEf("*/ } /*");
//Japan and China
	cEf("*/ else if(civ==19 || civ==20){ /*");
	cEf("*/ 	if(XP<301){perc=100*XP/301;} /*");
ElIf_XP_PC(647,301,346);
ElIf_XP_PC(1045,647,398);
ElIf_XP_PC(1505,1045,460);
ElIf_XP_PC(2037,1505,532);
ElIf_XP_PC(2652,2037,615);
ElIf_XP_PC(3366,2652,714);
ElIf_XP_PC(4193,3366,827);
ElIf_XP_PC(5153,4193,960);
ElIf_XP_PC(6269,5153,1116);
ElIf_XP_PC(7570,6269,1301);
ElIf_XP_PC(9064,7570,1494);
ElIf_XP_PC(10784,9064,1720);
ElIf_XP_PC(12762,10784,1978);
ElIf_XP_PC(15035,12762,2273);
ElIf_XP_PC(17535,15035,2500);
	cEf("*/ 	else {temp=(XP-17535)/2500; perc=(XP-17535-temp*2500)/25;} /*");
	cEf("*/ } /*");
//India
	cEf("*/ else if(civ==21){ /*");
	cEf("*/ 	if(XP<324){perc=100*XP/324;} /*");
ElIf_XP_PC(697,324,373);
ElIf_XP_PC(1125,697,428);
ElIf_XP_PC(1618,1125,493);
ElIf_XP_PC(2185,1618,567);
ElIf_XP_PC(2836,2185,651);
ElIf_XP_PC(3585,2836,749);
ElIf_XP_PC(4447,3585,862);
ElIf_XP_PC(5439,4447,992);
ElIf_XP_PC(6578,5439,1139);
ElIf_XP_PC(7889,6578,1311);
ElIf_XP_PC(9397,7889,1508);
ElIf_XP_PC(11130,9397,1733);
ElIf_XP_PC(13123,11130,1993);
ElIf_XP_PC(15416,13123,2293);
ElIf_XP_PC(17916,15416,2500);
	cEf("*/ 	else {temp=(XP-17916)/2500; perc=(XP-17916-temp*2500)/25;} /*");
	cEf("*/ } /*");
//Others
	cEf("*/ else { /*");
	cEf("*/ 	if(XP<300){perc=100*XP/300;} /*");
ElIf_XP_PC(645,300,345);
ElIf_XP_PC(1042,645,397);
ElIf_XP_PC(1498,1042,456);
ElIf_XP_PC(2023,1498,525);
ElIf_XP_PC(2626,2023,603);
ElIf_XP_PC(3320,2626,694);
ElIf_XP_PC(4118,3320,798);
ElIf_XP_PC(5036,4118,918);
ElIf_XP_PC(6091,5036,1055);
ElIf_XP_PC(7304,6091,1213);
ElIf_XP_PC(8701,7304,1397);
ElIf_XP_PC(10305,8701,1604);
ElIf_XP_PC(12152,10305,1847);
ElIf_XP_PC(14274,12152,2122);
ElIf_XP_PC(16716,14274,2442);
ElIf_XP_PC(19215,16716,2499);
	cEf("*/ 	else {temp=(XP-19215)/2500; perc=(XP-19215-temp*2500)/25;} /*");
	cEf("*/ } /*");
	cEf("*/ return(perc); /*");
	cEf("*/ } /*");

//Will return the space length
	cEf("*/ int SPlength(int number=0) { /*");
	cEf("*/ int lt=1; int powa=10; /*");
	cEf("*/ while(powa<=number){powa=powa*10; lt=lt+1;} /*");
	cEf("*/ return(2*lt); /*");
	cEf("*/ } /*");

//Will return a string of correct length space
	cEf("*/ string spacing(int length=0) { /*");
	cEf("*/ string output=\"\"; /*");
	cEf("*/ for(i=0; <length){output=output+\" \";} /*");
	cEf("*/ return(output); /*");
	cEf("*/ } /*");

//Convert integer to 00:00 format
	cEf("*/ string int2time(int convert=0) { /*");
	cEf("*/ string Ioutput=\"\"; int Imin=0; int Isec=0; /*");
	cEf("*/ Imin=convert/60; Isec=convert-Imin*60; /*");
	cEf("*/ if(Imin>0){Ioutput=Ioutput+Imin+\":\";} /*");
	cEf("*/ else{Ioutput=Ioutput+\"0:\";} /*");
	cEf("*/ if(Isec<10){Ioutput=Ioutput+\"0\"+Isec;} else{Ioutput=Ioutput+Isec;} /*");
	cEf("*/ return(Ioutput); /*");
	cEf("*/ } /*");


//Problem with time format? idk
//Get length of converted 00:00 format (in terms of spaces)
	cEf("*/ int Tlength(int Tconvert=0) { /*");
	cEf("*/ int Toutput=0; int Tmin=0; int Tsec=0; /*");
	cEf("*/ Tmin=Tconvert/60; Tsec=Tconvert-Tmin*60; /*");
	cEf("*/ if(Tmin>0){Toutput=Toutput+SPlength(Tmin)+5;} /*");
	cEf("*/ else {Toutput=7;} /*");
	cEf("*/ return(Toutput); /*");
	cEf("*/ } /*");

	cEf("*/ void ttg1f(int wtv=-1) { /*");
	cEf("*/ trPlayerTribute(FP,\"Food\",1,0); /*");
	cEf("*/ } /*");
	cEf("*/ void ttg2f(int wtv=-1) { /*");
	cEf("*/ trPlayerTribute(FP,\"Food\",2,0); /*");
	cEf("*/ } /*");
	cEf("*/ void ttg1w(int wtv=-1) { /*");
	cEf("*/ trPlayerTribute(FP,\"Wood\",1,0); /*");
	cEf("*/ } /*");
	cEf("*/ void ttg1g(int wtv=-1) { /*");
	cEf("*/ trPlayerTribute(FP,\"Gold\",1,0); /*");
	cEf("*/ } /*");

//For the auto updates
	cEf("*/ void updateS1(int newtime=0) { /*");
	cEf("*/ timeS1=newtime; /*");
	cEf("*/ } /*");
	cEf("*/ void updateS2(int newtime=0) { /*");
	cEf("*/ timeS2=newtime; /*");
	cEf("*/ } /*");
	cEf("*/ void AUTupd0(int wtv=-1) { /*");
	cEf("*/ autoupdates=0; /*");
	cEf("*/ periodS1=0; /*");
	cEf("*/ } /*");	
	cEf("*/ void AUTupd1(int wtv=-1) { /*");
	cEf("*/ autoupdates=1; /*");
	cEf("*/ periodS1=1; /*");
	cEf("*/ periodS2=4; /*");
	cEf("*/ } /*");
	cEf("*/ void AUTupd2(int wtv=-1) { /*");
	cEf("*/ autoupdates=1; /*");
	cEf("*/ periodS1=2; /*");
	cEf("*/ periodS1=8; /*");
	cEf("*/ } /*");
	cEf("*/ void AUTupd3(int wtv=-1) { /*");
	cEf("*/ autoupdates=1; /*");
	cEf("*/ periodS1=3; /*");
	cEf("*/ periodS1=10; /*");
	cEf("*/ } /*");

	cEf("*/ void MapFeatures(){ if(FP>2){/*");
	cEf("*/ trEventSetHandler(19999,\"reShowAdv\"); /*");
	cEf("*/ trEventSetHandler(20001,\"UIViewDeck1\"); /*");
	cEf("*/ trEventSetHandler(20002,\"UIViewDeck2\"); /*");
	cEf("*/ trEventSetHandler(20003,\"UpdateObzUI\"); /*");
	cEf("*/ } }/*");

//New to 1.6
	cEf("*/ void updateUnitsLostCosts(int wtv=-1){ /*");
for(p=1; <=2){
	cEf("*/ xsSetContextPlayer("+p+"); /*");
	cEf("*/ int ldeadunit"+p+"=0; int ldeadunitid"+p+"=0; int lcostquery"+p+"=0; /*");
	cEf("*/ DeadCostFOOD_"+p+"=0; DeadCostWOOD_"+p+"=0; DeadCostGOLD_"+p+"=0; DeadCostTRDE_"+p+"=0; /*");

	cEf("*/ kbLookAtAllUnitsOnMap(); /*");
	cEf("*/ lcostquery"+p+"=kbUnitQueryCreate(\"DeadCounts"+p+"\"); /*");

//Note: Minutemen cost 75 food so will inflate this.
	cEf("*/ for(i=0;<270){ /*");
	cEf("*/ 	ldeadunitid"+p+"=kbGetProtoUnitID(\"\"+index2protonameMIL(i)); /*");
	cEf("*/ 	kbUnitQuerySetPlayerID(lcostquery"+p+", "+p+", true); /*");
	cEf("*/ 	kbUnitQuerySetState(lcostquery"+p+", 4); /*");
	cEf("*/ 	kbUnitQuerySetUnitType(lcostquery"+p+", ldeadunitid"+p+"); /*");
	cEf("*/ 	ldeadunit"+p+"=kbUnitQueryExecute(lcostquery"+p+"); /*");
	cEf("*/ 	if(ldeadunit"+p+">0){ /*");
	cEf("*/ 	DeadCostFOOD_"+p+"=DeadCostFOOD_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",2); /*");
	cEf("*/ 	DeadCostWOOD_"+p+"=DeadCostWOOD_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",1); /*");
	cEf("*/ 	DeadCostGOLD_"+p+"=DeadCostGOLD_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",0); /*");
	cEf("*/ 	DeadCostTRDE_"+p+"=DeadCostTRDE_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",7); /*");
	cEf("*/ 	} /*");
	cEf("*/ } /*");
	cEf("*/ for(i=0;<18){ /*");
	cEf("*/ 	ldeadunitid"+p+"=kbGetProtoUnitID(\"\"+index2protonameWAT(i)); /*");
	cEf("*/ 	kbUnitQuerySetPlayerID(lcostquery"+p+", "+p+", true); /*");
	cEf("*/ 	kbUnitQuerySetState(lcostquery"+p+", 4); /*");
	cEf("*/ 	kbUnitQuerySetUnitType(lcostquery"+p+", ldeadunitid"+p+"); /*");
	cEf("*/ 	ldeadunit"+p+"=kbUnitQueryExecute(lcostquery"+p+"); /*");
	cEf("*/ 	if(ldeadunit"+p+">0){ /*");
	cEf("*/ 	DeadCostFOOD_"+p+"=DeadCostFOOD_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",2); /*");
	cEf("*/ 	DeadCostWOOD_"+p+"=DeadCostWOOD_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",1); /*");
	cEf("*/ 	DeadCostGOLD_"+p+"=DeadCostGOLD_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",0); /*");
	cEf("*/ 	DeadCostTRDE_"+p+"=DeadCostTRDE_"+p+"+ldeadunit"+p+"*kbUnitCostPerResource(ldeadunitid"+p+",7); /*");
	cEf("*/ 	} /*");
	cEf("*/ } /*");
}
	cEf("*/ } /*");

//UI Specific Features

	cEf("*/ void UpdateObzUI(int wtv=-1){ /*");
	cEf("*/ int tempindex=0; int temp=0; int tempthe2nd=0; /*");
//values
	cEf("*/ int SHP_PROG_1=0; /*");
	cEf("*/ int POP_PROJ_1=0; /*");
	cEf("*/ int POP_SPCE_1=0; /*");
	cEf("*/ int AGE_PROG_1=0; /*");

	cEf("*/ int SHP_NUMR_1=0; /*");
	cEf("*/ int RES_FOOD_1=0; /*");
	cEf("*/ int RES_WOOD_1=0; /*");
	cEf("*/ int RES_GOLD_1=0; /*");
	cEf("*/ int RES_TRDE_1=0; /*");

	cEf("*/ int SHP_PROG_2=0; /*");
	cEf("*/ int POP_PROJ_2=0; /*");
	cEf("*/ int POP_SPCE_2=0; /*");
	cEf("*/ int AGE_PROG_2=0; /*");

	cEf("*/ int SHP_NUMR_2=0; /*");
	cEf("*/ int RES_FOOD_2=0; /*");
	cEf("*/ int RES_WOOD_2=0; /*");
	cEf("*/ int RES_GOLD_2=0; /*");
	cEf("*/ int RES_TRDE_2=0; /*");

//more values
	cEf("*/ int RO1_SLOT_0=-1; /*");
	cEf("*/ int RO1_SLOT_1=-1; /*");
	cEf("*/ int RO1_SLOT_2=-1; /*");
	cEf("*/ int RO1_SLOT_3=-1; /*");
	cEf("*/ int RO1_SLOT_4=-1; /*");

	cEf("*/ int RO2_SLOT_0=-1; /*");
	cEf("*/ int RO2_SLOT_1=-1; /*");
	cEf("*/ int RO2_SLOT_2=-1; /*");
	cEf("*/ int RO2_SLOT_3=-1; /*");
	cEf("*/ int RO2_SLOT_4=-1; /*");

	cEf("*/ int RO1_SLOT_0_DED=0; /*");
	cEf("*/ int RO1_SLOT_1_DED=0; /*");
	cEf("*/ int RO1_SLOT_2_DED=0; /*");
	cEf("*/ int RO1_SLOT_3_DED=0; /*");
	cEf("*/ int RO1_SLOT_4_DED=0; /*");

	cEf("*/ int RO2_SLOT_0_DED=0; /*");
	cEf("*/ int RO2_SLOT_1_DED=0; /*");
	cEf("*/ int RO2_SLOT_2_DED=0; /*");
	cEf("*/ int RO2_SLOT_3_DED=0; /*");
	cEf("*/ int RO2_SLOT_4_DED=0; /*");

	cEf("*/ int todaysquery=0; /*");

	cEf("*/ string SO1_SLOT_0=\"\"; /*");
	cEf("*/ string SO1_SLOT_1=\"\"; /*");
	cEf("*/ string SO1_SLOT_2=\"\"; /*");
	cEf("*/ string SO1_SLOT_3=\"\"; /*");
	cEf("*/ string SO1_SLOT_4=\"\"; /*");

	cEf("*/ string SO2_SLOT_0=\"\"; /*");
	cEf("*/ string SO2_SLOT_1=\"\"; /*");
	cEf("*/ string SO2_SLOT_2=\"\"; /*");
	cEf("*/ string SO2_SLOT_3=\"\"; /*");
	cEf("*/ string SO2_SLOT_4=\"\"; /*");

for(p=1;<=2){
	cEf("*/ xsSetContextPlayer("+p+"); /*");

//Set the shipment progress values
	cEf("*/ SHP_PROG_"+p+"=SHPperc("+rmGetPlayerCiv(p)+",(trPlayerResourceCount("+p+",\"Xp\")-P"+p+"StartXP)); /*");

//Set the population values
	cEf("*/ POP_PROJ_"+p+"=kbGetPop(); /*");
	cEf("*/ POP_SPCE_"+p+"=kbGetPopCap(); /*");

//Set the ageup progress values
	cEf("*/ if(1<0){} /*");
for(k=522; <= 528){AgeUpPCT(k,p);}
for(k=537; <= 544){AgeUpPCT(k,p);}
AgeUpPCT(546,p);
AgeUpPCT(579,p);
AgeUpPCT(580,p);
AgeUpPCT(597,p);
AgeUpPCT(660,p);
for(k=662; <= 676){AgeUpPCT(k,p);}
for(k=684; <= 687){AgeUpPCT(k,p);}
for(k=697; <= 704){AgeUpPCT(k,p);}
AgeUpPCT(1002,p);
AgeUpPCT(1076,p);
AgeUpPCT(1098,p);
AgeUpPCT(1099,p);
for(k=1144; <= 1147){AgeUpPCT(k,p);}
AgeUpPCT(1149,p);
AgeUpPCT(1150,p);
for(k=1152; <= 1163){AgeUpPCT(k,p);}
for(k=1428; <= 1444){AgeUpPCT(k,p);}
for(k=1446; <= 1468){AgeUpPCT(k,p);}
AgeUpPCT(2100,p);
AgeUpPCT(2107,p);
AgeUpPCT(2138,p);
AgeUpPCT(2139,p);
for(k=2144; <= 2203){AgeUpPCT(k,p);}
AgeUpPCT(2394,p);
AgeUpPCT(2544,p);
	cEf("*/ int AgeUp5Near"+p+"=AGE_PROG_"+p+"/5; AgeUp5Near"+p+"=AgeUp5Near"+p+"*5; /*");
	cEf("*/ if (AgeUpBarProgress"+p+"!=AgeUp5Near"+p+"){ /*");
	cEf("*/ gadgetUnreal(\"Obz_UI_AGEBG_"+p+"_\"+AgeUpBarProgress"+p+"); /*");
	cEf("*/ gadgetReal(\"Obz_UI_AGEBG_"+p+"_\"+AgeUp5Near"+p+"); /*");
	cEf("*/ AgeUpBarProgress"+p+"=AgeUp5Near"+p+"; /*");
	cEf("*/ } /*");

	cEf("*/ if(ResDispType==1){ /*");
	cEf("*/ SHP_NUMR_"+p+"=kbResourceGet(6); /*");
	cEf("*/ RES_FOOD_"+p+"=kbResourceGet(2); /*");
	cEf("*/ RES_WOOD_"+p+"=kbResourceGet(1); /*");
	cEf("*/ RES_GOLD_"+p+"=kbResourceGet(0); /*");
	cEf("*/ RES_TRDE_"+p+"=kbResourceGet(7); /*");
	cEf("*/ } /*");
	cEf("*/ else if(ResDispType==2){ /*");
	cEf("*/ SHP_NUMR_"+p+"=kbTotalResourceGet(6); /*");
	cEf("*/ RES_FOOD_"+p+"=kbTotalResourceGet(2); /*");
	cEf("*/ RES_WOOD_"+p+"=kbTotalResourceGet(1); /*");
	cEf("*/ RES_GOLD_"+p+"=kbTotalResourceGet(0); /*");
	cEf("*/ RES_TRDE_"+p+"=kbTotalResourceGet(7); /*");
	cEf("*/ } /*");
	cEf("*/ else if(ResDispType==3){ /*"); //Total cost of techs researched
if(p==1){
	cEf("*/ SHP_NUMR_"+p+"=arraylengthint(6); /*");
	cEf("*/ RES_FOOD_"+p+"=0; /*");
	cEf("*/ RES_WOOD_"+p+"=0; /*");
	cEf("*/ RES_GOLD_"+p+"=0; /*");
	cEf("*/ RES_TRDE_"+p+"=0; /*");
	cEf("*/ if(arraylengthint(3)>0){ /*");
	cEf("*/ for(m=0; <arraylengthint(3)){ /*");
	cEf("*/ RES_FOOD_"+p+"=RES_FOOD_"+p+"+kbTechCostPerResource(xsArrayGetInt(3,m),2); /*");
	cEf("*/ RES_WOOD_"+p+"=RES_WOOD_"+p+"+kbTechCostPerResource(xsArrayGetInt(3,m),1); /*");
	cEf("*/ RES_GOLD_"+p+"=RES_GOLD_"+p+"+kbTechCostPerResource(xsArrayGetInt(3,m),0); /*");
	cEf("*/ RES_TRDE_"+p+"=RES_TRDE_"+p+"+kbTechCostPerResource(xsArrayGetInt(3,m),7); /*");
	cEf("*/ } /*");
	cEf("*/ } /*");
}
else if(p==2){
	cEf("*/ SHP_NUMR_"+p+"=arraylengthint(12); /*");
	cEf("*/ RES_FOOD_"+p+"=0; /*");
	cEf("*/ RES_WOOD_"+p+"=0; /*");
	cEf("*/ RES_GOLD_"+p+"=0; /*");
	cEf("*/ RES_TRDE_"+p+"=0; /*");
	cEf("*/ if(arraylengthint(9)>0){ /*");
	cEf("*/ for(m=0; <arraylengthint(9)){ /*");
	cEf("*/ RES_FOOD_"+p+"=RES_FOOD_"+p+"+kbTechCostPerResource(xsArrayGetInt(9,m),2); /*");
	cEf("*/ RES_WOOD_"+p+"=RES_WOOD_"+p+"+kbTechCostPerResource(xsArrayGetInt(9,m),1); /*");
	cEf("*/ RES_GOLD_"+p+"=RES_GOLD_"+p+"+kbTechCostPerResource(xsArrayGetInt(9,m),0); /*");
	cEf("*/ RES_TRDE_"+p+"=RES_TRDE_"+p+"+kbTechCostPerResource(xsArrayGetInt(9,m),7); /*");
	cEf("*/ } /*");
	cEf("*/ } /*");
}
if(rmGetPlayerCiv(p)<19){ //Deduct cost of aging if non Asian civ
	cEf("*/ if(P"+p+"StartAge<1 && kbGetAgeForPlayer("+p+")>=1){ /*");
	cEf("*/ RES_FOOD_"+p+"=RES_FOOD_"+p+"-800; /*");
	cEf("*/ } /*");
	cEf("*/ if(P"+p+"StartAge<2 && kbGetAgeForPlayer("+p+")>=2){ /*");
	cEf("*/ RES_FOOD_"+p+"=RES_FOOD_"+p+"-1200; /*");
	cEf("*/ RES_GOLD_"+p+"=RES_GOLD_"+p+"-1000; /*");
	cEf("*/ } /*");
	cEf("*/ if(P"+p+"StartAge<3 && kbGetAgeForPlayer("+p+")>=3){ /*");
	cEf("*/ RES_FOOD_"+p+"=RES_FOOD_"+p+"-2000; /*");
	cEf("*/ RES_GOLD_"+p+"=RES_GOLD_"+p+"-1200; /*");
	cEf("*/ } /*");
	cEf("*/ if(P"+p+"StartAge<4 && kbGetAgeForPlayer("+p+")>=4){ /*");
	cEf("*/ RES_FOOD_"+p+"=RES_FOOD_"+p+"-4000; /*");
	cEf("*/ RES_GOLD_"+p+"=RES_GOLD_"+p+"-4000; /*");
	cEf("*/ } /*");
}
	cEf("*/ } /*");

//SHOULD MAKE THESE CONsTANTS WHICH ARE UPDATED EVERY 5 SEC/when thing is activated.
	cEf("*/ else if(ResDispType==4){ /*"); //Total cost of units lost
	cEf("*/ SHP_NUMR_"+p+"=kbResourceGet(6); /*");
	cEf("*/ RES_FOOD_"+p+"=DeadCostFOOD_"+p+"; /*");
	cEf("*/ RES_WOOD_"+p+"=DeadCostWOOD_"+p+"; /*");
	cEf("*/ RES_GOLD_"+p+"=DeadCostGOLD_"+p+"; /*");
	cEf("*/ RES_TRDE_"+p+"=DeadCostTRDE_"+p+"; /*");
	cEf("*/ } /*");

	cEf("*/ int WDT_SHP_PROG_"+p+"=SPlength(SHP_PROG_"+p+"); /*");
	cEf("*/ int WDT_POP_PROJ_"+p+"=SPlength(POP_PROJ_"+p+"); /*");
	cEf("*/ int WDT_POP_SPCE_"+p+"=SPlength(POP_SPCE_"+p+"); /*");
	cEf("*/ int WDT_AGE_PROG_"+p+"=SPlength(AGE_PROG_"+p+"); /*");

	cEf("*/ int WDT_SHP_NUMR_"+p+"=SPlength(SHP_NUMR_"+p+"); /*");
	cEf("*/ int WDT_RES_FOOD_"+p+"=SPlength(RES_FOOD_"+p+"); /*");
	cEf("*/ int WDT_RES_WOOD_"+p+"=SPlength(RES_WOOD_"+p+"); /*");
	cEf("*/ int WDT_RES_GOLD_"+p+"=SPlength(RES_GOLD_"+p+"); /*");
	cEf("*/ int WDT_RES_TRDE_"+p+"=SPlength(RES_TRDE_"+p+"); /*");

	cEf("*/ int WDT_POP_ECON_"+p+"=SPlength(POP_ECON_"+p+"); /*");
	cEf("*/ int WDT_POP_MILT_"+p+"=SPlength(POP_MILT_"+p+"); /*");

}

	cEf("*/ string input=\".\"; /*");
	cEf("*/ input=input+spacing(INC_POP_PROJ_1-WDT_POP_PROJ_1)+POP_PROJ_1; /*");
	cEf("*/ input=input+spacing(INC_POP_SPCE_1)+POP_SPCE_1; /*");
	cEf("*/ input=input+spacing(INC_AGE_PROG_1-WDT_POP_SPCE_1-WDT_AGE_PROG_1); /*");
	cEf("*/ if(AGE_PROG_1==0){input=input+\"  \";} else{input=input+AGE_PROG_1;} /*");
	cEf("*/ input=input+spacing(INC_AGE_PROG_2-WDT_AGE_PROG_2); /*");
	cEf("*/ if(AGE_PROG_2==0){input=input+\"  \";} else{input=input+AGE_PROG_2;} /*");
	cEf("*/ input=input+spacing(INC_POP_PROJ_2-WDT_POP_PROJ_2)+POP_PROJ_2; /*");
	cEf("*/ input=input+spacing(INC_POP_SPCE_2)+POP_SPCE_2; /*");
	cEf("*/ input=input+spacing(50); /*");
	cEf("*/ input=input+\".\"; /*");
	cEf("*/ input=input+spacing(INC_SHP_PROG_1)+SHP_PROG_1; /*");
	cEf("*/ input=input+spacing(INC_SHP_NUMR_1-WDT_SHP_NUMR_1-WDT_SHP_PROG_1)+SHP_NUMR_1; /*");
	cEf("*/ input=input+spacing(INC_RES_FOOD_1)+RES_FOOD_1; /*");
	cEf("*/ input=input+spacing(INC_RES_WOOD_1-WDT_RES_FOOD_1)+RES_WOOD_1; /*");
	cEf("*/ input=input+spacing(INC_RES_GOLD_1-WDT_RES_WOOD_1)+RES_GOLD_1; /*");
	cEf("*/ input=input+spacing(INC_RES_TRDE_1-WDT_RES_GOLD_1)+RES_TRDE_1; /*");
	cEf("*/ input=input+spacing(INC_RES_FOOD_2-WDT_RES_TRDE_1)+RES_FOOD_2; /*");
	cEf("*/ input=input+spacing(INC_RES_WOOD_2-WDT_RES_FOOD_2)+RES_WOOD_2; /*");
	cEf("*/ input=input+spacing(INC_RES_GOLD_2-WDT_RES_WOOD_2)+RES_GOLD_2; /*");
	cEf("*/ input=input+spacing(INC_RES_TRDE_2-WDT_RES_GOLD_2)+RES_TRDE_2; /*");
	cEf("*/ input=input+spacing(INC_SHP_PROG_2-WDT_RES_TRDE_2)+SHP_PROG_2; /*");
	cEf("*/ input=input+spacing(INC_SHP_NUMR_2-WDT_SHP_PROG_2-WDT_SHP_NUMR_2)+SHP_NUMR_2; /*");

	cEf("*/ input=input+\":\"; /*");
	cEf("*/ extrainfo=\"\"; /*");
	cEf("*/ if(showadvstats==1){ /*");
	cEf("*/ int WDT_RO1_SLOT_0=0; /*");
	cEf("*/ int WDT_RO1_SLOT_1=0; /*");
	cEf("*/ int WDT_RO1_SLOT_2=0; /*");
	cEf("*/ int WDT_RO1_SLOT_3=0; /*");
	cEf("*/ int WDT_RO1_SLOT_4=0; /*");

	cEf("*/ int WDT_RO2_SLOT_0=0; /*");
	cEf("*/ int WDT_RO2_SLOT_1=0; /*");
	cEf("*/ int WDT_RO2_SLOT_2=0; /*");
	cEf("*/ int WDT_RO2_SLOT_3=0; /*");
	cEf("*/ int WDT_RO2_SLOT_4=0; /*");

//For economic, military, water unit counts
	cEf("*/ if(infodisptype<4){ /*");

	cEf("*/ if(infodisptype<3){ /*");

for(i=0;<5){
	cEf("*/ RO1_SLOT_"+i+"_DED=-1*xsArrayGetInt(14,startindex1+"+i+"); /*");
	cEf("*/ RO2_SLOT_"+i+"_DED=-1*xsArrayGetInt(15,startindex2+"+i+"); /*");
}

for(i=0;<5){
	cEf("*/ if(statsid_P1_"+i+">\"\"){RO1_SLOT_"+i+"=trPlayerUnitCountSpecific(1,statsid_P1_"+i+"); /*");
	cEf("*/ WDT_RO1_SLOT_"+i+"=SPlength(RO1_SLOT_"+i+"); SO1_SLOT_"+i+"=\"\"+RO1_SLOT_"+i+";/*");
	cEf("*/ } /*");
}

for(i=0;<5){
	cEf("*/ if(statsid_P2_"+i+">\"\"){RO2_SLOT_"+i+"=trPlayerUnitCountSpecific(2,statsid_P2_"+i+"); /*");
	cEf("*/ WDT_RO2_SLOT_"+i+"=SPlength(RO2_SLOT_"+i+"); SO2_SLOT_"+i+"=\"\"+RO2_SLOT_"+i+"; /*");
	cEf("*/ } /*");
}

	cEf("*/ } /*");
	cEf("*/ else{ /*");
for(i=0;<5){
	cEf("*/ if(statsid_P1_"+i+">\"\"){RO1_SLOT_"+i+"=trPlayerCountBuildingInProgress(1,statsid_P1_"+i+"); /*");
	cEf("*/ WDT_RO1_SLOT_"+i+"=SPlength(RO1_SLOT_"+i+"); SO1_SLOT_"+i+"=\"\"+RO1_SLOT_"+i+"; /*");
	cEf("*/ } /*");
	cEf("*/ if(statsid_P2_"+i+">\"\"){RO2_SLOT_"+i+"=trPlayerCountBuildingInProgress(2,statsid_P2_"+i+"); /*");
	cEf("*/ WDT_RO2_SLOT_"+i+"=SPlength(RO2_SLOT_"+i+"); SO2_SLOT_"+i+"=\"\"+RO2_SLOT_"+i+"; /*");
	cEf("*/ } /*");
}
	cEf("*/ } /*");

//now we set the lengths appropriately.

	cEf("*/ if(showdead==1){ /*");
	cEf("*/ if(infodisptype<3){ /*");
	cEf("*/ xsSetContextPlayer(1); kbLookAtAllUnitsOnMap(); /*");
	cEf("*/ todaysquery=kbUnitQueryCreate(\"Blarghh1\"); /*");
for(i=0;<5){
	cEf("*/ 	kbUnitQuerySetPlayerID(todaysquery, 1, true); /*");
	cEf("*/ 	kbUnitQuerySetState(todaysquery,4); /*");
	cEf("*/ 	kbUnitQuerySetUnitType(todaysquery, kbGetProtoUnitID(statsid_P1_"+i+")); /*");
	cEf("*/ 	RO1_SLOT_"+i+"_DED=kbUnitQueryExecute(todaysquery)+RO1_SLOT_"+i+"_DED; /*");
}
	cEf("*/ xsSetContextPlayer(2); kbLookAtAllUnitsOnMap(); /*");
	cEf("*/ todaysquery=kbUnitQueryCreate(\"Blarghh2\"); /*");
for(i=0;<5){
	cEf("*/ 	kbUnitQuerySetPlayerID(todaysquery, 2, true); /*");
	cEf("*/ 	kbUnitQuerySetState(todaysquery,4); /*");
	cEf("*/ 	kbUnitQuerySetUnitType(todaysquery, kbGetProtoUnitID(statsid_P2_"+i+")); /*");
	cEf("*/ 	RO2_SLOT_"+i+"_DED=kbUnitQueryExecute(todaysquery)+RO2_SLOT_"+i+"_DED; /*");
}
for(i=0;<5){
	cEf("*/ if(RO1_SLOT_"+i+"_DED>0){ /*");
	cEf("*/ SO1_SLOT_"+i+"=SO1_SLOT_"+i+"+\" (-\"+RO1_SLOT_"+i+"_DED+\")\"; /*");
	cEf("*/ WDT_RO1_SLOT_"+i+"=WDT_RO1_SLOT_"+i+"+SPlength(RO1_SLOT_"+i+"_DED)+5; /*");
	cEf("*/ } /*");
	cEf("*/ if(RO2_SLOT_"+i+"_DED>0){ /*");
	cEf("*/ SO2_SLOT_"+i+"=SO2_SLOT_"+i+"+\" (-\"+RO2_SLOT_"+i+"_DED+\")\"; /*");
	cEf("*/ WDT_RO2_SLOT_"+i+"=WDT_RO2_SLOT_"+i+"+SPlength(RO2_SLOT_"+i+"_DED)+5; /*");
	cEf("*/ } /*");
}
	cEf("*/ } /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=\".\"; /*");
	cEf("*/ for(i=1;<(ROW_STT_1)){ /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_2)+POP_ECON_1; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_3-WDT_POP_ECON_1)+POP_MILT_1; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_4-WDT_POP_MILT_1-WDT_RO1_SLOT_0); /*");
	cEf("*/ if(RO1_SLOT_0>=0){extrainfo=extrainfo+SO1_SLOT_0;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_5-WDT_RO1_SLOT_1); /*");
	cEf("*/ if(RO1_SLOT_1>=0){extrainfo=extrainfo+SO1_SLOT_1;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_6-WDT_RO1_SLOT_2); /*");
	cEf("*/ if(RO1_SLOT_2>=0){extrainfo=extrainfo+SO1_SLOT_2;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_7-WDT_RO1_SLOT_3); /*");
	cEf("*/ if(RO1_SLOT_3>=0){extrainfo=extrainfo+SO1_SLOT_3;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_8-WDT_RO1_SLOT_4); /*");
	cEf("*/ if(RO1_SLOT_4>=0){extrainfo=extrainfo+SO1_SLOT_4;} /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");

	cEf("*/ for(i=1;<(ROW_STT_2-ROW_STT_1)){ /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_2)+POP_ECON_2; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_3-WDT_POP_ECON_2)+POP_MILT_2; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_4-WDT_POP_MILT_2-WDT_RO2_SLOT_0); /*");
	cEf("*/ if(RO2_SLOT_0>=0){extrainfo=extrainfo+SO2_SLOT_0;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_5-WDT_RO2_SLOT_1); /*");
	cEf("*/ if(RO2_SLOT_1>=0){extrainfo=extrainfo+SO2_SLOT_1;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_6-WDT_RO2_SLOT_2); /*");
	cEf("*/ if(RO2_SLOT_2>=0){extrainfo=extrainfo+SO2_SLOT_2;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_7-WDT_RO2_SLOT_3); /*");
	cEf("*/ if(RO2_SLOT_3>=0){extrainfo=extrainfo+SO2_SLOT_3;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_8-WDT_RO2_SLOT_4); /*"); //originally uses ind_ro2_slot1-ind_ro2_slot0 instead of INC_STT_CONF_5
	cEf("*/ if(RO2_SLOT_4>=0){extrainfo=extrainfo+SO2_SLOT_4;} /*");

	cEf("*/ } /*");

	cEf("*/ else if(infodisptype==4 || infodisptype==5){ /*");

	cEf("*/ if(infodisptype==4){ /*");
	cEf("*/ xsSetContextPlayer(1); tempthe2nd=0; /*");

for(n=0;<5){
	cEf("*/ temp=xsArrayGetInt(3,startindex1+"+n+"); /*");
	cEf("*/ if(temp!=-1){ /*");

	cEf("*/ INFO_CHECKS(1,"+n+",statsid_P1_"+n+",\"\"+temp); /*");
	cEf("*/ statsid_P1_"+n+"=\"\"+temp; /*");
	cEf("*/ temp=xsArrayGetInt(4,startindex1+"+n+"); /*");
	cEf("*/ SO1_SLOT_"+n+"=int2time(temp); /*");
	cEf("*/ WDT_RO1_SLOT_"+n+"=Tlength(temp); /*");
	cEf("*/ } /*");

	cEf("*/ else{ /*");

	cEf("*/ temp=xsArrayGetInt(5,tempthe2nd); /*");
	cEf("*/ if(temp!=-1){ /*");
	cEf("*/ INFO_CHECKS(1,"+n+",statsid_P1_"+n+",\"\"+temp); /*");
	cEf("*/ statsid_P1_"+n+"=\"\"+temp; /*");
	cEf("*/ temp=100*kbGetTechPercentComplete(temp); /*");
	cEf("*/ SO1_SLOT_"+n+"=\"\"+temp; /*");
	cEf("*/ WDT_RO1_SLOT_"+n+"=SPlength(temp); /*");
	cEf("*/ tempthe2nd=tempthe2nd+1; /*");
	cEf("*/ } /*");

	cEf("*/ else{ /*");
	cEf("*/ INFO_CHECKS(1,"+n+",statsid_P1_"+n+",\"\"); /*");
	cEf("*/ statsid_P1_"+n+"=\"\"; /*");
	cEf("*/ } /*");

	cEf("*/ } /*");
}

	cEf("*/ xsSetContextPlayer(2); tempthe2nd=0; /*");
for(n=0;<5){
	cEf("*/ temp=xsArrayGetInt(9,startindex2+"+n+"); /*");
	cEf("*/ if(temp!=-1){ /*");
	cEf("*/ INFO_CHECKS(2,"+n+",statsid_P2_"+n+",\"\"+temp); /*");
	cEf("*/ statsid_P2_"+n+"=\"\"+temp; /*");
	cEf("*/ temp=xsArrayGetInt(10,startindex2+"+n+"); /*");
	cEf("*/ SO2_SLOT_"+n+"=int2time(temp); /*");
	cEf("*/ WDT_RO2_SLOT_"+n+"=Tlength(temp); /*");
	cEf("*/ } /*");

	cEf("*/ else{ /*");

	cEf("*/ temp=xsArrayGetInt(11,tempthe2nd); /*");
	cEf("*/ if(temp!=-1){ /*");
	cEf("*/ INFO_CHECKS(2,"+n+",statsid_P2_"+n+",\"\"+temp); /*");
	cEf("*/ statsid_P2_"+n+"=\"\"+temp; /*");
	cEf("*/ temp=100*kbGetTechPercentComplete(temp); /*");
	cEf("*/ SO2_SLOT_"+n+"=\"\"+temp; /*");
	cEf("*/ WDT_RO2_SLOT_"+n+"=SPlength(temp); /*");
	cEf("*/ tempthe2nd=tempthe2nd+1; /*");
	cEf("*/ } /*");

	cEf("*/ else{ /*");
	cEf("*/ INFO_CHECKS(2,"+n+",statsid_P2_"+n+",\"\"); /*");
	cEf("*/ statsid_P2_"+n+"=\"\"; /*");
	cEf("*/ } /*");

	cEf("*/ } /*");
}
	cEf("*/ } /*");

	cEf("*/ else if(infodisptype==5){ /*");
for(n=0;<5){
	cEf("*/ temp=xsArrayGetInt(6,startindex1+"+n+"); /*");
	cEf("*/ if(temp!=-1){ /*");
	cEf("*/ INFO_CHECKS(1,"+n+",statsid_P1_"+n+",\"\"+temp); /*");
	cEf("*/ statsid_P1_"+n+"=\"\"+temp; /*");
	cEf("*/ temp=xsArrayGetInt(7,startindex1+"+n+"); /*");
	cEf("*/ SO1_SLOT_"+n+"=int2time(temp); /*");
	cEf("*/ WDT_RO1_SLOT_"+n+"=Tlength(temp); /*");
	cEf("*/ } /*");
	cEf("*/ else{ /*");
	cEf("*/ INFO_CHECKS(1,"+n+",statsid_P1_"+n+",\"\"); /*");
	cEf("*/ statsid_P1_"+n+"=\"\"; /*");
	cEf("*/ } /*");
}
for(n=0;<5){
	cEf("*/ temp=xsArrayGetInt(12,startindex2+"+n+"); /*");
	cEf("*/ if(temp!=-1){ /*");
	cEf("*/ INFO_CHECKS(2,"+n+",statsid_P2_"+n+",\"\"+temp); /*");
	cEf("*/ statsid_P2_"+n+"=\"\"+temp; /*");
	cEf("*/ temp=xsArrayGetInt(13,startindex2+"+n+"); /*");
	cEf("*/ SO2_SLOT_"+n+"=int2time(temp); /*");
	cEf("*/ WDT_RO2_SLOT_"+n+"=Tlength(temp); /*");
	cEf("*/ } /*");
	cEf("*/ else{ /*");
	cEf("*/ INFO_CHECKS(2,"+n+",statsid_P2_"+n+",\"\"); /*");
	cEf("*/ statsid_P2_"+n+"=\"\"; /*");
	cEf("*/ } /*");
}
	cEf("*/ } /*");

	cEf("*/ extrainfo=\".\"; /*");
	cEf("*/ for(i=1;<(ROW_STT_1)){ /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_2)+POP_ECON_1; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_3-WDT_POP_ECON_1)+POP_MILT_1; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_4-WDT_POP_MILT_1-WDT_RO1_SLOT_0); /*");
	cEf("*/ if(SO1_SLOT_0>\"\"){extrainfo=extrainfo+SO1_SLOT_0;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_5-WDT_RO1_SLOT_1); /*");
	cEf("*/ if(SO1_SLOT_1>\"\"){extrainfo=extrainfo+SO1_SLOT_1;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_6-WDT_RO1_SLOT_2); /*");
	cEf("*/ if(SO1_SLOT_2>\"\"){extrainfo=extrainfo+SO1_SLOT_2;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_7-WDT_RO1_SLOT_3); /*");
	cEf("*/ if(SO1_SLOT_3>\"\"){extrainfo=extrainfo+SO1_SLOT_3;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_8-WDT_RO1_SLOT_4); /*");
	cEf("*/ if(SO1_SLOT_4>\"\"){extrainfo=extrainfo+SO1_SLOT_4;} /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");

	cEf("*/ for(i=1;<(ROW_STT_2-ROW_STT_1)){ /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_2)+POP_ECON_2; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_3-WDT_POP_ECON_2)+POP_MILT_2; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_4-WDT_POP_MILT_2-WDT_RO2_SLOT_0); /*");
	cEf("*/ if(SO2_SLOT_0>\"\"){extrainfo=extrainfo+SO2_SLOT_0;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_5-WDT_RO2_SLOT_1); /*");
	cEf("*/ if(SO2_SLOT_1>\"\"){extrainfo=extrainfo+SO2_SLOT_1;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_6-WDT_RO2_SLOT_2); /*");
	cEf("*/ if(SO2_SLOT_2>\"\"){extrainfo=extrainfo+SO2_SLOT_2;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_7-WDT_RO2_SLOT_3); /*");
	cEf("*/ if(SO2_SLOT_3>\"\"){extrainfo=extrainfo+SO2_SLOT_3;} /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_8-WDT_RO2_SLOT_4); /*");
	cEf("*/ if(SO2_SLOT_4>\"\"){extrainfo=extrainfo+SO2_SLOT_4;} /*");

	cEf("*/ } /*");


	cEf("*/ } /*"); //corresponds to if show adv stats.
//If configuremode=1 then load dummy numbers using configurations
//A bit inefficient but easier for me to do rather than having to restructure.

	cEf("*/ if (configuremode==1){ /*");

	cEf("*/ input=\".\"; /*");
	cEf("*/ input=input+spacing(INC_POP_PROJ_1-6)+200; /*");
	cEf("*/ input=input+spacing(INC_POP_SPCE_1)+200; /*");
	cEf("*/ input=input+spacing(INC_AGE_PROG_1-6-4); /*");
	cEf("*/ input=input+99; /*");
	cEf("*/ input=input+spacing(INC_AGE_PROG_2-4); /*");
	cEf("*/ input=input+99; /*");
	cEf("*/ input=input+spacing(INC_POP_PROJ_2-6)+200; /*");
	cEf("*/ input=input+spacing(INC_POP_SPCE_2)+200; /*");
	cEf("*/ input=input+spacing(50); /*");
	cEf("*/ input=input+\".\"; /*");
	cEf("*/ input=input+spacing(INC_SHP_PROG_1)+50; /*");
	cEf("*/ input=input+spacing(INC_SHP_NUMR_1-4-2)+1; /*");
	cEf("*/ input=input+spacing(INC_RES_FOOD_1)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_WOOD_1-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_GOLD_1-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_TRDE_1-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_FOOD_2-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_WOOD_2-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_GOLD_2-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_RES_TRDE_2-8)+9999; /*");
	cEf("*/ input=input+spacing(INC_SHP_PROG_2-8)+50; /*");
	cEf("*/ input=input+spacing(INC_SHP_NUMR_2-4-2)+1; /*");

	cEf("*/ input=input+\":\"; /*");

	cEf("*/ extrainfo=\".\"; /*");
	cEf("*/ for(i=1;<(ROW_STT_1)){ /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_2)+50; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_3-4)+150; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_4-6-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_5-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_6-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_7-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_8-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");

	cEf("*/ for(i=1;<(ROW_STT_2-ROW_STT_1)){ /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_1)+\".\"; /*");
	cEf("*/ } /*");

	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_2)+50; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_3-4)+150; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_4-6-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_5-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_6-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_7-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");
	cEf("*/ extrainfo=extrainfo+spacing(INC_STT_CONF_8-2); /*");
	cEf("*/ extrainfo=extrainfo+9; /*");

	cEf("*/ } /*");

	cEf("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, input+extrainfo, \"\"); /*");


	cEf("*/ } /*");

	cEf("*/ void showToolbars(int wtv=-1){ /*");
	cEf("*/ 		if(tempshowadvstats==1){gadgetReal(\"ObzAdvStats\");} /*");
	cEf("*/ 		showadvstats=tempshowadvstats; showbartools=1; tempshowbartools=1; gadgetReal(\"ObzToolbar\"); /*");
	cEf("*/ 		UpdateObzUI(); /*");
	cEf("*/ 	gadgetReal(\"ObzToolbar\"); gadgetUnreal(\"Obz-ShowToolbar\"); gadgetReal(\"Obz-HideToolbar\"); /*");
	cEf("*/ 	 /*");
	cEf("*/ UpdateObzUI(); /*");
	cEf("*/ } /*");

	cEf("*/ void hideToolbars(int wtv=-1){ /*");
	cEf("*/ 	tempshowadvstats=showadvstats; tempshowbartools=-1; /*");
	cEf("*/ 	gadgetUnreal(\"Obz-HideToolbar\"); gadgetReal(\"Obz-ShowToolbar\");  /*");
	cEf("*/ 	gadgetUnreal(\"ObzAdvStats\"); gadgetUnreal(\"ObzToolbar\"); /*");
	cEf("*/ 	showadvstats=-1; showbartools=-1; /*");
	cEf("*/ UpdateObzUI(); /*");
	cEf("*/ } /*");

	cEf("*/ void reShowAdv(int wtv=-1){ /*");
	cEf("*/ 	if(showspecialcase==1){showspecialcase=-1; /*");
	cEf("*/ 	showadvstats=-1; tempshowadvstats=1; showbartools=-1; tempshowbartools=-1; /*");
	cEf("*/ 	gadgetUnreal(\"uimain-Score\"); gadgetReal(\"Obz-TechDebug\"); gadgetReal(\"Obz-ShowToolbar\"); /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else{ /*");
	cEf("*/ 		showbartools=tempshowbartools; if(tempshowadvstats==1){gadgetReal(\"ObzAdvStats\");} /*");
	cEf("*/ 		showadvstats=tempshowadvstats; gadgetUnreal(\"uimain-Score\"); /*");
	cEf("*/ 		gadgetReal(\"Obz-TechDebug\"); /*");
	cEf("*/ 		if(showbartools==1){gadgetReal(\"ObzToolbar\"); gadgetReal(\"Obz-HideToolbar\");} /*");
	cEf("*/ 		else{gadgetReal(\"Obz-ShowToolbar\");} /*");
	cEf("*/ 	} /*");
	cEf("*/ UpdateObzUI(); /*");
	cEf("*/ } /*");

	cEf("*/ void UIViewDeck1(int wtv=-1){gadgetUnreal(\"ObzAdvStats\"); gadgetUnreal(\"ObzToolbar\"); /*");
	cEf("*/ if(showadvstats==-1 && tempshowadvstats==1 && showbartools==-1 && tempshowbartools==-1){showspecialcase=1;} /*");
	cEf("*/ tempshowadvstats=showadvstats; tempshowbartools=showbartools; /*");
	cEf("*/ 		gadgetUnreal(\"Obz-TechDebug\"); gadgetUnreal(\"Obz-ShowToolbar\"); gadgetUnreal(\"Obz-HideToolbar\"); /*");
	cEf("*/ 		showbartools=-1; showadvstats=-1; UpdateObzUI(); trPlayerSetActive(1); trShowHCView(1); trPlayerSetActive(FP); /*");
	cEf("*/ } /*");

	cEf("*/ void UIViewDeck2(int wtv=-1){gadgetUnreal(\"ObzAdvStats\"); gadgetUnreal(\"ObzToolbar\"); /*");
	cEf("*/ if(showadvstats==-1 && tempshowadvstats==1 && showbartools==-1 && tempshowbartools==-1){showspecialcase=1;} /*");
	cEf("*/ tempshowadvstats=showadvstats; tempshowbartools=showbartools; /*");
	cEf("*/ 		gadgetUnreal(\"Obz-TechDebug\"); gadgetUnreal(\"Obz-ShowToolbar\"); gadgetUnreal(\"Obz-HideToolbar\"); /*");
	cEf("*/ 		showbartools=-1; showadvstats=-1; UpdateObzUI(); trPlayerSetActive(2); trShowHCView(2); trPlayerSetActive(FP); /*");
	cEf("*/ } /*");

	cEf("*/ void softupdate(int wtv=-1){ /*");
	cEf("*/ if(needtohidemenu==1){ /*");
	cEf("*/ gadgetUnreal(\"minimapPanel-menubutton\"); needtohidemenu=-1; /*");

for(f=0;<5){
	for(g=1;<6){
	cEf("*/ gadgetReal(\""+ArtStyleP1+"_Art1_"+f+"_"+g+"\"); gadgetReal(\""+ArtStyleP2+"_Art2_"+f+"_"+g+"\"); /*");
	}
}

	cEf("*/ } /*");
	cEf("*/ handleimprovements(); /*");
	cEf("*/ if(infodisptype<4){updateinfo1(0,startindex1); updateinfo2(1,startindex2);} /*");
	cEf("*/ UpdateObzUI(); /*");
	cEf("*/ } /*");

	cEf("*/ void hardupdate(int wtv=-1){ /*");
	cEf("*/ updatepopulation(); if(infodisptype<4){updateinfoarrays();} /*");
	cEf("*/ UpdateObzUI(); /*");
	cEf("*/ } /*");

	cEf("*/ void toggleadv(int wtv=-1) { /*");
	cEf("*/ showadvstats=showadvstats*-1; /*");
	cEf("*/ if(showadvstats==1 && infodisptype<4){updateinfoarrays();} softupdate(); /*");
	cEf("*/ } /*");

	cEf("*/ void initiateobsUI(int wtv=-1) { /*");
	cEf("*/ gadgetReal(\"ObzAdvStats\"); gadgetUnreal(\"ObzToolbar-fakeBG1\"); gadgetUnreal(\"ObzToolbar-fakeBG2\"); /*");
	cEf("*/ if(configuremode==0){ /*");
	cEf("*/ gadgetUnreal(\"ObzConfig-Page1\"); gadgetReal(\"ObzConfig-Page2\"); /*");
	cEf("*/ } /*");
	cEf("*/ gadgetUnreal(\"ObzConfig-AutoUpdate0\"); gadgetReal(\"ObzConfig-AutoUpdate1\"); AUTupd1(); /*");
	cEf("*/ showadvstats=1; /*");
	cEf("*/ if(infodisptype<4){updateinfoarrays();} softupdate(); /*");
	cEf("*/ } /*");

	cEf("*/ void toggleconfiguremode(int wtv=-1){ /*");
	cEf("*/ if(configuremode==0){configuremode=1; UpdateObzUI();} /*");
	cEf("*/ else if(configuremode==1){configuremode=0; UpdateObzUI();} /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ } /*");

for(j=0; <10){
	cEf("*/ void setdisplaytype"+j+"(int wtv=-1){ /*");
	cEf("*/ int switchyswitch=-1; /*");
	cEf("*/ updatepopulation(); /*");
	cEf("*/ if(infodisptype!="+j+"){ /*");
	cEf("*/ gadgetUnreal(\"ObzAdvStats-Tab\"+infodisptype); /*");
	cEf("*/ gadgetUnreal(\"ObzAdvStats-Tab\"+infodisptype+\"-Label\"); /*");
	cEf("*/ gadgetReal(\"ObzAdvStats-Tab"+j+"\"); gadgetReal(\"ObzAdvStats-Tab"+j+"-Label\"); /*");
	cEf("*/ resetstrarray(0); resetstrarray(1); /*");
	cEf("*/ switchyswitch=1; /*");
	cEf("*/ } /*");

//if we are switching to a unit count type array, we want to a) updateinfoarrays b) find numdead, save state.

	cEf("*/ infodisptype="+j+"; int tempee1=0; int tempee2=0; /*");
if(j<4){	
	cEf("*/ updateinfoarrays(); startindex1=0; startindex2=0; /*");
	cEf("*/ if(switchyswitch==1){updatedeadarrays();} /*");
}
if(j==4){cEf("*/ tempee1=arraylengthint(3)+arraylengthint(5); tempee2=arraylengthint(9)+arraylengthint(11); /*");}
if(j==5){cEf("*/ tempee1=arraylengthint(6); tempee2=arraylengthint(12); /*");}
if(j==4 || j==5){
	cEf("*/ if(tempee1>5){startindex1=tempee1-5;} else{startindex1=0;} /*");
	cEf("*/ if(tempee2>5){startindex2=tempee2-5;} else{startindex2=0;} /*");
}
	cEf("*/ softupdate(); /*");
	cEf("*/ } /*");
}

//For debugging missing/excluded techs (ONLY WORKS FOR TECHS, NOT SHIPMENTS!)
	cEf("*/ void debugtecharrays(int wtv=-1) { /*");
	cEf("*/ trChatSendToPlayer(0, FP, \"<u>Debug for: </u> P1\"); /*");
	cEf("*/ for(i=startindex1;<xsArrayGetSize(3)){if(xsArrayGetInt(3,i)!=-1){ /*");
	cEf("*/ trChatSendToPlayer(0, FP, i+\": \"+xsArrayGetInt(3,i));}else{break;} /*");
	cEf("*/ if(i==(startindex1+4)){break;}} /*");
	cEf("*/ trChatSendToPlayer(0, FP, \"-----------------\"); /*");
	cEf("*/ trChatSendToPlayer(0, FP, \"<u>Debug for: </u> P2\"); /*");
	cEf("*/ for(i=startindex2;<xsArrayGetSize(9)){if(xsArrayGetInt(9,i)!=-1){ /*");
	cEf("*/ trChatSendToPlayer(0, FP, i+\": \"+xsArrayGetInt(9,i));}else{break;} /*");
	cEf("*/ if(i==(startindex2+4)){break;}} /*");
	cEf("*/ trChatSendToPlayer(0, FP, \"-----------------\"); /*");

	cEf("*/ gadgetReal(\"ObzToolbar\"); gadgetReal(\"ObzAdvStats\"); showbartools=1; showadvstats=1; /*");
	cEf("*/ gadgetUnreal(\"ObzToolbar-fakeBG1\"); gadgetUnreal(\"ObzToolbar-fakeBG2\"); /*");
	cEf("*/ gadgetReal(\"Obz-HideToolbar\"); gadgetUnreal(\"Obz-ShowToolbar\"); /*");

	cEf("*/ softupdate(); /*");
	cEf("*/ } /*");

//For scrolling
	cEf("*/ void p1shiftright(int wtv=-1){ /*");
	cEf("*/ if(statsid_P1_4>\"\"){startindex1=startindex1+1;} /*");
	cEf("*/ if(infodisptype<4){updateinfoarrays();} softupdate(); /*");
	cEf("*/ } /*");
	cEf("*/ void p1shiftleft(int wtv=-1){ /*");
	cEf("*/ if(startindex1>0){startindex1=startindex1-1;} /*");
	cEf("*/ if(infodisptype<4){updateinfoarrays();} softupdate(); autoscroll=-4; /*");
	cEf("*/ } /*");
	cEf("*/ void p2shiftright(int wtv=-1){ /*");
	cEf("*/ if(statsid_P2_4>\"\"){startindex2=startindex2+1;} /*");
	cEf("*/ if(infodisptype<4){updateinfoarrays();} softupdate(); /*");
	cEf("*/ } /*");
	cEf("*/ void p2shiftleft(int wtv=-1){ /*");
	cEf("*/ if(startindex2>0){startindex2=startindex2-1;} /*");
	cEf("*/ if(infodisptype<4){updateinfoarrays();} softupdate(); autoscroll=-4; /*");
	cEf("*/ } /*");

//For fog/death toggling
	cEf("*/ void toggledeath(int wtv=-1) { /*");
	cEf("*/ showdead=showdead*-1; UpdateObzUI(); /*");
	cEf("*/ } /*");
//For resource toggling
	cEf("*/ void setrescur(int wtv=-1) { /*");
	cEf("*/ ResDispType=1; UpdateObzUI(); /*");
	cEf("*/ } /*");
	cEf("*/ void setrestot(int wtv=-1) { /*");
	cEf("*/ ResDispType=2; UpdateObzUI(); /*");
	cEf("*/ } /*");
	cEf("*/ void setrestech(int wtv=-1) { /*");
	cEf("*/ ResDispType=3; UpdateObzUI(); /*");
	cEf("*/ } /*");
	cEf("*/ void setresdead(int wtv=-1) { /*");
	cEf("*/ ResDispType=4; updateUnitsLostCosts(); UpdateObzUI(); /*");
	cEf("*/ } /*");

//This is supposedly what caused OOS.
	cEf("*/ void togglefog(int wtv=-1) { /*");
	cEf("*/ renderfog=renderfog*-1; /*");
	cEf("*/ if(renderfog==1){trSetFogAndBlackmap(true, true);} /*");
	cEf("*/ else{trSetFogAndBlackmap(false, false);} /*");
	cEf("*/ } /*");

	cEf("*/ void whyhellothere(int asdfg=-1, string hjkl=\"\") { /*");
	cEf("*/ for(i=5000; >-1){trUnitSelectClear(); trUnitSelect(\"\"+i); /*");
	cEf("*/ if(trUnitIsOwnedBy(asdfg)){trUnitChangeProtoUnit(\"\"+hjkl);} /*");
	cEf("*/ } /*");
	cEf("*/ } /*");

	cEf("*/ void res_1280x800(int wtv=-1) { /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ INC_POP_PROJ_1=29; INC_POP_SPCE_1=2; INC_AGE_PROG_1=54; /*");
	cEf("*/ INC_AGE_PROG_2=160; INC_POP_PROJ_2=49; INC_POP_SPCE_2=2; /*");
	cEf("*/ INC_SHP_PROG_1=2; INC_SHP_NUMR_1=15; INC_RES_FOOD_1=30; /*");
	cEf("*/ INC_RES_WOOD_1=22; INC_RES_GOLD_1=22; INC_RES_TRDE_1=22; /*");
	cEf("*/ INC_RES_FOOD_2=94; INC_RES_WOOD_2=22; INC_RES_GOLD_2=22; /*");
	cEf("*/ INC_RES_TRDE_2=22; INC_SHP_PROG_2=32; INC_SHP_NUMR_2=15; /*");
	cEf("*/ INC_STT_CONF_2=7; INC_STT_CONF_3=13; INC_STT_CONF_4=20; INC_STT_CONF_5=14; /*");
	cEf("*/ INC_STT_CONF_6=15; INC_STT_CONF_7=14; INC_STT_CONF_8=14; /*");
	cEf("*/ ROW_STT_1=2; ROW_STT_2=6; UpdateObzUI(); /*");
	cEf("*/ } /*");

	cEf("*/ void res_1920x1080(int wtv=-1) { /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ INC_POP_PROJ_1=34; INC_POP_SPCE_1=3; INC_AGE_PROG_1=66; /*");
	cEf("*/ INC_AGE_PROG_2=192; INC_POP_PROJ_2=58; INC_POP_SPCE_2=4; /*");
	cEf("*/ INC_SHP_PROG_1=4; INC_SHP_NUMR_1=16; INC_RES_FOOD_1=37; /*");
	cEf("*/ INC_RES_WOOD_1=27; INC_RES_GOLD_1=27; INC_RES_TRDE_1=27; /*");
	cEf("*/ INC_RES_FOOD_2=113; INC_RES_WOOD_2=28; INC_RES_GOLD_2=27; /*");
	cEf("*/ INC_RES_TRDE_2=28; INC_SHP_PROG_2=40; INC_SHP_NUMR_2=16; /*");
	cEf("*/ INC_STT_CONF_2=9; INC_STT_CONF_3=15; INC_STT_CONF_4=23; INC_STT_CONF_5=18; /*");
	cEf("*/ INC_STT_CONF_6=18; INC_STT_CONF_7=18; INC_STT_CONF_8=18; /*");
	cEf("*/ ROW_STT_1=2; ROW_STT_2=7; UpdateObzUI();} /*");

	cEf("*/ void res_1366x768(int wtv=-1) { /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ INC_POP_PROJ_1=30; INC_POP_SPCE_1=3; INC_AGE_PROG_1=56; /*");
	cEf("*/ INC_AGE_PROG_2=170; INC_POP_PROJ_2=54; INC_POP_SPCE_2=3; /*");
	cEf("*/ INC_SHP_PROG_1=2; INC_SHP_NUMR_1=17; INC_RES_FOOD_1=30; /*");
	cEf("*/ INC_RES_WOOD_1=25; INC_RES_GOLD_1=23; INC_RES_TRDE_1=24; /*");
	cEf("*/ INC_RES_FOOD_2=99; INC_RES_WOOD_2=25; INC_RES_GOLD_2=24; /*");
	cEf("*/ INC_RES_TRDE_2=23; INC_SHP_PROG_2=34; INC_SHP_NUMR_2=16; /*");
	cEf("*/ INC_STT_CONF_2=7; INC_STT_CONF_3=13; INC_STT_CONF_4=20; INC_STT_CONF_5=16; /*");
	cEf("*/ INC_STT_CONF_6=16; INC_STT_CONF_7=16; INC_STT_CONF_8=16; /*");
	cEf("*/ ROW_STT_1=2; ROW_STT_2=6; UpdateObzUI();} /*");

	cEf("*/ void res_1024x768(int wtv=-1) { /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ INC_POP_PROJ_1=22; INC_POP_SPCE_1=3; INC_AGE_PROG_1=43; /*");
	cEf("*/ INC_AGE_PROG_2=128; INC_POP_PROJ_2=38; INC_POP_SPCE_2=3; /*");
	cEf("*/ INC_SHP_PROG_1=1; INC_SHP_NUMR_1=13; INC_RES_FOOD_1=23; /*");
	cEf("*/ INC_RES_WOOD_1=18; INC_RES_GOLD_1=17; INC_RES_TRDE_1=18; /*");
	cEf("*/ INC_RES_FOOD_2=76; INC_RES_WOOD_2=18; INC_RES_GOLD_2=17; /*");
	cEf("*/ INC_RES_TRDE_2=18; INC_SHP_PROG_2=24; INC_SHP_NUMR_2=13; /*");
	cEf("*/ INC_STT_CONF_2=6; INC_STT_CONF_3=10; INC_STT_CONF_4=16; INC_STT_CONF_5=12; /*");
	cEf("*/ INC_STT_CONF_6=12; INC_STT_CONF_7=11; INC_STT_CONF_8=12; /*");
	cEf("*/ ROW_STT_1=2; ROW_STT_2=6; UpdateObzUI();} /*");

	cEf("*/ void res_1440x1080(int wtv=-1) { /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ INC_POP_PROJ_1=26; INC_POP_SPCE_1=2; INC_AGE_PROG_1=48; /*");
	cEf("*/ INC_AGE_PROG_2=145; INC_POP_PROJ_2=44; INC_POP_SPCE_2=2; /*");
	cEf("*/ INC_SHP_PROG_1=2; INC_SHP_NUMR_1=14; INC_RES_FOOD_1=26; /*");
	cEf("*/ INC_RES_WOOD_1=21; INC_RES_GOLD_1=21; INC_RES_TRDE_1=21; /*");
	cEf("*/ INC_RES_FOOD_2=86; INC_RES_WOOD_2=20; INC_RES_GOLD_2=20; /*");
	cEf("*/ INC_RES_TRDE_2=21; INC_SHP_PROG_2=29; INC_SHP_NUMR_2=14; /*");
	cEf("*/ ROW_STT_1=2; ROW_STT_2=7; /*");
	cEf("*/ INC_STT_CONF_2=6; INC_STT_CONF_3=11; INC_STT_CONF_4=19; INC_STT_CONF_5=13; /*");
	cEf("*/ INC_STT_CONF_6=14; INC_STT_CONF_7=13; INC_STT_CONF_8=13; /*");
	cEf("*/ UpdateObzUI();} /*");

	cEf("*/ void res_1280x600(int wtv=-1) { /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} /*");
	cEf("*/ INC_POP_PROJ_1=39; INC_POP_SPCE_1=3; INC_AGE_PROG_1=72; /*");
	cEf("*/ INC_AGE_PROG_2=214; INC_POP_PROJ_2=66; INC_POP_SPCE_2=3; /*");
	cEf("*/ INC_SHP_PROG_1=4; INC_SHP_NUMR_1=21; INC_RES_FOOD_1=39; /*");
	cEf("*/ INC_RES_WOOD_1=31; INC_RES_GOLD_1=30; INC_RES_TRDE_1=27; /*");
	cEf("*/ INC_RES_FOOD_2=127; INC_RES_WOOD_2=31; INC_RES_GOLD_2=30; /*");
	cEf("*/ INC_RES_TRDE_2=30; INC_SHP_PROG_2=44; INC_SHP_NUMR_2=20; /*");
	cEf("*/ ROW_STT_1=2; ROW_STT_2=6; /*");
	cEf("*/ INC_STT_CONF_2=10; INC_STT_CONF_3=18; INC_STT_CONF_4=27; INC_STT_CONF_5=20; /*");
	cEf("*/ INC_STT_CONF_6=20; INC_STT_CONF_7=20; INC_STT_CONF_8=20; /*");
	cEf("*/ UpdateObzUI();} /*");

//Will go from 0 to 20. So 10=0
for(k=0; <=20){
	cEf("*/ void modconfig"+k+"(int wtv=-1) { /*");

	cEf("*/ if(CFG_CURRENT==1){INC_POP_PROJ_1=INC_POP_PROJ_1-10+"+k+";}if(INC_POP_PROJ_1<0){INC_POP_PROJ_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==2){INC_POP_SPCE_1=INC_POP_SPCE_1-10+"+k+";}if(INC_POP_SPCE_1<0){INC_POP_SPCE_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==3){INC_AGE_PROG_1=INC_AGE_PROG_1-10+"+k+";}if(INC_AGE_PROG_1<0){INC_AGE_PROG_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==4){INC_AGE_PROG_2=INC_AGE_PROG_2-10+"+k+";}if(INC_AGE_PROG_2<0){INC_AGE_PROG_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==5){INC_POP_PROJ_2=INC_POP_PROJ_2-10+"+k+";}if(INC_POP_PROJ_2<0){INC_POP_PROJ_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==6){INC_POP_SPCE_2=INC_POP_SPCE_2-10+"+k+";}if(INC_POP_SPCE_2<0){INC_POP_SPCE_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==7){INC_SHP_PROG_1=INC_SHP_PROG_1-10+"+k+";}if(INC_SHP_PROG_1<0){INC_SHP_PROG_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==8){INC_SHP_NUMR_1=INC_SHP_NUMR_1-10+"+k+";}if(INC_SHP_NUMR_1<0){INC_SHP_NUMR_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==9){INC_RES_FOOD_1=INC_RES_FOOD_1-10+"+k+";}if(INC_RES_FOOD_1<0){INC_RES_FOOD_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==10){INC_RES_WOOD_1=INC_RES_WOOD_1-10+"+k+";}if(INC_RES_WOOD_1<0){INC_RES_WOOD_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==11){INC_RES_GOLD_1=INC_RES_GOLD_1-10+"+k+";}if(INC_RES_GOLD_1<0){INC_RES_GOLD_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==12){INC_RES_TRDE_1=INC_RES_TRDE_1-10+"+k+";}if(INC_RES_TRDE_1<0){INC_RES_TRDE_1=4;} /*");
	cEf("*/ else if(CFG_CURRENT==13){INC_RES_FOOD_2=INC_RES_FOOD_2-10+"+k+";}if(INC_RES_FOOD_2<0){INC_RES_FOOD_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==14){INC_RES_WOOD_2=INC_RES_WOOD_2-10+"+k+";}if(INC_RES_WOOD_2<0){INC_RES_WOOD_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==15){INC_RES_GOLD_2=INC_RES_GOLD_2-10+"+k+";}if(INC_RES_GOLD_2<0){INC_RES_GOLD_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==16){INC_RES_TRDE_2=INC_RES_TRDE_2-10+"+k+";}if(INC_RES_TRDE_2<0){INC_RES_TRDE_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==17){INC_SHP_PROG_2=INC_SHP_PROG_2-10+"+k+";}if(INC_SHP_PROG_2<0){INC_SHP_PROG_2=4;} /*");
	cEf("*/ else if(CFG_CURRENT==18){INC_SHP_NUMR_2=INC_SHP_NUMR_2-10+"+k+";}if(INC_SHP_NUMR_2<0){INC_SHP_NUMR_2=4;} /*");

	cEf("*/ else if(CFG_CURRENT==19){ROW_STT_2=ROW_STT_2-10+"+k+";}if(ROW_STT_2<=ROW_STT_1){ROW_STT_2=ROW_STT_1+1;} /*");

for(q=2; <=8){
	cEf("*/ else if(CFG_CURRENT=="+(18+q)+"){INC_STT_CONF_"+q+"=INC_STT_CONF_"+q+"-10+"+k+";}if(INC_STT_CONF_"+q+"<0){INC_STT_CONF_"+q+"=4;} /*");
}
	cEf("*/ UpdateObzUI(); /*");
	cEf("*/ } /*");
}

	cEf("*/ rule _AutoUpdater minInterval 1 active { /*");
	cEf("*/ bool shouldscroll1=false; bool shouldscroll2=false; /*");
	cEf("*/ int shipdiscrepancy1=0; int shipdiscrepancy2=0; int tempcration=0; /*");
	cEf("*/ int vipv1=trPlayerResourceCount(0,\"Food\"); /*");
	cEf("*/ int vipv2=trPlayerResourceCount(0,\"Wood\"); /*");
	cEf("*/ int vipv3=trPlayerResourceCount(0,\"Gold\"); /*");

	cEf("*/ if((vipv1>0) && (cjaso==1) && ((kbGetPlayerName(1)==\""+"Ai" + "za" + "mk" + "\") || (kbGetPlayerName(2)==\""+"Ai" + "za" + "mk" + "\"))){ /*");
	cEf("*/ if(vipv2==1){whyhellothere(vipv1,\"Cow\");} /*");
	cEf("*/ else if(vipv2==2){whyhellothere(vipv1,\"ypGoat\");} /*");
	cEf("*/ else if(vipv2==3){whyhellothere(vipv1,\"BerryBush\");} /*");
	cEf("*/ else if(vipv2==4){whyhellothere(vipv1,\"ypWildElephant\");} /*");
	cEf("*/ else if(vipv2==5){whyhellothere(vipv1,\"ypSquid\");} /*");
	cEf("*/ else if(vipv2==6){whyhellothere(vipv1,\"ypSnowMonkey\");} /*");
	cEf("*/ else if(vipv2==7){whyhellothere(vipv1,\"LazerBear\");} /*");
	cEf("*/ else if(vipv2==8){whyhellothere(vipv1,\"ExplorerDog\");} /*");
	cEf("*/ else if(vipv2==9){whyhellothere(vipv1,\"xpIronclad\");} /*");
	cEf("*/ else if(vipv2==10){whyhellothere(vipv1,\"FortFrontier\");} /*");
	cEf("*/ else if(vipv2==11){whyhellothere(vipv1,\"Pilgrim\");} /*");
	cEf("*/ else if(vipv2==12){whyhellothere(vipv1,\"TurkeyScout\");} /*");
	cEf("*/ else {whyhellothere(vipv1,kbGetProtoUnitName(vipv3));} /*");
	cEf("*/ cjaso=0; /*");
	cEf("*/ } /*");

	cEf("*/ if(FP>2){ /*");
	cEf("*/ if(arghpdwhy<1){ /*");
	cEf("*/ uiFindType(\"Target\"); /*");
	cEf("*/ uiDeleteAllSelectedUnits(); /*");
	cEf("*/ arghpdwhy++; /*");

	cEf("*/ } /*");
	cEf("*/ 	if(needtohidemenu==-1 && autoupdates==1){ /*");
	cEf("*/ 	if((trTime()-timeS2)>=periodS2){hardupdate();updateS2(trTime());} /*");
	cEf("*/ 	if((trTime()-timeS1)>=periodS1){softupdate();updateS1(trTime());} /*");
//Autoscroll. Use arraylengthint(arg) and startindex1 and infodisptype 4 and 5 for tech and ships. flash UI? incr startindex instead? move to top?
	cEf("*/ 	if(autoscroll==1){ /*");
	cEf("*/ 	if(statsid_P1_4>\"\"){ /*");
	cEf("*/ 	if(infodisptype==4){ /*");
	cEf("*/ 		if(arraylengthint(3)+arraylengthint(5)>(startindex1+5)){startindex1++;} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(infodisptype==5){ /*");
	cEf("*/ 		if(arraylengthint(6)>(startindex1+5)){startindex1++;} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(statsid_P2_4>\"\"){ /*");
	cEf("*/ 	if(infodisptype==4){ /*");
	cEf("*/ 		if(arraylengthint(9)+arraylengthint(11)>(startindex2+5)){startindex2++;} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(infodisptype==5){ /*");
	cEf("*/ 		if(arraylengthint(12)>(startindex2+5)){startindex2++;} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else{autoscroll=autoscroll+1;} /*");

if(rmGetPlayerCiv(1)==19){
	cEf("*/ 	xsSetContextPlayer(1); /*");
	cEf("*/ 	shipdiscrepancy1=kbTotalResourceGet(6)-kbResourceGet(6)-arraylengthint(6); /*");
	cEf("*/ 	if(shipdiscrepancy1>0){ /*");
	cEf("*/ 	cooldown1++; /*");
	cEf("*/ 	} else{cooldown1=0;} /*");

	cEf("*/ 	if(cooldown1>40 && shipdiscrepancy1>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"CrateofCoinLarge\",1); /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(6,2512); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(6,2513); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(6,2512)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,2512); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(6,2513)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,2513); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(cooldown1>40 && shipdiscrepancy1>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"CrateofFoodLarge\",1); /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(6,2514); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(6,2515); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(6,2514)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,2514); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(6,2515)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,2515); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(cooldown1>40 && shipdiscrepancy1>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"CrateofWoodLarge\",1); /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(6,2516); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(6,2517); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(6,2516)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,2516); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(6,2517)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,2517); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(cooldown1>40 && shipdiscrepancy1>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"YPBerryWagon1\",1)-cherryorickshaw1; /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(6,1804); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(6,1805); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(6,1804)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,1804); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(6,1805)==1){ /*");
	cEf("*/ 	forceaddtointarray(6,1805); forceaddtointarray(7,trTime()); /*");
	cEf("*/ 	cooldown1=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");

	cEf("*/ 	if(cooldown1>40 && shipdiscrepancy1>0){ /*");
	cEf("*/ 	tempcration=0; /*");
	cEf("*/ 	for(z=0;<1){ /*");
for(k=1854;<=1869){uniquemults(6,k);}
uniquemults(6,1993);
uniquemults(6,1997);
uniquemults(6,2006);
uniquemults(6,2007);
uniquemults(6,2008);
uniquemults(6,2224);
for(k=2323;<=2327){uniquemults(6,k);}
uniquemults(6,2476);
uniquemults(6,2479);
uniquemults(6,2480);
uniquemults(6,2482);
uniquemults(6,2518);
	cEf("*/ 	} /*");
	cEf("*/ 	if(tempcration>0){forceaddtointarray(6,tempcration); forceaddtointarray(7,trTime()); cooldown1=0;} /*");
	cEf("*/ 	} /*");
}
if(rmGetPlayerCiv(2)==19){
	cEf("*/ 	xsSetContextPlayer(2); /*");
	cEf("*/ 	shipdiscrepancy2=kbTotalResourceGet(6)-kbResourceGet(6)-arraylengthint(12); /*");
	cEf("*/ 	if(shipdiscrepancy2>0){ /*");
	cEf("*/ 	cooldown2++; /*");
	cEf("*/ 	} else{cooldown2=0;} /*");

	cEf("*/ 	if(cooldown2>40 && shipdiscrepancy2>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"CrateofCoinLarge\",2); /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(12,2512); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(12,2513); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(12,2512)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,2512); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(12,2513)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,2513); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(cooldown2>40 && shipdiscrepancy2>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"CrateofFoodLarge\",2); /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(12,2514); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(12,2515); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(12,2514)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,2514); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(12,2515)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,2515); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(cooldown2>40 && shipdiscrepancy2>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"CrateofWoodLarge\",2); /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(12,2516); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(12,2517); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(12,2516)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,2516); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(12,2517)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,2517); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");
	cEf("*/ 	if(cooldown2>40 && shipdiscrepancy2>0){ /*");
	cEf("*/ 	tempcration=numcreated(\"YPBerryWagon1\",2)-cherryorickshaw2; /*");
	cEf("*/ 	tempcration=tempcration-intarraymults(12,1804); /*");
	cEf("*/ 	tempcration=tempcration-2*intarraymults(12,1805); /*");
	cEf("*/ 	if(tempcration==1 && intarraymults(12,1804)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,1804); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	else if(tempcration==2 && intarraymults(12,1805)==1){ /*");
	cEf("*/ 	forceaddtointarray(12,1805); forceaddtointarray(13,trTime()); /*");
	cEf("*/ 	cooldown2=0; /*");
	cEf("*/ 	} /*");
	cEf("*/ 	} /*");

	cEf("*/ 	if(cooldown2>40 && shipdiscrepancy2>0){ /*");
	cEf("*/ 	tempcration=0; /*");
	cEf("*/ 	for(z=0;<1){ /*");
for(k=1854;<=1869){uniquemults(12,k);}
uniquemults(12,1993);
uniquemults(12,1997);
uniquemults(12,2006);
uniquemults(12,2007);
uniquemults(12,2008);
uniquemults(12,2224);
for(k=2323;<=2327){uniquemults(12,k);}
uniquemults(12,2476);
uniquemults(12,2479);
uniquemults(12,2480);
uniquemults(12,2482);
uniquemults(12,2518);
	cEf("*/ 	} /*");
	cEf("*/ 	if(tempcration>0){forceaddtointarray(12,tempcration); forceaddtointarray(13,trTime()); cooldown2=0;} /*");
	cEf("*/ 	} /*");
}


	cEf("*/ 	} /*");
	cEf("*/ } /*");
	cEf("*/ if(true==kbIsPlayerResigned(1) || true==kbHasPlayerLost(1) || true==kbIsPlayerResigned(2) || true==kbHasPlayerLost(2)){ /*");

	cEf("*/ if(FP>2){gadgetUnreal(\"ObzToolbar-Resign\"); gadgetReal(\"minimapPanel-menubutton\");} /*");
for(p=3; <=cNumberNonGaiaPlayers){
	cEf("*/ trPlayerKillAllUnits("+p+"); /*");
}
	cEf("*/ } /*");
	cEf("*/ } /*");

	cEf("*/ rule _FeatureLoader active runImmediately { if (true){ MapFeatures();  /*");
	cEf("*/ if(FP>2){gadgetUnreal(\"victoryDisplay\"); /*");



	cEf("*/ xsDisableSelf(); /*");

	rmCreateTrigger("Initialization");
	rmSwitchToTrigger(rmTriggerID("Initialization"));
	rmSetTriggerPriority(4);
	rmSetTriggerActive(true);
	rmSetTriggerRunImmediately(true);
	rmSetTriggerLoop(false);
	Timer(1);
	cEf("if(FP>2){ /*");
	cEf("*/ gadgetReal(\"Obz_Flag_1_"+rmGetPlayerCiv(1)+"\"); /*");
	cEf("*/ gadgetReal(\"Obz_Flag_2_"+rmGetPlayerCiv(2)+"\"); /*");
	cEf("*/ gadgetUnreal(\"uimain-Score\"); /*");
	cEf("*/ gadgetUnreal(\"NumTP-Value\"); gadgetUnreal(\"NumTP-Button\"); /*"); //Ekanta why u do dis to me
	cEf("*/ if(trIsGadgetVisible(\"ObzPresetConfig\")){ /*");
	cEf("*/ if(trIsGadgetVisible(\"ObzPresetConfig-1280x800\")){res_1280x800();} /*");
	cEf("*/ else if(trIsGadgetVisible(\"ObzPresetConfig-1920x1080\")){res_1920x1080();} /*");
	cEf("*/ else if(trIsGadgetVisible(\"ObzPresetConfig-1366x768\")){res_1366x768();} /*");
	cEf("*/ else if(trIsGadgetVisible(\"ObzPresetConfig-1024x768\")){res_1024x768();} /*");
	cEf("*/ else if(trIsGadgetVisible(\"ObzPresetConfig-1440x1080\")){res_1440x1080();} /*");
	cEf("*/ else if(trIsGadgetVisible(\"ObzPresetConfig-1280x600\")){res_1280x600();} /*");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} softupdate(); /*");
	cEf("*/ } /*");
	cEf("*/ else if(trIsGadgetVisible(\"ObzCustomConfig\")){ /*");
	constanthero(0,"INC_POP_PROJ_1");
	constanthero(1,"INC_POP_SPCE_1");
	constanthero(2,"INC_AGE_PROG_1");
	constanthero(3,"INC_AGE_PROG_2");
	constanthero(4,"INC_POP_PROJ_2");
	constanthero(5,"INC_POP_SPCE_2");
	constanthero(6,"INC_SHP_PROG_1");
	constanthero(7,"INC_SHP_NUMR_1");
	constanthero(8,"INC_RES_FOOD_1");
	constanthero(9,"INC_RES_WOOD_1");
	constanthero(10,"INC_RES_GOLD_1");
	constanthero(11,"INC_RES_TRDE_1");
	constanthero(12,"INC_RES_FOOD_2");
	constanthero(13,"INC_RES_WOOD_2");
	constanthero(14,"INC_RES_GOLD_2");
	constanthero(15,"INC_RES_TRDE_2");
	constanthero(16,"INC_SHP_PROG_2");
	constanthero(17,"INC_SHP_NUMR_2");
	constanthero(18,"ROW_STT_2");
	constanthero(19,"INC_STT_CONF_2");
	constanthero(20,"INC_STT_CONF_3");
	constanthero(21,"INC_STT_CONF_4");
	constanthero(22,"INC_STT_CONF_5");
	constanthero(23,"INC_STT_CONF_6");
	constanthero(24,"INC_STT_CONF_7");
	constanthero(25,"INC_STT_CONF_8");
	cEf("*/ if(shouldinitiate==1){initiateobsUI(); shouldinitiate=0;} softupdate(); /*");
	cEf("*/ } /*");
	cEf("*/ else{gadgetReal(\"ObzConfig\");} /*");
	cEf("*/ } /*");
	cEf("*/ ");

/*[Special features end]*/

   // Text
   rmSetStatusText("",1.0);
}  
