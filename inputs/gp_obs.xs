// OBS GREAT PLAINS Z v2 An edited version of the default ES great plains, with an added observer mode.
// DO NOT EDIT OR CLAIM AS OWN! MANY HOURS OF WORK WERE PUT INTO THIS.
// Made by Aizamk. An upgrade of the original obs maps made by pftq many many years ago.
// Special thanks to Mister SCP and Neur0n.
// If you have any suggestions, please send them to Aizamk@yahoo.com .
// If there are no problems with this release, I'll be starting the conversion to TAD!
// I hope to get that done by the end of August.
// Note that this map may fail (though this is not always the case) if you have custom triggers installed.

// Main entry point for random map script

include "mercenaries.xs";

/*[User defined functions start]*/

string bkslash="\\";
string fmt="	:						> ";

void custCondition(string xs="") {
        rmAddTriggerCondition("Timer");
        rmSetTriggerConditionParam("Param1","0) && ("+xs+");//");
}

void custEffect(string xs="") {
	rmAddTriggerEffect("SetIdleProcessing");
	 rmSetTriggerEffectParam("IdleProc", "true); "+xs+" trSetUnitIdleProcessing(true");
}

void Timer(float time=0) {
	rmAddTriggerCondition("Timer");
	rmSetTriggerConditionParam("Param1", ""+time);
}


/*[User defined functions end]*/

// Main entry point for random map script
void main(void)
{

/*[Randomness starts]*/
//Note: this is for defining unique rec game names.

string igae=""+rmGetPlayerCiv(1);
string agkf=""+rmGetPlayerCiv(2);
string fiaw=""+rmRandInt(0,9);
if(cNumberNonGaiaPlayers>2){fiaw=""+rmGetPlayerCiv(3);}

int motdset=rmRandInt(1,30);

/*[Randomness ends]*/

   // Text
   // These status text lines are used to manually animate the map generation progress bar
   rmSetStatusText("",0.01);

   //Chooses which natives appear on the map
   int subCiv0=-1;
   int subCiv1=-1;
   int subCiv2=-1;
   int subCiv3=-1;
   int subCiv4=-1;
   int subCiv5=-1;

	// Choose which variation to use.  1=southeast trade route, 2=northwest trade route
	int whichMap=rmRandInt(1,2);
	// int whichMap=2;

	// Are there extra meeting poles?
	int extraPoles=rmRandInt(1,2);
	extraPoles=1;
	// int extraPoles=2;

   if (rmAllocateSubCivs(6) == true)
   {
		subCiv0=rmGetCivID("Comanche");
		rmEchoInfo("subCiv0 is Comanche "+subCiv0);
		if (subCiv0 >= 0)
			rmSetSubCiv(0, "Comanche");

		subCiv1=rmGetCivID("Lakota");
		rmEchoInfo("subCiv1 is Lakota "+subCiv1);
		if (subCiv1 >= 0)
			rmSetSubCiv(1, "Lakota");

		subCiv2=rmGetCivID("Lakota");
		rmEchoInfo("subCiv2 is Lakota "+subCiv2);
		if (subCiv2 >= 0)
			rmSetSubCiv(2, "Lakota");
		
		subCiv3=rmGetCivID("Comanche");
		rmEchoInfo("subCiv3 is Comanche "+subCiv3);
		if (subCiv3 >= 0)
			rmSetSubCiv(3, "Comanche");
		
		subCiv4=rmGetCivID("Comanche");
		rmEchoInfo("subCiv4 is Comanche "+subCiv4);
		if (subCiv4 >= 0)
			rmSetSubCiv(4, "Comanche");
		
		subCiv5=rmGetCivID("Lakota");
		rmEchoInfo("subCiv5 is Lakota "+subCiv5);
		if (subCiv5 >= 0)
			rmSetSubCiv(5, "Lakota");
   }

   // Picks the map size
	int playerTiles=10500;

	int size=2.0*sqrt(2*playerTiles);
	rmEchoInfo("Map size="+size+"m x "+size+"m");
	rmSetMapSize(size, size);

	// Picks a default water height
	rmSetSeaLevel(0.0);

    // Picks default terrain and water
	rmSetMapElevationParameters(cElevTurbulence, 0.02, 7, 0.5, 8.0);
	rmSetBaseTerrainMix("great plains drygrass");
	rmTerrainInitialize("yukon\ground1_yuk", 5);
	rmSetLightingSet("great plains");
	rmSetMapType("greatPlains");
	rmSetMapType("land");
	rmSetWorldCircleConstraint(true);
	rmSetMapType("grass");

	chooseMercs();

	// Define some classes. These are used later for constraints.
	int classPlayer=rmDefineClass("player");
	rmDefineClass("classPatch");
	rmDefineClass("starting settlement");
	rmDefineClass("startingUnit");
	rmDefineClass("classForest");
	rmDefineClass("importantItem");
	rmDefineClass("secrets");
	rmDefineClass("natives");	
	rmDefineClass("classHillArea");
	rmDefineClass("socketClass");
	rmDefineClass("nuggets");

   // -------------Define constraints
   // These are used to have objects and areas avoid each other
   
   // Map edge constraints
   int playerEdgeConstraint=rmCreateBoxConstraint("player edge of map", rmXTilesToFraction(6), rmZTilesToFraction(6), 1.0-rmXTilesToFraction(6), 1.0-rmZTilesToFraction(6), 0.01);

   // Player constraints
   int playerConstraint=rmCreateClassDistanceConstraint("player vs. player", classPlayer, 10.0);
   int smallMapPlayerConstraint=rmCreateClassDistanceConstraint("stay away from players a lot", classPlayer, 70.0);
   int nuggetPlayerConstraint=rmCreateClassDistanceConstraint("nuggets stay away from players a lot", classPlayer, 40.0);

   // Resource avoidance
   int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 25.0);
   int coinForestConstraint=rmCreateClassDistanceConstraint("coin vs. forest", rmClassID("classForest"), 15.0);
   int avoidBison=rmCreateTypeDistanceConstraint("bison avoids food", "bison", 40.0);
   int avoidPronghorn=rmCreateTypeDistanceConstraint("pronghorn avoids food", "pronghorn", 35.0);
	int avoidCoin=rmCreateTypeDistanceConstraint("coin avoids coin", "gold", 35.0);
	int avoidStartingCoin=rmCreateTypeDistanceConstraint("starting coin avoids coin", "gold", 22.0);
   int avoidNugget=rmCreateTypeDistanceConstraint("nugget avoid nugget", "AbstractNugget", 40.0);
   int avoidNuggetSmall=rmCreateTypeDistanceConstraint("avoid nuggets by a little", "AbstractNugget", 10.0);

	int avoidFastCoin=-1;

	avoidFastCoin=rmCreateTypeDistanceConstraint("fast coin avoids coin", "gold", rmXFractionToMeters(0.16));


   // Avoid impassable land
   int avoidImpassableLand=rmCreateTerrainDistanceConstraint("avoid impassable land", "Land", false, 6.0);
   int shortAvoidImpassableLand=rmCreateTerrainDistanceConstraint("short avoid impassable land", "Land", false, 2.0);
   int patchConstraint=rmCreateClassDistanceConstraint("patch vs. patch", rmClassID("classPatch"), 5.0);

   // Unit avoidance - for things that aren't in the starting resources.
   int avoidStartingUnits=rmCreateClassDistanceConstraint("objects avoid starting units", rmClassID("startingUnit"), 30.0);
   int avoidStartingUnitsSmall=rmCreateClassDistanceConstraint("objects avoid starting units small", rmClassID("startingUnit"), 5.0);

   // Decoration avoidance
   int avoidAll=rmCreateTypeDistanceConstraint("avoid all", "all", 6.0);

   // VP avoidance
   int avoidTradeRoute = rmCreateTradeRouteDistanceConstraint("trade route", 6.0);
   int avoidTradeRouteSmall = rmCreateTradeRouteDistanceConstraint("trade route small", 4.0);
   // int avoidTradeRoutePlayer = rmCreateTradeRouteDistanceConstraint("trade route player", 30.0);
   int avoidImportantItem=rmCreateClassDistanceConstraint("important stuff avoids each other", rmClassID("importantItem"), 15.0);

	int avoidSocket=rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 8.0);
	int avoidSocketMore=rmCreateClassDistanceConstraint("bigger socket avoidance", rmClassID("socketClass"), 15.0);

   // Constraint to avoid water.
   int avoidWater4 = rmCreateTerrainDistanceConstraint("avoid water", "Land", false, 4.0);
   int avoidWater20 = rmCreateTerrainDistanceConstraint("avoid water medium", "Land", false, 20.0);
   int avoidWater40 = rmCreateTerrainDistanceConstraint("avoid water long", "Land", false, 40.0);

	// Avoid the hilly areas.
	int avoidHills = rmCreateClassDistanceConstraint("avoid Hills", rmClassID("classHillArea"), 5.0);
	
	// natives avoid natives
	int avoidNatives = rmCreateClassDistanceConstraint("avoid Natives", rmClassID("natives"), 40.0);
	int avoidNativesNuggets = rmCreateClassDistanceConstraint("nuggets avoid Natives", rmClassID("natives"), 20.0);

	int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.47), rmDegreesToRadians(0), rmDegreesToRadians(360));

   // Text
   rmSetStatusText("",0.10);
	
   // TRADE ROUTE PLACEMENT
   int tradeRouteID = rmCreateTradeRoute();

   int socketID=rmCreateObjectDef("sockets to dock Trade Posts");
   rmSetObjectDefTradeRouteID(socketID, tradeRouteID);

   rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
	rmSetObjectDefAllowOverlap(socketID, true);
   rmAddObjectDefToClass(socketID, rmClassID("socketClass"));
   rmSetObjectDefMinDistance(socketID, 0.0);
   rmSetObjectDefMaxDistance(socketID, 8.0);

	if ( whichMap == 1 )
	{
		rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.6);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.2, 0.25, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.2, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.8, 0.25, 10, 4);
	}
	else
	{
		rmAddTradeRouteWaypoint(tradeRouteID, 0.0, 0.4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.2, 0.75, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.5, 0.8, 10, 4);
		rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.8, 0.75, 10, 4);
	}

	rmAddRandomTradeRouteWaypoints(tradeRouteID, 1.0, 0.5, 20, 4);

   bool placedTradeRoute = rmBuildTradeRoute(tradeRouteID, "dirt");
   if(placedTradeRoute == false)
      rmEchoError("Failed to place trade route"); 
  
	// add the sockets along the trade route.
   vector socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.1);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.9);
   rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

	if ( extraPoles > 1 )
	{
	   socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.35);
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);

		socketLoc = rmGetTradeRouteWayPoint(tradeRouteID, 0.65);
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc);
	}
      
   // DEFINE AREAS
   // Set up player starting locations.
   for(i=2; <=7) {
	rmSetPlacementTeam(i);
	if ( whichMap == 1 )
   {
	   rmSetPlacementSection(0.7, 0.3); // 0.5
   }
   else
   {
	   rmSetPlacementSection(0.2, 0.8); // 0.5	   	
   }
   rmSetTeamSpacingModifier(0.7);
   rmPlacePlayersCircular(0.38, 0.38, 0);
	}
	rmSetPlacementTeam(-1);
	

   
   if ( whichMap == 1 )
   {
	   rmSetPlacementSection(0.7, 0.3); // 0.5
   }
   else
   {
	   rmSetPlacementSection(0.2, 0.8); // 0.5	   	
   }
   rmSetTeamSpacingModifier(0.7);
   rmPlacePlayersCircular(0.38, 0.38, 0);

   // Set up player areas.
   float playerFraction=rmAreaTilesToFraction(100);
   for(i=1; <3)
   {
      // Create the area.
      int id=rmCreateArea("Player"+i);
      // Assign to the player.
      rmSetPlayerArea(i, id);
      // Set the size.
      rmSetAreaSize(id, playerFraction, playerFraction);
      rmAddAreaToClass(id, classPlayer);
      rmSetAreaMinBlobs(id, 1);
      rmSetAreaMaxBlobs(id, 1);
      rmAddAreaConstraint(id, playerConstraint); 
      rmAddAreaConstraint(id, playerEdgeConstraint); 
	  rmAddAreaConstraint(id, avoidTradeRoute); 
      rmSetAreaLocPlayer(id, i);
      rmSetAreaWarnFailure(id, false);
   }

	int numTries = -1;
	int failCount = -1;

	// Text
   rmSetStatusText("",0.20);

	// Two hilly areas
   int hillNorthwestID=rmCreateArea("northwest hills");
   int avoidNW = rmCreateAreaDistanceConstraint("avoid nw", hillNorthwestID, 3.0);

   rmSetAreaSize(hillNorthwestID, 0.2, 0.2);
   rmSetAreaWarnFailure(hillNorthwestID, false);
   rmSetAreaSmoothDistance(hillNorthwestID, 10);
	rmSetAreaMix(hillNorthwestID, "great plains grass");
	rmAddAreaTerrainLayer(hillNorthwestID, "great_plains\ground8_gp", 0, 4);
   rmAddAreaTerrainLayer(hillNorthwestID, "great_plains\ground1_gp", 4, 10);
   rmSetAreaElevationType(hillNorthwestID, cElevTurbulence);
   rmSetAreaElevationVariation(hillNorthwestID, 6.0);
   rmSetAreaBaseHeight(hillNorthwestID, 5);
   rmSetAreaElevationMinFrequency(hillNorthwestID, 0.05);
   rmSetAreaElevationOctaves(hillNorthwestID, 3);
   rmSetAreaElevationPersistence(hillNorthwestID, 0.3);      
   rmSetAreaElevationNoiseBias(hillNorthwestID, 0.5);
   rmSetAreaElevationEdgeFalloffDist(hillNorthwestID, 20.0);
	rmSetAreaCoherence(hillNorthwestID, 0.9);
	rmSetAreaLocation(hillNorthwestID, 0.5, 0.9);
   rmSetAreaEdgeFilling(hillNorthwestID, 5);
	rmAddAreaInfluenceSegment(hillNorthwestID, 0.1, 0.95, 0.9, 0.95);
	rmSetAreaHeightBlend(hillNorthwestID, 1);
	rmSetAreaObeyWorldCircleConstraint(hillNorthwestID, false);
	rmAddAreaToClass(hillNorthwestID, rmClassID("classHillArea"));
	rmBuildArea(hillNorthwestID);

   int hillSoutheastID=rmCreateArea("southeast hills");
   int avoidSE = rmCreateAreaDistanceConstraint("avoid se", hillSoutheastID, 3.0);

   rmSetAreaSize(hillSoutheastID, 0.2, 0.2);
   rmSetAreaWarnFailure(hillSoutheastID, false);
   rmSetAreaSmoothDistance(hillSoutheastID, 10);
	rmSetAreaMix(hillSoutheastID, "great plains grass");
	rmAddAreaTerrainLayer(hillSoutheastID, "great_plains\ground8_gp", 0, 4);
   rmAddAreaTerrainLayer(hillSoutheastID, "great_plains\ground1_gp", 4, 10);
   rmSetAreaElevationType(hillSoutheastID, cElevTurbulence);
   rmSetAreaElevationVariation(hillSoutheastID, 6.0);
   rmSetAreaBaseHeight(hillSoutheastID, 5);
   rmSetAreaElevationMinFrequency(hillSoutheastID, 0.05);
   rmSetAreaElevationOctaves(hillSoutheastID, 3);
   rmSetAreaElevationPersistence(hillSoutheastID, 0.3);
   rmSetAreaElevationNoiseBias(hillSoutheastID, 0.5);
   rmSetAreaElevationEdgeFalloffDist(hillSoutheastID, 20.0);
	rmSetAreaCoherence(hillSoutheastID, 0.9);
	rmSetAreaLocation(hillSoutheastID, 0.5, 0.1);
   rmSetAreaEdgeFilling(hillSoutheastID, 5);
	rmAddAreaInfluenceSegment(hillSoutheastID, 0.1, 0.05, 0.9, 0.05);
	rmSetAreaHeightBlend(hillSoutheastID, 1);
	rmSetAreaObeyWorldCircleConstraint(hillSoutheastID, false);
	rmAddAreaToClass(hillSoutheastID, rmClassID("classHillArea"));
	rmBuildArea(hillSoutheastID);

   // Build the areas.
   // rmBuildAllAreas();

   // Text
   rmSetStatusText("",0.30);

   // Placement order of non-trade route stuff
   // Natives -> Secrets -> Nuggets -> Small Ponds (whee)


    //STARTING UNITS and RESOURCES DEFS
	int startingUnits = rmCreateStartingUnitsObjectDef(5.0);
	rmSetObjectDefMinDistance(startingUnits, 7.0);
	rmSetObjectDefMaxDistance(startingUnits, 12.0);

	int startingTCID = rmCreateObjectDef("startingTC");
	if ( rmGetNomadStart())
	{
		rmAddObjectDefItem(startingTCID, "CoveredWagon", 1, 0.0);
	}
	else
	{
		rmAddObjectDefItem(startingTCID, "TownCenter", 1, 0.0);
	}
	rmAddObjectDefToClass(startingTCID, rmClassID("startingUnit"));
	rmSetObjectDefMinDistance(startingTCID, 0.0);
	rmSetObjectDefMaxDistance(startingTCID, 9.0);
	rmAddObjectDefConstraint(startingTCID, avoidTradeRouteSmall);

	// rmAddObjectDefConstraint(startingTCID, avoidTradeRoute);

	int StartAreaTreeID=rmCreateObjectDef("starting trees");
	rmAddObjectDefItem(StartAreaTreeID, "TreeGreatPlains", 1, 0.0);
	rmSetObjectDefMinDistance(StartAreaTreeID, 12.0);
	rmSetObjectDefMaxDistance(StartAreaTreeID, 18.0);
	rmAddObjectDefConstraint(StartAreaTreeID, avoidStartingUnitsSmall);

	int StartBisonID=rmCreateObjectDef("starting bison");
	rmAddObjectDefItem(StartBisonID, "Bison", 4, 4.0);
	rmSetObjectDefMinDistance(StartBisonID, 10.0);
	rmSetObjectDefMaxDistance(StartBisonID, 12.0);
	rmSetObjectDefCreateHerd(StartBisonID, true);
	rmAddObjectDefConstraint(StartBisonID, avoidStartingUnitsSmall);

	int playerNuggetID=rmCreateObjectDef("player nugget");
	rmAddObjectDefItem(playerNuggetID, "nugget", 1, 0.0);
	rmAddObjectDefToClass(playerNuggetID, rmClassID("nuggets"));
	rmAddObjectDefToClass(playerNuggetID, rmClassID("startingUnit"));
    rmSetObjectDefMinDistance(playerNuggetID, 28.0);
    rmSetObjectDefMaxDistance(playerNuggetID, 35.0);
	rmAddObjectDefConstraint(playerNuggetID, avoidNugget);
	rmAddObjectDefConstraint(playerNuggetID, avoidNativesNuggets);
	rmAddObjectDefConstraint(playerNuggetID, avoidTradeRouteSmall);
	rmAddObjectDefConstraint(playerNuggetID, circleConstraint);
	// rmAddObjectDefConstraint(playerNuggetID, avoidImportantItem);

	rmSetStatusText("",0.40);
	
	int silverType = -1;
	int playerGoldID = -1;

 	for(i=1; <3)
	{
		rmPlaceObjectDefAtLoc(startingTCID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		// vector closestPoint=rmGetUnitPosition(rmGetUnitPlacedOfPlayer(startingUnits, i));
		// rmSetHomeCityGatherPoint(i, closestPoint);

		// Everyone gets two ore ObjectDefs, one pretty close, the other a little further away.
		silverType = rmRandInt(1,10);
		playerGoldID = rmCreateObjectDef("player silver closer "+i);
		rmAddObjectDefItem(playerGoldID, "mine", 1, 0.0);
		// rmAddObjectDefToClass(playerGoldID, rmClassID("importantItem"));
		rmAddObjectDefConstraint(playerGoldID, avoidTradeRoute);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingCoin);
		rmAddObjectDefConstraint(playerGoldID, avoidStartingUnitsSmall);
		rmSetObjectDefMinDistance(playerGoldID, 15.0);
		rmSetObjectDefMaxDistance(playerGoldID, 20.0);
		
		// Place two gold mines
		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerGoldID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		// Placing starting trees...
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(StartAreaTreeID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmPlaceObjectDefAtLoc(StartBisonID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));

		rmSetNuggetDifficulty(1, 1);
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(playerNuggetID, 0, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
	}

	// NATIVE AMERICANS
   float NativeVillageLoc = rmRandFloat(0,1); //

   if (subCiv0 == rmGetCivID("Comanche"))
   {  
      int comancheVillageAID = -1;
      int comancheVillageType = rmRandInt(1,5);
      comancheVillageAID = rmCreateGrouping("comanche village A", "native comanche village "+comancheVillageType);
      rmSetGroupingMinDistance(comancheVillageAID, 0.0);
      rmSetGroupingMaxDistance(comancheVillageAID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(comancheVillageAID, avoidImpassableLand);
      rmAddGroupingToClass(comancheVillageAID, rmClassID("importantItem"));
      rmAddGroupingToClass(comancheVillageAID, rmClassID("natives"));
      rmAddGroupingConstraint(comancheVillageAID, avoidNatives);
      rmAddGroupingConstraint(comancheVillageAID, avoidTradeRoute);
	  rmAddGroupingConstraint(comancheVillageAID, avoidStartingUnits);
		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(comancheVillageAID, 0, 0.5, 0.5);
		}
		else
		{
			rmPlaceGroupingAtLoc(comancheVillageAID, 0, 0.35, 0.15);
		}
	}

   if (subCiv1 == rmGetCivID("Lakota"))
   {   
      int lakotaVillageAID = -1;
      int lakotaVillageType = rmRandInt(1,5);
      lakotaVillageAID = rmCreateGrouping("lakota village A", "native lakota village "+lakotaVillageType);
      rmSetGroupingMinDistance(lakotaVillageAID, 0.0);
      rmSetGroupingMaxDistance(lakotaVillageAID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(lakotaVillageAID, avoidImpassableLand);
      rmAddGroupingToClass(lakotaVillageAID, rmClassID("importantItem"));
      rmAddGroupingToClass(lakotaVillageAID, rmClassID("natives"));
      rmAddGroupingConstraint(lakotaVillageAID, avoidNatives);
      rmAddGroupingConstraint(lakotaVillageAID, avoidTradeRoute);
	  rmAddGroupingConstraint(lakotaVillageAID, avoidStartingUnits);
		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(lakotaVillageAID, 0, 0.7, 0.6); 
		}
		else
		{
			rmPlaceGroupingAtLoc(lakotaVillageAID, 0, 0.65, 0.15); 
		}
	}

   // Text
   rmSetStatusText("",0.50);
	if(subCiv2 == rmGetCivID("Lakota"))
   {   
      int lakotaVillageID = -1;
      lakotaVillageType = rmRandInt(1,5);
      lakotaVillageID = rmCreateGrouping("lakota village", "native lakota village "+lakotaVillageType);
      rmSetGroupingMinDistance(lakotaVillageID, 0.0);
      rmSetGroupingMaxDistance(lakotaVillageID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(lakotaVillageID, avoidImpassableLand);
      rmAddGroupingToClass(lakotaVillageID, rmClassID("importantItem"));
      rmAddGroupingToClass(lakotaVillageID, rmClassID("natives"));
      rmAddGroupingConstraint(lakotaVillageID, avoidNatives);
		rmAddGroupingConstraint(lakotaVillageID, avoidTradeRoute);
		rmAddGroupingConstraint(lakotaVillageID, avoidStartingUnits);
		if ( extraPoles == 1 )
		{
			if ( whichMap == 1 )
			{
				rmPlaceGroupingAtLoc(lakotaVillageID, 0, 0.2, 0.8);
			}
			else
			{
				rmPlaceGroupingAtLoc(lakotaVillageID, 0, 0.15, 0.85);
			}
		}
   }

	if(subCiv3 == rmGetCivID("Comanche"))
   {   
      int comancheVillageBID = -1;
      comancheVillageType = rmRandInt(1,5);
      comancheVillageBID = rmCreateGrouping("comanche village B", "native comanche village "+comancheVillageType);
      rmSetGroupingMinDistance(comancheVillageBID, 0.0);
      rmSetGroupingMaxDistance(comancheVillageBID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(comancheVillageBID, avoidImpassableLand);
      rmAddGroupingToClass(comancheVillageBID, rmClassID("importantItem"));
      rmAddGroupingToClass(comancheVillageBID, rmClassID("natives"));
      rmAddGroupingConstraint(comancheVillageBID, avoidNatives);
	  rmAddGroupingConstraint(comancheVillageBID, avoidTradeRoute);
	  rmAddGroupingConstraint(comancheVillageBID, avoidStartingUnits);
		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(comancheVillageBID, 0, 0.3, 0.6);
		}
		else
		{
			rmPlaceGroupingAtLoc(comancheVillageBID, 0, 0.35, 0.45);
		}
   }

	if(subCiv4 == rmGetCivID("Comanche"))
   {
      int comancheVillageID = -1;
      comancheVillageType = rmRandInt(1,5);
      comancheVillageID = rmCreateGrouping("comanche village", "native comanche village "+comancheVillageType);
      rmSetGroupingMinDistance(comancheVillageID, 0.0);
	  rmSetGroupingMaxDistance(comancheVillageID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(comancheVillageID, avoidImpassableLand);
      rmAddGroupingToClass(comancheVillageID, rmClassID("importantItem"));
      rmAddGroupingToClass(comancheVillageID, rmClassID("natives"));
      rmAddGroupingConstraint(comancheVillageID, avoidNatives);
      rmAddGroupingConstraint(comancheVillageID, avoidTradeRoute);
	  rmAddGroupingConstraint(comancheVillageID, avoidStartingUnits);
		if ( extraPoles == 1 )
		{
			if ( whichMap == 1 )
			{
				rmPlaceGroupingAtLoc(comancheVillageID, 0, 0.8, 0.8);
			}
			else
			{
				rmPlaceGroupingAtLoc(comancheVillageID, 0, 0.85, 0.85);
			}
		}
	
	}

   if (subCiv5 == rmGetCivID("Lakota"))
   {   
      int lakotaVillageBID = -1;
      lakotaVillageType = rmRandInt(1,5);
      lakotaVillageBID = rmCreateGrouping("lakota village B", "native lakota village "+lakotaVillageType);
      rmSetGroupingMinDistance(lakotaVillageBID, 0.0);
      rmSetGroupingMaxDistance(lakotaVillageBID, rmXFractionToMeters(0.1));
      rmAddGroupingConstraint(lakotaVillageBID, avoidImpassableLand);
      rmAddGroupingToClass(lakotaVillageBID, rmClassID("importantItem"));
      rmAddGroupingToClass(lakotaVillageBID, rmClassID("natives"));
      rmAddGroupingConstraint(lakotaVillageBID, avoidNatives);
      rmAddGroupingConstraint(lakotaVillageBID, avoidTradeRoute);
	  rmAddGroupingConstraint(lakotaVillageBID, avoidStartingUnits);

		if ( whichMap == 1 )
		{
			rmPlaceGroupingAtLoc(lakotaVillageBID, 0, 0.5, 0.9);
		}
		else
		{
			rmPlaceGroupingAtLoc(lakotaVillageBID, 0, 0.65, 0.45);
		}
   }

   // Text
   rmSetStatusText("",0.60);

   // Define and place Nuggets
	int nuggetID= rmCreateObjectDef("nugget"); 
	rmAddObjectDefItem(nuggetID, "Nugget", 1, 0.0);
	rmSetObjectDefMinDistance(nuggetID, 0.0);
	rmSetObjectDefMaxDistance(nuggetID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(nuggetID, shortAvoidImpassableLand);
  	rmAddObjectDefConstraint(nuggetID, avoidNugget);
	rmAddObjectDefConstraint(nuggetID, nuggetPlayerConstraint);
  	rmAddObjectDefConstraint(nuggetID, playerConstraint);
  	rmAddObjectDefConstraint(nuggetID, avoidTradeRoute);
	rmAddObjectDefConstraint(nuggetID, avoidSocketMore);
	rmAddObjectDefConstraint(nuggetID, avoidNativesNuggets);
	rmAddObjectDefConstraint(nuggetID, circleConstraint);
  	rmAddObjectDefConstraint(nuggetID, avoidAll);
	rmSetNuggetDifficulty(1, 3);
	rmPlaceObjectDefAtLoc(nuggetID, 0, 0.5, 0.5, 8);
   
   // Text
   rmSetStatusText("",0.70); 

	// Ponds o' Fun
	int pondClass=rmDefineClass("pond");
   int pondConstraint=rmCreateClassDistanceConstraint("ponds avoid ponds", rmClassID("pond"), 60.0);

   // int numPonds=rmRandInt(1,4);
	int numPonds=4;
   for(i=0; <numPonds)
   {
      int smallPondID=rmCreateArea("small pond"+i);
      rmSetAreaSize(smallPondID, rmAreaTilesToFraction(170), rmAreaTilesToFraction(200));
      rmSetAreaWaterType(smallPondID, "great plains pond");
      rmSetAreaBaseHeight(smallPondID, 4);
      rmSetAreaMinBlobs(smallPondID, 1);
      rmSetAreaMaxBlobs(smallPondID, 5);
      rmSetAreaMinBlobDistance(smallPondID, 5.0);
      rmSetAreaMaxBlobDistance(smallPondID, 70.0);
      rmAddAreaToClass(smallPondID, pondClass);
      rmSetAreaCoherence(smallPondID, 0.5);
      rmSetAreaSmoothDistance(smallPondID, 5);
      rmAddAreaConstraint(smallPondID, pondConstraint);
      rmAddAreaConstraint(smallPondID, playerConstraint);
      rmAddAreaConstraint(smallPondID, avoidTradeRoute);
		rmAddAreaConstraint(smallPondID, avoidSocket);
		rmAddAreaConstraint(smallPondID, avoidImportantItem);
		rmAddAreaConstraint(smallPondID, avoidNW);
		rmAddAreaConstraint(smallPondID, avoidSE);
		rmAddAreaConstraint(smallPondID, avoidStartingUnits);
		rmAddAreaConstraint(smallPondID, avoidNuggetSmall);
      rmSetAreaWarnFailure(smallPondID, false);
      rmBuildArea(smallPondID);
   }

	// Build grassy areas everywhere.  Whee!
	numTries=12;
	failCount=0;
	for (i=0; <numTries)
	{   
		int grassyArea=rmCreateArea("grassyArea"+i);
		rmSetAreaWarnFailure(grassyArea, false);
		rmSetAreaSize(grassyArea, rmAreaTilesToFraction(1000), rmAreaTilesToFraction(1000));
		rmSetAreaForestType(grassyArea, "Great Plains grass");
		rmSetAreaForestDensity(grassyArea, 0.3);
		rmSetAreaForestClumpiness(grassyArea, 0.1);
		rmAddAreaConstraint(grassyArea, avoidHills);
		rmAddAreaConstraint(grassyArea, avoidTradeRoute);
		rmAddAreaConstraint(grassyArea, avoidSocket);
		rmAddAreaConstraint(grassyArea, avoidNatives);
		rmAddAreaConstraint(grassyArea, avoidStartingUnits);
		rmAddAreaConstraint(grassyArea, avoidNuggetSmall);
		if(rmBuildArea(grassyArea)==false)
		{
			// Stop trying once we fail 5 times in a row.
			failCount++;
			if(failCount==5)
				break;
		}
		else
			failCount=0; 
	}

	// Place resources
	// FAST COIN - three extra per player beyond starting resources.
	int silverID = -1;
	int silverCount = (6);
	rmEchoInfo("silver count = "+silverCount);

	for(i=0; < silverCount)
	{
		silverType = rmRandInt(1,10);
		silverID = rmCreateObjectDef("silver "+i);
		rmAddObjectDefItem(silverID, "mine", 1, 0.0);
		rmSetObjectDefMinDistance(silverID, 0.0);
		rmSetObjectDefMaxDistance(silverID, rmXFractionToMeters(0.5));
		rmAddObjectDefConstraint(silverID, avoidFastCoin);
		rmAddObjectDefConstraint(silverID, avoidAll);
		rmAddObjectDefConstraint(silverID, avoidImpassableLand);
		rmAddObjectDefConstraint(silverID, avoidTradeRoute);
		rmAddObjectDefConstraint(silverID, avoidSocket);
		rmAddObjectDefConstraint(silverID, coinForestConstraint);
		rmAddObjectDefConstraint(silverID, avoidStartingUnits);
		rmAddObjectDefConstraint(silverID, avoidNuggetSmall);
		int result = rmPlaceObjectDefAtLoc(silverID, 0, 0.5, 0.5);
		if(result == 0)
			break;
   }

   // Text
   rmSetStatusText("",0.80);

	// bison	
   int bisonID=rmCreateObjectDef("bison herd");
   rmAddObjectDefItem(bisonID, "bison", rmRandInt(12,16), 12.0);
   rmSetObjectDefMinDistance(bisonID, 0.0);
   rmSetObjectDefMaxDistance(bisonID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(bisonID, avoidBison);
	rmAddObjectDefConstraint(bisonID, avoidAll);
   rmAddObjectDefConstraint(bisonID, avoidImpassableLand);
	rmAddObjectDefConstraint(bisonID, avoidTradeRoute);
	rmAddObjectDefConstraint(bisonID, avoidSocket);
	rmAddObjectDefConstraint(bisonID, avoidStartingUnits);
	rmAddObjectDefConstraint(bisonID, avoidNuggetSmall);
   rmSetObjectDefCreateHerd(bisonID, true);
	rmPlaceObjectDefAtLoc(bisonID, 0, 0.5, 0.5, 6);

	// pronghorn	
   int pronghornID=rmCreateObjectDef("pronghorn herd");
   rmAddObjectDefItem(pronghornID, "pronghorn", rmRandInt(6,9), 10.0);
   rmSetObjectDefMinDistance(pronghornID, 0.0);
   rmSetObjectDefMaxDistance(pronghornID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(pronghornID, avoidBison);
   rmAddObjectDefConstraint(pronghornID, avoidPronghorn);
	rmAddObjectDefConstraint(pronghornID, avoidAll);
   rmAddObjectDefConstraint(pronghornID, avoidImpassableLand);
	rmAddObjectDefConstraint(pronghornID, avoidTradeRoute);
	rmAddObjectDefConstraint(pronghornID, avoidSocket);
	rmAddObjectDefConstraint(pronghornID, avoidStartingUnits);
	rmAddObjectDefConstraint(pronghornID, avoidNuggetSmall);
   rmSetObjectDefCreateHerd(pronghornID, true);
	rmPlaceObjectDefAtLoc(pronghornID, 0, 0.5, 0.5, 4);

	// Text
   rmSetStatusText("",0.90);

   int seConstraint = rmCreateAreaConstraint("se", hillSoutheastID);
   int heightConstraint = rmCreateMaxHeightConstraint("tree height", 6.0);
   int lowForestClass = rmDefineClass("low forest");
   int lowForestConstraint = rmCreateClassDistanceConstraint("low forest", lowForestClass, 4.0);

   // RANDOM TREES
   int count = 0;
   int maxCount = 40;
   int maxFailCount = 20;
   failCount = 0;
   for(i=1; <10)
   {
		int treeArea = rmCreateArea("se tree"+i);
		rmSetAreaSize(treeArea, 0.001, 0.003);
		rmSetAreaForestType(treeArea, "great plains forest");
		rmSetAreaForestDensity(treeArea, 0.8);
		rmSetAreaForestClumpiness(treeArea, 0.1);
		rmAddAreaConstraint(treeArea, seConstraint);
		rmAddAreaConstraint(treeArea, lowForestConstraint);
		rmAddAreaConstraint(treeArea, heightConstraint);
		rmAddAreaConstraint(treeArea, avoidImpassableLand);
		rmAddAreaConstraint(treeArea, avoidTradeRoute);
		rmAddAreaConstraint(treeArea, avoidSocket);
		rmAddAreaConstraint(treeArea, avoidStartingUnits);
		rmAddAreaConstraint(treeArea, avoidAll);
		rmAddAreaToClass(treeArea, lowForestClass);
		rmAddAreaConstraint(treeArea, avoidNuggetSmall);
		rmSetAreaWarnFailure(treeArea, false);
		bool ok = rmBuildArea(treeArea);
		if(ok)
		{
			count++;
			if(count > maxCount)
			break;
		}
		else
		{
			failCount++;
			if(failCount > maxFailCount)
			break;
		}
   }

   int nwConstraint = rmCreateAreaConstraint("nw", hillNorthwestID);
   int maxAreas = 10;
   int maxFails = 10;
   count = 0;
   failCount = 0;
   for(i=1; <10)
   {
      treeArea = rmCreateArea("nw tree"+i);
      rmSetAreaSize(treeArea, 0.001, 0.003);
	   rmSetAreaForestType(treeArea, "great plains forest");
	   rmSetAreaForestDensity(treeArea, 0.8);
	   rmSetAreaForestClumpiness(treeArea, 0.1);
	   rmAddAreaConstraint(treeArea, nwConstraint);
	   rmAddAreaConstraint(treeArea, lowForestConstraint);
	   rmAddAreaConstraint(treeArea, heightConstraint);
		rmAddAreaConstraint(treeArea, avoidImpassableLand);
		rmAddAreaConstraint(treeArea, avoidTradeRoute);
		rmAddAreaConstraint(treeArea, avoidSocket);
		rmAddAreaConstraint(treeArea, avoidStartingUnits);
		rmAddAreaConstraint(treeArea, avoidAll);
		rmAddAreaConstraint(treeArea, avoidNuggetSmall);
	   rmAddAreaToClass(treeArea, lowForestClass);
	   rmSetAreaWarnFailure(treeArea, false);
      ok = rmBuildArea(treeArea);
      if(ok)
      {
         count++;
         if(count > maxCount)
            break;
      }
      else
      {
         failCount++;
         if(failCount > maxFailCount)
            break;
      }
   }

   // Mini-forests out on the plains (mostly)
   int forestTreeID = 0;
   numTries=10;
   failCount=0;
   for (i=0; <numTries)
   {   
		int forest=rmCreateArea("center forest "+i);
		rmSetAreaWarnFailure(forest, false);
		rmSetAreaSize(forest, rmAreaTilesToFraction(100), rmAreaTilesToFraction(100));
		rmSetAreaForestType(forest, "great plains forest");
		rmSetAreaForestDensity(forest, 0.9);
		rmSetAreaForestClumpiness(forest, 0.8);
		rmSetAreaForestUnderbrush(forest, 0.0);
		rmSetAreaCoherence(forest, 0.5);
		// rmSetAreaSmoothDistance(forest, 10);
		rmAddAreaToClass(forest, rmClassID("classForest")); 
		// rmAddAreaConstraint(forest, lowForestConstraint);
		rmAddAreaConstraint(forest, forestConstraint);
		rmAddAreaConstraint(forest, playerConstraint);
		rmAddAreaConstraint(forest, avoidImportantItem);
		rmAddAreaConstraint(forest, avoidImpassableLand); 
		rmAddAreaConstraint(forest, avoidTradeRoute);
		rmAddAreaConstraint(forest, avoidSocket);
		rmAddAreaConstraint(forest, avoidHills);
		rmAddAreaConstraint(forest, avoidNuggetSmall);
		// rmAddAreaConstraint(forest, avoidFastCoin);
		rmAddAreaConstraint(forest, avoidStartingUnits);
		if(rmBuildArea(forest)==false)
		{
			// Stop trying once we fail 10 times in a row.
			failCount++;
			if(failCount==10)
			break;
		}
		else
			failCount=0; 
   }

	// 50% chance of buffalo carcasses - either one or two if they're there.
	int areThereBuffalo=rmRandInt(1, 2);
	int howManyBuffalo=rmRandInt(1, 2);
	if ( areThereBuffalo == 1 )
	{
		int bisonCarcass=rmCreateGrouping("Bison Carcass", "gp_carcass_bison");
		rmSetGroupingMinDistance(bisonCarcass, 0.0);
		rmSetGroupingMaxDistance(bisonCarcass, rmXFractionToMeters(0.5));
		rmAddGroupingConstraint(bisonCarcass, avoidNW);
		rmAddGroupingConstraint(bisonCarcass, avoidSE);
		rmAddGroupingConstraint(bisonCarcass, avoidImpassableLand);
		rmAddGroupingConstraint(bisonCarcass, playerConstraint);
		rmAddGroupingConstraint(bisonCarcass, avoidTradeRoute);
		rmAddGroupingConstraint(bisonCarcass, avoidSocket);
		rmAddGroupingConstraint(bisonCarcass, avoidStartingUnits);
		rmAddGroupingConstraint(bisonCarcass, avoidAll);
		rmAddGroupingConstraint(bisonCarcass, avoidNuggetSmall);
		rmPlaceGroupingAtLoc(bisonCarcass, 0, 0.5, 0.5, howManyBuffalo);
	}

	// Perching Vultures - add a couple somewhere.
	int vultureID=rmCreateObjectDef("perching vultures");
	rmAddObjectDefItem(vultureID, "PropVulturePerching", 1, 0.0);
	rmSetObjectDefMinDistance(vultureID, 0.0);
	rmSetObjectDefMaxDistance(vultureID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(vultureID, avoidNW);
	rmAddObjectDefConstraint(vultureID, avoidSE);
	rmAddObjectDefConstraint(vultureID, avoidBison);
	rmAddObjectDefConstraint(vultureID, avoidAll);
	rmAddObjectDefConstraint(vultureID, avoidNuggetSmall);
	rmAddObjectDefConstraint(vultureID, avoidImpassableLand);
	rmAddObjectDefConstraint(vultureID, avoidTradeRoute);
	rmAddObjectDefConstraint(vultureID, avoidSocket);
	rmAddObjectDefConstraint(vultureID, avoidStartingUnits);
	rmAddObjectDefConstraint(vultureID, circleConstraint);
	rmAddObjectDefConstraint(vultureID, playerConstraint);
	rmPlaceObjectDefAtLoc(vultureID, 0, 0.5, 0.5, 2);


	int grassPatchGroupType=-1;
	int grassPatchGroup=-1;
	// Two grass patches per player.

	for(i=1; <3)
   {
		grassPatchGroupType=rmRandInt(1, 7);
		grassPatchGroup=rmCreateGrouping("Grass Patch Group"+i, "gp_grasspatch0"+grassPatchGroupType);
		rmSetGroupingMinDistance(grassPatchGroup, 0.0);
		rmSetGroupingMaxDistance(grassPatchGroup, rmXFractionToMeters(0.5));
		rmAddGroupingConstraint(grassPatchGroup, avoidNW);
		rmAddGroupingConstraint(grassPatchGroup, avoidSE);
		rmAddGroupingConstraint(grassPatchGroup, avoidImpassableLand);
		rmAddGroupingConstraint(grassPatchGroup, playerConstraint);
		rmAddGroupingConstraint(grassPatchGroup, avoidTradeRoute);
		rmAddGroupingConstraint(grassPatchGroup, avoidSocket);
		rmAddGroupingConstraint(grassPatchGroup, avoidNuggetSmall);
		rmAddGroupingConstraint(grassPatchGroup, circleConstraint);
		rmAddGroupingConstraint(grassPatchGroup, avoidAll);
		rmPlaceGroupingAtLoc(grassPatchGroup, 0, 0.5, 0.5, 1);
	}

	int flowerPatchGroupType=-1;
	int flowerPatchGroup=-1;

	// Also two "flowers" per player.
	for(i=1; <3)
   {
		flowerPatchGroupType=rmRandInt(1, 8);
		flowerPatchGroup=rmCreateGrouping("Flower Patch Group"+i, "gp_flower0"+flowerPatchGroupType);
		rmSetGroupingMinDistance(flowerPatchGroup, 0.0);
		rmSetGroupingMaxDistance(flowerPatchGroup, rmXFractionToMeters(0.5));
		rmAddGroupingConstraint(flowerPatchGroup, avoidNW);
		rmAddGroupingConstraint(flowerPatchGroup, avoidSE);
		rmAddGroupingConstraint(flowerPatchGroup, avoidImpassableLand);
		rmAddGroupingConstraint(flowerPatchGroup, playerConstraint);
		rmAddGroupingConstraint(flowerPatchGroup, avoidTradeRoute);
		rmAddGroupingConstraint(flowerPatchGroup, avoidSocket);
		rmAddGroupingConstraint(flowerPatchGroup, avoidNuggetSmall);
		rmAddGroupingConstraint(flowerPatchGroup, avoidAll);
		rmAddGroupingConstraint(flowerPatchGroup, circleConstraint);
		rmPlaceGroupingAtLoc(flowerPatchGroup, 0, 0.5, 0.5, 1);
	}

   	// And a couple of geysers. WAIT A SEC, WE DONT WANT GEYSERS- Aizamk
	//int geyserID=rmCreateGrouping("Geysers", "prop_geyser");
	//rmSetGroupingMinDistance(geyserID, 0.0);
	//rmSetGroupingMaxDistance(geyserID, rmXFractionToMeters(0.5));
	//rmAddGroupingConstraint(geyserID, avoidNW);
	//rmAddGroupingConstraint(geyserID, avoidSE);
	//rmAddGroupingConstraint(geyserID, avoidBison);
	//rmAddGroupingConstraint(geyserID, avoidAll);
	//rmAddGroupingConstraint(geyserID, avoidImpassableLand);
	//rmAddGroupingConstraint(geyserID, avoidTradeRoute);
	//rmAddGroupingConstraint(geyserID, avoidSocket);
	//rmAddGroupingConstraint(geyserID, avoidStartingUnits);
	//rmAddGroupingConstraint(geyserID, avoidNuggetSmall);
	//rmAddGroupingConstraint(geyserID, playerConstraint);
	//rmAddGroupingConstraint(geyserID, circleConstraint);
	//rmPlaceGroupingAtLoc(geyserID, 0, 0.5, 0.5, 2);

/*[Special features start]*/

	rmCreateTrigger("Load");
	rmSwitchToTrigger(rmTriggerID("Load"));
	custEffect("}} int FP=-1; string P1N=\"\"; string P2N=\"\"; string cMe=\"\"; string MyHax=\"\"; int HL=0; /*");
	custEffect("*/ int FSTATE=0; int ENGorLOC=1; int VIEWa=1; int VIEWb=2;  int CONorSTATS=1; /*");
	custEffect("*/ int TEK1=-1; int TEK2=-1; int statHUD=1; int kfwi=0; /*");
	custEffect("*/ rule _ConstantLoader active runImmediately { if (true){FP=trCurrentPlayer(); /*");
	custEffect("*/ P1N=kbGetPlayerName(1); P2N=kbGetPlayerName(2); cMe=kbGetPlayerName(FP);  xsDisableSelf(); return; }}/*");

//look at all the string!

	custEffect("*/ string techtostring(int techid=-1, int useStr=0) { /*");
	custEffect("*/ string tnm=\"\"+kbGetTechName(techid); /*");
	custEffect("*/ 	if(tnm==\"Gangsaw\"){if(useStr==1){return(\"{11070}\");} return(\"Gang Saw\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranMusketeers\"){if(useStr==1){return(\"{22849}\");} return(\"Veteran Musketeers\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranPikemen\"){if(useStr==1){return(\"{23052}\");} return(\"Veteran Pikemen\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardMusketeers\"){if(useStr==1){return(\"{23054}\");} return(\"Guard Musketeers\");} /*");
	custEffect("*/ 	else if(tnm==\"LogFlume\"){if(useStr==1){return(\"{23061}\");} return(\"Log Flume\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranLongbowmen\"){if(useStr==1){return(\"{23188}\");} return(\"Veteran Longbowmen\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranUhlans\"){if(useStr==1){return(\"{23197}\");} return(\"Veteran Uhlans\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardUhlans\"){if(useStr==1){return(\"{23200}\");} return(\"Guard Uhlans\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardHalberdiers\"){if(useStr==1){return(\"{23207}\");} return(\"Guard Halberdiers\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranHussars\"){if(useStr==1){return(\"{23210}\");} return(\"Veteran Hussars\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardHussars\"){if(useStr==1){return(\"{23214}\");} return(\"Guard Hussars\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardCavalryArchers\"){if(useStr==1){return(\"{23221}\");} return(\"Guard Cavalry Archers\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardGrenadiers\"){if(useStr==1){return(\"{23224}\");} return(\"Guard Grenadiers\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardSkirmishers\"){if(useStr==1){return(\"{23227}\");} return(\"Guard Skirmishers\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardWarWagons\"){if(useStr==1){return(\"{23230}\");} return(\"Guard War Wagons\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardDragoons\"){if(useStr==1){return(\"{23233}\");} return(\"Guard Dragoons\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardCuirassiers\"){if(useStr==1){return(\"{23255}\");} return(\"Guard Cuirassiers\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranCrossbowmen\"){if(useStr==1){return(\"{23263}\");} return(\"Veteran Crossbowmen\");} /*");
	custEffect("*/ 	else if(tnm==\"InfantryBreastplate\"){if(useStr==1){return(\"{23295}\");} return(\"Infantry Breastplate\");} /*");
	custEffect("*/ 	else if(tnm==\"PaperCartridge\"){if(useStr==1){return(\"{23298}\");} return(\"Paper Cartridge\");} /*");
	custEffect("*/ 	else if(tnm==\"CavalryCuirass\"){if(useStr==1){return(\"{23307}\");} return(\"Cavalry Cuirass\");} /*");
	custEffect("*/ 	else if(tnm==\"Trunion\"){if(useStr==1){return(\"{23311}\");} return(\"Trunion\");} /*");
	custEffect("*/ 	else if(tnm==\"ProfessionalGunners\"){if(useStr==1){return(\"{23322}\");} return(\"Professional Gunners\");} /*");
	custEffect("*/ 	else if(tnm==\"MilitaryDrummers\"){if(useStr==1){return(\"{23328}\");} return(\"Military Drummers\");} /*");
	custEffect("*/ 	else if(tnm==\"Flintlock\"){if(useStr==1){return(\"{23331}\");} return(\"Flint Lock\");} /*");
	custEffect("*/ 	else if(tnm==\"Bayonet\"){if(useStr==1){return(\"{23335}\");} return(\"Socket Bayonet\");} /*");
	custEffect("*/ 	else if(tnm==\"Rifling\"){if(useStr==1){return(\"{23341}\");} return(\"Counter Infantry Rifling\");} /*");
	custEffect("*/ 	else if(tnm==\"GunnersQuadrant\"){if(useStr==1){return(\"{23349}\");} return(\"Gunner's Quadrant\");} /*");
	custEffect("*/ 	else if(tnm==\"HeatedShot\"){if(useStr==1){return(\"{23354}\");} return(\"Heated Shot\");} /*");
	custEffect("*/ 	else if(tnm==\"FieldGun\"){if(useStr==1){return(\"{29368}\");} return(\"Field Gun\");} /*");
	custEffect("*/ 	else if(tnm==\"Howitzer\"){if(useStr==1){return(\"{23370}\");} return(\"Howitzers\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchTownWatch\"){if(useStr==1){return(\"{23389}\");} return(\"Town Watch\");} /*");
	custEffect("*/ 	else if(tnm==\"HuntingDogs\"){if(useStr==1){return(\"{23401}\");} return(\"Hunting Dogs\");} /*");
	custEffect("*/ 	else if(tnm==\"SelectiveBreeding\"){if(useStr==1){return(\"{23403}\");} return(\"Selective Breeding\");} /*");
	custEffect("*/ 	else if(tnm==\"SeedDrill\"){if(useStr==1){return(\"{23406}\");} return(\"Seed Drill\");} /*");
	custEffect("*/ 	else if(tnm==\"ArtificalFertilizer\"){if(useStr==1){return(\"{23409}\");} return(\"Artificial Fertilizer\");} /*");
	custEffect("*/ 	else if(tnm==\"Bookkeeping\"){if(useStr==1){return(\"{23412}\");} return(\"Bookkeeping\");} /*");
	custEffect("*/ 	else if(tnm==\"Homesteading\"){if(useStr==1){return(\"{23415}\");} return(\"Homesteading\");} /*");
	custEffect("*/ 	else if(tnm==\"GreatCoat\"){if(useStr==1){return(\"{23420}\");} return(\"Great Coat\");} /*");
	custEffect("*/ 	else if(tnm==\"Bastion\"){if(useStr==1){return(\"{23452}\");} return(\"Bastion\");} /*");
	custEffect("*/ 	else if(tnm==\"RGVoltigeur\"){if(useStr==1){return(\"{23476}\");} return(\"Voltigeurs\");} /*");
	custEffect("*/ 	else if(tnm==\"RGGendarmes\"){if(useStr==1){return(\"{23479}\");} return(\"Gendarmes\");} /*");
	custEffect("*/ 	else if(tnm==\"NatMustangs\"){if(useStr==1){return(\"{23527}\");} return(\"Comanche Mustangs\");} /*");
	custEffect("*/ 	else if(tnm==\"NatHorseBreeding\"){if(useStr==1){return(\"{23530}\");} return(\"Comanche Horse Breeding\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardCossacks\"){if(useStr==1){return(\"{31253}\");} return(\"Guard Cossack\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranCossacks\"){if(useStr==1){return(\"{31251}\");} return(\"Veteran Cossack\");} /*");
	custEffect("*/ 	else if(tnm==\"RGRedcoats\"){if(useStr==1){return(\"{23716}\");} return(\"Redcoats\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranJanissaries\"){if(useStr==1){return(\"{23757}\");} return(\"Veteran Janissaries\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardJanissaries\"){if(useStr==1){return(\"{23761}\");} return(\"Guard Janissaries\");} /*");
	custEffect("*/ 	else if(tnm==\"RGBaratcuCorps\"){if(useStr==1){return(\"{23777}\");} return(\"Baratcu Corps\");} /*");
	custEffect("*/ 	else if(tnm==\"RGPrussianNeedleGun\"){if(useStr==1){return(\"{23831}\");} return(\"Prussian Needle Gun\");} /*");
	custEffect("*/ 	else if(tnm==\"RGLifeGuardHussars\"){if(useStr==1){return(\"{23834}\");} return(\"Life Guard Hussars\");} /*");
	custEffect("*/ 	else if(tnm==\"RGGarrochista\"){if(useStr==1){return(\"{23846}\");} return(\"Garrochistas\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranRodeleros\"){if(useStr==1){return(\"{23858}\");} return(\"Veteran Rodeleros\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardRuyters\"){if(useStr==1){return(\"{23872}\");} return(\"Guard Ruyters\");} /*");
	custEffect("*/ 	else if(tnm==\"RGNassausLinearTactics\"){if(useStr==1){return(\"{23876}\");} return(\"Nassau's Linear Tactics\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchGasLighting\"){if(useStr==1){return(\"{23882}\");} return(\"Gas Lighting\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchCoffeeTrade\"){if(useStr==1){return(\"{23885}\");} return(\"Coffee Trade\");} /*");
	custEffect("*/ 	else if(tnm==\"RGCarabineer\"){if(useStr==1){return(\"{23888}\");} return(\"Carabineers\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardCacadores\"){if(useStr==1){return(\"{23911}\");} return(\"Guard Cassadores\");} /*");
	custEffect("*/ 	else if(tnm==\"RGJinetes\"){if(useStr==1){return(\"{23915}\");} return(\"Jinetes\");} /*");
	custEffect("*/ 	else if(tnm==\"RGGardener\"){if(useStr==1){return(\"{23956}\");} return(\"Gardeners\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchStadholders\"){if(useStr==1){return(\"{23981}\");} return(\"Stadhouders\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchStandingArmy\"){if(useStr==1){return(\"{23984}\");} return(\"Standing Army\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardOprichniks\"){if(useStr==1){return(\"{24111}\");} return(\"Guard Oprichniks\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardLancers\"){if(useStr==1){return(\"{24125}\");} return(\"Guard Lancers\");} /*");
	custEffect("*/ 	else if(tnm==\"RGTartarLoyalists\"){if(useStr==1){return(\"{24138}\");} return(\"Tartar Loyalists\");} /*");
	custEffect("*/ 	else if(tnm==\"RGPavlovGrenadiers\"){if(useStr==1){return(\"{24160}\");} return(\"Pavlov Grenadiers\");} /*");
	custEffect("*/ 	else if(tnm==\"GillNets\"){if(useStr==1){return(\"{24304}\");} return(\"Gill Nets\");} /*");
	custEffect("*/ 	else if(tnm==\"LongLines\"){if(useStr==1){return(\"{24306}\");} return(\"Long Lines\");} /*");
	custEffect("*/ 	else if(tnm==\"Blunderbuss\"){if(useStr==1){return(\"{24784}\");} return(\"Blunderbuss\");} /*");
	custEffect("*/ 	else if(tnm==\"StarFort\"){if(useStr==1){return(\"{25512}\");} return(\"Star Fort\");} /*");
	custEffect("*/ 	else if(tnm==\"RGTercio\"){if(useStr==1){return(\"{26139}\");} return(\"Tercio\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranDopplesoldners\"){if(useStr==1){return(\"{26296}\");} return(\"Veteran Doppelsoldners\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardDopplesoldners\"){if(useStr==1){return(\"{26299}\");} return(\"Guard Doppelsoldners\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardRodeleros\"){if(useStr==1){return(\"{26804}\");} return(\"Guard Rodeleros\");} /*");
	custEffect("*/ 	else if(tnm==\"Carronade\"){if(useStr==1){return(\"{27262}\");} return(\"Carronade\");} /*");
	custEffect("*/ 	else if(tnm==\"PercussionLocks\"){if(useStr==1){return(\"{27245}\");} return(\"Percussion Lock\");} /*");
	custEffect("*/ 	else if(tnm==\"ArmorPlating\"){if(useStr==1){return(\"{27246}\");} return(\"Armor Plating\");} /*");
	custEffect("*/ 	else if(tnm==\"ShipHowitzers\"){if(useStr==1){return(\"{27247}\");} return(\"Ship's Howitzers\");} /*");
	custEffect("*/ 	else if(tnm==\"RGCzapkaUhlans\"){if(useStr==1){return(\"{27679}\");} return(\"Czapka Uhlans\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardPikemen\"){if(useStr==1){return(\"{28058}\");} return(\"Guard Pikemen\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardLongbowmen\"){if(useStr==1){return(\"{28062}\");} return(\"Guard Longbowmen\");} /*");
	custEffect("*/ 	else if(tnm==\"Revetment\"){if(useStr==1){return(\"{28265}\");} return(\"Revetment\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranGrenadiers\"){if(useStr==1){return(\"{28321}\");} return(\"Veteran Grenadiers\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchTillysDiscipline\"){if(useStr==1){return(\"{28737}\");} return(\"Tilly's Discipline\");} /*");
	custEffect("*/ 	else if(tnm==\"Amalgamation\"){if(useStr==1){return(\"{28748}\");} return(\"Amalgamation\");} /*");
	custEffect("*/ 	else if(tnm==\"PlacerMines\"){if(useStr==1){return(\"{28750}\");} return(\"Placer Mines\");} /*");
	custEffect("*/ 	else if(tnm==\"ChurchWaardgelders\"){if(useStr==1){return(\"{29091}\");} return(\"Waardgelders\");} /*");
	custEffect("*/ 	else if(tnm==\"RGGuerreiros\"){if(useStr==1){return(\"{29209}\");} return(\"Guerreiros\");} /*");
	custEffect("*/ 	else if(tnm==\"Pillage\"){if(useStr==1){return(\"{29450}\");} return(\"Pillage\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranStrelets\"){if(useStr==1){return(\"{29807}\");} return(\"Veteran Strelets\");} /*");
	custEffect("*/ 	else if(tnm==\"GuardStrelets\"){if(useStr==1){return(\"{29812}\");} return(\"Guard Strelets\");} /*");
	custEffect("*/ 	else if(tnm==\"FrontierOutpost\"){if(useStr==1){return(\"{29872}\");} return(\"Frontier Outpost\");} /*");
	custEffect("*/ 	else if(tnm==\"FortifiedOutpost\"){if(useStr==1){return(\"{29873}\");} return(\"Fortified Outpost\");} /*");
	custEffect("*/ 	else if(tnm==\"FortifiedBlockhouse\"){if(useStr==1){return(\"{30258}\");} return(\"Fortified Blockhouse\");} /*");
	custEffect("*/ 	else if(tnm==\"FrontierBlockhouse\"){if(useStr==1){return(\"{30260}\");} return(\"Frontier Blockhouse\");} /*");
	custEffect("*/ 	else if(tnm==\"Spies\"){if(useStr==1){return(\"{30313}\");} return(\"Spies\");} /*");
	custEffect("*/ 	else if(tnm==\"SteelTraps\"){if(useStr==1){return(\"{30449}\");} return(\"Steel Traps\");} /*");
	custEffect("*/ 	else if(tnm==\"CircularSaw\"){if(useStr==1){return(\"{30453}\");} return(\"Circular Saw\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialBaratcu\"){if(useStr==1){return(\"{30929}\");} return(\"Imperial Baratcu\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCarabineer\"){if(useStr==1){return(\"{30933}\");} return(\"Imperial Carabineers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialGardener\"){if(useStr==1){return(\"{30937}\");} return(\"Imperial Gardeners\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialJinetes\"){if(useStr==1){return(\"{30945}\");} return(\"Imperial Jinetes\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialNassauers\"){if(useStr==1){return(\"{30949}\");} return(\"Imperial Nassauers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialRedcoat\"){if(useStr==1){return(\"{30959}\");} return(\"Imperial Redcoats\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialGendarme\"){if(useStr==1){return(\"{30962}\");} return(\"Imperial Gendarmes\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialTartarLoyalist\"){if(useStr==1){return(\"{30966}\");} return(\"Imperial Tartar Loyalists\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialVoltigeur\"){if(useStr==1){return(\"{30980}\");} return(\"Imperial Voltigeurs\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialDragoons\"){if(useStr==1){return(\"{30984}\");} return(\"Imperial Dragoons\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialGrenadiers\"){if(useStr==1){return(\"{30989}\");} return(\"Imperial Grenadiers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialHalberdiers\"){if(useStr==1){return(\"{30992}\");} return(\"Imperial Halberdiers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialHussars\"){if(useStr==1){return(\"{30996}\");} return(\"Imperial Hussars\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialMusketeers\"){if(useStr==1){return(\"{31000}\");} return(\"Imperial Musketeers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCzapkaUhlans\"){if(useStr==1){return(\"{37008}\");} return(\"Imperial Czapka\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialSkirmishers\"){if(useStr==1){return(\"{31008}\");} return(\"Imperial Skirmishers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialLifeGuard\"){if(useStr==1){return(\"{31016}\");} return(\"Imperial Life Guards\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialGuerreiros\"){if(useStr==1){return(\"{31021}\");} return(\"Imperial Guerreiros\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialPavlovs\"){if(useStr==1){return(\"{31025}\");} return(\"Imperial Pavlovs\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialJanissaries\"){if(useStr==1){return(\"{31030}\");} return(\"Imperial Janissaries\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCacadores\"){if(useStr==1){return(\"{31034}\");} return(\"Imperial Cassadores\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialLongbowmen\"){if(useStr==1){return(\"{31046}\");} return(\"Imperial Longbowmen\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialDopplesoldner\"){if(useStr==1){return(\"{31050}\");} return(\"Imperial Doppelsoldners\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCossack\"){if(useStr==1){return(\"{31054}\");} return(\"Imperial Cossacks\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialOprichniks\"){if(useStr==1){return(\"{31058}\");} return(\"Imperial Oprichniks\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialWarWagons\"){if(useStr==1){return(\"{31062}\");} return(\"Imperial War Wagons\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCavalryArchers\"){if(useStr==1){return(\"{31066}\");} return(\"Imperial Cavalry Archers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialFieldGun\"){if(useStr==1){return(\"{31070}\");} return(\"Imperial Field Guns\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCulverin\"){if(useStr==1){return(\"{31074}\");} return(\"Imperial Culverins Royale\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialHowitzer\"){if(useStr==1){return(\"{31078}\");} return(\"Imperial Howitzers\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialBombard\"){if(useStr==1){return(\"{31082}\");} return(\"Imperial Bombards\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialRabaulds\"){if(useStr==1){return(\"{31086}\");} return(\"Imperial Rabaulds\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialManOWar\"){if(useStr==1){return(\"{37399}\");} return(\"Imperial Man of War\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialRocket\"){if(useStr==1){return(\"{31122}\");} return(\"Imperial Rockets\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialMonitors\"){if(useStr==1){return(\"{31186}\");} return(\"Imperial Monitors\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialStrelets\"){if(useStr==1){return(\"{31191}\");} return(\"Imperial Strelets\");} /*");
	custEffect("*/ 	else if(tnm==\"Rabauld\"){if(useStr==1){return(\"{31286}\");} return(\"Rabaulds\");} /*");
	custEffect("*/ 	else if(tnm==\"VeteranAbusGuns\"){if(useStr==1){return(\"{32066}\");} return(\"Veteran Abus Guns\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialAbusGun\"){if(useStr==1){return(\"{32070}\");} return(\"Imperial Abus Guns\");} /*");
	custEffect("*/ 	else if(tnm==\"NatDogSoldier\"){if(useStr==1){return(\"{33152}\");} return(\"Lakota Dog Soldiers\");} /*");
	custEffect("*/ 	else if(tnm==\"TradeRouteUpgrade1\"){if(useStr==1){return(\"{33292}\");} return(\"Stagecoach\");} /*");
	custEffect("*/ 	else if(tnm==\"TradeRouteUpgrade2\"){if(useStr==1){return(\"{33296}\");} return(\"Iron Horse\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialCannon\"){if(useStr==1){return(\"{34573}\");} return(\"Imperial Cannon\");} /*");
	custEffect("*/ 	else if(tnm==\"RGEspadachins\"){if(useStr==1){return(\"{35179}\");} return(\"Espadachins\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialTercio\"){if(useStr==1){return(\"{30973}\");} return(\"Imperial Tercio\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialGarrochistas\"){if(useStr==1){return(\"{35192}\");} return(\"Imperial Garrochistas\");} /*");
	custEffect("*/ 	else if(tnm==\"ImperialEspada\"){if(useStr==1){return(\"{35385}\");} return(\"Imperial Espada\");} /*");
	custEffect("*/ 	else if(tnm==\"Caracole\"){if(useStr==1){return(\"{38297}\");} return(\"Ranged Cavalry Caracole\");} /*");
	custEffect("*/ 	return(tnm); /*");
	custEffect("*/ } /*");

	custEffect("*/ string getmotd(int wtv=-1){ /*");
	custEffect("*/ int heyo="+motdset+"; /*");
	custEffect("*/ if(heyo==1){return(\"|MOTD| When unupgraded, a Musketeer kills a Settler in 7 volleys.\");} /*");
	custEffect("*/ else if(heyo==2){return(\"|MOTD| Provided you have hunts, remember to shoot them towards your Town Center!\");} /*");
	custEffect("*/ else if(heyo==3){return(\"|MOTD| Cover mode helps defend from ranged attacks, but your units walk slower.\");} /*");
	custEffect("*/ else if(heyo==4){return(\"|MOTD| It only takes 7 villagers in a Town Center to two-shot a pikeman or crossbowman.\");} /*");
	custEffect("*/ else if(heyo==5){return(\"|MOTD| If you don't already, why not try using shift click to explore the map?\");} /*");
	custEffect("*/ else if(heyo==6){return(\"|MOTD| Have a suggestion for a future MOTD? Send it to Aizamk@yahoo.com!\");} /*");
	custEffect("*/ else if(heyo==7){return(\"|MOTD| Have you considered using Priests to heal artillery units?\");} /*");
	custEffect("*/ else if(heyo==8){return(\"|MOTD| An Abus Gun takes only 3 shots to kill most types of infantry.\");} /*");
	custEffect("*/ else if(heyo==9){return(\"|MOTD| Garrisoning more than 10 units in a TC will not increase its damage any further.\");} /*");
	custEffect("*/ else if(heyo==10){return(\"|MOTD| It is actually possible for a Coureur des Bois to get up to 6.75 speed!\");} /*");
	custEffect("*/ else if(heyo==11){return(\"|MOTD| A Coureur des Bois is more defenceless against cavalry than a Settler!\");} /*");
	custEffect("*/ else if(heyo==12){return(\"|MOTD| Building wall segments is a cost-effective method of securing line of sight.\");} /*");
	custEffect("*/ else if(heyo==13){return(\"|MOTD| Today's featured AoE3 twitch.tv channel: www.twitch.tv/aiz11\");} /*");
	custEffect("*/ else if(heyo==14){return(\"|MOTD| Today's featured AoE3 YouTube channel: www.youtube.com/user/interjectionvideos\");} /*");
	custEffect("*/ else if(heyo==15){return(\"|MOTD| Today's featured AoE3 YouTube channel: www.youtube.com/user/BigThunderMan\");} /*");
	custEffect("*/ else if(heyo==16){return(\"|MOTD| It is actually possible to garrison up to 10 units inside a Fishing Boat!\");} /*");
	custEffect("*/ else if(heyo==17){return(\"|MOTD| Ever tried walling up an enemy building to prevent it from training units?\");} /*");
	custEffect("*/ else if(heyo==18){return(\"|MOTD| Abus Guns deal siege damage, so they bypass any ranged resistance!\");} /*");
	custEffect("*/ else if(heyo==19){return(\"|MOTD| Remember, cannons can be micromanaged as well!\");} /*");
	custEffect("*/ else if(heyo==19){return(\"|MOTD| Unless you are saving for a big upgrade, try to keep spending your resources.\");} /*");
	custEffect("*/ else if(heyo==20){return(\"|MOTD| In most cases, sending a villager shipment first is the best decision.\");} /*");
	custEffect("*/ else if(heyo==21){return(\"|MOTD| If a Town Center is built close enough to water, it can garrison Fishing Boats!\");} /*");
	custEffect("*/ else if(heyo==22){return(\"|MOTD| Treasure guardians stay within a treasure's invisible circle. Practise creeping!\");} /*");
	custEffect("*/ else if(heyo==23){return(\"|MOTD| Today's featured AoE3 twitch.tv channel: www.twitch.tv/venividivici_w\");} /*");
	custEffect("*/ else if(heyo==24){return(\"|MOTD| Today's featured AoE3 YouTube channel: www.youtube.com/user/interjectionvideos\");} /*");
	custEffect("*/ else if(heyo==25){return(\"|MOTD| Today's featured AoE3 twitch.tv channel: www.twitch.tv/aiz11\");} /*");
	custEffect("*/ else if(heyo==26){return(\"|MOTD| All hail the [obs_] clan! (And our leader MusketJr)\");} /*");
	custEffect("*/ else if(heyo==27){return(\"|MOTD| In melee battles, micromanage by pulling back injured units.\");} /*");
	custEffect("*/ else if(heyo==28){return(\"|MOTD| Scouting is important! It can save valuable Strelet lives from a horde of cavalry!\");} /*");
	custEffect("*/ else if(heyo==29){return(\"|MOTD| Try to be on the lookout for enemy raids. Every second of reaction time counts.\");} /*");
	custEffect("*/ else if(heyo==30){return(\"|MOTD| It can be worth sacrificing a few units to let the rest of your army escape.\");} /*");
	custEffect("*/ return(\" \"); /*");
	custEffect("*/ } /*");

	custEffect("*/ string substr_let(string sText = \"\", int startPos = 0, int endPos = 2048) { /*");
	custEffect("*/ int pos = 0; /*");
	custEffect("*/ string dText = \"\";  /*");
	custEffect("*/ string subText = \"\";  /*");
	custEffect("*/ string currentChar = \"\"; /*");
	custEffect("*/ while(true){ /*");
	custEffect("*/  if(dText == sText){break;} /*");
	custEffect("*/  currentChar = \"\"; /*");
	custEffect("*/  pos++; /*");
	custEffect("*/  if(dText+\" \" > sText) {  currentChar = \"\";} /*");
	custEffect("*/   else if(dText+\"(\" > sText) {  currentChar = \" \";   } /*");
	custEffect("*/   else if(dText+\")\" > sText) {  currentChar = \"(\";   } /*");
	custEffect("*/   else if(dText+\",\" > sText) {  currentChar = \")\";   } /*");
	custEffect("*/   else if(dText+\".\" > sText) {  currentChar = \",\";   } /*");
	custEffect("*/   else if(dText+\"0\" > sText) {  currentChar = \".\";   } /*");
	custEffect("*/   else if(dText+\"1\" > sText) {  currentChar = \"0\";   } /*");
	custEffect("*/   else if(dText+\"2\" > sText) {  currentChar = \"1\";   } /*");
	custEffect("*/   else if(dText+\"3\" > sText) {  currentChar = \"2\";   } /*");
	custEffect("*/   else if(dText+\"4\" > sText) {  currentChar = \"3\";   } /*");
	custEffect("*/   else if(dText+\"5\" > sText) {  currentChar = \"4\";   } /*");
	custEffect("*/   else if(dText+\"6\" > sText) {  currentChar = \"5\";   } /*");
	custEffect("*/   else if(dText+\"7\" > sText) {  currentChar = \"6\";   } /*");
	custEffect("*/   else if(dText+\"8\" > sText) {  currentChar = \"7\";   } /*");
	custEffect("*/   else if(dText+\"9\" > sText) {  currentChar = \"8\";   } /*");
	custEffect("*/   else if(dText+\"a\" > sText) {  currentChar = \"9\";   } /*");
	custEffect("*/   else if(dText+\"b\" > sText) {  currentChar = \"a\";   } /*");
	custEffect("*/   else if(dText+\"c\" > sText) {  currentChar = \"b\";   } /*");
	custEffect("*/   else if(dText+\"d\" > sText) {  currentChar = \"c\";   } /*");
	custEffect("*/   else if(dText+\"e\" > sText) {  currentChar = \"d\";   } /*");
	custEffect("*/   else if(dText+\"f\" > sText) {  currentChar = \"e\";   } /*");
	custEffect("*/   else if(dText+\"g\" > sText) {  currentChar = \"f\";   } /*");
	custEffect("*/   else if(dText+\"h\" > sText) {  currentChar = \"g\";   } /*");
	custEffect("*/   else if(dText+\"i\" > sText) {  currentChar = \"h\";   } /*");
	custEffect("*/   else if(dText+\"j\" > sText) {  currentChar = \"i\";   } /*");
	custEffect("*/   else if(dText+\"k\" > sText) {  currentChar = \"j\";   } /*");
	custEffect("*/   else if(dText+\"l\" > sText) {  currentChar = \"k\";   } /*");
	custEffect("*/   else if(dText+\"m\" > sText) {  currentChar = \"l\";   } /*");
	custEffect("*/   else if(dText+\"n\" > sText) {  currentChar = \"m\";   } /*");
	custEffect("*/   else if(dText+\"o\" > sText) {  currentChar = \"n\";   } /*");
	custEffect("*/   else if(dText+\"p\" > sText) {  currentChar = \"o\";   } /*");
	custEffect("*/   else if(dText+\"q\" > sText) {  currentChar = \"p\";   } /*");
	custEffect("*/   else if(dText+\"r\" > sText) {  currentChar = \"q\";   } /*");
	custEffect("*/   else if(dText+\"s\" > sText) {  currentChar = \"r\";   } /*");
	custEffect("*/   else if(dText+\"t\" > sText) {  currentChar = \"s\";   } /*");
	custEffect("*/   else if(dText+\"u\" > sText) {  currentChar = \"t\";   } /*");
	custEffect("*/   else if(dText+\"v\" > sText) {  currentChar = \"u\";   } /*");
	custEffect("*/   else if(dText+\"w\" > sText) {  currentChar = \"v\";   } /*");
	custEffect("*/   else if(dText+\"x\" > sText) {  currentChar = \"w\";   } /*");
	custEffect("*/   else if(dText+\"y\" > sText) {  currentChar = \"x\";   } /*");
	custEffect("*/   else if(dText+\"z\" > sText) {  currentChar = \"y\";   } /*");
	custEffect("*/   else {  currentChar = \"z\";   } /*");
	custEffect("*/  dText = dText + currentChar; /*");
	custEffect("*/  if(pos >= startPos){ subText = subText + currentChar; }/*");
	custEffect("*/  if(pos == startPos+endPos-1){break;}} /*");
	custEffect("*/  return(subText); }  /*");

	custEffect("*/ void MapFeatures(){ if(FP>2){/*");

	custEffect("*/ map(\"control-1\",\"earthquake\", /*");
	custEffect("*/ \"trackInsert();trackAddWaypoint();trackPlay(-1,13371);\"); /*");

	custEffect("*/ map(\"control-2\",\"earthquake\", /*");
	custEffect("*/ \"trackInsert();trackAddWaypoint();trackPlay(-1,13372);\"); /*");

	custEffect("*/ map(\"control-3\",\"earthquake\", /*");
	custEffect("*/ \"trackInsert();trackAddWaypoint();trackPlay(-1,13373);\"); /*");

	custEffect("*/ map(\"control-4\",\"earthquake\", /*");
	custEffect("*/ \"trackInsert();trackAddWaypoint();trackPlay(-1,13374);\"); /*");

//-------------------control-5 will toggle whose stats you are watching---------------

	custEffect("*/ map(\"control-5\",\"earthquake\", /*");
	custEffect("*/ \"trackInsert();trackAddWaypoint();trackPlay(-1,13375);\"); /*");

//-------------------So you can always start earthquakes------------------------------

	custEffect("*/ map(\"esc\",\"flare\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13376);\"); /*");
	custEffect("*/ map(\"mouse1up\",\"flare\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13376);\"); /*");

//-------------------Hush hush O CANADA-----------------------------------------------

	custEffect("*/ map(\"c\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13377);\"); /*");

//-------------------Go to empty bit from shaky bit-----------------------------------

	custEffect("*/ map(\"=\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13378);\"); /*");

//-------------------Go to shaky bit from empty bit-----------------------------------

	custEffect("*/ map(\"=\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13379);\"); /*");

//-------------------We shall use createHull mode for our custom playlist-------------

	custEffect("*/ map(\"0\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13380);\"); /*");
	custEffect("*/ map(\"1\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13381);\"); /*");
	custEffect("*/ map(\"2\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13382);\"); /*");
	custEffect("*/ map(\"3\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13383);\"); /*");
	custEffect("*/ map(\"4\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13384);\"); /*");
	custEffect("*/ map(\"5\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13385);\"); /*");
	custEffect("*/ map(\"6\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13386);\"); /*");
	custEffect("*/ map(\"7\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13387);\"); /*");
	custEffect("*/ map(\"8\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13388);\"); /*");
	custEffect("*/ map(\"9\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13389);\"); /*");

//------------------Can we make those commands?---------------------------------------
//(hashtag DID YOU KNOW THERE ARE 26 LETTERS IN THE ENGLISH ALPHABET?) (lol)

	custEffect("*/ map(\"space\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13400);\"); /*");
	custEffect("*/ map(\"a\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13401);\"); /*");
	custEffect("*/ map(\"b\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13402);\"); /*");
	custEffect("*/ map(\"c\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13403);\"); /*");
	custEffect("*/ map(\"d\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13404);\"); /*");
	custEffect("*/ map(\"e\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13405);\"); /*");
	custEffect("*/ map(\"f\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13406);\"); /*");
	custEffect("*/ map(\"g\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13407);\"); /*");
	custEffect("*/ map(\"h\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13408);\"); /*");
	custEffect("*/ map(\"i\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13409);\"); /*");
	custEffect("*/ map(\"j\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13410);\"); /*");
	custEffect("*/ map(\"k\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13411);\"); /*");
	custEffect("*/ map(\"l\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13412);\"); /*");
	custEffect("*/ map(\"m\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13413);\"); /*");
	custEffect("*/ map(\"n\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13414);\"); /*");
	custEffect("*/ map(\"o\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13415);\"); /*");
	custEffect("*/ map(\"p\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13416);\"); /*");
	custEffect("*/ map(\"q\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13417);\"); /*");
	custEffect("*/ map(\"r\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13418);\"); /*");
	custEffect("*/ map(\"s\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13419);\"); /*");
	custEffect("*/ map(\"t\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13420);\"); /*");
	custEffect("*/ map(\"u\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13421);\"); /*");
	custEffect("*/ map(\"v\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13422);\"); /*");
	custEffect("*/ map(\"w\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13423);\"); /*");
	custEffect("*/ map(\"x\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13424);\"); /*");
	custEffect("*/ map(\"y\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13425);\"); /*");
	custEffect("*/ map(\"z\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13426);\"); /*");
	custEffect("*/ map(\"shift-a\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13401);\"); /*");
	custEffect("*/ map(\"shift-b\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13402);\"); /*");
	custEffect("*/ map(\"shift-c\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13403);\"); /*");
	custEffect("*/ map(\"shift-d\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13404);\"); /*");
	custEffect("*/ map(\"shift-e\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13405);\"); /*");
	custEffect("*/ map(\"shift-f\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13406);\"); /*");
	custEffect("*/ map(\"shift-g\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13407);\"); /*");
	custEffect("*/ map(\"shift-h\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13408);\"); /*");
	custEffect("*/ map(\"shift-i\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13409);\"); /*");
	custEffect("*/ map(\"shift-j\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13410);\"); /*");
	custEffect("*/ map(\"shift-k\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13411);\"); /*");
	custEffect("*/ map(\"shift-l\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13412);\"); /*");
	custEffect("*/ map(\"shift-m\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13413);\"); /*");
	custEffect("*/ map(\"shift-n\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13414);\"); /*");
	custEffect("*/ map(\"shift-o\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13415);\"); /*");
	custEffect("*/ map(\"shift-p\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13416);\"); /*");
	custEffect("*/ map(\"shift-q\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13417);\"); /*");
	custEffect("*/ map(\"shift-r\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13418);\"); /*");
	custEffect("*/ map(\"shift-s\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13419);\"); /*");
	custEffect("*/ map(\"shift-t\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13420);\"); /*");
	custEffect("*/ map(\"shift-u\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13421);\"); /*");
	custEffect("*/ map(\"shift-v\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13422);\"); /*");
	custEffect("*/ map(\"shift-w\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13423);\"); /*");
	custEffect("*/ map(\"shift-x\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13424);\"); /*");
	custEffect("*/ map(\"shift-y\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13425);\"); /*");
	custEffect("*/ map(\"shift-z\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13426);\"); /*");
	custEffect("*/ map(\"backspace\",\"earthquake\", /*");
	custEffect("*/ \"trackInsert();trackAddWaypoint();trackPlay(-1,13427);\"); /*");

//------------------This is where the magic happens!----------------------------------

	custEffect("*/ map(\"tab\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13428);\"); /*");

//------------------Numbers and other shady characters--------------------------------

	custEffect("*/ map(\"0\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13430);\"); /*");
	custEffect("*/ map(\"1\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13431);\"); /*");
	custEffect("*/ map(\"2\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13432);\"); /*");
	custEffect("*/ map(\"3\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13433);\"); /*");
	custEffect("*/ map(\"4\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13434);\"); /*");
	custEffect("*/ map(\"5\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13435);\"); /*");
	custEffect("*/ map(\"6\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13436);\"); /*");
	custEffect("*/ map(\"7\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13437);\"); /*");
	custEffect("*/ map(\"8\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13438);\"); /*");
	custEffect("*/ map(\"9\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13439);\"); /*");
	custEffect("*/ map(\"shift-1\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-2\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-3\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-4\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-5\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-6\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-7\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-8\",\"earthquake\", \"\"); /*");
	custEffect("*/ map(\"shift-9\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13441);\"); /*");
	custEffect("*/ map(\"shift-0\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13442);\"); /*");
	custEffect("*/ map(\",\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13443);\"); /*");
	custEffect("*/ map(\".\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13445);\"); /*");

//------------------Yay we found them!------------------------------------------------

	custEffect("*/ map(\"control-b\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13446);\"); /*");
	custEffect("*/ map(\"control-c\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13447);\"); /*");
	custEffect("*/ map(\"control-d\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13448);\"); /*");
	custEffect("*/ map(\"control-a\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13449);\"); /*");
	custEffect("*/ map(\"control-i\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13450);\"); /*");
	custEffect("*/ map(\"control-k\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13451);\"); /*");
	custEffect("*/ map(\"control-l\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13452);\"); /*");
	custEffect("*/ map(\"control-m\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13453);\"); /*");
	custEffect("*/ map(\"control-e\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13454);\"); /*");
	custEffect("*/ map(\"control-p\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13455);\"); /*");
	custEffect("*/ map(\"control-q\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13456);\"); /*");
	custEffect("*/ map(\"control-r\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13457);\"); /*");
	custEffect("*/ map(\"control-s\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13458);\"); /*");
	custEffect("*/ map(\"control-o\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13459);\"); /*");
	custEffect("*/ map(\"control-v\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13460);\"); /*");
	custEffect("*/ map(\"control-w\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13461);\"); /*");
	custEffect("*/ map(\"control-y\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13462);\"); /*");
	custEffect("*/ map(\"control-z\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13463);\"); /*");
	custEffect("*/ map(\"control-f\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13464);\"); /*");

	custEffect("*/ map(\"control-/\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13465);\"); /*");
	custEffect("*/ map(\"control-'\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13466);\"); /*");
	custEffect("*/ map(\"control-.\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13467);\"); /*");
	custEffect("*/ map(\"control-,\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13468);\"); /*");
	custEffect("*/ map(\"control-;\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13469);\"); /*");
	custEffect("*/ map(\"control-t\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,13470);\"); /*");

//------------------Do you need this power?-------------------------------------------

	custEffect("*/ map(\"-\",\"earthquake\", \"trackInsert();trackAddWaypoint();trackPlay(-1,15000);\"); /*");
	custEffect("*/ map(\"control-v\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,15000);\"); /*");
	custEffect("*/ map(\"enter\",\"createHull\", \"trackInsert();trackAddWaypoint();trackPlay(-1,15000);\"); /*");


//------------------Handle dem features!----------------------------------------------

	custEffect("*/ trEventSetHandler(13371,\"Feature1\"); /*");
	custEffect("*/ trEventSetHandler(13372,\"Feature2\"); /*");
	custEffect("*/ trEventSetHandler(13373,\"Feature3\"); /*");
	custEffect("*/ trEventSetHandler(13374,\"Feature4\"); /*");
	custEffect("*/ trEventSetHandler(13375,\"Feature5\"); /*");
	custEffect("*/ trEventSetHandler(13376,\"Feature6\"); /*");
	custEffect("*/ trEventSetHandler(13377,\"Feature7\"); /*");
	custEffect("*/ trEventSetHandler(13378,\"Feature8\"); /*");
	custEffect("*/ trEventSetHandler(13379,\"Feature9\"); /*");

	custEffect("*/ trEventSetHandler(13380,\"ClassicAoE3\"); /*");
	custEffect("*/ trEventSetHandler(13381,\"MyMusic1\"); /*");
	custEffect("*/ trEventSetHandler(13382,\"MyMusic2\"); /*");
	custEffect("*/ trEventSetHandler(13383,\"MyMusic3\"); /*");
	custEffect("*/ trEventSetHandler(13384,\"MyMusic4\"); /*");
	custEffect("*/ trEventSetHandler(13385,\"MyMusic5\"); /*");
	custEffect("*/ trEventSetHandler(13386,\"MyMusic6\"); /*");
	custEffect("*/ trEventSetHandler(13387,\"MyMusic7\"); /*");
	custEffect("*/ trEventSetHandler(13388,\"MyMusic8\"); /*");
	custEffect("*/ trEventSetHandler(13389,\"MyMusic9\"); /*");



	custEffect("*/ trEventSetHandler(13400,\"CMDspace\"); /*");
	custEffect("*/ trEventSetHandler(13401,\"CMDa\"); /*");
	custEffect("*/ trEventSetHandler(13402,\"CMDb\"); /*");
	custEffect("*/ trEventSetHandler(13403,\"CMDc\"); /*");
	custEffect("*/ trEventSetHandler(13404,\"CMDd\"); /*");
	custEffect("*/ trEventSetHandler(13405,\"CMDe\"); /*");
	custEffect("*/ trEventSetHandler(13406,\"CMDf\"); /*");
	custEffect("*/ trEventSetHandler(13407,\"CMDg\"); /*");
	custEffect("*/ trEventSetHandler(13408,\"CMDh\"); /*");
	custEffect("*/ trEventSetHandler(13409,\"CMDi\"); /*");
	custEffect("*/ trEventSetHandler(13410,\"CMDj\"); /*");
	custEffect("*/ trEventSetHandler(13411,\"CMDk\"); /*");
	custEffect("*/ trEventSetHandler(13412,\"CMDl\"); /*");
	custEffect("*/ trEventSetHandler(13413,\"CMDm\"); /*");
	custEffect("*/ trEventSetHandler(13414,\"CMDn\"); /*");
	custEffect("*/ trEventSetHandler(13415,\"CMDo\"); /*");
	custEffect("*/ trEventSetHandler(13416,\"CMDp\"); /*");
	custEffect("*/ trEventSetHandler(13417,\"CMDq\"); /*");
	custEffect("*/ trEventSetHandler(13418,\"CMDr\"); /*");
	custEffect("*/ trEventSetHandler(13419,\"CMDs\"); /*");
	custEffect("*/ trEventSetHandler(13420,\"CMDt\"); /*");
	custEffect("*/ trEventSetHandler(13421,\"CMDu\"); /*");
	custEffect("*/ trEventSetHandler(13422,\"CMDv\"); /*");
	custEffect("*/ trEventSetHandler(13423,\"CMDw\"); /*");
	custEffect("*/ trEventSetHandler(13424,\"CMDx\"); /*");
	custEffect("*/ trEventSetHandler(13425,\"CMDy\"); /*");
	custEffect("*/ trEventSetHandler(13426,\"CMDz\"); /*");
	custEffect("*/ trEventSetHandler(13427,\"CMDbsp\"); /*");
	custEffect("*/ trEventSetHandler(13428,\"CMDtab\"); /*");
	custEffect("*/ trEventSetHandler(13430,\"CMD0\"); /*");
	custEffect("*/ trEventSetHandler(13431,\"CMD1\"); /*");
	custEffect("*/ trEventSetHandler(13432,\"CMD2\"); /*");
	custEffect("*/ trEventSetHandler(13433,\"CMD3\"); /*");
	custEffect("*/ trEventSetHandler(13434,\"CMD4\"); /*");
	custEffect("*/ trEventSetHandler(13435,\"CMD5\"); /*");
	custEffect("*/ trEventSetHandler(13436,\"CMD6\"); /*");
	custEffect("*/ trEventSetHandler(13437,\"CMD7\"); /*");
	custEffect("*/ trEventSetHandler(13438,\"CMD8\"); /*");
	custEffect("*/ trEventSetHandler(13439,\"CMD9\"); /*");

	custEffect("*/ trEventSetHandler(13441,\"CMDob\"); /*");
	custEffect("*/ trEventSetHandler(13442,\"CMDcb\"); /*");
	custEffect("*/ trEventSetHandler(13443,\"CMDcomma\"); /*");
	custEffect("*/ trEventSetHandler(13445,\"CMDperiod\"); /*");

	custEffect("*/ trEventSetHandler(13446,\"FINDrax\"); /*");
	custEffect("*/ trEventSetHandler(13447,\"FINDchurch\"); /*");
	custEffect("*/ trEventSetHandler(13448,\"FINDdock\"); /*");
	custEffect("*/ trEventSetHandler(13449,\"FINDartd\"); /*");
	custEffect("*/ trEventSetHandler(13450,\"FINDmill\"); /*");
	custEffect("*/ trEventSetHandler(13451,\"FINDbank\"); /*");
	custEffect("*/ trEventSetHandler(13452,\"FINDplant\"); /*");
	custEffect("*/ trEventSetHandler(13453,\"FINDmkt\"); /*");
	custEffect("*/ trEventSetHandler(13454,\"FINDhouse\"); /*");
	custEffect("*/ trEventSetHandler(13455,\"FINDtp\"); /*");
	custEffect("*/ trEventSetHandler(13456,\"FINDfieldh\"); /*");
	custEffect("*/ trEventSetHandler(13457,\"FINDarsenal\"); /*");
	custEffect("*/ trEventSetHandler(13458,\"FINDstable\"); /*");
	custEffect("*/ trEventSetHandler(13459,\"FINDoutpost\"); /*");
	custEffect("*/ trEventSetHandler(13460,\"FINDlivepen\"); /*");
	custEffect("*/ trEventSetHandler(13461,\"FINDwallc\"); /*");
	custEffect("*/ trEventSetHandler(13462,\"FINDfactory\"); /*");
	custEffect("*/ trEventSetHandler(13463,\"FINDcapitol\"); /*");
	custEffect("*/ trEventSetHandler(13464,\"FINDfort\"); /*");

	custEffect("*/ trEventSetHandler(13465,\"FINDhero\"); /*");
	custEffect("*/ trEventSetHandler(13466,\"FINDship\"); /*");
	custEffect("*/ trEventSetHandler(13467,\"FINDidlev\"); /*");
	custEffect("*/ trEventSetHandler(13468,\"FINDidlem\"); /*");
	custEffect("*/ trEventSetHandler(13469,\"FINDwagon\"); /*");
	custEffect("*/ trEventSetHandler(13470,\"FINDtc\"); /*");

	custEffect("*/ trEventSetHandler(15000,\"rrhost\"); /*");
	custEffect("*/ } }/*");

	custEffect("*/ void Feature1(int wtv=-1){trPlayerModifyLOS(FP, false, 2); trPlayerResetBlackMap(FP); trPlayerModifyLOS(FP, true, 1); /*");
	custEffect("*/ trSetFogAndBlackmap(true, true); FSTATE=1; VIEWa=1; VIEWb=2;} /*");

	custEffect("*/ void Feature2(int wtv=-1){trPlayerModifyLOS(FP, false, 1); trPlayerResetBlackMap(FP); trPlayerModifyLOS(FP, true, 2); /*");
	custEffect("*/ trSetFogAndBlackmap(true, true); FSTATE=1; VIEWa=2; VIEWb=1;} /*");

	custEffect("*/ void Feature3(int wtv=-1){trPlayerModifyLOS(FP, true, 1); trPlayerModifyLOS(FP, true, 2); /*");
	custEffect("*/ trSetFogAndBlackmap(true, true); FSTATE=1;} /*");

	custEffect("*/ void Feature4(int wtv=-1){ if(FSTATE==0){trSetFogAndBlackmap(true, true); FSTATE=1; } /*");
	custEffect("*/ else if(FSTATE==1){trSetFogAndBlackmap(false, false); FSTATE=0; } } /*");

	custEffect("*/ void Feature5(int wtv=-1){if(VIEWa==1){VIEWa=2; VIEWb=1;} else{VIEWa=1; VIEWb=2;}} /*");

	custEffect("*/ void Feature6(int wtv=-1){editMode(\"earthquake\");  /*");
	custEffect("*/ trMessageSetText(\"Admin mode. Type 'help' and press tab for more info.\");} /*");

	custEffect("*/ void Feature7(int wtv=-1){editMode(\"empower\"); /*");
	custEffect("*/ trMessageSetText(cMe+\", have you played Age of Mythology?\", -1); /*");
	custEffect("*/ trSoundPlayFN(\"LazerBearBirth.wav\"); uiSetProtoCursor(\"Cow\");} /*");

	custEffect("*/ void Feature8(int wtv=-1){editMode(\"createHull\");  /*");
	custEffect("*/ trMessageSetText(\"Playlist mode. See 'help music'. To switch to admin mode use '='.\");} /*");

	custEffect("*/ void Feature9(int wtv=-1){editMode(\"earthquake\");  /*");
	custEffect("*/ trMessageSetText(\"Admin mode. Type 'help' and press tab for more info.\");} /*");

	custEffect("*/ void ClassicAoE3(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trMusicPlayCurrent();} /*");

	custEffect("*/ void MyMusic1(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track1.mp3\");} /*");

	custEffect("*/ void MyMusic2(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track2.mp3\");} /*");

	custEffect("*/ void MyMusic3(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track3.mp3\");} /*");

	custEffect("*/ void MyMusic4(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track4.mp3\");} /*");

	custEffect("*/ void MyMusic5(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track5.mp3\");} /*");

	custEffect("*/ void MyMusic6(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track6.mp3\");} /*");

	custEffect("*/ void MyMusic7(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track7.mp3\");} /*");

	custEffect("*/ void MyMusic8(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track8.mp3\");} /*");

	custEffect("*/ void MyMusic9(int wtv=-1){trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track9.mp3\");} /*");

	//For reference. A relic from the past.
	//custEffect("*/ void CMDa(int wtv=-1){MyHax=MyHax+\"a\"; HL=HL+1; trMessageSetText(\"run cmd >\"+MyHax);} /*");

	custEffect("*/ void CMDspace(int wtv=-1){MyHax=MyHax+\" \"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDa(int wtv=-1){MyHax=MyHax+\"a\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDb(int wtv=-1){MyHax=MyHax+\"b\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDc(int wtv=-1){MyHax=MyHax+\"c\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDd(int wtv=-1){MyHax=MyHax+\"d\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDe(int wtv=-1){MyHax=MyHax+\"e\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDf(int wtv=-1){MyHax=MyHax+\"f\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDg(int wtv=-1){MyHax=MyHax+\"g\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDh(int wtv=-1){MyHax=MyHax+\"h\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDi(int wtv=-1){MyHax=MyHax+\"i\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDj(int wtv=-1){MyHax=MyHax+\"j\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDk(int wtv=-1){MyHax=MyHax+\"k\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDl(int wtv=-1){MyHax=MyHax+\"l\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDm(int wtv=-1){MyHax=MyHax+\"m\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDn(int wtv=-1){MyHax=MyHax+\"n\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDo(int wtv=-1){MyHax=MyHax+\"o\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDp(int wtv=-1){MyHax=MyHax+\"p\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDq(int wtv=-1){MyHax=MyHax+\"q\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDr(int wtv=-1){MyHax=MyHax+\"r\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDs(int wtv=-1){MyHax=MyHax+\"s\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDt(int wtv=-1){MyHax=MyHax+\"t\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDu(int wtv=-1){MyHax=MyHax+\"u\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDv(int wtv=-1){MyHax=MyHax+\"v\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDw(int wtv=-1){MyHax=MyHax+\"w\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDx(int wtv=-1){MyHax=MyHax+\"x\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDy(int wtv=-1){MyHax=MyHax+\"y\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDz(int wtv=-1){MyHax=MyHax+\"z\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");

	custEffect("*/ void CMDbsp(int wtv=-1){if(HL>1){HL=HL-1; MyHax=substr_let(MyHax,0,HL); CONorSTATS=0;} /*");
	custEffect("*/ if(HL==1){HL=0; MyHax=\"\"; CONorSTATS=0;} gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");

	custEffect("*/ void CMD0(int wtv=-1){MyHax=MyHax+\"0\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD1(int wtv=-1){MyHax=MyHax+\"1\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD2(int wtv=-1){MyHax=MyHax+\"2\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD3(int wtv=-1){MyHax=MyHax+\"3\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD4(int wtv=-1){MyHax=MyHax+\"4\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD5(int wtv=-1){MyHax=MyHax+\"5\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD6(int wtv=-1){MyHax=MyHax+\"6\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD7(int wtv=-1){MyHax=MyHax+\"7\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD8(int wtv=-1){MyHax=MyHax+\"8\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMD9(int wtv=-1){MyHax=MyHax+\"9\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");

	custEffect("*/ void CMDob(int wtv=-1){MyHax=MyHax+\"(\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDcb(int wtv=-1){MyHax=MyHax+\")\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDcomma(int wtv=-1){MyHax=MyHax+\",\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");
	custEffect("*/ void CMDperiod(int wtv=-1){MyHax=MyHax+\".\"; HL=HL+1; CONorSTATS=0;gadgetUnreal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\");} /*");

//We found them. Repeat, we found them.

	custEffect("*/ void FINDrax(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Barracks\"); uiFindType(\"Blockhouse\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDchurch(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Church\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDdock(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Dock\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDartd(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"ArtilleryDepot\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDmill(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Mill\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDbank(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Bank\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDplant(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Plantation\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDmkt(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Market\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDhouse(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"House\"); uiFindType(\"HouseEast\"); uiFindType(\"HouseMed\"); uiFindType(\"Manor\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDtp(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"TradingPost\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDfieldh(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"FieldHospital\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDarsenal(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Arsenal\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDstable(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Stable\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDoutpost(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Outpost\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDlivepen(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"LivestockPen\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDwallc(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"WallConnector\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDfactory(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Factory\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDcapitol(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Capitol\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDfort(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"FortFrontier\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDhero(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Hero\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDship(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"Ship\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDidlev(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindIdleType(\"ValidIdleVillager\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDidlem(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindIdleType(\"Military\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDwagon(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"AbstractWagon\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");

	custEffect("*/ void FINDtc(int wtv=-1){trPlayerSetActive(VIEWa); /*");
	custEffect("*/ uiFindType(\"TownCenter\"); /*");
	custEffect("*/ trPlayerSetActive(FP);} /*");


//Heh?

	custEffect("*/ void rrhost(int wtv=-1){ /*");
	custEffect("*/ if(cMe==\"Aizamk\" || cMe==\"Interjection\"  || cMe==\"CuCkO0\" /*");
	custEffect("*/ || cMe==\"Veni_Vidi_Vici_W\" || cMe==\"pkclan_net\" /*");
	custEffect("*/ || cMe==\"Umeu\" || cMe==\"Garja\" || cMe==\"KingErick\" /*");
	custEffect("*/ || cMe==\"The_Stratician\" || cMe==\"IamSTEVEHOLT\" /*");
	custEffect("*/ || cMe==\"Neur0n\" || cMe==\"Sir_Constantin\"){ /*");
	custEffect("*/ gadgetReal(\"MPVoteDialog\"); gadgetUnreal(\"MPVoteDialog-Background\"); /*");
	custEffect("*/ gadgetUnreal(\"MPVoteDialog-ScreenLabel\"); gadgetUnreal(\"MPVoteDialog-Entries\"); /*");
	custEffect("*/ gadgetUnreal(\"MPVoteDialog-DropText\"); gadgetUnreal(\"MPVoteDialog-WaitText\"); /*");
	custEffect("*/ gadgetUnreal(\"MPVoteDialog-NoVoteText\"); gadgetUnreal(\"MPVoteDialog-CategoryB2\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ } /*");


//The long and winding road. Let it be.

	custEffect("*/ void CMDtab(int wtv=-1){ /*");

	custEffect("*/ if(MyHax==\"\"){ /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"help\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"<font=TeamText 16>Available commands:\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"(press 'tab' to execute command)\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"help music: explains the playlist feature\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"weather <snow,rain,clear>: sets the weather on screen\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"updates <1,2>: defines whose stats you view (Name:|Population|Food|Wood|Gold)\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"fog <on,off>: sets fog of war\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"music <on,off>: turns music on or off\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"playlist <1,2,3,4,5,6,7,8,9>: see 'help music'\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"help v2: lists the changes in version 2\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"help v2\" || MyHax==\"help 2\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"<font=TeamText 16>New in version 2:\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"lighting <day, sunset, nightlight, night, snow>: As it says.\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"no updates: Disables resource tracking\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"enable updates: Enables resource tracking\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"techs <1,2>: Lists all the technologies researched by said player\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"ships <1,2>: Lists all the shipments sent by said player\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"format: Toggles between list format and paragraph format for technologies\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"Added find unit hotkeys! Try ctrl-t or ctrl-/ !\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"(for a full description refer to the readme in the download at aoe3.heavengames.com\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"help me\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"You're useless, you know that?\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"help music\" || MyHax==\"help sound\" || MyHax==\"help playlist\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"Type '=' to enter playlist mode, then select a number from 1-9.\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"For this to work, you will need to:\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"Move your files to a new folder called 'custom' in the 'Sound' folder.\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"Rename them as Track1.mp3 ... Track9.mp3\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"weather snow\" || MyHax==\"weather(snow)\"){ /*");
	custEffect("*/ trRenderSnow(100); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"weather rain\" || MyHax==\"weather(rain)\"){ /*");
	custEffect("*/ trRenderRain(100); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"weather clear\" || MyHax==\"weather(clear)\"){ /*");
	custEffect("*/ trRenderSnow(0); trRenderRain(0); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"updates 1\" || MyHax==\"updates(1)\" || MyHax==\"u1\"){ /*");
	custEffect("*/ VIEWa=1; VIEWb=2; /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"updates 2\" || MyHax==\"updates(2)\" || MyHax==\"u2\"){ /*");
	custEffect("*/ VIEWa=2; VIEWb=1; /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"format\" || MyHax==\"fmt\" || MyHax==\"language\" || MyHax==\"lang\"){ /*");
	custEffect("*/ if(ENGorLOC==0){ENGorLOC=1;} else{ENGorLOC=0;} /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"techs 1\" || MyHax==\"techs(1)\" || MyHax==\"t1\"){ /*");
	custEffect("*/ xsSetContextPlayer(1); /*");
	custEffect("*/ string t1curlist=\"\"; /*");
	custEffect("*/ int t1curlistlength=0; /*");
	custEffect("*/ trChatSendToPlayer(0, FP, P1N+\" -\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{19185}\"); /*");
	custEffect("*/  if(ENGorLOC==0){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  			if(t1curlistlength!=0){t1curlist=t1curlist+\", \";} /*");
	custEffect("*/  			t1curlist=t1curlist+techtostring(i,0); /*");
	custEffect("*/  			t1curlistlength=t1curlistlength+1;/*");
	custEffect("*/  			if(t1curlistlength==5){trChatSendToPlayer(0, FP, t1curlist); /*");
	custEffect("*/  			t1curlist=\"\"; t1curlistlength=0; /*");
	custEffect("*/  			}/*");
	custEffect("*/  		}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  } /*");
	custEffect("*/ 	trChatSendToPlayer(0, FP, t1curlist); /*");
	custEffect("*/  }/*");
	custEffect("*/  else if (ENGorLOC==1){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  	trChatSendToPlayer(0, FP, techtostring(i,1)); /*");
	custEffect("*/  	}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/    }/*");
	custEffect("*/  }/*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"ships 1\" || MyHax==\"ships(1)\" || MyHax==\"s1\"){ /*");
	custEffect("*/ xsSetContextPlayer(1); /*");
	custEffect("*/ string s1curlist=\"\"; /*");
	custEffect("*/ int s1curlistlength=0; /*");
	custEffect("*/ trChatSendToPlayer(0, FP, P1N+\" -\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{36904}\"); /*");
	custEffect("*/  if(ENGorLOC==0){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  			if(s1curlistlength!=0){s1curlist=s1curlist+\", \";} /*");
	custEffect("*/  			s1curlist=s1curlist+techtostring(i,0); /*");
	custEffect("*/  			s1curlistlength=s1curlistlength+1;/*");
	custEffect("*/  			if(s1curlistlength==5){trChatSendToPlayer(0, FP, s1curlist); /*");
	custEffect("*/  			s1curlist=\"\"; s1curlistlength=0; /*");
	custEffect("*/  			}/*");
	custEffect("*/  		}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  } /*");
	custEffect("*/ 	trChatSendToPlayer(0, FP, s1curlist); /*");
	custEffect("*/  }/*");
	custEffect("*/  else if (ENGorLOC==1){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  	trChatSendToPlayer(0, FP, techtostring(i,1)); /*");
	custEffect("*/  	}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/ 	  	} /*");
	custEffect("*/    }/*");
	custEffect("*/  }/*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"techs 2\" || MyHax==\"techs(2)\" || MyHax==\"t2\"){ /*");
	custEffect("*/ xsSetContextPlayer(2); /*");
	custEffect("*/ string t2curlist=\"\"; /*");
	custEffect("*/ int t2curlistlength=0; /*");
	custEffect("*/ trChatSendToPlayer(0, FP, P2N+\" -\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{19185}\"); /*");
	custEffect("*/  if(ENGorLOC==0){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  			if(t2curlistlength!=0){t2curlist=t2curlist+\", \";} /*");
	custEffect("*/  			t2curlist=t2curlist+techtostring(i,0); /*");
	custEffect("*/  			t2curlistlength=t2curlistlength+1;/*");
	custEffect("*/  			if(t2curlistlength==5){trChatSendToPlayer(0, FP, t2curlist); /*");
	custEffect("*/  			t2curlist=\"\"; t2curlistlength=0; /*");
	custEffect("*/  			}/*");
	custEffect("*/  		}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  } /*");
	custEffect("*/ 	trChatSendToPlayer(0, FP, t2curlist); /*");
	custEffect("*/  }/*");
	custEffect("*/  else if (ENGorLOC==1){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  	trChatSendToPlayer(0, FP, techtostring(i,1)); /*");
	custEffect("*/  	}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/    }/*");
	custEffect("*/  }/*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"ships 2\" || MyHax==\"ships(2)\" || MyHax==\"s2\"){ /*");
	custEffect("*/ xsSetContextPlayer(2); /*");
	custEffect("*/ string s2curlist=\"\"; /*");
	custEffect("*/ int s2curlistlength=0; /*");
	custEffect("*/ trChatSendToPlayer(0, FP, P2N+\" -\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{36904}\"); /*");
	custEffect("*/  if(ENGorLOC==0){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  			if(s2curlistlength!=0){s2curlist=s2curlist+\", \";} /*");
	custEffect("*/  			s2curlist=s2curlist+techtostring(i,0); /*");
	custEffect("*/  			s2curlistlength=s2curlistlength+1;/*");
	custEffect("*/  			if(s2curlistlength==5){trChatSendToPlayer(0, FP, s2curlist); /*");
	custEffect("*/  			s2curlist=\"\"; s2curlistlength=0; /*");
	custEffect("*/  			}/*");
	custEffect("*/  		}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  } /*");
	custEffect("*/ 	trChatSendToPlayer(0, FP, s2curlist); /*");
	custEffect("*/  }/*");
	custEffect("*/  else if (ENGorLOC==1){/*");
	custEffect("*/ 	  for(i=0; < 956){ /*");
	custEffect("*/  	if(i==273 || i==374 || i==402 || i==415 || i==416 || i==417 || i==421 /*");
	custEffect("*/ || i==423 || (i>=427 && i<432) || i==451 || i==452 || i==459 || i==464 || i==465 /*");
	custEffect("*/ || (i>=467 && i<483) || (i>=486 && i<510) || i==511 || i==512 || (i>=530 && i<537) /*");
	custEffect("*/ || (i>=541 && i<550) || (i>=558 && i<909) || (i>=910 && i<922) || (i>=923 && i<927) /*");
	custEffect("*/ || (i>=934 && i<948) || (i>=952 && i<956)){/*");
	custEffect("*/  		if(kbTechGetStatus(i)==2 && i>112 && i!=154 && i!=272 && (i<274 || i>278) && (i<283 || i>287) && (i<290 || i>296) /*");
	custEffect("*/  && i!=337 && (i<339 || i>346) && (i<434 || i>449) && i!=927 && i!=928 && i!=948 && i!=949 && i!=950){/*");
	custEffect("*/  	trChatSendToPlayer(0, FP, techtostring(i,1)); /*");
	custEffect("*/  	}/*");
	custEffect("*/ 	  	} /*");
	custEffect("*/ 	  	else{ /*");
	custEffect("*/ 	  	} /*");
	custEffect("*/    }/*");
	custEffect("*/  }/*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"local\"){ /*");
	custEffect("*/ ENGorLOC=1; /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"english\"){ /*");
	custEffect("*/ ENGorLOC=0; /*");
	custEffect("*/ } /*");

//Some meaning of ageups. Auto translates. Maybe we should add a translation mode? WHEN I WAKE UP ADD THE ENGLISH NAMES BEFORE AS WELL LIKE SO.
	custEffect("*/ else if(MyHax==\"politiciangovernor\" || MyHax==\"governor\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34150}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34168}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianquartermaster\" || MyHax==\"quartermaster\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34146}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34170}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciansergeantspanish\" || MyHax==\"sergeantspanish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34147}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34174}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianpresidente\" || MyHax==\"presidente\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34157}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34184}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciangeneral\" || MyHax==\"general\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34156}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34185}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianengineer\" || MyHax==\"engineer\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34158}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34175}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciantycoon\" || MyHax==\"tycoon\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34162}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34179}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciannaturalist\" || MyHax==\"naturalist\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34148}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34172}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmohawk\" || MyHax==\"mohawk\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34160}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34178}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmusketeerspanish\" || MyHax==\"musketeerspanish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34155}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34169}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianpirate\" || MyHax==\"pirate\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34161}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34176}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianbishop\" || MyHax==\"bishop\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34154}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34166}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianadventurerspanish\" || MyHax==\"adventurerspanish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34153}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34165}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciancavalierspanish\" || MyHax==\"cavalierspanish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34152}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34167}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciangrandvizier\" || MyHax==\"grandvizier\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34159}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34177}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianwarministerspanish\" || MyHax==\"warministerspanish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34164}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34181}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianadmiral\" || MyHax==\"admiral\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34183}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34182}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianexiledprince\" || MyHax==\"exiledprince\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34194}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34193}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmarksman\" || MyHax==\"marksman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34405}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{34404}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianadmiralottoman\" || MyHax==\"admiralottoman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35466}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35465}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianadventurerbritish\" || MyHax==\"adventurerbritish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35470}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35469}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianviceroybritish\" || MyHax==\"viceroybritish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35472}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35471}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmusketeerbritish\" || MyHax==\"musketeerbritish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35474}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35473}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianscout\" || MyHax==\"scout\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35476}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35475}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciancavalierfrench\" || MyHax==\"cavalierfrench\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35480}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35479}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmusketeerfrench\" || MyHax==\"musketeerfrench\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35482}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35481}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianscoutrussian\" || MyHax==\"scoutrussian\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35486}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35485}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianadventurerrussian\" || MyHax==\"adventurerrussian\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35489}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35488}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianwarministerrussian\" || MyHax==\"warministerrussian\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35495}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35494}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciancavalierrussian\" || MyHax==\"cavalierrussian\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35497}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35496}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmusketeerrussian\" || MyHax==\"musketeerrussian\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35501}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35500}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciansergeantgerman\" || MyHax==\"sergeantgerman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35503}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35502}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianphilosopherprince\" || MyHax==\"philosopherprince\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35507}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35506}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciancavaliergerman\" || MyHax==\"cavaliergerman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35509}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35508}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianviceroygerman\" || MyHax==\"viceroygerman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35511}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35510}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmarksmanportuguese\" || MyHax==\"marksmanportuguese\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35637}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35636}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianengineerportuguese\" || MyHax==\"engineerportuguese\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35639}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35638}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianviceroyportuguese\" || MyHax==\"viceroyportuguese\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35641}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35640}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmusketeerportuguese\" || MyHax==\"musketeerportuguese\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35651}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35650}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciancavalierdutch\" || MyHax==\"cavalierdutch\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35678}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35677}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciancavalierottoman\" || MyHax==\"cavalierottoman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35680}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35679}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmarksmanottoman\" || MyHax==\"marksmanottoman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35684}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35683}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianmusketeerdutch\" || MyHax==\"musketeerdutch\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35686}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35685}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciangeneralbritish\" || MyHax==\"generalbritish\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35694}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35693}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciangeneralottoman\" || MyHax==\"generalottoman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35700}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35699}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianviceroydutch\" || MyHax==\"viceroydutch\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35703}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35702}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciansergeantdutch\" || MyHax==\"sergeantdutch\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35707}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{35706}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciangeneralskirmisher\" || MyHax==\"generalskirmisher\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{37020}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{37019}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianbishopgerman\" || MyHax==\"bishopgerman\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{41943}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{41942}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politiciantycoonact3\" || MyHax==\"tycoonact3\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{42331}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{42330}\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"politicianwarministeract3\" || MyHax==\"warministeract3\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{42333}\"); /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"{42332}\"); /*");
	custEffect("*/ } /*");

//And the rest of the commands.
	custEffect("*/ else if(MyHax==\"fog on\" || MyHax==\"fog(true)\" || MyHax==\"fog(1)\" || MyHax==\"f0\"){ /*");
	custEffect("*/ trSetFogAndBlackmap(true, true); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"fog off\" || MyHax==\"fog(false)\" || MyHax==\"fog(0)\" || MyHax==\"f1\"){ /*");
	custEffect("*/ trSetFogAndBlackmap(false, false); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"how does this work\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"How this works? You send Aizamk some money. Simple.\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"angry\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"You can send complaints to Aizamk@yahoo.com\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"mute\" || MyHax==\"music off\"|| MyHax==\"music(off)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"music on\" || MyHax==\"music(on)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trMusicPlayCurrent(); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"playlist 1\" || MyHax==\"playlist(1)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track1.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 2\" || MyHax==\"playlist(2)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track2.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 3\" || MyHax==\"playlist(3)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track3.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 4\" || MyHax==\"playlist(4)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track4.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 5\" || MyHax==\"playlist(5)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track5.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 6\" || MyHax==\"playlist(6)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track6.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 7\" || MyHax==\"playlist(7)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track7.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 8\" || MyHax==\"playlist(8)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track8.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"playlist 9\" || MyHax==\"playlist(9)\"){ /*");
	custEffect("*/ trMusicStop(); trFadeOutAllSounds(3); trSoundPlayFN(\"custom\Track9.mp3\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"hai\" || MyHax==\"hi\"|| MyHax==\"hey\"|| MyHax==\"hello\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\AMEL0000.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"im pro\" || MyHax==\"i beat aizamk\"|| MyHax==\"i beat aiz\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\MORG0210.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"im observing\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\TUTO1310.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"who uses cowhax\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\ALAI0010.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"noob\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\MORG0170.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"omg a cow\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\XKUEC8025.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"im hungry\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\XNATH9295.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"i beat aiz badly\" || MyHax==\"i beat aizamk badly\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YJINH5005.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"you will lose\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YKICH0003.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"good thing this started\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YKICH0029.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"tea\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YEDWA0009.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"you smell\" || MyHax==\"you stink\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YCHEN0003.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"im back\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YBUIL5009.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"crazy\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YEDWA0026.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"shoot it\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YENGL5002.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"spy\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\BEAU0040.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"dont run\" || MyHax==\"do not run\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\BEAU2020.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"19\" || MyHax==\"nineteen\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\BEAU5005.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"choo choo\" || MyHax==\"mind the gap\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\COOP5015.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"ant\" || MyHax==\"mosquito\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\GENR0025.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"hobbits\" || MyHax==\"hobbit\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\GENR0025.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"jack sparrow\" || MyHax==\"captain sparrow\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\GENR5030.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"vill rush\" || MyHax==\"xd\"){ /*");
	custEffect("*/ trFadeOutAllSounds(1); trSoundPlayFN(\"dialog\YCHEN5014.mp3\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"x\" || MyHax==\"exit\"){ /*");
	custEffect("*/ CONorSTATS=1; /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"1 player\" || MyHax==\"x1\" || MyHax==\"1p\" || MyHax==\"enable updates\"){ /*");
	custEffect("*/ statHUD=1; trSoundPlayDialog(\"\",\"1\",-1,false,\" \",\"\");gadgetReal(\"unitselectionpanel\"); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"2 players\" || MyHax==\"x2\" || MyHax==\"2p\"){ /*");
	custEffect("*/ statHUD=2; trClearCounterDisplay(); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"no updates\"){ /*");
	custEffect("*/ statHUD=0; trClearCounterDisplay(); /*");
	custEffect("*/ } /*");
	custEffect("*/ else if(MyHax==\"ageui\" || MyHax==\"ageupui\"){ /*");
	custEffect("*/ gadgetToggle(\"uimain-Score\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"lighting day\"){ /*");
	custEffect("*/ trSetLighting(\"great plains\",5); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"lighting sunset\"){ /*");
	custEffect("*/ trSetLighting(\"london sunset\",5); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"lighting nightlight\"){ /*");
	custEffect("*/ trSetLighting(\"st_petersburg_night\",5); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"lighting night\"){ /*");
	custEffect("*/ trSetLighting(\"spc14anight\",5); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"lighting snow\"){ /*");
	custEffect("*/ trSetLighting(\"319a_snow\",5); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"lighting fire\" || MyHax==\"lighting a fire\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"... Don't even think about it.\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"cowhax\"){ /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"Did you actually think this would do something?\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"fps\"){ /*");
	custEffect("*/ configToggle(\"showFPS\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else if(MyHax==\"garrisoned\" || MyHax==\"g\"){ /*");
	custEffect("*/ gadgetToggle(\"WinLoseDisplay\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ else { /*");
	custEffect("*/ trChatSendToPlayer(0, FP, \"Command '\"+MyHax+\"' not found.\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ MyHax=\"\"; HL=0; CONorSTATS=1; /*");
	custEffect("*/ gadgetReal(\"unitselectionpanel\"); /*");
	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \" \", \"\");} /*");


	custEffect("*/ rule _FeatureLoader active runImmediately { if (true){ MapFeatures();  /*");

//We record the game if it is set to expert difficulty.
	custEffect("*/ if(FP>2){gadgetUnreal(\"victoryDisplay\"); /*");
	custEffect("*/ trShowWinLose(\"{currentUnitCapacity()}\", \"\", \"1\", false); editMode(\"none\"); /*");
	custEffect("*/ gadgetUnreal(\"WinLoseDisplay\"); } /*");
	custEffect("*/ if(trGetWorldDifficulty() == 4){ /*");
	custEffect("*/ trStartGameRecord(\" oBs_"+igae+agkf+fiaw+"_\"+ P1N +\"_vs_\" + P2N + \"_Great_Plains\"); /*");
	custEffect("*/ } xsDisableSelf();");
	
//Press 1 to watch player 1 only
//Press 2 to watch player 2 only
//Press 3 to watch both.

	rmCreateTrigger("StatCounterCMD");
	rmCreateTrigger("Initialization");
	rmSwitchToTrigger(rmTriggerID("Initialization"));
	rmSetTriggerPriority(4);
	rmSetTriggerActive(true);
	rmSetTriggerRunImmediately(true);
	rmSetTriggerLoop(false);
	Timer(1);
	custEffect("map(\"F3\", \"game\", \"\");"); // STOPS DAT ANNOYING PAUSING!
	custEffect("map(\"esc\", \"earthquake\", \"\");");
	custEffect("map(\"esc\", \"createHull\", \"\");");

//Units contained viewer


//Round and round the hax goes! Whee!
	custEffect("map(\"r\", \"empower\", \"uiWheelRotatePlacedUnit\");");
	custEffect("if(FP>2){editMode(\"earthquake\"); /*");
	custEffect("*/ trMessageSetText(\"Admin mode activated. For more info, WITHOUT PRESSING ENTER, type 'help' and press tab.\");}");
	rmAddTriggerEffect("Fire Event");
	rmSetTriggerEffectParamInt("EventID", rmTriggerID("StatCounterCMD"));


	rmSwitchToTrigger(rmTriggerID("StatCounterCMD"));
	rmSetTriggerPriority(3);
	rmSetTriggerActive(false);
	rmSetTriggerRunImmediately(false);
	rmSetTriggerLoop(true);
	Timer(0.5);
	custEffect("if(FP>2){ /*");
	custEffect("*/ if(statHUD==2){ /*");

	custEffect("*/ if(false==trIsGadgetVisible(\"unitselectionpanel-unitPortrait\") /*");
	custEffect("*/ && true==trIsGadgetVisible(\"unitselectionpanel\")){gadgetUnreal(\"unitselectionpanel\");} /*");

	custEffect("*/ if(false==trIsGadgetVisible(\"unitselectionpanel\") && false==trIsGadgetVisible(\"homecitytransportpanel-header\")){ /*");

	custEffect("*/ if(CONorSTATS==0){ /*");
	custEffect("*/ trSoundPlayDialog(\"\", \"1\", -1, false, \""+fmt+"\"+MyHax+\"_\", \"\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ if(CONorSTATS==1){ /*");
	custEffect("*/ string ta6=\"						| \";  /*");
	custEffect("*/ xsSetContextPlayer(VIEWa);  /*");
	custEffect("*/ string PopTextA=\" | [\"+kbGetPop()+\"/\"+kbGetPopCap()+\"]\";  /*");
	custEffect("*/ int FoodA=kbResourceGet(2);  /*");
	custEffect("*/ int WoodA=kbResourceGet(1);  /*");
	custEffect("*/ int GoldA=kbResourceGet(0);  /*");
	custEffect("*/ string PlayerAStats=kbGetPlayerName(VIEWa)+PopTextA+\" | \"+FoodA+\" | \"+WoodA+\" | \"+GoldA+\" | \"; /*");
	custEffect("*/ xsSetContextPlayer(VIEWb);  /*");
	custEffect("*/ string PopTextB=\" | [\"+kbGetPop()+\"/\"+kbGetPopCap()+\"]\";  /*");
	custEffect("*/ int FoodB=kbResourceGet(2);  /*");
	custEffect("*/ int WoodB=kbResourceGet(1);  /*");
	custEffect("*/ int GoldB=kbResourceGet(0);  /*");
	custEffect("*/ string PlayerBStats=kbGetPlayerName(VIEWb)+PopTextB+\" | \"+FoodB+\" | \"+WoodB+\" | \"+GoldB+\" | \"; /*");
	custEffect("*/ trSoundPlayDialog(\"\", \"1\", -1, false, ta6+PlayerAStats+\":\"+ta6+PlayerBStats, \"\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ } /*");

	custEffect("*/ else if((false==trIsGadgetVisible(\"unitselectionpanel\") && true==trIsGadgetVisible(\"homecitytransportpanel-header\")) /*");
	custEffect("*/ || true==trIsGadgetVisible(\"unitselectionpanel-unitPortrait\")){ /*");
	custEffect("*/ trSoundPlayDialog(\"\", \"1\", -1, false, \" \", \"\"); /*");
	custEffect("*/ } /*");

	custEffect("*/ xsSetContextPlayer(VIEWa); /*");
	custEffect("*/ int tekcount=0; /*");
	custEffect("*/ int tekprog1=0; /*");
	custEffect("*/ int tekprog2=0; /*");
	custEffect("*/ int tekprog3=0; /*");
	custEffect("*/ string tekstring=\"\"; /*");
	custEffect("*/ for(i=0;<956){ /*");
	custEffect("*/ 		if(kbGetTechPercentComplete(i)>0 && kbGetTechPercentComplete(i)<1){ /*");
	custEffect("*/ 		tekcount=tekcount+1; /*");

	custEffect("*/ 		if(tekcount==1){ /*");
	custEffect("*/ 		tekprog1=100*kbGetTechPercentComplete(i); /*");
	custEffect("*/ 		tekstring=tekstring+techtostring(i,0)+\": \"+tekprog1+\"%%%\"+\" \"; /*");
	custEffect("*/ 		} /*");

	custEffect("*/ 		else if(tekcount==2){ /*");
	custEffect("*/ 		tekprog2=100*kbGetTechPercentComplete(i); /*");
	custEffect("*/ 		tekstring=tekstring+techtostring(i,0)+\": \"+tekprog2+\"%%%\"+\" \"; /*");
	custEffect("*/ 		} /*");

	custEffect("*/ 		else if(tekcount==3){ /*");
	custEffect("*/ 		tekprog3=100*kbGetTechPercentComplete(i); /*");
	custEffect("*/ 		tekstring=tekstring+techtostring(i,0)+\": \"+tekprog3+\"%%%\"+\" \"; /*");
	custEffect("*/ 		} /*");

	custEffect("*/ 		} /*");
	custEffect("*/ } /*");
	custEffect("*/ if(tekstring>\"\"){ /*");
	custEffect("*/ trCounterAddTime(\"timmy\", 1, 0, tekstring+\"												\", -1); /*");
	custEffect("*/ } /*");

	custEffect("*/ } /*");
//OLD VERSION (ONE PLAYER) DEFAULT:
	custEffect("*/ else if(statHUD==1){ /*");
	custEffect("*/ xsSetContextPlayer(VIEWa);  /*");
	custEffect("*/ string PopText=\": | [\"+kbGetPop()+\"/\"+kbGetPopCap()+\"]\";  /*");
	custEffect("*/ int Food=kbResourceGet(2);  /*");
	custEffect("*/ int Wood=kbResourceGet(1);  /*");
	custEffect("*/ int Gold=kbResourceGet(0);  /*");
	custEffect("*/ trSetCounterDisplay(\"\"+kbGetPlayerName(VIEWa)+PopText+\" | \"+Food+\" | \"+Wood+\" | \"+Gold+\" | \"); /*");

//Bleh if I add this it lags more :(
//	custEffect("*/ if(true==trIsGadgetVisible(\"homecitytransportpanel-header\")){ /*");
//	custEffect("*/ trSoundPlayDialog(\"default\", \"1\", -1, false, \" \", \"\");} /*");

	custEffect("*/ int ttekcount=0; /*");
	custEffect("*/ int ttekprog1=0; /*");
	custEffect("*/ int ttekprog2=0; /*");
	custEffect("*/ int ttekprog3=0; /*");
	custEffect("*/ string ttekstring=\"\"; /*");
	custEffect("*/ for(i=0;<956){ /*");
	custEffect("*/ 		if(kbGetTechPercentComplete(i)>0 && kbGetTechPercentComplete(i)<1){ /*");
	custEffect("*/ 		ttekcount=ttekcount+1; /*");

	custEffect("*/ 		if(ttekcount==1){ /*");
	custEffect("*/ 		ttekprog1=100*kbGetTechPercentComplete(i); /*");
	custEffect("*/ 		ttekstring=ttekstring+techtostring(i,0)+\": \"+ttekprog1+\"%%%\"+\" \"; /*");
	custEffect("*/ 		} /*");

	custEffect("*/ 		else if(ttekcount==2){ /*");
	custEffect("*/ 		ttekprog2=100*kbGetTechPercentComplete(i); /*");
	custEffect("*/ 		ttekstring=ttekstring+techtostring(i,0)+\": \"+ttekprog2+\"%%%\"+\" \"; /*");
	custEffect("*/ 		} /*");

	custEffect("*/ 		else if(ttekcount==3){ /*");
	custEffect("*/ 		ttekprog3=100*kbGetTechPercentComplete(i); /*");
	custEffect("*/ 		ttekstring=ttekstring+techtostring(i,0)+\": \"+ttekprog3+\"%%%\"+\" \"; /*");
	custEffect("*/ 		} /*");

	custEffect("*/ 		} /*");
	custEffect("*/ } /*");
	custEffect("*/ if(ttekstring>\"\"){ /*");
	custEffect("*/ trCounterAddTime(\"timmy\", 1, 0, ttekstring+\"												\", -1); /*");
	custEffect("*/ } /*");

	custEffect("*/ } /*");

	custEffect("*/ } ");

	rmCreateTrigger("BlatantAdvertising");
	rmSwitchToTrigger(rmTriggerID("BlatantAdvertising"));
	rmSetTriggerPriority(2);
	rmSetTriggerActive(true);
	rmSetTriggerRunImmediately(false);
	Timer(12);
	custEffect("if(FP>2){trMessageSetText(\"\"+getmotd(1337));}");

	rmCreateTrigger("RevealMapUponEndGame");
	rmSwitchToTrigger(rmTriggerID("RevealMapUponEndGame"));
	rmSetTriggerPriority(4);
	rmSetTriggerActive(true);
	rmSetTriggerRunImmediately(false);
	custCondition("true==kbIsPlayerResigned(1) || true==kbHasPlayerLost(1) || true==kbIsPlayerResigned(2) || true==kbHasPlayerLost(2)");
	custEffect("trSetFogAndBlackmap(false, false); /*");
	custEffect("*/ trSoundPlayDialog(\"\", \"1\", -1, false, \" \", \"\");");

/*[Special features end]*/

   // Text
   rmSetStatusText("",1.0);
}  
