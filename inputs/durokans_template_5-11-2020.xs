/* Durokan's Template - May 11 2020 Version 1.3*/
//http://web.archive.org/web/20100421112530/http://hyenastudios.mugamo.com/aoe3rmstutorial.htm#I9

include "mercenaries.xs


  ";
include "ypAsianInclude.xs";
include "ypKOTHInclude.xs";
 
void main(void) {
	//rmSetStatusText("",0.01);
   // Picks the map size
	int playerTiles=11000;
	if (cNumberNonGaiaPlayers > 4){
		playerTiles = 10000;
	}else if (cNumberNonGaiaPlayers > 6){
		playerTiles = 8500;
	}
	
	int size = 2.0 * sqrt(cNumberNonGaiaPlayers*playerTiles);
	rmSetMapSize(size, size);

	rmSetMapType("land");
    rmSetMapType("grass");
    rmSetMapType("newEngland");
    rmTerrainInitialize("grass");
    rmSetLightingSet("saguenay");

    rmDefineClass("classForest");
    rmDefineClass("classPlateau");
    
    //Constraints
    int avoidPlateau=rmCreateClassDistanceConstraint("stuff vs. cliffs", rmClassID("classPlateau"), 12.0);

    int circleConstraint=rmCreatePieConstraint("circle Constraint", 0.5, 0.5, 0, rmZFractionToMeters(0.49), rmDegreesToRadians(0), rmDegreesToRadians(360));
   
    int forestConstraint=rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("classForest"), 30.0);
    int forestConstraintShort=rmCreateClassDistanceConstraint("object vs. forest", rmClassID("classForest"), 4.0);
    
    int avoidHunt=rmCreateTypeDistanceConstraint("hunts avoid hunts", "huntable", 50.0);
    
    //keeps things with this constraint within 10 units of water
    int waterHunt = rmCreateTerrainMaxDistanceConstraint("hunts stay near the water", "land", false, 10.0);

    int avoidHerd=rmCreateTypeDistanceConstraint("herds avoid herds", "herdable", 50.0);

    int avoidCoin=rmCreateTypeDistanceConstraint("avoid coin", "Mine", 10.0);
    int avoidCoinMed=rmCreateTypeDistanceConstraint("avoid coin medium", "Mine", 60.0);
    int avoidWaterShort = rmCreateTerrainDistanceConstraint("avoid water short 2", "Land", false, 5.0);

    int avoidTradeRouteSmall = rmCreateTradeRouteDistanceConstraint("objects avoid trade route small", 8.0);
    int avoidSocket=rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 25.0);
    
    int avoidTownCenter=rmCreateTypeDistanceConstraint("avoid Town Center", "townCenter", 35.0);
    int avoidTownCenterSmall=rmCreateTypeDistanceConstraint("avoid Town Center small", "townCenter", 15.0);
    int avoidTownCenterMore=rmCreateTypeDistanceConstraint("avoid Town Center more", "townCenter", 40.0);  
   
    int avoidNugget=rmCreateTypeDistanceConstraint("nugget avoid nugget", "AbstractNugget", 60.0);
    
    int flagLand = rmCreateTerrainDistanceConstraint("flag land", "land", true, 9.0);
  
 	// Player placing  
    float spawnSwitch = rmRandFloat(0,1.2);

	if (cNumberTeams == 2){
		if (spawnSwitch <=0.6){
			rmSetPlacementTeam(0);
			rmPlacePlayersLine(0.2, 0.3, 0.2, 0.7, 0, 0);
			rmSetPlacementTeam(1);
			rmPlacePlayersLine(0.8, 0.7, 0.8, 0.3, 0, 0);
		}else{
			rmSetPlacementTeam(1);
			rmPlacePlayersLine(0.2, 0.3, 0.2, 0.7, 0, 0);
			rmSetPlacementTeam(0);
			rmPlacePlayersLine(0.8, 0.7, 0.8, 0.3, 0, 0);
		}
	}else{
		rmPlacePlayersCircular(0.4, 0.4, 0.02);
	}
		
    chooseMercs();
    rmSetStatusText("",0.1); 
    
    // Picks default terrain and water
    // rmSetMapElevationParameters(long type, float minFrequency, long numberOctaves, float persistence, float heightVariation)
    rmSetMapElevationParameters(cElevTurbulence, 0.1, 4, 0.2, 3.0);
    rmSetBaseTerrainMix("coastal_honshu_b");
    rmTerrainInitialize("coastal_japan\ground_dirt4_co_japan", 0.0);
    // rmSetMapElevationParameters(long type, float minFrequency, long numberOctaves, float persistence, float heightVariation)
    rmSetMapElevationParameters(cElevTurbulence, 0.04, 5, 0.2, 4);


    //sets up the mainland
    //"coastal_japan\ground_grass3_co_japan"
   int continent2 = rmCreateArea("continent");
   rmSetAreaSize(continent2, 1.0, 1.0);
   rmSetAreaLocation(continent2, 0.5, 0.5);
   //rmSetAreaTerrainType(continent2, g1);
   rmSetAreaMix(continent2, "coastal_honshu_b");
   rmSetAreaCoherence(continent2, 1.0);
   rmBuildArea(continent2); 

    //code fragment to create an Area and gives a constraint to avoid that area.
    int classCenter = rmDefineClass("center");
    int avoidCenter = rmCreateClassDistanceConstraint("avoid center", rmClassID("center"), 3.0);
    
    int center = rmCreateArea("center");
    rmAddAreaToClass(center, rmClassID("center"));
    rmSetAreaSize(center, .1, .1);
    rmSetAreaLocation(center, 0.5, 0.5);
    rmSetAreaCoherence(center, 1.0);
    rmBuildArea(center);   
	
	
    //end

    rmSetStatusText("",0.2);
       
    //creates cliffs

    
    //creates a lake from .6,.3 to .62,.52
    int straightLake=rmCreateArea("straightLake");
    rmSetAreaLocation(straightLake, 0.6, 0.3);
    rmAddAreaInfluenceSegment(straightLake, 0.6, 0.3, 0.62, 0.52);
    rmSetAreaSize(straightLake, .03, .03);      
    rmSetAreaWaterType(straightLake, "california coast");
    rmSetAreaBaseHeight(straightLake, 0.0);
    rmSetAreaCoherence(straightLake, .95);
    rmBuildArea(straightLake);
    
    //creates a round lake
    int roundLake=rmCreateArea("roundLake");
    rmSetAreaLocation(roundLake, 0.375, 0.525);
    rmSetAreaSize(roundLake, .03, .03);      
    rmSetAreaWaterType(roundLake, "california coast");
    rmSetAreaBaseHeight(roundLake, 0.0);
    rmSetAreaCoherence(roundLake, .95);
    rmBuildArea(roundLake);
    
	rmSetStatusText("",0.4);
	
   
  
//START TR
    //trade routes
    int socketID=rmCreateObjectDef("sockets to dock Trade Posts");
    rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
    rmSetObjectDefAllowOverlap(socketID, true);
    rmSetObjectDefMinDistance(socketID, 0.0);
    rmSetObjectDefMaxDistance(socketID, 6.0);      
   
    int tradeRouteID = rmCreateTradeRoute();
    rmSetObjectDefTradeRouteID(socketID, tradeRouteID);
    rmAddTradeRouteWaypoint(tradeRouteID, .72, .02);
    //rmAddTradeRouteWaypoint(tradeRouteID, .2, .2);
    rmAddTradeRouteWaypoint(tradeRouteID, .0, .6);

    rmBuildTradeRoute(tradeRouteID, "water");

    vector socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID, 0.5);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc1);
    socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID, 0.15);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc1);
    socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID, 0.85);
    rmPlaceObjectDefAtPoint(socketID, 0, socketLoc1);
    
    //TR 2
    int socketID2=rmCreateObjectDef("sockets to dock Trade Posts2");
    rmAddObjectDefItem(socketID2, "SocketTradeRoute", 1, 0.0);
    rmSetObjectDefAllowOverlap(socketID2, true);
    rmSetObjectDefMinDistance(socketID2, 0.0);
    rmSetObjectDefMaxDistance(socketID2, 6.0);      
   
    int tradeRouteID2 = rmCreateTradeRoute();
    rmSetObjectDefTradeRouteID(socketID2, tradeRouteID2);
    rmAddTradeRouteWaypoint(tradeRouteID2, .28, .98);
    //rmAddTradeRouteWaypoint(tradeRouteID2, .7, .7);
    rmAddTradeRouteWaypoint(tradeRouteID2, 1.0, .4);

    rmBuildTradeRoute(tradeRouteID2, "water");

    vector socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID2, 0.15);
    rmPlaceObjectDefAtPoint(socketID2, 0, socketLoc2);
   
    socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID2, 0.50);
    rmPlaceObjectDefAtPoint(socketID2, 0, socketLoc2);
   
    socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID2, 0.85);
    rmPlaceObjectDefAtPoint(socketID2, 0, socketLoc2);
        
    //end tr 2		 
//END TR

    int cliffs = rmCreateArea("cliffs");
    rmSetAreaSize(cliffs, 0.015, 0.015); 
    rmAddAreaToClass(cliffs, rmClassID("classPlateau"));
    //rmAddAreaTerrainReplacement(cliffs, "texas\ground4_tex", "california\ground6_cal");
    rmSetAreaCliffType(cliffs, "Texas");
    rmSetAreaCliffEdge(cliffs, 1, 1.0, 0.0, 0.0, 2); //4,.225 looks cool too
    rmSetAreaCliffPainting(cliffs, true, true, true, 1.5, true);
    rmSetAreaCliffHeight(cliffs, 3, 0.1, 0.5);
    rmSetAreaCoherence(cliffs, .75);
    rmSetAreaLocation(cliffs, .5, .75);		
    rmBuildArea(cliffs);
    rmSetStatusText("",0.3);
    
//START NATIVES	
    int subCiv0 = -1;
    int subCiv1 = -1;
    subCiv0 = rmGetCivID("zen");
    subCiv1 = rmGetCivID("zen");
    rmSetSubCiv(0, "zen");
    rmSetSubCiv(1, "zen");
	
   int nativeID0 = -1;
   int nativeID1 = -1;

   nativeID0 = rmCreateGrouping("zen temple", "native zen temple cj 0"+4);
   rmSetGroupingMinDistance(nativeID0, 0.00);
   rmSetGroupingMaxDistance(nativeID0, 0.00);
   rmAddGroupingToClass(nativeID0, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeID0, 0, 0.25, 0.75);

   nativeID1 = rmCreateGrouping("zen temple ", "native zen temple cj 0"+5);
   rmSetGroupingMinDistance(nativeID1, 0.00);
   rmSetGroupingMaxDistance(nativeID1, 0.00);
   rmAddGroupingToClass(nativeID1, rmClassID("natives"));
   rmPlaceGroupingAtLoc(nativeID1, 0, 0.75, 0.25);
   
//END NATIVES   

    //starting objects
    int playerStart = rmCreateStartingUnitsObjectDef(5.0);
    rmSetObjectDefMinDistance(playerStart, 3.0);
    rmSetObjectDefMaxDistance(playerStart, 12.0);
   
    //beautiful and relativeley symmetric first / second mines
    int classStartingResource = rmDefineClass("startingResource");
    int classGold = rmDefineClass("Gold");
    int avoidGoldTypeFar = rmCreateTypeDistanceConstraint("avoid gold type  far ", "gold", 34.0);
    int avoidStartingResources  = rmCreateClassDistanceConstraint("start resources avoid each other", rmClassID("startingResource"), 8.0);

//STARTS CREATING OBJECTS THAT SPAWN AT EACH TC
    int baseGold = rmCreateObjectDef("starting gold");
    rmAddObjectDefItem(baseGold, "mine", 1, 0.0);
    rmSetObjectDefMinDistance(baseGold, 14.0);
    rmSetObjectDefMaxDistance(baseGold, 20.0);
    rmAddObjectDefToClass(baseGold, classStartingResource);
    rmAddObjectDefToClass(baseGold, classGold);
    rmAddObjectDefConstraint(baseGold, avoidTradeRouteSmall);
    //rmAddObjectDefConstraint(baseGold, avoidImpassableLand);
    rmAddObjectDefConstraint(baseGold, avoidStartingResources);

    int berryID = rmCreateObjectDef("starting berries");
    rmAddObjectDefItem(berryID, "BerryBush", 2, 6.0);
    rmSetObjectDefMinDistance(berryID, 8.0);
    rmSetObjectDefMaxDistance(berryID, 12.0);
    rmAddObjectDefConstraint(berryID, avoidCoin);

    int treeID = rmCreateObjectDef("starting trees");
    rmAddObjectDefItem(treeID, "treeNewEngland", rmRandInt(6,9), 10.0);
    rmSetObjectDefMinDistance(treeID, 12.0);
    rmSetObjectDefMaxDistance(treeID, 18.0);
    rmAddObjectDefConstraint(treeID, avoidTownCenterSmall);
    rmAddObjectDefConstraint(treeID, avoidCoin);

    int foodID = rmCreateObjectDef("starting hunt");
    rmAddObjectDefItem(foodID, "elk", 6, 8.0);
    rmSetObjectDefMinDistance(foodID, 10.0);
    rmSetObjectDefMaxDistance(foodID, 10.0);
    rmSetObjectDefCreateHerd(foodID, false);

    int foodID2 = rmCreateObjectDef("starting hunt 2");
    rmAddObjectDefItem(foodID2, "elk", 7, 8.0);
    rmSetObjectDefMinDistance(foodID2, 40.0);
    rmSetObjectDefMaxDistance(foodID2, 40.0);
    rmSetObjectDefCreateHerd(foodID2, true);
                   
    int foodID3 = rmCreateObjectDef("starting hunt 3");
    rmAddObjectDefItem(foodID3, "elk", 8, 8.0);
    rmSetObjectDefMinDistance(foodID3, 65.0);
    rmSetObjectDefMaxDistance(foodID3, 65.0);
    rmSetObjectDefCreateHerd(foodID3, true);
//END TC SPAWN DEFINITIONS
    
    rmSetStatusText("",0.5);   
  
//for each player, place their tc and a bunch of objects at or around their tc
int i = 0;
    for(i=1; < cNumberNonGaiaPlayers + 1) {
		int id=rmCreateArea("Player"+i);
		rmSetPlayerArea(i, id);
		int startID = rmCreateObjectDef("object"+i);
		rmAddObjectDefItem(startID, "TownCenter", 1, 5.0);
		rmSetObjectDefMinDistance(startID, 0.0);
        rmSetObjectDefMaxDistance(startID, 10.0);
		rmPlaceObjectDefAtLoc(startID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(berryID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(treeID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(foodID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(baseGold, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(baseGold, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(foodID2, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        //rmPlaceObjectDefAtLoc(foodID3, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        rmPlaceObjectDefAtLoc(playerStart, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
        
        int waterFlag = rmCreateObjectDef("HC water flag "+i);
        rmAddObjectDefItem(waterFlag, "HomeCityWaterSpawnFlag", 1, 0.0);
        rmSetObjectDefMinDistance(waterFlag, 0);
        rmSetObjectDefMaxDistance(waterFlag, rmXFractionToMeters(0.5));
        rmPlaceObjectDefAtLoc(waterFlag, i, .5, .5, 1);
	}
	
	
//end TC loop

	rmSetStatusText("",0.6);
    


//places a bunch of hunts
    int mapHunts = rmCreateObjectDef("mapHunts");
	rmAddObjectDefItem(mapHunts, "deer", rmRandInt(7,8), 14.0);
	rmSetObjectDefCreateHerd(mapHunts, true);
	rmSetObjectDefMinDistance(mapHunts, 0);
	rmSetObjectDefMaxDistance(mapHunts, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(mapHunts, circleConstraint);
	rmAddObjectDefConstraint(mapHunts, avoidTownCenterMore);
	rmAddObjectDefConstraint(mapHunts, avoidHunt);
	rmAddObjectDefConstraint(mapHunts, avoidPlateau);	
	rmPlaceObjectDefAtLoc(mapHunts, 0, 0.5, 0.5, 6*cNumberNonGaiaPlayers);
//end hunts

//places mines
  	int mapMines = rmCreateObjectDef("mapMines");
	rmAddObjectDefItem(mapMines, "mine", 1, 1.0);
	rmSetObjectDefMinDistance(mapMines, 0.0);
	rmSetObjectDefMaxDistance(mapMines, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(mapMines, avoidCoinMed);
	rmAddObjectDefConstraint(mapMines, avoidTownCenterMore);
	rmAddObjectDefConstraint(mapMines, avoidSocket);
	rmAddObjectDefConstraint(mapMines, avoidPlateau);	
	rmAddObjectDefConstraint(mapMines, avoidWaterShort);
	rmAddObjectDefConstraint(mapMines, forestConstraintShort);
	rmAddObjectDefConstraint(mapMines, circleConstraint);
	rmPlaceObjectDefAtLoc(mapMines, 0, 0.5, 0.5, 5*cNumberNonGaiaPlayers);
//end mines

//places treasures 
	int mapNugs= rmCreateObjectDef("mapNugs"); 
	rmAddObjectDefItem(mapNugs, "Nugget", 1, 0.0); 
	rmSetObjectDefMinDistance(mapNugs, 0.0); 
	rmSetObjectDefMaxDistance(mapNugs, rmXFractionToMeters(0.5)); 
	rmAddObjectDefConstraint(mapNugs, avoidNugget); 
	rmAddObjectDefConstraint(mapNugs, circleConstraint);
	rmAddObjectDefConstraint(mapNugs, avoidTownCenter);
	rmAddObjectDefConstraint(mapNugs, forestConstraintShort);
	rmAddObjectDefConstraint(mapNugs, avoidTradeRouteSmall);
	rmAddObjectDefConstraint(mapNugs, avoidSocket); 
	rmAddObjectDefConstraint(mapNugs, avoidPlateau);	
	rmSetNuggetDifficulty(1, 2); 
	rmPlaceObjectDefAtLoc(mapNugs, 0, 0.5, 0.5, 5*cNumberNonGaiaPlayers);   
	rmSetStatusText("",0.7);
//end treasures

//places trees
    int mapTrees=rmCreateObjectDef("map trees");
    rmAddObjectDefItem(mapTrees, "treeNewEngland", rmRandInt(8,10), rmRandFloat(14.0,20.0));
    rmAddObjectDefToClass(mapTrees, rmClassID("classForest")); 
    rmSetObjectDefMinDistance(mapTrees, 0);
    rmSetObjectDefMaxDistance(mapTrees, rmXFractionToMeters(0.45));
    rmAddObjectDefConstraint(mapTrees, avoidTradeRouteSmall);
    rmAddObjectDefConstraint(mapTrees, avoidSocket);
    rmAddObjectDefConstraint(mapTrees, circleConstraint);
    rmAddObjectDefConstraint(mapTrees, forestConstraint);
    rmAddObjectDefConstraint(mapTrees, avoidTownCenter);	
    rmAddObjectDefConstraint(mapTrees, avoidPlateau);	
    rmAddObjectDefConstraint(mapTrees, avoidWaterShort);	
    rmPlaceObjectDefAtLoc(mapTrees, 0, 0.5, 0.5, 100*cNumberNonGaiaPlayers);
//end trees

	
    rmSetStatusText("",0.8);
   
//fish and their constraints placed together at the end for ease of removal
   int fishVsFishID=rmCreateTypeDistanceConstraint("fish v fish", "ypFishCatfish", 20.0);
   int fishVsFishID2=rmCreateTypeDistanceConstraint("fish v fish", "ypSquid", 20.0);
   int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 8.0);
   int whaleVsWhaleID=rmCreateTypeDistanceConstraint("whale v whale", "HumpbackWhale", 25.0);
   int whaleLand = rmCreateTerrainDistanceConstraint("whale land", "land", true, 15.0);

   int fishID=rmCreateObjectDef("fish Mahi");
   rmAddObjectDefItem(fishID, "ypFishCatfish", 1, 0.0);
   rmSetObjectDefMinDistance(fishID, 0.0);
   rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fishID, fishVsFishID);
   rmAddObjectDefConstraint(fishID, fishLand);
   rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 4*cNumberNonGaiaPlayers);

   int fish2ID=rmCreateObjectDef("fish Tarpon");
   rmAddObjectDefItem(fish2ID, "ypSquid", 1, 0.0);
   rmSetObjectDefMinDistance(fish2ID, 0.0);
   rmSetObjectDefMaxDistance(fish2ID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(fish2ID, fishVsFishID2);
   rmAddObjectDefConstraint(fish2ID, fishLand);
   rmPlaceObjectDefAtLoc(fish2ID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);
//end fish
    
   int whaleID=rmCreateObjectDef("whale");
   rmAddObjectDefItem(whaleID, "HumpbackWhale", 1, 0.0);
   rmSetObjectDefMinDistance(whaleID, 0.0);
   rmSetObjectDefMaxDistance(whaleID, rmXFractionToMeters(0.5));
   rmAddObjectDefConstraint(whaleID, whaleVsWhaleID);
   rmAddObjectDefConstraint(whaleID, whaleLand);
   rmPlaceObjectDefAtLoc(whaleID, 0, 0.5, 0.5, 2*cNumberNonGaiaPlayers);
//end whales

   
	rmSetStatusText("",0.9);
    
}
 
