%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list_lab.c"

#define J_VECTOR 1
#define J_NUM 2
#define J_STR 3
#define J_BOOL 4
#define J_UNKNOWN -21
// J_UNKNOWN is for functions that don't appear defined in some documentation and have unclear returns (not really used)// for my purposes, they preted to have the Numeric return type and will be replaced over time as more is found out about them
#define J_BLANK 5

void add_ids(node_ptr);
void add_with_type(char*, int, int);
void append_param(char*, int);
void append_param_arr(char*, int*, int);
void add_func_with_params(char*, int, int*, int);
int cmp_int_arr(int*, int*, int, int);
char* j_to_str(int);
extern int linect;
extern int yylex();
extern char* yytext;
int yyerror(char*);

static node_ptr class_scope = NULL;
static node_ptr my_list = NULL;
static node2_ptr func_stack = NULL;
int debug;

char* curr_func_name;

// some inline detection could be better

%}

%union{
 char* str;
 int num;
 float decimal;
 struct s1 {char* str; int kind; int j_type;} strnum;
}

%token INCLUDE_T VOID_T RETURN_T
%token INT_T LONG_T FLOAT_T BOOL_T VECTOR_T STRING_T 
%token IF_T ELIF_T ELSE_T TRUE_T FALSE_T FOR_T WHILE_T BREAK_T CONTINUE_T
%token INCR DECR LT EQ LE GE GT NE ASSIGN PLUS MINUS MULT DIVIDE BANG
%token COMMA LP RP LB RB LC RC SEMI QUOTE
%token PLUS_EQ MINUS_EQ DIV_EQ MULT_EQ
%token <strnum> STRING_LIT ID
%token <num> INT_LIT
%token <decimal> FLOAT_LIT

%type <num> exp ctr var_type param_list param_starter
%nonassoc ASSIGN
%right BANG
%left PLUS MINUS OR AND
%left LT LE GT GE EQ NE
%left MULT DIVIDE MOD
%left INCR DECR

%%
program : decl_list
 ;
decl_list:
 | decl_list statement
 | decl_list function
 | decl_list INCLUDE_T STRING_LIT SEMI
 ;
var_type : VOID_T { $$ = 0;}
 | VECTOR_T { $$ = J_VECTOR;}
 | INT_T { $$ = J_NUM;}
 | FLOAT_T { $$ = J_NUM;}
 | BOOL_T { $$ = J_BOOL;}
 | STRING_T { $$ = J_STR;}
 | LONG_T { $$ = J_NUM;}
 ;
func_param : parameters
 | VOID_T
;
function : var_type ID LP {
	$2.kind = LFUNC;
        if(debug){
               printf("Line %d: function: \"%s\" added to scope: %d\n",linect,$2.str,my_list->value);
 	       printf("Line %d: Enter function scope \"%s\"\n",linect,$2.str);
        }
        add_with_type($2.str,LFUNC,$1);
	my_list = push(my_list,0);
	if(debug){
		printf("Line %d: entering function declaration: %s\n", linect, $2.str);
	}	
	curr_func_name = strdup($2.str); 
}func_param RP LC statements RC {
	if(debug){
		printf("Line %d: exiting function declaration in scope %s\n",linect, $2.str);
		printf("Line %d: here are the variables in scope of %s:\n",linect,$2.str);
		print_list(my_list);
	}
	my_list = pop(my_list);
	curr_func_name = NULL;
}
 ;
statements : statement statements
 |
 ;
param_starter : var_type ID {
	add_with_type($2.str,LVAR,$1);
	if(debug){
		printf("Line %d: id %s added to scope of %s with type %d\n",linect, $2.str, curr_func_name, $1);
	}
	append_param(curr_func_name, $1);
} 
;
parameters : param_starter COMMA parameters{
}
 | param_starter ASSIGN exp COMMA parameters{
	if($1 != $3){
		printf("Line %d: variable parameter typing mismatch of %s and %s\n",linect, j_to_str($1), j_to_str($3));
	}
}
 | param_starter ASSIGN exp{
	if($1 != $3){
		printf("Line %d: variable parameter typing mismatch of %s and %s\n",linect, j_to_str($1), j_to_str($3));
	}
}
 | param_starter {
	// this line intentionally left blank
}
 | COMMA parameters {
  append_param(curr_func_name, J_BLANK);
 }
 |
 ;
statement : define_stmt SEMI
 | condition_stmt
 | while_stmt
 | for_stmt
 | return_stmt SEMI
 | call_stmt SEMI
 | assignment_stmt SEMI
 | BREAK_T SEMI
 | CONTINUE_T SEMI
 | ctr SEMI 
 | ID PLUS_EQ exp SEMI 	{
	content_ptr temp = get_ptr(my_list, $1.str, LVAR);
	if(temp){
		if(temp->j_type != $3){
			printf("Error -- Line %d: ID %s must be numeric\n",linect, $1.str);
		}
	}else{
		printf("Error -- Line %d: ID %s was not found in table\n",linect, $1.str);
	}
	if($3 != J_NUM){
		printf("Error -- Line %d: exp must be numeric\n",linect);
	}
}
 | ID MINUS_EQ exp SEMI {
	content_ptr temp = get_ptr(my_list, $1.str, LVAR);
	if(temp){
		if(temp->j_type != $3){
			printf("Error -- Line %d: ID %s must be numeric\n",linect, $1.str);
		}
	}else{
		printf("Error -- Line %d: ID %s was not found in table\n",linect, $1.str);
	}
	if($3 != J_NUM){
		printf("Error -- Line %d: exp must be numeric\n",linect);
	}
}
 | ID DIV_EQ exp SEMI {
	content_ptr temp = get_ptr(my_list, $1.str, LVAR);
	if(temp){
		if(temp->j_type != $3){
			printf("Error -- Line %d: ID %s must be numeric\n",linect, $1.str);
		}
	}else{
		printf("Error -- Line %d: ID %s was not found in table\n",linect, $1.str);
	}
	if($3 != J_NUM){
		printf("Error -- Line %d: exp must be numeric\n",linect);
	}
}
 | ID MULT_EQ exp SEMI {
	content_ptr temp = get_ptr(my_list, $1.str, LVAR);
	if(temp){
		if(temp->j_type != $3){
			printf("Error -- Line %d: ID %s must be numeric\n",linect, $1.str);
		}
	}else{
		printf("Error -- Line %d: ID %s was not found in table\n",linect, $1.str);
	}
	if($3 != J_NUM){
		printf("Error -- Line %d: exp must be numeric\n",linect);
	}
}
 ;
ctr : exp INCR {
	if($1 != J_NUM){
		printf("Error -- Line %d: exp must be numeric\n",linect);
	}
	$$ = $1;
}
 | exp DECR {
	if($1 != J_NUM){
		printf("Error -- Line %d: exp must be numeric\n",linect);
	}
	$$ = $1;
}
;
while_stmt : WHILE_T LP exp {
	if($3 != J_BOOL){
		printf("Error -- Line %d: exp must be bool\n",linect);
	}
} RP LC RC 
 ;
for_bool : EQ
 | NE
 | LT
 | LE
 | GT
 | GE
 ;
for_stmt : FOR_T LP assignment_stmt SEMI for_bool exp {
	if($6 != J_NUM){
		printf("Error -- Line %d: exp in for loop must be numeric\n",linect);
	}
} RP statement_num
 ;
condition_stmt : IF_T LP exp {
	if($3 != J_BOOL){
		printf("Error -- Line %d: exp must be bool\n",linect);
	}
}RP statement_num optional_elif optional_else
 ;
statement_num : LC statements RC
 | statement
 ;
optional_elif : ELIF_T LP exp {
	if($3 != J_BOOL){
		printf("Error -- Line %d: exp must be bool\n",linect);
	}
}RP statement_num optional_elif
 |
 ;
optional_else : ELSE_T statement_num
 |
 ;
define_stmt : var_type ID ASSIGN exp {
	add_with_type($2.str,LVAR,$1);
	content_ptr temp = get_ptr(my_list, $2.str, LVAR);
	if(temp){
		if(temp->j_type != $4){
			printf("Error -- Line %d: ID: \"%s\" of type %s mismatches expression of type %s\n",linect,$2.str,j_to_str(temp->j_type),j_to_str($4));
		}
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$2.str,temp);
		}
	}else{
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $2.str);
	}
}
 ;
assignment_stmt : ID ASSIGN exp {
	content_ptr temp = get_ptr(my_list, $1.str, LVAR);
	if(temp){
		if(temp->j_type != $3){
			printf("Error -- Line %d: ID: \"%s\" of type %s mismatches expression of type %s\n",linect,$1.str,j_to_str(temp->j_type),j_to_str($3));
		}
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$1.str,temp);
		}
	}else{
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $1.str);
	}
	
}
 ;
return_stmt : RETURN_T exp {
	// should pass type up so this can be checked to match function return
}
 ;
call_stmt : ID LP RP {
	content_ptr temp = get_ptr(my_list, $1.str, LFUNC);
	if(temp){
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$1.str,temp);
		}
	}else{
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $1.str);
	}
}
 | ID LP param_list RP {
	content_ptr temp = get_ptr(my_list, $1.str, LFUNC);
	if(temp){
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$1.str,temp);
		}
		for(int i = 0; i < 20; i++){
			if(temp->param_list[i] == -1){
				break;
			}
		}
		int watcher = 0;
		watcher = cmp_int_arr(temp->param_list, peek2(func_stack), 20, 20);
		if(watcher != -1){
			if(watcher == 0){
				printf("Error -- Line %d, parameter of function %s expected length is %d and got %d\n", linect, $1.str, temp->param_list[0]-1, peek2(func_stack)[0]-1);
			}else{
				printf("Error -- Line %d, func %s parameter %d is of the wrong type\n", linect, $1.str, watcher);
			}
		}
	}else{
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $1.str);
	}
	func_stack = pop2(func_stack);
}
 ;
exp : exp EQ exp {
	if(($1 == J_BOOL) && ($3 == J_BOOL)){
		$$ = J_BOOL;
	}else if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp NE exp {
	if(($1 == J_BOOL) && ($3 == J_BOOL)){
		$$ = J_BOOL;
	}else if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp LT exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp LE exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp GT exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp GE exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types \n",linect);
	}
}
 | exp AND exp {
	if(($1 == J_BOOL) && ($3 == J_BOOL)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp OR exp {
	if(($1 == J_BOOL) && ($3 == J_BOOL)){
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | BANG exp {
	// this doesnt make sense at all.
	if($2 == J_BOOL){	
		$$ = J_BOOL;
	}else{
		$$ = -1;
		printf("Error -- Line %d: mismatched expr types\n",linect);
	}
}
 | exp PLUS exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_NUM;
	}else if(($1 == J_STR) || ($3 == J_STR)){
		$$ = J_STR;
	}else{
		printf("Error -- Line %d: mismatched expr types\n",linect);
		$$ = -1;
	}
}
 | exp MINUS exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_NUM;
	}else{
		printf("Error -- Line %d: mismatched expr types\n",linect);
		$$ = -1;
	}
}
 | exp MULT exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_NUM;
	}else{
		printf("Error -- Line %d: mismatched expr types\n",linect);
		$$ = -1;
	}
}
 | exp DIVIDE exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_NUM;
	}else{
		printf("Error -- Line %d: mismatched expr types\n",linect);
		$$ = -1;
	}
}
 | exp MOD exp {
	if(($1 == J_NUM) && ($3 == J_NUM)){
		$$ = J_NUM;
	}else{
		printf("Error -- Line %d: mismatched expr types\n",linect);
		$$ = -1;
	}
}
 | MINUS exp {
	// this is NOT an operator
	// can only be used to assign a negative number but cannot be used in the form "r = -r". MUST do "r = r * -1"
	if($2 == J_NUM){
		$$ = J_NUM;
	}else{
		printf("Error -- Line %d: mismatched expr types\n",linect);
		$$ = -1;
	}
}
 | LP exp RP {
	$$ = $2;
}
 | ID LP RP {
	content_ptr temp = get_ptr(my_list, $1.str, LFUNC);
	if(temp){
		$$ = temp->j_type;
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$1.str,temp);
		}
	}else{
		$$ = -1;
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $1.str);
	}
}
 | ID LP param_list RP {
	content_ptr temp = get_ptr(my_list, $1.str, LFUNC);
	if(temp){
		$$ = temp->j_type;
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$1.str,temp);
		}
		int watcher = 0;
		watcher = cmp_int_arr(temp->param_list, peek2(func_stack), 20, 20);
		if(watcher != -1){
			if(watcher == 0){
				printf("Error -- Line %d, parameter of function %s expected length is %d and got %d\n", linect, $1.str, temp->param_list[0]-1, peek2(func_stack)[0]-1);
			}else{
				printf("Error -- Line %d, parameter %d is of the wrong type\n", linect, watcher);
			}
		}
	}else{
		$$ = -1;
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $1.str);
	}
	func_stack = pop2(func_stack);
}
 | ctr {
	$$ = $1;
}
 | INT_LIT {
	$$ = J_NUM;
}
 | STRING_LIT {
	$$ = J_STR;
}
 | FLOAT_LIT {
	$$ = J_NUM;
}
 | ID {
	content_ptr temp = get_ptr(my_list, $1.str, LVAR);
	if(temp){
		$$ = temp->j_type;
		if(debug){
			printf("Line %d: ID: \"%s\" detected in scope %d\n",linect,$1.str,temp);
		}
	}else{
		$$ = -1;
		printf("Error -- Line %d: ID: \"%s\" not found\n",linect, $1.str);
	}
}
 | TRUE_T {
	$$ = J_BOOL;
}
 | FALSE_T {
	$$ = J_BOOL;
}
 ;
param_list : param_list COMMA exp {
	$$ = $1 + 1;
	add_item2(func_stack, $3);
}
 | exp {
	$$ = 1;
	func_stack = push2(func_stack);
	add_item2(func_stack, $1);
	//print_list2(func_stack);
}
 | param_list COMMA {
	$$ = $1 + 1;
	add_item2(func_stack, J_BLANK);
}
 | {}
 ;
%%

int main() {
 //toggle to 0 to turn off prints, toggle for 1 to turn on prints
 debug = 0;

 if(debug){
	printf("Entering global scope\n");
 }
 //class_scope = push(class_scope,0); 
 my_list = push(my_list,0);
 add_ids(my_list);
 yyparse();
  
 if(debug){
 	print_list(my_list);
 }
 delete_list(my_list);
 printf("Parsing of .xs file completed.\n");
 return 0;
}

int yyerror(char *s) {
// This is the funciton that gets called if yacc detects a syntax error
printf("Syntax Error line %d\n",linect);
printf("msg: %s\n",yytext);
exit(1);
}

void add_with_type(char* str, int type, int j_t){
	add(my_list, str, type);
	content_ptr temp = get_ptr(my_list, str, type);
	if(temp){
       		temp->j_type = j_t;
	}
}

void append_param(char* str, int j_type){
	content_ptr temp = get_ptr(my_list, str, LFUNC);
	if(temp){
       		int x = temp->param_list[0];
		if(x > 15){
			printf("cannot handle more than 16 parameters tbh nobody was supposed to see this error\n");
			exit(-500);
		}
		temp->param_list[x] = j_type;
		temp->param_list[0]++;
		temp->param++;	
	}
}

void append_param_arr(char* str, int* arr, int len){
	int i = 0;
	for(i = 0; i < len; i++){
		append_param(str, arr[i]);
		
		//printf("appended %d to %s\n",arr[i], str);
	}
}

void add_func_with_params(char* str, int j_t, int* arr, int len){
	add_with_type(str, LFUNC, j_t);
	append_param_arr(str, arr, len);
}



void add_ids(node_ptr my_list){
	add_with_type(strdup("cNumberNonGaiaPlayers"), LVAR, J_NUM);
	add_with_type(strdup("cElevTurbulence"), LVAR, J_NUM);
	add_with_type(strdup("cNumberTeams"), LVAR, J_NUM);
	add_with_type(strdup("cNumberPlayers"), LVAR, J_NUM);
	add_with_type(strdup("cNumArrayTypeCap"), LVAR, J_NUM);
	add_with_type(strdup("cNumAllTechs"), LVAR, J_NUM);	
	add_with_type(strdup("cFilename"), LVAR, J_STR);	
	add_with_type(strdup("cNumEcoTypes"), LVAR, J_NUM);	
	add_with_type(strdup("cNumMilTypes"), LVAR, J_NUM);	
	add_with_type(strdup("cNumWatTypes"), LVAR, J_NUM);	
	add_with_type(strdup("cNumAllTypes"), LVAR, J_NUM);
	add_with_type(strdup("cNumTrackedTechs"), LVAR, J_NUM);
	add_with_type(strdup("cNumTrackedTechsRemoved"), LVAR, J_NUM);

	//I have no idea where the following variables come from however they are in one of Aiz's maps and are defined before entering that script
	add_with_type(strdup("RealP1FromWest"), LVAR, J_NUM);
	add_with_type(strdup("RealP2FromWest"), LVAR, J_NUM);
	add_with_type(strdup("RealP3FromWest"), LVAR, J_NUM);
	add_with_type(strdup("RealP4FromWest"), LVAR, J_NUM);
	add_with_type(strdup("RealP5FromWest"), LVAR, J_NUM);
	add_with_type(strdup("RealP6FromWest"), LVAR, J_NUM);
	add_with_type(strdup("RealP1FromNorth"), LVAR, J_NUM);
	add_with_type(strdup("RealP2FromNorth"), LVAR, J_NUM);
	add_with_type(strdup("RealP3FromNorth"), LVAR, J_NUM);
	add_with_type(strdup("RealP4FromNorth"), LVAR, J_NUM);
	add_with_type(strdup("RealP5FromNorth"), LVAR, J_NUM);
	add_with_type(strdup("RealP6FromNorth"), LVAR, J_NUM);
	add_with_type(strdup("numconfigvariables"), LVAR, J_NUM);



	int p0[] = {J_BOOL};
	add_func_with_params(strdup("rmSetOcean"), J_UNKNOWN , p0, 1);

	int p1[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetNuggetDifficulty"), J_UNKNOWN , p1, 2);

	int p2[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaCliffRandomWaypoints"), J_UNKNOWN , p2, 5);

	int p3[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaCliffWaypoint"), J_UNKNOWN , p3, 3);

	int p4[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmFindCloserArea"), J_UNKNOWN , p4, 4);

	int p5[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmFindClosestPoint"), J_UNKNOWN , p5, 3);

	int p6[] = {};
	add_func_with_params(strdup("rmClearClosestPointConstraints"), J_UNKNOWN , p6, 0);

	int p7[] = {J_NUM};
	add_func_with_params(strdup("rmAddClosestPointConstraint"), J_UNKNOWN , p7, 1);

	int p8[] = {J_BOOL};
	add_func_with_params(strdup("rmEnableLocalWater"), J_UNKNOWN , p8, 1);

	int p9[] = {J_STR};
	add_func_with_params(strdup("rmIsMapType"), J_BOOL , p9, 1);

	int p10[] = {J_STR};
	add_func_with_params(strdup("rmSetMapType"), J_UNKNOWN , p10, 1);

	int p11[] = {J_NUM};
	add_func_with_params(strdup("rmGetUnitPosition"), J_VECTOR , p11, 1);

	int p12[] = {J_NUM};
	add_func_with_params(strdup("rmGetHomeCityLevel"), J_NUM , p12, 1);

	int p13[] = {};
	add_func_with_params(strdup("rmGetHighHomeCityLevel"), J_NUM , p13, 0);

	int p14[] = {};
	add_func_with_params(strdup("rmGetAverageHomeCityLevel"), J_NUM , p14, 0);

	int p15[] = {};
	add_func_with_params(strdup("rmGetLowHomeCityLevel"), J_NUM , p15, 0);

	int p16[] = {J_NUM, J_VECTOR};
	add_func_with_params(strdup("rmSetHomeCityWaterSpawnPoint"), J_BOOL , p16, 2);

	int p17[] = {J_NUM, J_VECTOR};
	add_func_with_params(strdup("rmSetHomeCityGatherPoint"), J_BOOL , p17, 2);

	int p18[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmBuildTradeRoute"), J_BOOL , p18, 2);

	int p19[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmGetTradeRouteWayPoint"), J_VECTOR , p19, 2);

	int p20[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateTradeRouteWaypointsInArea"), J_UNKNOWN , p20, 3);

	int p21[] = {J_NUM, J_VECTOR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddRandomTradeRouteWaypointsVector"), J_UNKNOWN , p21, 4);

	int p22[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddRandomTradeRouteWaypoints"), J_UNKNOWN , p22, 5);

	int p23[] = {J_NUM, J_VECTOR};
	add_func_with_params(strdup("rmAddTradeRouteWaypointVector"), J_UNKNOWN , p23, 2);

	int p24[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddTradeRouteWaypoint"), J_UNKNOWN , p24, 3);

	int p25[] = {};
	add_func_with_params(strdup("rmCreateTradeRoute"), J_NUM , p25, 0);

	int p26[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddGroupingToClass"), J_BOOL , p26, 2);

	int p27[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetGroupingMaxDistance"), J_UNKNOWN , p27, 2);

	int p28[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetGroupingMinDistance"), J_UNKNOWN , p28, 2);

	int p29[] = {J_NUM, J_NUM, J_NUM ,J_NUM};
	add_func_with_params(strdup("rmPlaceGroupingInArea"), J_UNKNOWN , p29, 4);

	int p30[] = {J_NUM, J_NUM, J_VECTOR, J_NUM};
	add_func_with_params(strdup("rmPlaceGroupingAtPoint"), J_BOOL , p30, 4);

	int p31[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlaceGroupingAtLoc"), J_BOOL , p31, 5);

	int p32[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddGroupingConstraint"), J_BOOL , p32, 2);

	int p33[] = {J_STR, J_STR};
	add_func_with_params(strdup("rmCreateGrouping"), J_NUM , p33, 2);

	int p34[] = {J_BOOL};
	add_func_with_params(strdup("rmSetIgnoreForceToGaia"), J_UNKNOWN , p34, 1);

	int p35[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmDefineConstant"), J_UNKNOWN , p35, 2);

	int p36[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmGetUnitPlacedOfPlayer"), J_NUM , p36, 2);

	int p37[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmGetUnitPlaced"), J_NUM, p37, 2);

	int p38[] = {J_NUM};
	add_func_with_params(strdup("rmGetNumberUnitsPlaced"), J_NUM, p38, 1);

	int p39[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddUnitsToArmy"), J_UNKNOWN , p39, 3);

	int p40[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmCreateArmy"), J_UNKNOWN , p40, 2);

	int p41[] = {J_STR, J_NUM, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerEffectParamArmy"), J_UNKNOWN , p41, 4);

	int p42[] = {J_STR, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerEffectParamFloat"), J_UNKNOWN , p42, 3);

	int p43[] = {J_STR, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerEffectParamInt"), J_UNKNOWN , p43, 3);

	int p44[] = {J_STR, J_STR, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerEffectParam"), J_UNKNOWN , p44, 3);

	int p45[] = {J_STR};
	add_func_with_params(strdup("rmAddTriggerEffect"), J_UNKNOWN , p45, 1);

	int p46[] = {J_STR, J_NUM, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerConditionParamArmy"), J_UNKNOWN , p46, 4);

	int p47[] = {J_STR, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerConditionParamFloat"), J_UNKNOWN , p47, 3);

	int p48[] = {J_STR, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerConditionParamInt"), J_UNKNOWN , p48, 2);

	int p49[] = {J_STR, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetTriggerConditionParam"), J_UNKNOWN , p49, 3);

	int p50[] = {J_STR};
	add_func_with_params(strdup("rmAddTriggerCondition"), J_UNKNOWN , p50, 1);

	int p51[] = {J_BOOL};
	add_func_with_params(strdup("rmSetTriggerLoop"), J_UNKNOWN , p51, 1);

	int p52[] = {J_BOOL};
	add_func_with_params(strdup("rmSetTriggerRunImmediately"), J_UNKNOWN , p52, 1);

	int p53[] = {J_BOOL};
	add_func_with_params(strdup("rmSetTriggerActive"), J_UNKNOWN , p53, 1);

	int p54[] = {J_NUM};
	add_func_with_params(strdup("rmSetTriggerPriority"), J_UNKNOWN , p54, 1);

	int p55[] = {J_STR};
	add_func_with_params(strdup("rmTriggerID"), J_NUM, p55, 1);

	int p56[] = {J_NUM};
	add_func_with_params(strdup("rmSwitchToTrigger"), J_UNKNOWN , p56, 1);

	int p57[] = {J_STR};
	add_func_with_params(strdup("rmCreateTrigger"), J_UNKNOWN , p57, 1);

	int p58[] = {J_STR};
	add_func_with_params(strdup("rmSetVPFile"), J_UNKNOWN , p58, 1);

	int p59[] = {J_NUM};
	add_func_with_params(strdup("sqrt"), J_NUM , p59, 1);

	int p60[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmSetStatusText"), J_UNKNOWN , p60, 2);

	int p61[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddConnectionEndConstraint"), J_BOOL , p61, 2);

	int p62[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddConnectionStartConstraint"), J_BOOL , p62, 2);

	int p63[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddConnectionConstraint"), J_BOOL , p63, 2);

	int p64[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddConnectionToClass"), J_UNKNOWN , p64, 2);

	int p65[] = {J_NUM};
	add_func_with_params(strdup("rmBuildConnection"), J_UNKNOWN , p65, 1);

	int p66[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionBaseTerrainCost"), J_UNKNOWN , p66, 2);

	int p67[] = {J_NUM, J_STR, J_NUM};
	add_func_with_params(strdup("rmSetConnectionTerrainCost"), J_UNKNOWN , p67, 3);

	int p68[] = {J_NUM, J_STR, J_STR};
	add_func_with_params(strdup("rmAddConnectionTerrainReplacement"), J_UNKNOWN , p68, 3);

	int p69[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionSmoothDistance"), J_UNKNOWN , p69, 2);

	int p70[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionHeightBlend"), J_UNKNOWN , p70, 2);

	int p71[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetConnectionWarnFailure"), J_UNKNOWN , p71, 2);

	int p72[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionCoherence"), J_UNKNOWN , p72, 2);

	int p73[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionBaseHeight"), J_UNKNOWN , p73, 2);

	int p74[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionWidth"), J_UNKNOWN , p74, 3);

	int p75[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetConnectionPositionVariance"), J_UNKNOWN , p75, 2);

	int p76[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddConnectionArea"), J_UNKNOWN , p76, 2);

	int p77[] = {J_NUM, J_NUM, J_BOOL, J_NUM};
	add_func_with_params(strdup("rmSetConnectionType"), J_UNKNOWN , p77, 4);

	int p78[] = {J_STR};
	add_func_with_params(strdup("rmCreateConnection"), J_UNKNOWN , p78, 1);

	int p79[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefInRandomAreaOfClass"), J_UNKNOWN , p79, 4);

	int p80[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefAtRandomAreaOfClass"), J_UNKNOWN , p80, 4);

	int p81[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefInArea"), J_UNKNOWN , p81, 4);

	int p82[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefAtAreaLoc"), J_UNKNOWN , p82, 4);

	int p83[] = {J_NUM, J_BOOL, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefPerPlayer"), J_UNKNOWN , p83, 3);

	int p84[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetObjectDefTradeRouteID"), J_UNKNOWN , p84, 2);

	int p85[] = {J_NUM, J_NUM, J_VECTOR, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefAtPoint"), J_UNKNOWN , p85, 4);

	int p86[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlaceObjectDefAtLoc"), J_UNKNOWN , p86, 5);

	int p87[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddObjectDefItemByTypeID"), J_UNKNOWN , p87, 4);

	int p88[] = {J_NUM, J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddObjectDefItem"), J_UNKNOWN , p88, 4);

	int p89[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetObjectDefForceFullRotation"), J_UNKNOWN , p89, 2);

	int p90[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetObjectDefAllowOverlap"), J_UNKNOWN , p90, 2);

	int p91[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetObjectDefHerdAngle"), J_UNKNOWN , p91, 2);

	int p92[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetObjectDefCreateHerd"), J_UNKNOWN , p92, 2);

	int p93[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetObjectDefGarrisonStartingUnits"), J_UNKNOWN , p93, 2);

	int p94[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetObjectDefGarrisonSecondaryUnits"), J_UNKNOWN , p94, 2);

	int p95[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetObjectDefMaxDistance"), J_UNKNOWN , p95, 2);

	int p96[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetObjectDefMinDistance"), J_UNKNOWN , p96, 2);

	int p97[] = {J_NUM};
	add_func_with_params(strdup("rmCreateStartingUnitsObjectDef"), J_NUM , p97, 1);

	int p98[] = {J_STR};
	add_func_with_params(strdup("rmCreateObjectDef"), J_NUM , p98, 1);

	int p99[] = {J_STR};
	add_func_with_params(strdup("rmConstraintID"), J_NUM , p99, 1);

	int p100[] = {J_STR};
	add_func_with_params(strdup("rmClassID"), J_NUM , p100, 1);

	int p101[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddObjectDefToClass"), J_BOOL , p101, 2);

	int p102[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaToClass"), J_BOOL , p102, 2);

	int p103[] = {J_STR};
	add_func_with_params(strdup("rmDefineClass"), J_NUM , p103, 1);

	int p104[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddObjectDefConstraint"), J_BOOL , p104, 2);

	int p105[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddFairLocConstraint"), J_BOOL , p105, 2);

	int p106[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateMaxHeightConstraint"), J_NUM , p106, 2);

	int p107[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateHCGPEnemyConstraint"), J_BOOL , p107, 3);

	int p108[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateHCGPAllyConstraint"), J_BOOL , p108, 3);

	int p109[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateHCGPSelfConstraint"), J_BOOL , p109, 3);

	int p110[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateHCGPConstraint"), J_BOOL , p110, 2);

	int p111[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaConstraint"), J_BOOL , p111, 2);

	int p112[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateTradeRouteDistanceConstraint"), J_NUM , p112, 2);

	int p113[] = {J_STR, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmCreateCornerConstraint"), J_NUM , p113, 3);

	int p114[] = {J_STR, J_STR, J_BOOL, J_NUM};
	add_func_with_params(strdup("rmCreateTerrainMaxDistanceConstraint"), J_NUM , p114, 4);

	int p115[] = {J_STR, J_STR, J_BOOL, J_NUM};
	add_func_with_params(strdup("rmCreateTerrainDistanceConstraint"), J_NUM , p115, 4);

	int p116[] = {J_STR, J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateTypeDistanceConstraint"), J_NUM , p116, 3);

	int p117[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateClassDistanceConstraint"), J_NUM , p117, 3);

	int p118[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateCliffRampMaxDistanceConstraint"), J_NUM , p118, 3);

	int p119[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateCliffRampDistanceConstraint"), J_NUM , p119, 3);

	int p120[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateCliffRampConstraint"), J_NUM , p120, 2);

	int p121[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateCliffEdgeMaxDistanceConstraint"), J_NUM , p121, 3);

	int p122[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateCliffEdgeDistanceConstraint"), J_NUM , p122, 3);

	int p123[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateCliffEdgeConstraint"), J_NUM , p123, 2);

	int p124[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateEdgeMaxDistanceConstraint"), J_NUM , p124, 3);

	int p125[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateEdgeDistanceConstraint"), J_NUM , p125, 3);

	int p126[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateEdgeConstraint"), J_NUM , p126, 2);

	int p127[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateAreaMaxDistanceConstraint"), J_NUM , p127, 3);

	int p128[] = {J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateAreaDistanceConstraint"), J_NUM , p128, 3);

	int p129[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateAreaConstraint"), J_NUM , p129, 2);

	int p130[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateAreaOverlapConstraint"), J_NUM , p130, 2);

	int p131[] = {J_STR, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreatePieConstraint"), J_NUM , p131, 8);

	int p132[] = {J_STR, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmCreateBoxConstraint"), J_NUM , p132, 6);

	int p133[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetTeamArea"), J_NUM , p133, 2);

	int p134[] = {J_NUM};
	add_func_with_params(strdup("rmGetNumberPlayersOnTeam"), J_NUM , p134, 1);

	int p135[] = {J_NUM};
	add_func_with_params(strdup("rmGetPlayerCulture"), J_UNKNOWN , p135, 1); // num or str, not sure

	int p136[] = {J_NUM};
	add_func_with_params(strdup("rmGetPlayerCiv"), J_NUM, p136, 1);

	int p137[] = {J_NUM};
	add_func_with_params(strdup("rmGetPlayerTeam"), J_NUM , p137, 1);
	
	/*
	int p138[] = {J_NUM};
	add_func_with_params(strdup("rmGetPlayerTeam"), J_STR , p138, 1);
	*/

	int p139[] = {J_NUM, J_STR, J_NUM};
	add_func_with_params(strdup("rmMultiplyPlayerResource"), J_UNKNOWN , p139, 3);

	int p140[] = {J_NUM, J_STR, J_NUM};
	add_func_with_params(strdup("rmAddPlayerResource"), J_UNKNOWN , p140, 3);

	int p141[] = {J_NUM, J_STR, J_NUM};
	add_func_with_params(strdup("rmSetPlayerResource"), J_UNKNOWN , p141, 3);

	int p142[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmFairLocZFraction"), J_NUM , p142, 2);

	int p143[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmFairLocXFraction"), J_NUM , p143, 2);

	int p144[] = {J_NUM};
	add_func_with_params(strdup("rmGetNumberFairLocs"), J_NUM , p144, 1);

	int p145[] = {};
	add_func_with_params(strdup("rmResetFairLocs"), J_UNKNOWN , p145, 0);

	int p146[] = {};
	add_func_with_params(strdup("rmPlaceFairLocs"), J_BOOL , p146, 0);

	int p147[] = {J_STR, J_BOOL, J_BOOL, J_NUM, J_NUM, J_NUM, J_NUM, J_BOOL, J_BOOL};
	add_func_with_params(strdup("rmAddFairLoc"), J_UNKNOWN , p147, 9);

	int p148[] = {J_NUM};
	add_func_with_params(strdup("rmPlayerLocZFraction"), J_NUM, p148, 1);

	int p149[] = {J_NUM};
	add_func_with_params(strdup("rmPlayerLocXFraction"), J_NUM, p149, 1);

	int p150[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetPlayerArea"), J_UNKNOWN , p150, 2);

	int p151[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlacePlayer"), J_UNKNOWN , p151, 3);

	int p152[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlacePlayersRiver"), J_UNKNOWN , p152, 4);

	int p153[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlacePlayersLine"), J_UNKNOWN , p153, 6);

	int p154[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlacePlayersSquare"), J_UNKNOWN , p154, 3);

	int p155[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPlacePlayersCircular"), J_UNKNOWN , p155, 3);

	int p156[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetPlacementSection"), J_UNKNOWN , p156, 2);

	int p157[] = {J_NUM};
	add_func_with_params(strdup("rmSetPlacementTeam"), J_UNKNOWN , p157, 1);

	int p158[] = {J_NUM};
	add_func_with_params(strdup("rmSetTeamSpacingModifier"), J_UNKNOWN , p158, 1);

	int p159[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetPlayerPlacementArea"), J_UNKNOWN , p159, 4);

	int p160[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetPlayerLocation"), J_UNKNOWN , p160, 3);

	int p161[] = {J_STR, J_NUM, J_NUM, J_NUM, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmAddMerc"), J_UNKNOWN , p161, 6);

	int p162[] = {J_STR};
	add_func_with_params(strdup("rmGetCivID"), J_NUM , p162, 1);

	int p163[] = {J_NUM, J_STR, J_BOOL};
	add_func_with_params(strdup("rmSetSubCiv"), J_UNKNOWN , p163, 3);

	int p164[] = {J_NUM};
	add_func_with_params(strdup("rmAllocateSubCivs"), J_UNKNOWN , p164, 1);

	int p165[] = {J_NUM};
	add_func_with_params(strdup("rmSetGaiaCiv"), J_UNKNOWN , p165, 1);

	int p166[] = {J_STR, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmDoLightingEffect"), J_UNKNOWN , p166, 4);

	int p167[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmDoLightingFade"), J_UNKNOWN , p167, 2);

	int p168[] = {J_STR};
	add_func_with_params(strdup("rmSetLightingSet"), J_UNKNOWN , p168, 1);

	int p169[] = {};
	add_func_with_params(strdup("rmFillMapCorners"), J_UNKNOWN , p169, 0);

	int p170[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmTerrainInitialize"), J_UNKNOWN , p170, 2);

	int p171[] = {J_NUM};
	add_func_with_params(strdup("rmSetWindMagnitude"), J_UNKNOWN , p171, 1);

	int p172[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetGlobalStormLength"), J_UNKNOWN , p172, 2);

	int p173[] = {J_NUM};
	add_func_with_params(strdup("rmSetGlobalRain"), J_UNKNOWN , p173, 1);

	int p174[] = {J_NUM};
	add_func_with_params(strdup("rmSetGlobalSnow"), J_UNKNOWN , p174, 1);

	int p175[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaReveal"), J_UNKNOWN , p175, 2);

	int p176[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationNoiseBias"), J_UNKNOWN , p176, 2);

	int p177[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationEdgeFalloffDist"), J_UNKNOWN , p177, 2);

	int p178[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationVariation"), J_UNKNOWN , p178, 2);

	int p179[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationPersistence"), J_UNKNOWN , p179, 2);

	int p180[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationOctaves"), J_UNKNOWN , p180, 2);

	int p181[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationMinFrequency"), J_UNKNOWN , p181, 2);

	int p182[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaElevationType"), J_UNKNOWN , p182, 2);

	int p183[] = {J_NUM, J_STR, J_STR};
	add_func_with_params(strdup("rmAddAreaTerrainReplacement"), J_UNKNOWN , p183, 3);

	int p184[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmAddAreaRemoveType"), J_UNKNOWN , p184, 2);

	int p185[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaInfluenceSegment"), J_UNKNOWN , p185, 5);

	int p186[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaInfluencePoint"), J_UNKNOWN , p186, 3);

	int p187[] = {J_STR};
	add_func_with_params(strdup("rmAreaID"), J_NUM , p187, 1);

	int p188[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaHeightBlend"), J_UNKNOWN , p188, 2);

	int p189[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaSmoothDistance"), J_UNKNOWN , p189, 2);

	int p190[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaCoherence"), J_UNKNOWN , p190, 2);

	int p191[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaMaxBlobDistance"), J_UNKNOWN , p191, 2);

	int p192[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaMinBlobDistance"), J_UNKNOWN , p192, 2);

	int p193[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaMaxBlobs"), J_UNKNOWN , p193, 2);

	int p194[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaMinBlobs"), J_UNKNOWN , p194, 2);

	int p195[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetAreaTerrainLayerVariance"), J_UNKNOWN , p195, 2);

	int p196[] = {J_NUM, J_STR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaTerrainLayer"), J_UNKNOWN , p196, 4);

	int p197[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetAreaObeyWorldCircleConstraint"), J_UNKNOWN , p197, 2);

	int p198[] = {J_BOOL};
	add_func_with_params(strdup("rmSetWorldCircleConstraint"), J_UNKNOWN , p198, 1);

	int p199[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetRiverFoundationParams"), J_UNKNOWN , p199, 2);

	int p200[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverReveal"), J_UNKNOWN , p200, 2);

	int p201[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverAvoid"), J_UNKNOWN , p201, 3);

	int p202[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverAddShallows"), J_UNKNOWN , p202, 3);

	int p203[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverAddShallow"), J_UNKNOWN , p203, 2);

	int p204[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverSetShallowRadius"), J_UNKNOWN , p204, 2);

	int p205[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverSetBankNoiseParams"), J_UNKNOWN , p205, 7);

	int p206[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverAddWaypoint"), J_UNKNOWN , p206, 3);

	int p207[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverConnectRiver"), J_UNKNOWN , p207, 4);

	int p208[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverSetConnections"), J_UNKNOWN , p208, 3);

	int p209[] = {J_NUM, J_STR, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmRiverCreate"), J_UNKNOWN , p209, 6);

	int p210[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddAreaCliffEdgeAvoidClass"), J_UNKNOWN , p210, 3);

	int p211[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaCliffHeight"), J_UNKNOWN , p211, 4);

	int p212[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaCliffEdge"), J_UNKNOWN , p212, 6);

	int p213[] = {J_NUM, J_BOOL, J_BOOL, J_BOOL, J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetAreaCliffPainting"), J_UNKNOWN , p213, 6);

	int p214[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmSetAreaCliffType"), J_UNKNOWN , p214, 2);

	int p215[] = {J_NUM, J_VECTOR, J_NUM, J_NUM};
	add_func_with_params(strdup("rmGetAreaClosestPoint"), J_VECTOR , p215, 4);

	int p215_and_a_half[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmSetAreaWaterType"), J_UNKNOWN , p215_and_a_half, 2);
	
	int p216[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaForestClumpiness"), J_UNKNOWN , p216, 2);

	int p217[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaForestDensity"), J_UNKNOWN , p217, 2);

	int p218[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmSetAreaForestType"), J_UNKNOWN , p218, 2);

	int p219[] = {J_NUM, J_BOOL};
	add_func_with_params(strdup("rmSetAreaWarnFailure"), J_UNKNOWN , p219, 2);

	int p220[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaBaseHeight"), J_UNKNOWN , p220, 2);

	int p221[] = {J_NUM, J_STR, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPaintAreaTerrainByAngle"), J_UNKNOWN , p221, 5);

	int p222[] = {J_NUM, J_STR, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmPaintAreaTerrainByHeight"), J_UNKNOWN , p222, 5);

	int p223[] = {J_NUM};
	add_func_with_params(strdup("rmPaintAreaTerrain"), J_UNKNOWN , p223, 1);

	int p224[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmSetAreaMix"), J_UNKNOWN , p224, 2);

	int p225[] = {J_NUM, J_STR};
	add_func_with_params(strdup("rmSetAreaTerrainType"), J_UNKNOWN , p225, 2);

	int p226[] = {};
	add_func_with_params(strdup("rmBuildAllAreas"), J_UNKNOWN , p226, 0);

	int p227[] = {J_NUM};
	add_func_with_params(strdup("rmBuildArea"), J_UNKNOWN , p227, 1);

	int p228[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaLocTeam"), J_UNKNOWN , p228, 2);

	int p229[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaLocPlayer"), J_UNKNOWN , p229, 2);

	int p230[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaLocation"), J_UNKNOWN , p230, 3);

	int p231[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaEdgeFilling"), J_UNKNOWN , p231, 2);

	int p232[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetAreaSize"), J_UNKNOWN , p232, 3);

	int p233[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmCreateArea"), J_NUM , p233, 2);

	int p234[] = {J_NUM};
	add_func_with_params(strdup("rmZFractionToMeters"), J_NUM , p234, 1);

	int p235[] = {J_NUM};
	add_func_with_params(strdup("rmXFractionToMeters"), J_NUM , p235, 1);

	int p236[] = {J_NUM};
	add_func_with_params(strdup("rmZMetersToFraction"), J_NUM , p236, 1);

	int p237[] = {J_NUM};
	add_func_with_params(strdup("rmXMetersToFraction"), J_NUM , p237, 1);

	int p238[] = {J_NUM};
	add_func_with_params(strdup("rmTilesToMeters"), J_NUM , p238, 1);

	int p239[] = {J_NUM};
	add_func_with_params(strdup("rmMetersToTiles"), J_NUM , p239, 1);

	int p240[] = {J_NUM};
	add_func_with_params(strdup("rmDegreesToRadians"), J_NUM , p240, 1);

	int p241[] = {J_NUM};
	add_func_with_params(strdup("rmZTilesToFraction"), J_NUM , p241, 1);

	int p242[] = {J_NUM};
	add_func_with_params(strdup("rmZFractionToTiles"), J_NUM , p242, 1);

	int p243[] = {J_NUM};
	add_func_with_params(strdup("rmXTilesToFraction"), J_NUM , p243, 1);

	int p244[] = {J_NUM};
	add_func_with_params(strdup("rmXFractionToTiles"), J_NUM , p244, 1);

	int p245[] = {J_NUM};
	add_func_with_params(strdup("rmAreaTilesToFraction"), J_NUM , p245, 1);

	int p246[] = {J_NUM};
	add_func_with_params(strdup("rmAreaFractionToTiles"), J_NUM , p246, 1);

	int p247[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetMapClusteringNoiseParams"), J_UNKNOWN , p247, 3);

	int p248[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetMapClusteringObjectParams"), J_UNKNOWN , p248, 3);

	int p249[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetMapClusteringPlacementParams"), J_UNKNOWN , p249, 4);

	int p250[] = {J_STR, J_STR};
	add_func_with_params(strdup("rmPlaceMapClusters"), J_UNKNOWN , p250, 2);

	int p251[] = {J_NUM};
	add_func_with_params(strdup("rmSetMapElevationHeightBlend"), J_UNKNOWN , p251, 1);

	int p252[] = {J_STR, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddMapTerrainByAngleInfo"), J_UNKNOWN , p252, 4);

	int p253[] = {J_STR, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmAddMapTerrainByHeightInfo"), J_UNKNOWN , p253, 4);

	int p254[] = {J_STR};
	add_func_with_params(strdup("rmSetBaseTerrainMix"), J_UNKNOWN , p254, 1);

	int p255[] = {J_NUM, J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetMapElevationParameters"), J_UNKNOWN , p255, 5);

	int p256[] = {J_STR};
	add_func_with_params(strdup("rmSetSeaType"), J_UNKNOWN , p256, 1);

	int p257[] = {};
	add_func_with_params(strdup("rmGetSeaLevel"), J_NUM , p257, 0);

	int p258[] = {J_NUM};
	add_func_with_params(strdup("rmSetSeaLevel"), J_UNKNOWN , p258, 1);

	int p259[] = {};
	add_func_with_params(strdup("rmGetMapZSize"), J_NUM , p259, 0);

	int p260[] = {};
	add_func_with_params(strdup("rmGetMapXSize"), J_NUM , p260, 2);

	int p261[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmSetMapSize"), J_UNKNOWN , p261, 2);

	int p262[] = {};
	add_func_with_params(strdup("rmGetIsKOTH"), J_BOOL , p262, 0);

	int p263[] = {};
	add_func_with_params(strdup("rmGetIsRelicCapture"), J_BOOL , p263, 0);

	int p264[] = {};
	add_func_with_params(strdup("rmGetIsFFA"), J_BOOL , p264, 0);

	int p265[] = {};
	add_func_with_params(strdup("rmGetNomadStart"), J_BOOL , p265, 0);

	int p266[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmRandInt"), J_NUM , p266, 2);

	int p267[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("rmRandFloat"), J_NUM , p267, 2);

	int p268[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmEchoInfo"), J_UNKNOWN , p268, 2);

	int p269[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmEchoError"), J_UNKNOWN , p269, 2);

	int p270[] = {J_STR, J_NUM};
	add_func_with_params(strdup("rmEchoWarning"), J_UNKNOWN , p270, 2);
	
	int p271[] = {};
	add_func_with_params(strdup("chooseMercs"), J_UNKNOWN , p271, 0);

	int p272[] = {};
	add_func_with_params(strdup("rmIsObserverMode"), J_BOOL , p272, 0);
	
	int p273[] = {J_NUM, J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("ypKingsHillPlacer"), J_UNKNOWN , p273, 4);
	
	int p274[] = {J_VECTOR};
	add_func_with_params(strdup("xsVectorGetX"), J_NUM, p274, 1);
	
	int p275[] = {J_VECTOR};
	add_func_with_params(strdup("xsVectorGetZ"), J_NUM, p275, 1);
	
	int p276[] = {J_NUM};
	add_func_with_params(strdup("ypIsAsian"), J_BOOL, p276, 1);

	int p277[] = {J_NUM, J_NUM};
	add_func_with_params(strdup("ypMonasteryBuilder"), J_NUM, p277, 2);

	int p278[] = {J_NUM};
	add_func_with_params(strdup("rPFromWest"), J_NUM, p278, 1);

	int p279[] = {J_NUM};
	add_func_with_params(strdup("rPFromNorth"), J_NUM, p279, 1);

	int p280[] = {J_NUM};
	add_func_with_params(strdup("configVar"), J_NUM, p280, 1);

	//special
	int p281[] = {J_NUM};
	add_func_with_params(strdup("FireEv"), J_NUM, p281, 1);

	int p282[] = {J_NUM};
	add_func_with_params(strdup("int2DD"), J_NUM, p282, 1);

	int p283[] = {J_NUM};
	add_func_with_params(strdup("Num2Let"), J_STR, p283, 1);

	int p284[] = {J_NUM, J_NUM, J_NUM};
	add_func_with_params(strdup("xpPC"), J_NUM, p284, 3);

	int p285[] = {J_STR, J_STR};
	add_func_with_params(strdup("CharaCon"), J_UNKNOWN, p285, 2);
}

// Expect -1 if this is true, otherwise expect the index returned that doesn't match
// on second thought, it appears that the second array is still valid if it is a left-subset of the original
// (It can be shorter but must be equal until the end)
// if lengths differ, return -2
int cmp_int_arr(int* a, int* b, int a_len, int b_len){
	if(a_len != b_len){
		return 0;
	}

	if(a[0] < b[0]){
		return 0;
	}

	for(int i = 1; i < a_len; i++){
		if(a[i] != b[i]){
			if(b[i] == -1){
				return -1;
			}
			return i;
		}
	}

	return -1;
}

char* j_to_str(int j_type){
	if(j_type == J_VECTOR){
		return "vector";
	}else if(j_type == J_NUM){
		return "numeric";
	}else if(j_type == J_STR){
		return "string";
	}else if(j_type == J_BOOL){
		return "bool";
	}else if(j_type == J_UNKNOWN){
		return "_variable_unknown_type";
	}else if(j_type == J_BLANK){
		return "_variable_blank_type";
	}else{
	return "____unexpectected___type___";
	}
}
