%{
#include <stdio.h>
#include <stdlib.h>
#include "aoe.tab.h"

int linect = 1;

int count_newlines_in_str(char* str){
        int count = 0;
        char current_char = 'a';
        int i = 0;
	while(current_char != '\0'){
	         current_char = str[sizeof(char) * i];
                 if(current_char == '\n'){
                         count++;
                 }
                 i++;
         }
         return count;
 } 
%}

newline [\n]|([\r][\n])

%%
include	{return INCLUDE_T;}
break	{return BREAK_T;}
continue {return CONTINUE_T;}
void	{return VOID_T;}
return  {return RETURN_T;}
int		{return INT_T;}
long	{return LONG_T;}
float	{return FLOAT_T;}
bool 	{return BOOL_T;}
vector  {return VECTOR_T;}
string  {return STRING_T;}
if		{return IF_T;}
else	{return ELSE_T;}
true	{return TRUE_T;}
false	{return FALSE_T;}
for		{return FOR_T;}
while	{return WHILE_T;}
"else if" {return ELIF_T;}
\+\+	{return INCR;}
\-\- 	{return DECR;}
\<		{return LT;}
\=\=	{return EQ;}
\<\=	{return LE;}
\>\=	{return GE;}
\>		{return GT;}
\!\=	{return NE;}
\&\& 	{return AND;}
\|\|	{return OR;}
\=		{return ASSIGN;}
\+		{return PLUS; }
\-		{return MINUS; }
\*		{return MULT; }
\% 		{return MOD;}
\/		{return DIVIDE;}
\!		{return BANG; }
\,		{return COMMA; }
\(		{return LP;}
\)		{return RP;}
\[		{return LB;}
\]		{return RB;}
\{		{return LC;}
\}		{return RC;}
\;		{return SEMI;}
\+\=		{return PLUS_EQ;}
\-\=		{return MINUS_EQ;}
\/\=		{return DIV_EQ;}
\*\=		{return MULT_EQ;}
[a-zA-Z_][A-Za-z0-9_]*	{yylval.strnum.str=(char*)strdup(yytext); return ID;}
[0-9]+				{yylval.num=atoi(yytext); return INT_LIT;}
\.[0-9]+		{yylval.decimal=atoi(yytext); return FLOAT_LIT;}
[0-9]+\.		{yylval.decimal=atoi(yytext); return FLOAT_LIT;}
[0-9]+\.[0-9]+	{yylval.decimal=atoi(yytext); return FLOAT_LIT;}
[ \t]		;
\"(\\.|[^"\\])*\" {yylval.strnum.str=(char*)strdup(yytext); linect+=count_newlines_in_str(yytext); return STRING_LIT;}
{newline}		{linect++;}
"/*"([^*]|\*+[^*/])*\*+"/"	 {linect+=count_newlines_in_str(yytext);}
\/\/.* ;
.		{printf("Line %d: Illegal character: %s\n",linect, yytext); }	
%%

