aoe:	lex.yy.c aoe.tab.c 
	gcc -g -o aoe lex.yy.c aoe.tab.c  -lfl

lex.yy.c:	aoe.l
	flex aoe.l

aoe.tab.c:	aoe.y
	bison -vd aoe.y

clean:	
	rm -f lex.yy.c aoe.tab.* aoe.output

realclean:
	rm -f lex.yy.c aoe.tab.* aoe.output aoe

